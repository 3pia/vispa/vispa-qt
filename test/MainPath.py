import sys
import os

try:
    distBaseDirectory=os.path.abspath(os.path.join(os.path.dirname(__file__),".."))
except Exception:
    distBaseDirectory=os.path.abspath(os.path.join(os.path.dirname(sys.argv[0]),".."))


sysList = [distBaseDirectory]
sysList.extend(sys.path)
sys.path=sysList

# Need to put THIS Vispa release before every other installed Vispa releases on the system.
# Other Vispa releases might be imported instead of this, if it is in the OS 'PATH' or 'PYTHONPATH' variable.

from Vispa.Main.Preferences import *
# Add preferences directory to sys path to enable user plugins in ~/.vispa/UserPlugins/
sys.path.append(os.path.abspath(preferencesDirectory))

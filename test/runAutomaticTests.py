#!/usr/bin/env python
import unittest
import logging
logging.root.setLevel(logging.CRITICAL)

#These modules are imported and the automatic test suits are executed
testModules = []
testModules.append('Gui')
testModules.append('Main')
testModules.append('Plugins')
testModules.append('Share')
testModules.append('Views')

suite = unittest.TestSuite()

for m in testModules:
  module = __import__(m, globals(), locals(),[], -1)
  s= module.getAutomaticSuite()
  suite.addTest(s)

runner = unittest.TextTestRunner(verbosity=2)
runner.run(suite)



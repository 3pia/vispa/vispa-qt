#! /usr/bin/env python
import unittest
import os.path
import sys

import logging
import sys
import platform

import Path

from Vispa.Main import FileManager

filePathToTest = [
    "testPathHandling.py",
    "../runAutomaticTests.py",
    "../Gui"
    ]

filePathSpecialToWindows = [
    "../Main\\testPathHandling.py",
    "../Main\\../Main\\testPathHandling.py"
    ]

class FileManagerPathHandling(unittest.TestCase):
    def setUp(self):
        self.oldcwd=os.getcwd()
        os.chdir(os.path.dirname(__file__))

    def testPathConversions(self):
        for filePath in filePathToTest:
            self.assertTrue(FileManager.is_relative_path(filePath))
            convertedPath = FileManager.absolute_path(filePath)
            self.assertTrue(os.path.exists(convertedPath))
            target = os.getcwd()
            self.assertTrue(os.path.exists(FileManager.relative_path(convertedPath,target)))
            self.assertTrue(os.path.exists(FileManager.relative_path(target,convertedPath)))

    def testWindowsPathConversions(self):
        if platform.system()=="Windows":
            for filePath in filePathSpecialToWindows:
                self.assertTrue(FileManager.is_relative_path(filePath))
                convertedPath = FileManager.absolute_path(filePath)
                self.assertTrue(os.path.exists(convertedPath))
                target = os.getcwd()
                self.assertTrue(os.path.exists(FileManager.relative_path(convertedPath,target)))
                self.assertTrue(os.path.exists(FileManager.relative_path(target,convertedPath)))
	else:
	    sys.stdout.write("no Windows system, not tested ... ")
	    sys.stdout.flush() 

    def tearDown(self):
        os.chdir(self.oldcwd)

if __name__ == "__main__":
    unittest.main()

import unittest
import os


interactiveTestModules=["testMainWindow"]
automaticTestModules=["testPathHandling"]

# return testsuite
def getInteractiveSuite():
  """
  Returns a suite with all tests popping up a gui, so human interaction
  is needed to decide success or failure.
  """
  loader = unittest.TestLoader()
  suite = unittest.TestSuite()
  for test in interactiveTestModules:
    m = __import__(test,globals(), locals())
    s = loader.loadTestsFromModule(m)
    suite.addTest(s)
  return suite

def getAutomaticSuite():
  """
  Returns a suite of all tests without the need of human interaction.
  """
  loader = unittest.TestLoader()
  suite = unittest.TestSuite()
  for test in automaticTestModules:
    m = __import__(test,globals(), locals())
    s = loader.loadTestsFromModule(m)
    suite.addTest(s)
  return suite
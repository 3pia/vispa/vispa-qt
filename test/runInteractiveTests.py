#!/usr/bin/env python
import unittest
import logging

print "##### Interactive VISPA tests"
print "## The following tests are interactive tests."
print "## This means that:"
print "## - A lot of windows will pop up that need to be closed manually."
print "## - The interpretation of the graphical displays will often require expert judgment"
print "## \n## Failing tests should however never happen and should immediately be reported\n## \n"


logging.root.setLevel(logging.CRITICAL)

#These modules are imported and the interactive test suits are executed
testModules = []
testModules.append('Gui')
testModules.append('Main')
testModules.append('Plugins')
testModules.append('Share')
testModules.append('Views')

suite = unittest.TestSuite()

for m in testModules:
  module = __import__(m, globals(), locals(),[], -1)
  s= module.getInteractiveSuite()
  suite.addTest(s)

runner = unittest.TextTestRunner(verbosity=2)
runner.run(suite)



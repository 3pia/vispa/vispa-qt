#! /usr/bin/env python
import unittest
import os.path
import sys

import logging
import sys 

import Path

from Vispa.Main.Application import Application
from Vispa.Share import Profiling
from Vispa.Main.PluginManager import pluginmanager as pluginmanager

class ConfigBrowserTestCase(unittest.TestCase):
    def testConfigBrowser(self):
        global test
        logging.debug(self.__class__.__name__ +': testRun()')
        self.app=Application(sys.argv)
        self.app.mainWindow().setWindowTitle("test ConfigBrowser")
        for plugin in pluginmanager.plugins():
            if plugin.__class__.__name__=="ConfigEditorPlugin":
                self.app.openFile(os.path.join(os.path.dirname(__file__),"patLayer1_fromAOD_full_cfg.py"))
        else:
            logging.debug(self.__class__.__name__ +': ConfigBrowser not laded')
        if not hasattr(unittest,"NO_GUI_TEST"):
            self.app.run()

if __name__ == "__main__":
    Profiling.analyze("unittest.main()",__file__)

#!/usr/bin/env python
import unittest
import tempfile
import time
import Path
from Vispa.Plugins.LocalBatchManagerPlugin.LocalBatchManager import LocalBatchManager
import logging


class testLocalBatchManager(unittest.TestCase):
  def setUp(self):
    self.batchManager = LocalBatchManager(tempfile.gettempdir())

  def testAddRemoveJob(self):
    self.batchManager.addJob("this/should/crash","","")
    jobs = self.batchManager.getJobs()
    self.assertEqual(len(jobs),1)
    time.sleep(1)
    self.assertEqual(jobs[0].getStatus(),'Crashed')
    self.batchManager.removeJob(jobs[0])
    self.assertEqual(len(self.batchManager.getJobs()),0)

  def tearDown(self):
    self.batchManager.shutdown()
if __name__ == "__main__":
  logging.root.setLevel(10)
  unittest.main()


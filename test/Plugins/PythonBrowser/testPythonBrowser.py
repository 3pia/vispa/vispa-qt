#! /usr/bin/env python
import unittest
import os.path
import sys

import logging
import sys

import Path

try:
    from Vispa.Plugins.PythonBrowser.PythonBrowserPlugin import PythonBrowserPlugin as plugin
except ImportError:
    plugin=None

from Vispa.Main.PluginManager import pluginmanager as pluginmanager

from Vispa.Main.Application import *
from Vispa.Share import Profiling

class PythonBrowserTestCase(unittest.TestCase):
    def testPythonBrowser(self):
        logging.debug(self.__class__.__name__ +': testRun()')
        if plugin==None:
            return

        self.app=Application(sys.argv)

        pluginAvailable = False
        for registeredPlugin in pluginmanager.plugins():
            if isinstance(registeredPlugin, plugin):
                pluginAvailable = True

        if pluginAvailable:
            self.app.mainWindow().setWindowTitle("test PythonBrowser")
            self.app.openFile(os.path.join(os.path.dirname(__file__), "../../../Vispa/Main/","MainWindow.py"))

        if not hasattr(unittest,"NO_GUI_TEST"):
            self.assertEqual(self.app.run(), 0)

if __name__ == "__main__":
    Profiling.analyze("unittest.main()",__file__)

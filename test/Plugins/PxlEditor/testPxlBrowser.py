#! /usr/bin/env python
import unittest
import os.path
import sys

import logging
import sys 

import Path

from Vispa.Main.Application import *
from Vispa.Share import Profiling

class PxlEditorTestCase(unittest.TestCase):
    def testPxlEditor(self):
        logging.debug(self.__class__.__name__ +': testRun()')
        self.app=Application(sys.argv)
        self.app.mainWindow().setWindowTitle("test PxlBrowser")
        self.app.openFile(os.path.join(os.path.dirname(__file__), "madgraph-Higgs-m120_10.pxlio"))
        if not hasattr(unittest,"NO_GUI_TEST"):
            self.app.run()

if __name__ == "__main__":
    Profiling.analyze("unittest.main()",__file__)

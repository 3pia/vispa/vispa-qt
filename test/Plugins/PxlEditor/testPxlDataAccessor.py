#! /usr/bin/env python
import unittest
import os.path
import sys

import logging
import sys 


import Path
from Vispa.Main.Preferences import *
sys.path.append(os.path.join(baseDirectory,"Vispa/Plugins/PxlEditor"))

from Vispa.Main.Exceptions import *
try:
    from pxl.hep import *
    from pxl.astro import *
    from pxl.core import *
except Exception:
    logging.error(__name__ +": "+ exception_traceback())

try:
  from PxlDataAccessor import *
except:
  logging.warning("Exception thrown on import PxlDataAccessor - aborting")
from Vispa.Share import Profiling

def countObjects(accessor,object=None,i=0):
    #logging.debug(__name__ + ": countObjects")
    i+=1
    if object==None:
        for child in accessor.topLevelObjects():
            i=countObjects(accessor,child,i)
    else:
        for child in accessor.children(object):
            i=countObjects(accessor,child,i)
    return i 

def countMotherRelations(accessor,object=None,i=0):
    i+=len(accessor.motherRelations(object))
    if object==None:
        for child in accessor.topLevelObjects():
            i=countMotherRelations(accessor,child,i)
    else:
        for child in accessor.children(object):
            i=countMotherRelations(accessor,child,i)
    return i 

def countDaughterRelations(accessor,object=None,i=0):
    i+=len(accessor.daughterRelations(object))
    if object==None:
        for child in accessor.topLevelObjects():
            i=countDaughterRelations(accessor,child,i)
    else:
        for child in accessor.children(object):
            i=countDaughterRelations(accessor,child,i)
    return i 

class PxlDataAccessorTestCase(unittest.TestCase):
    def testDecayTree(self):
        logging.debug(self.__class__.__name__ +': testDecayTree()')
        event = Event()
        
        eventview1 = EventView()
        event.setObject(eventview1)
        particle1 = Particle()
        particle1.setName("particle 1")
        particle1.setPdgNumber(11)
        eventview1.setObject(particle1)
        eventview1.setIndexEntry("index 1",particle1)
        
        eventview2 = event.createEventView()
        particle2 = eventview2.createParticle()
        particle2.setName("particle 2")
        particle3 = eventview2.createParticle()
        particle3.setName("particle 3")
        particle4 = eventview2.createParticle()
        particle4.setName("particle 4")
        particle5 = eventview2.createParticle()
        particle5.setName("particle 5")
        particle6 = eventview2.createParticle()
        particle6.setName("particle 6")
        particle5.linkMother(particle3)
        particle5.linkMother(particle4)
        particle6.linkMother(particle5)
        
        accessor=PxlDataAccessor()
        accessor.setDataObjects([event])

        self.assertEqual(countObjects(accessor),10)
        self.assertEqual(countMotherRelations(accessor),3)
        self.assertEqual(countDaughterRelations(accessor),3)

        self.assertEqual(len(accessor.properties(particle1)),22)
        self.assertEqual(accessor.propertyValue(particle1,"Name"),"particle 1")
        self.assertEqual(accessor.isLepton(particle1),True)
    
if __name__ == "__main__":
    Profiling.analyze("unittest.main()",__file__,"PxlDataAccessor")

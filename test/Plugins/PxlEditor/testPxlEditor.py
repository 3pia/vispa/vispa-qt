#! /usr/bin/env python
import unittest
import os.path
import sys

import logging
import sys 


import Path
from Vispa.Main.Preferences import *

from Vispa.Main.PluginManager import pluginmanager
from Vispa.Main.Application import *
from Vispa.Share import Profiling

class PxlEditorTestCase(unittest.TestCase):
    def testPxlEditor(self):
        logging.debug(self.__class__.__name__ +': testRun()')
        self.app=Application(sys.argv)
        self.app.mainWindow().setWindowTitle("test PxlEditor")
        for plugin in pluginmanager.plugins():
            if isinstance(plugin,Vispa.Plugins.PxlEditor.PxlPlugin.PxlPlugin):
                self.tab=plugin.newFile()
        if not hasattr(unittest,"NO_GUI_TEST"):
            self.app.run()

if __name__ == "__main__":
    unittest.main()

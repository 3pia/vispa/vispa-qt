#! /usr/bin/env python
import unittest
import os.path
import sys

import logging
import sys 

try:
    from Vispa.Plugins.PxlEditor.PxlDataAccessor import *
except:
  logging.warning("Exception thrown on import PxlDataAccessor - aborting")

from Vispa.Share import Profiling

class PxlDataAccessorFileInputTestCase(unittest.TestCase):
    def testIO(self):
        logging.debug(self.__class__.__name__ +': testIO()')

        accessor=PxlDataAccessor()
        accessor.open(os.path.join(os.path.dirname(__file__), "madgraph-Higgs-m120_10.pxlio"))
        logging.debug("accessor.numberOfEvents()")
        self.assertEqual(accessor.numberOfEvents(),None)

        self.assertEqual(accessor.eventNumber(),1)
        self.assertEqual(int(float(accessor.propertyValue(accessor.children(accessor.children(accessor.topLevelObjects()[0])[0])[4],"Mass"))),119)

        accessor.goto(2)
        self.assertEqual(accessor.eventNumber(),2)
        self.assertEqual(int(float(accessor.propertyValue(accessor.children(accessor.children(accessor.topLevelObjects()[0])[0])[4],"Mass"))),119)

        accessor.goto(1)
        self.assertEqual(accessor.eventNumber(),1)
        self.assertEqual(int(float(accessor.propertyValue(accessor.children(accessor.children(accessor.topLevelObjects()[0])[0])[4],"Mass"))),119)

        accessor.goto(11)
        self.assertEqual(accessor.eventNumber(),10)
        self.assertEqual(int(float(accessor.propertyValue(accessor.children(accessor.children(accessor.topLevelObjects()[0])[0])[4],"Mass"))),801)

        accessor.goto(9)
        self.assertEqual(accessor.eventNumber(),9)
        self.assertEqual(int(float(accessor.propertyValue(accessor.children(accessor.children(accessor.topLevelObjects()[0])[0])[4],"Mass"))),120)

        accessor.goto(8)
        self.assertEqual(accessor.eventNumber(),8)
        self.assertEqual(int(float(accessor.propertyValue(accessor.children(accessor.children(accessor.topLevelObjects()[0])[0])[4],"Mass"))),119)

        self.assertEqual(accessor.numberOfEvents(),10)

if __name__ == "__main__":
    Profiling.analyze("unittest.main()",__file__,"PxlDataAccessor")

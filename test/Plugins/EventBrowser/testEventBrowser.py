#! /usr/bin/env python
import unittest
import os.path
import sys

import logging
import sys


import Path
from Vispa.Main.Preferences import *

from Vispa.Main.Application import Application
from Vispa.Plugins.EventBrowser.EventBrowserPlugin import EventBrowserPlugin
from Vispa.Plugins.EventBrowser.EventBrowserTab import EventBrowserTab
from Vispa.Share import Profiling

from Share.TestDataAccessor import TestDataAccessor

class EventBrowserTestCase(unittest.TestCase):
    def testEventBrowser(self):
        logging.debug(self.__class__.__name__ +': testRun()')
        self.app=Application(sys.argv)
        self.app.mainWindow().setWindowTitle("test EventBrowser")
        self.plugin=EventBrowserPlugin()
        self.plugin.startUp()
        self.tab = EventBrowserTab(self.plugin,self.app.mainWindow())
        self.tab.setDataAccessor(TestDataAccessor())
        self.app.mainWindow().addTab(self.tab)
        self.tab.updateContent()
        self.tab.updateViewMenu()
        if not hasattr(unittest,"NO_GUI_TEST"):
            self.app.run()

if __name__ == "__main__":
    #Profiling.analyze("unittest.main()",__file__)
    unittest.main()

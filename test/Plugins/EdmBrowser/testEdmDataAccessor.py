#! /usr/bin/env python
import unittest
import os.path
import sys

import logging
import sys 

accessorAvailable = False
from Vispa.Main.Exceptions import *
try:
    from Vispa.Plugins.EdmBrowser.EdmDataAccessor import *
    accessorAvailable = True
except Exception:
    logging.info("Cannot open EdmDataAccessor: " + exception_traceback())

from Vispa.Share import Profiling

def countObjects(accessor,object=None,i=0):
    #logging.debug(__name__ + ": countObjects")
    i+=1
    if object==None:
        for child in accessor.topLevelObjects():
            i=countObjects(accessor,child,i)
    else:
        for child in accessor.children(object):
            i=countObjects(accessor,child,i)
    return i 

def countMotherRelations(accessor,object=None,i=0):
    i+=len(accessor.motherRelations(object))
    if object==None:
        for child in accessor.topLevelObjects():
            i=countMotherRelations(accessor,child,i)
    else:
        for child in accessor.children(object):
            i=countMotherRelations(accessor,child,i)
    return i 

def countDaughterRelations(accessor,object=None,i=0):
    i+=len(accessor.daughterRelations(object))
    if object==None:
        for child in accessor.topLevelObjects():
            i=countDaughterRelations(accessor,child,i)
    else:
        for child in accessor.children(object):
            i=countDaughterRelations(accessor,child,i)
    return i 

class EdmDataAccessorTestCase(unittest.TestCase):
    def testExample(self):
        logging.debug(self.__class__.__name__ +': testExample()')
        
        if not accessorAvailable:
            return
        
        accessor = EdmDataAccessor()
        
        return
        
        # We need to check whether we still want to deliver the root files #986
        accessor.open(os.path.join(os.path.dirname(__file__), "../../../examples/EdmBrowser/","QCDDiJet_Pt50to80_Summer09_RECO_3_1_X_10events.root"))
#        accessor.open(os.path.join(os.path.dirname(__file__), "../../../examples/EdmBrowser/","RECO_3_5_0.txt"))
        
        self.assertEqual(accessor.numberOfEvents(),10)
        self.assertEqual(accessor.eventNumber(),1)

        accessor.next()
        self.assertEqual(accessor.eventNumber(),2)

        accessor.first()
        self.assertEqual(accessor.eventNumber(),1)

        accessor.last()
        self.assertEqual(accessor.eventNumber(),10)

        accessor.previous()
        self.assertEqual(accessor.eventNumber(),9)

        accessor.goto(8)
        self.assertEqual(accessor.eventNumber(),8)

        self.assertEqual(accessor.numberOfEvents(),10)

if __name__ == "__main__":
    Profiling.analyze("unittest.main()",__file__)

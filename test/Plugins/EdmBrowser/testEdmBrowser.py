#! /usr/bin/env python
import unittest
import os.path
import sys

import logging
import sys 

import Path

from Vispa.Main.Application import *
from Vispa.Main.PluginManager import pluginmanager as pluginmanager
from Vispa.Share import Profiling

try:
    from Vispa.Plugins.EdmBrowser.EdmBrowserPlugin import EdmBrowserPlugin as plugin
except:
    plugin = 0
    pass

class EdmBrowserTestCase(unittest.TestCase):
    def testEdmBrowser(self):
        
        logging.debug(self.__class__.__name__ +': testRun()')
        
        if plugin == 0:
            return
        
        self.app=Application(sys.argv)
        
        pluginAvailable = False
        pluginmanager.loadPlugins()
        for registeredPlugin in pluginmanager.plugins():
            if isinstance(registeredPlugin, plugin):
                pluginAvailable = True
        
        if pluginAvailable:
            self.app.mainWindow().setWindowTitle("test EdmBrowser")
            self.app.openFile(os.path.join(os.path.dirname(__file__), "../../../examples/EdmBrowser/","QCDDiJet_Pt50to80_Summer09_RECO_3_1_X_10events.root"))
        if not hasattr(unittest,"NO_GUI_TEST"):
            self.app.run()

if __name__ == "__main__":
    Profiling.analyze("unittest.main()",__file__)

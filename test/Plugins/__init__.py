import unittest
import os
import logging
import sys
import platform

logging.debug("Loading Plugin Tests")
dirName = os.path.join(os.path.dirname(__file__))
logging.debug(" Path: %s" % (dirName))

loader = unittest.TestLoader()
suite = unittest.TestSuite()
tests = []
tests.append('AnalysisDesigner')
tests.append('PxlEditor')
tests.append('Browser')
tests.append('ConfigEditor')
tests.append('EdmBrowser')
tests.append('EventBrowser')
tests.append('PxlEditor')
tests.append('PythonBrowser')

if platform.system()!="Windows":
  tests.append('BatchSystem')
  tests.append('LocalBatchManager')
  tests.append('CondorBatchManager')
  tests.append('GridBatchManagerPlugin')


def getInteractiveSuite():
  """
  Returns a suite with all tests popping up a gui, so human interaction
  is needed to decide success or failure.
  """
  for test in tests:
    m = __import__(test, globals(), locals())
    s = m.getInteractiveSuite()
    suite.addTest(s)
  return suite

def getAutomaticSuite():
  """
  Returns a suite of all tests without the need of human interaction.
  """
  for test in tests:
    m = __import__(test, globals(), locals())
    s = m.getAutomaticSuite()
    suite.addTest(s)
  return suite

  return suite

#! /usr/bin/env python
import unittest
import os.path
import sys
import time

import logging
import sys 

from Vispa.Plugins.GridBatchManagerPlugin.GridJob import GridJob
from ConfigParser import ConfigParser



class TestGridJob(unittest.TestCase):
  def testSerializeDeserialize(self):
    b0 = GridJob('foo')
    b0.setJDLFile('myJDL')
    b0.setGridJobID('123')
    b0.addToOutputSandbox('Foo')
    b0.addToOutputSandbox('Bar')
    b0.setCE('theCE')

    c = ConfigParser()
    b0.serialize(c)

    b1 = GridJob('bar')
    b1.deserialize(c)

    self.assertEqual(b0.getJDLFile(), b1.getJDLFile())
    self.assertEqual(b0.getGridJobID(), b1.getGridJobID())
    self.assertEqual(b0.getCE(), b1.getCE())
    self.assertEqual(b0.getOutputSandbox(), b1.getOutputSandbox())
    self.assertEqual(b0.getInputSandbox(), b1.getInputSandbox())


if __name__== "__main__":
  unittest.main()



#! /usr/bin/env python
import unittest
import os.path
import sys
from PyQt4 import QtCore, QtGui

import signal
import logging
import sys 


from Vispa.Plugins.BatchSystem.BatchManagerAccessor import ManagerAccessor 
from Vispa.Plugins.GridBatchManagerPlugin import GridBatchManager
from Vispa.Plugins.GridBatchManagerPlugin import ManagerStatusAndConfigurationWidget 

if __name__ == "__main__":
  logging.debug("Running GridManagerStatusWidgetTest")
  signal.signal(signal.SIGINT, signal.SIG_DFL)
  manager = GridBatchManager.GridBatchManager('/tmp/foo')

  app = QtGui.QApplication(sys.argv)
  w = ManagerStatusAndConfigurationWidget.ManagerStatusAndConfigurationWidget(manager) 
  w.show()
  app.exec_()

#! /usr/bin/env python
import unittest
import os.path
import sys

import logging
import sys

import Path
from Vispa.Main.Preferences import *
from Vispa.Main.PluginManager import pluginmanager
from Vispa.Main.Application import *
from Vispa.Share import Profiling

class AnalysisDesignerTestCase(unittest.TestCase):
    def testAnalysisDesigner(self):
        logging.debug(self.__class__.__name__ +': testRun()')
        self.app=Application(sys.argv)
        self.app.mainWindow().setWindowTitle("test AnalysisDesigner")
        for plugin in pluginmanager.plugins():
            if hasattr(plugin,"newAnalysisDesignerTab"):
                self.tab=plugin.newAnalysisDesignerTab()
        if not hasattr(unittest,"NO_GUI_TEST"):
            self.app.run()

if __name__ == "__main__":
    unittest.main()
    #Profiling.analyze("unittest.main()",__file__)

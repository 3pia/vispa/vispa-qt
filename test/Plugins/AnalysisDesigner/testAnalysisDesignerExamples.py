#! /usr/bin/env python
import unittest
import os.path
import os
import sys

import logging
import sys 

import Path

examplesDirectory = os.path.abspath(os.path.join(os.path.dirname(__file__),"../../..","examples/AnalysisDesigner"))

examples = []
for example in os.listdir(examplesDirectory):
    if os.path.splitext(example)[1] == ".xml":
        examples.append(example)

try:
    import ROOT
    ROOT.gROOT.SetBatch(True)
    for example in os.listdir(os.path.join(examplesDirectory, "withROOT")):
        if os.path.splitext(example)[1] == ".xml":
            examples.append("withROOT/"+example)
except ImportError:
    logging.debug(self.__class__.__name__ +': ROOT not found, ignoring ROOT-based examples')

from Vispa.Main.PluginManager import pluginmanager
from Vispa.Main.Application import *
from Vispa.Share import Profiling

class AnalysisDesignerTestCase(unittest.TestCase):
    def testAnalysisDesignerExample(self):
        logging.debug(self.__class__.__name__ +': testAnalysisDesignerExample()')
        self.app=Application(sys.argv)
        self.app.mainWindow().setWindowTitle("test AnalysisDesigner examples")
        cwd = os.getcwd()
        
        exampleOutPath = os.path.join(os.path.dirname(__file__), "../../../examples/output")
        if not os.path.exists(exampleOutPath):
            os.mkdir(exampleOutPath)
        
        os.chdir(exampleOutPath)
        
        for example in examples:
            self.app.openFile(os.path.join(examplesDirectory,example))
            for plugin in pluginmanager.plugins():
                if hasattr(plugin,"switchToExecutionMode"):
                    plugin.switchToExecutionMode()

        if not hasattr(unittest,"NO_GUI_TEST"):
            self.app.run()
            
        os.chdir(cwd)

if __name__ == "__main__":
    Profiling.analyze("unittest.main()",__file__)

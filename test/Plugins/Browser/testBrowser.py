#! /usr/bin/env python
import unittest
import os.path
import sys

import logging
import sys


import Path
from Vispa.Main.Preferences import *

from Vispa.Main.Application import Application
from Vispa.Plugins.Browser.BrowserPlugin import BrowserPlugin
from Vispa.Plugins.Browser.BrowserTab import BrowserTab
from Vispa.Share import Profiling

from Share.TestDataAccessor import TestDataAccessor

class BrowserTestCase(unittest.TestCase):
    def testBrowser(self):
        logging.debug(self.__class__.__name__ +': testRun()')
        self.app=Application(sys.argv)
        self.app.mainWindow().setWindowTitle("test Browser")
        self.plugin=BrowserPlugin()
        self.plugin.startUp()
        self.tab = BrowserTab(self.plugin,self.app.mainWindow())
        self.tab.setDataAccessor(TestDataAccessor())
        self.app.mainWindow().addTab(self.tab)
        self.tab.updateContent()
        self.tab.updateViewMenu()
        if not hasattr(unittest,"NO_GUI_TEST"):
            self.app.run()

if __name__ == "__main__":
    unittest.main()
    #Profiling.analyze("unittest.main()",__file__)

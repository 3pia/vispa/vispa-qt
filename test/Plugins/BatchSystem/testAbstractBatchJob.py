#! /usr/bin/env python
import unittest
import Path
from ConfigParser import ConfigParser
from Vispa.Plugins.BatchSystem.AbstractBatchJob import AbstractBatchJob



class TestAbstractBatchJob(unittest.TestCase):
  def testSerializeDeserialize(self):
    b0 = AbstractBatchJob('foo')
    c = ConfigParser()
    b0.serialize(c)

    b1 = AbstractBatchJob('bar')
    b1.deserialize(c)

    self.assertEqual(b0.getId(), b1.getId())
    self.assertEqual(b0.getCommand(), b1.getCommand())
    self.assertEqual(b0.getSubmissionTime(), b1.getSubmissionTime())
    self.assertEqual(b0.getGroup(), b1.getGroup())
    self.assertEqual(b0.getOutputFileName(), b1.getOutputFileName())
    self.assertEqual(b0.getErrorFileName(), b1.getErrorFileName())
    self.assertEqual(b0.getExecutionScriptName(), b1.getExecutionScriptName())


if __name__== "__main__":
  unittest.main()

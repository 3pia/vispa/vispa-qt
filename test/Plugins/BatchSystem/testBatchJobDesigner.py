#! /usr/bin/env python
import unittest
import os.path
import sys

import signal
import logging
import sys 


from PyQt4.QtCore import SIGNAL

import Path

from Vispa.Plugins.BatchSystem.BatchJobDesignerTab  import BatchJobDesignerTab
from Vispa.Plugins.BatchSystem import BatchSystemPlugin 
from Vispa.Main import Application
from Vispa.Main.GuiFacade import guiFacade

if __name__ == "__main__":
  signal.signal(signal.SIGINT, signal.SIG_DFL)

  app = Application.Application(sys.argv)
  plugin = BatchSystemPlugin.BatchSystemPlugin()
  tab= BatchJobDesignerTab(plugin)
  tab.setCommand("/bin/sleep")
  tab.addOption("default","Time","",3, True)
  tab.addOption("default","Foo","","Bar")
  print "******************"
  print "THIS ERROR MESSAGE IS NO BUG BUT A FEATURE POF THE NON GUI TEST"
  tab.addParameterRange("FOO",[1,2,3])
  print "******************"
  guiFacade.addTab(tab)
  #designer.show()
  app.run()

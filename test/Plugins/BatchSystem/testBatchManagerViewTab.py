#! /usr/bin/env python
import unittest
import os.path
import sys

import signal
import logging
import sys 


from PyQt4.QtCore import SIGNAL

import Path

from Vispa.Plugins.BatchSystem.BatchManagerViewTab import BatchManagerViewTab
from Vispa.Plugins.LocalBatchManagerPlugin.LocalBatchManagerPlugin import LocalBatchManagerPlugin 
from Vispa.Plugins.BatchSystem import BatchSystemPlugin
from Vispa.Main import Application
from Vispa.Main.GuiFacade import guiFacade

if __name__ == "__main__":
  signal.signal(signal.SIGINT, signal.SIG_DFL)

  app = Application.Application(sys.argv)
  plugin = LocalBatchManagerPlugin()
  bm = plugin.getBatchManager()
  bm.addJob('/bin/sleep 1', "", "")
  bm.addJob('this shall carash', "" , "")
  plugin = BatchSystemPlugin.BatchSystemPlugin()
  tab= BatchManagerViewTab(plugin)
  guiFacade.addTab(tab)
  #designer.show()
  app.run()

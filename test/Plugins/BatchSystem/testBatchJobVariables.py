#!/usr/bin/env python
import unittest
import os.path
import sys

import logging
import sys

import Path
from Vispa.Plugins.BatchSystem.ParameterRange import ParameterRange
from Vispa.Plugins.BatchSystem.OptionValueDecorator import OptionValueDecorator


class testBatchJobParameterRange(unittest.TestCase):
  def setUp(self):
    self.cmdlOption = ParameterRange("name")


  def testScriptInt(self):
    script = "return [1,2,3]"
    self.cmdlOption.setScript(script)
    self.cmdlOption.updateValue()
    self.assertEqual(self.cmdlOption.getValues(), [1,2,3])


  def testScriptFloat(self):
    script = "return [0.0, 0.5]"
    self.cmdlOption.setScript(script)
    self.cmdlOption.updateValue()
    self.assertEqual(self.cmdlOption.getValues(), [0.0,0.5])


  def testScriptString(self):
    script = "return ['a','b']"
    self.cmdlOption.setScript(script)
    self.cmdlOption.updateValue()
    self.assertEqual(self.cmdlOption.getValues(), ["a","b"])



class testOptionValueDecorator(unittest.TestCase):
  def setUp(self):
    self.cmdlOption = OptionValueDecorator("name")


  def testDecoration1(self):
    optionDict = {'testVal' : 2}
    script = """
    return 2*valueDict['testVal']
    """
    self.cmdlOption.setScript(script)
    #self.cmdlOption.getV(optionDict)
    self.assertEqual(self.cmdlOption.getValue(optionDict), 4)


  def testDecoration2(self):
    optionDict = {'A' : 2, 'B' : 3}
    script = """
    return valueDict['A'] + valueDict['B']
    """
    self.cmdlOption.setScript(script)
    #self.cmdlOption.getV(optionDict)
    self.assertEqual(self.cmdlOption.getValue(optionDict), 5)


  def testDecoration_String(self):
    optionDict = {'A' : 2, 'B' : 3}
    script = """
    return "%i_%i" % (valueDict['A'], valueDict['B'])
    """
    self.cmdlOption.setScript(script)
    #self.cmdlOption.getV(optionDict)
    self.assertEqual(self.cmdlOption.getValue(optionDict), "2_3")



if __name__ == "__main__":
  unittest.main()

#! /usr/bin/env python
import os.path
import sys

import logging
import sys


import Path
from copy import copy

from Vispa.Plugins.BatchSystem.CommandLineOption import CommandLineOption
from Vispa.Plugins.BatchSystem.CommandLineDesigner import CommandLineDesigner
from Vispa.Plugins.BatchSystem.CommandLineDesigner import CommandLineXmlFileReader, CommandLineXmlFileWriter
from Vispa.Plugins.BatchSystem.ParameterRange import ParameterRange
from Vispa.Plugins.BatchSystem.OptionValueDecorator import OptionValueDecorator

import unittest
import tempfile
import os
from xml.etree import ElementTree as etree


class testCommandLineOption(unittest.TestCase):
  def setUp(self):
    self.option = CommandLineOption("Foo",0)


  def testHasDecorator(self):
    self.assertFalse(self.option.hasDecorator())

    d = OptionValueDecorator("")
    self.option.setDecorator(d)
    self.assertTrue(self.option.hasDecorator())



class testParameterCombinations(unittest.TestCase):
  def setUp(self):
    self.__commandLineDesigner = CommandLineDesigner()

  def testEmptyRanges(self):
    pSets = self.__commandLineDesigner._generateParameterDicts()
    self.assertEqual(len(pSets), 1)

  def testCombinations(self):
    """
    Test if correct combinations are generated from the Parameter ranges
    """
    L1 = [1,2,3]
    P1 = ParameterRange('P1', L1 )
    L2 = ['A','B']
    P2 = ParameterRange('P2', L2)
    self.__commandLineDesigner.addParameterRange(P1)
    self.__commandLineDesigner.addParameterRange(P2)

    pSets = self.__commandLineDesigner._generateParameterDicts()
    S1 = []
    for s in L2:
      S1.extend(L1)

    S2 = []
    for s in L1:
      S2.extend(L2)
    self.assertEqual(len(pSets), 6)
    for p in pSets:
      self.assertTrue(p.has_key(P1.getName()))
      self.assertTrue(p.has_key(P2.getName()))
      self.assertTrue(p[P1.getName()] in L1)
      self.assertTrue(p[P2.getName()] in L2)
      S1.remove(p[P1.getName()])
      S2.remove(p[P2.getName()])
    self.assertEqual(len(S1), 0)
    self.assertEqual(len(S2), 0)



class testBuildCommandLine(unittest.TestCase):
  def setUp(self):
    self.__commandLineDesigner = CommandLineDesigner()
    self.__commandLineDesigner.setCommand("/path/to/foobar")
  def testWithoutCombinationsAndDecorator(self):
    a = CommandLineOption("Foo","--foo=",2)
    self.__commandLineDesigner.addOption(a)
    cmdl = self.__commandLineDesigner.buildCommandLines()
    self.assertEqual(cmdl,["/path/to/foobar --foo=2"])


  def testMultipleOptions(self):
    a = CommandLineOption("Foo","--foo=",2)
    b = CommandLineOption("Bar","--bar=",3)
    self.__commandLineDesigner.addOption(a)
    self.__commandLineDesigner.addOption(b)
    cmdl = self.__commandLineDesigner.buildCommandLines()
    self.assertEqual(cmdl,["/path/to/foobar --foo=2 --bar=3"])


  def testWithoutCombinationsButDecorator(self):
    a = CommandLineOption("Foo","--foo=",2)
    b = ParameterRange('Bar',[2])
    decorator = OptionValueDecorator("Mul2")
    decorator.setScript("return valueDict['Bar']*2")
    a.setDecorator(decorator)

    self.__commandLineDesigner.addOption(a)
    self.__commandLineDesigner.addParameterRange(b)
    cmdl = self.__commandLineDesigner.buildCommandLines()
    self.assertEqual(cmdl,["/path/to/foobar --foo=4"])


  def testWithCombinationsAndDecorator(self):
    a = CommandLineOption("Foo","--foo=",1)
    b = CommandLineOption("Bar","--bar=",2)
    c = CommandLineOption("FooBar","--FooBar=","")
    self.__commandLineDesigner.addOption(a)
    self.__commandLineDesigner.addOption(b)
    self.__commandLineDesigner.addOption(c)

    aRange = ParameterRange('FooRange',[1,2])
    bRange = ParameterRange('BarRange',[2,3])
    self.__commandLineDesigner.addParameterRange(aRange)
    self.__commandLineDesigner.addParameterRange(bRange)

    decorator2 = OptionValueDecorator("StringOut")
    decorator2.setScript("return 'a_%i_%i' %(valueDict['FooRange'], valueDict['BarRange'])")
    c.setDecorator(decorator2)

    cmdl = self.__commandLineDesigner.buildCommandLines()
    #cmdl = buildCommandLines([a,b,c], [aRange,bRange])
    self.assertEqual(len(cmdl), 4)


class testCommandLineDesignerSerialization(unittest.TestCase):
  def testFileSerialization(self):
    commandLineDesigner = CommandLineDesigner()
    p = ParameterRange('Foo', [1,2,3])
    commandLineDesigner.addParameterRange(p)

    od = OptionValueDecorator('Foo')
    od.setScript('return "123"')
    commandLineDesigner.addOptionDecorator(od)

    o = CommandLineOption('Foo', '--help' , 1, 'gr' )
    commandLineDesigner.addOption(o)
    o.setDecorator(od)

    outFile = tempfile.NamedTemporaryFile(delete=False)
    outFile.close()
    CommandLineXmlFileWriter.serialize(commandLineDesigner, outFile.name)
    reader = CommandLineXmlFileReader(outFile.name)
    cl2 = reader.deserialize()
    self.assertEqual(len(cl2.getParameterRanges()),
        len(commandLineDesigner.getParameterRanges()))
    p2 = cl2.getParameterRanges()[0]
    self.assertTrue(p2.getValues() == p.getValues())

    self.assertEqual(len(cl2.getOptionDecorators()),
        len(commandLineDesigner.getOptionDecorators()))
    self.assertEqual(len(cl2.getOptions()),
        len(commandLineDesigner.getOptions()))
    self.assertTrue(cl2.getOptions()[0].hasDecorator())
    self.assertEqual(cl2.getOptions()[0].getDecorator().getScript(),
        od.getScript()  )

    os.unlink(outFile.name)



class testObjectSerialization(unittest.TestCase):
  def setUp(self):
    self.rootNode = etree.Element('RootElement')


  def __writeReadParamterRange(self, p):
    CommandLineXmlFileWriter.serializeParameterRange(p, self.rootNode)
    pd = CommandLineXmlFileReader.deserializeParameterRange(self.rootNode.getchildren()[0])
    return pd

  def testParameterRange_int(self):
    p = ParameterRange('Foo', [1,2,3])
    pd = self.__writeReadParamterRange(p)
    self.assertEqual(pd.getValues(), p.getValues())

  def testParameterRange_float(self):
    p = ParameterRange('Foo', [1.2,2.1,3.0])
    pd = self.__writeReadParamterRange(p)
    self.assertEqual(pd.getValues(), p.getValues())

  def testParameterRange_bool(self):
    p = ParameterRange('Foo', [True, False])
    pd = self.__writeReadParamterRange(p)
    self.assertEqual(pd.getValues(), p.getValues())

  def testParameterRange_str(self):
    p = ParameterRange('Foo', ['a', 'fuubar'])
    pd = self.__writeReadParamterRange(p)
    self.assertEqual(pd.getValues(), p.getValues())

  def testParameterRange(self):
    p = ParameterRange('Foo', [1,2,3])
    pd = self.__writeReadParamterRange(p)
    self.assertEqual(len(self.rootNode.getchildren()), 1)
    self.assertEqual(pd.getName(), p.getName())
    self.assertEqual(pd.getValues(), p.getValues())


  def testoption(self):
    p = CommandLineOption('Foo', '--help' , 1, 'gr' )
    CommandLineXmlFileWriter.serializeOption(p, self.rootNode)
    self.assertEqual(len(self.rootNode.getchildren()), 1)
    pd = CommandLineXmlFileReader.deserializeOption(self.rootNode.getchildren()[0])
    self.assertEqual(pd.getName(), p.getName())
    self.assertEqual(pd.getGroup(), p.getGroup())
    self.assertEqual(pd.getOptionString(), p.getOptionString())
    self.assertEqual(pd.getDecorator(), p.getDecorator())
    self.assertEqual(pd.isActive(), p.isActive())

  def testOptionDecorator(self):
    p = OptionValueDecorator('Foo')
    CommandLineXmlFileWriter.serializeOptionDecorator(p, self.rootNode)
    self.assertEqual(len(self.rootNode.getchildren()), 1)
    pd = CommandLineXmlFileReader.deserializeOptionDecorator(self.rootNode.getchildren()[0])
    self.assertEqual(pd.getName(), p.getName())







if __name__ == "__main__":
  unittest.main()

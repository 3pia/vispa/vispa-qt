import unittest
import os

# import all tests from subtest files

# return testsuite
def getInteractiveSuite():
  """
  Returns a suite with all tests popping up a gui, so human interaction
  is needed to decide success or failure.
  """
  suite = unittest.TestSuite()
  return suite

def getAutomaticSuite():
  """
  Returns a suite of all tests without the need of human interaction.
  """
  loader = unittest.TestLoader()
  suite = unittest.TestSuite()
  tests = [f[: - 3] for f in
      os.listdir(os.path.join(os.path.dirname(__file__))) if f.startswith("test") and f.endswith(".py")]
  for test in tests:
    m = __import__(test, globals(), locals())
    s = loader.loadTestsFromModule(m)
    suite.addTest(s)
  return suite


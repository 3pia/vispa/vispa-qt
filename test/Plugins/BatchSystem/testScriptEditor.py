#! /usr/bin/env python
import unittest
import os.path
import sys
from PyQt4 import QtCore, QtGui

import signal
import logging
import sys 


from Vispa.Plugins.BatchSystem.ScriptEditor import ScriptEditor 

if __name__ == "__main__":
  logging.debug("Test ScriptEditor")
  signal.signal(signal.SIGINT, signal.SIG_DFL)

  app = QtGui.QApplication(sys.argv)
  w = ScriptEditor("PREFIXLABEL") 
  w.show()
  app.exec_()

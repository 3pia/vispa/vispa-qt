#! /usr/bin/env python
import unittest
import Path
from Vispa.Plugins.BatchSystem.AbstractBatchManager import BatchManager
from Vispa.Plugins.BatchSystem.AbstractBatchJob import AbstractBatchJob



class TestAbstractBatchManager(unittest.TestCase):
  def setUp(self):
    self.__batchManager = BatchManager()


  def test_addJob(self):
    for i in range(5):
      job = AbstractBatchJob('foo')
      self.__batchManager._addJob(job)
      self.assertEqual(self.__batchManager.getNumberOfJobsWithStatus('Created'), i+1)

  def test_JobStatusChanged(self):
    """ Test job counting on status change"""
    job = AbstractBatchJob('foo')
    self.__batchManager._addJob(job)
    self.assertEqual(self.__batchManager.getNumberOfJobsWithStatus('Created'), 1)
    for i in range(3):
      self.__batchManager._jobStatusChanged(job)
      self.assertEqual(self.__batchManager.getNumberOfJobsWithStatus('Created'), 1)
      self.assertEqual(self.__batchManager.getNumberOfJobsWithStatus('Unknown'), 0)

    for i in range(3):
      job.setStatus('Unknown')
      self.__batchManager._jobStatusChanged(job)
      self.assertEqual(self.__batchManager.getNumberOfJobsWithStatus('Created'), 0)
      self.assertEqual(self.__batchManager.getNumberOfJobsWithStatus('Unknown'), 1)





if __name__== "__main__":
  unittest.main()

#!/usr/bin/env python
import unittest
import tempfile
import time

from Vispa.Plugins.CondorBatchManagerPlugin.CondorBatchManager import CondorBatchManager, CondorBatchJob
if CondorBatchManager.checkSystemAvailability():
  class testCondorBatchManager(unittest.TestCase):
    def setUp(self):
      self.batchManager = CondorBatchManager(tempfile.gettempdir())
  
    def test_addJob(self):
      self.assertEqual(len(self.batchManager.getJobs()), 0)
      job = CondorBatchJob('foo')
      self.batchManager._addJob(job)
      self.assertEqual(len(self.batchManager.getJobs()), 1)


if __name__ == "__main__":
  unittest.main()


#! /usr/bin/env python
import unittest
from ConfigParser import ConfigParser
from Vispa.Plugins.CondorBatchManagerPlugin.CondorBatchJob import CondorBatchJob 



class TestCondorBatchJob(unittest.TestCase):
  def testSerializeDeserialize(self):
    b0 = CondorBatchJob('foo')
    b0.setLogFileName('foo.log')
    b0.setSteeringFileName('bar.steer')
    b0.setCondorId('42.0')
    c = ConfigParser()
    b0.serialize(c)

    b1 = CondorBatchJob('bar')
    b1.deserialize(c)

    self.assertEqual(b0.getSteeringFileName(), b1.getSteeringFileName())
    self.assertEqual(b0.getCondorId(), b1.getCondorId())
    self.assertEqual(b0.getLogFileName(), b1.getLogFileName())

    

if __name__== "__main__":
  unittest.main()

### Skeleton for astroSpectrum.py
# created by VISPA
# Mon Oct 11 13:52:52 2010
### PyAnalyse skeleton script

from pxl import modules
from pxl import core, astro
import ROOT
from math import *
class Example(modules.PythonModule):
    def __init__(self):
        ''' Initialize private variables '''
        modules.PythonModule.__init__(self)
        self.counter = 0
        self.hist = ROOT.TH1F("spectrum","Spectrum;log10(Energy [eV]);Entries",100,18,22)
    def initialize(self, module):
        ''' Initialize module options '''
        module.addOption("SaveFigureToFile", "If a non empty filename is given, the figure will be saved to it instead of shown", "")
        self.__module = module

    def beginJob(self, parameters=None):
        '''Executed before the first object comes in'''
        print '*** Begin job'
        self.__fileName = self.__module.getOption("SaveFigureToFile")
        if self.__fileName == "":
          ROOT.gROOT.SetBatch(False)
        else:
          ROOT.gROOT.SetBatch(True)


    def beginRun(self):
        '''Executed before each run'''
        pass

    def analyse(self, object):
        '''Executed on every object'''
        #event=core.toEvent(object)
        self.counter+=1
        bc = core.toBasicContainer(object)
        uhecrs = bc.getObjectsOfType(astro.UHECR)
        for cr in uhecrs:
          self.hist.Fill(log10(cr.getEnergy())+18)
        return object

    def endRun(self):
        '''Executed after each run'''
        pass

    def endJob(self):
        '''Executed after the last object'''
        print '*** End job'
        print self.counter, "Objects"
        c1 = ROOT.TCanvas()
        c1.SetLogy()
        self.hist.Draw()
        if self.__fileName == "":
          c1.WaitPrimitive()
        else:
          print 'Creating outputfile:', self.__fileName
          c1.SaveAs(self.__fileName)


### Skeleton for training.py
# created by VISPA
# Thu Sep 17 13:04:20 2009

#PyModule Skeleton

import os
from ROOT import TFile, TMVA, TCut, std, TString, gROOT, gStyle,TCanvas,TFile,vector

from pxl import modules

class TmvaTraining(modules.PythonModule):
    #----------------------------------------------------------------------
    def initialize(self, module):
        self.__module=module
        self.__module.addStringOption("variable_names", "Names of variables used for training", "sum_jets_mass delta_jets_eta jet1_pt jet1_eta jet1_phi")
        self.__module.addSink("SignalTraining", "SignalTraining")
        self.__module.addSink("SignalTest", "SignalTest")
        self.__module.addSink("BackgroundTraining", "BackgroundTraining")
        self.__module.addSink("BackgroundTest", "BackgroundTest")
        self.__module.addSource("Black hole", "Black hole")
    
    #----------------------------------------------------------------------
    def beginJob(self, parameters=None):
        '''Executed before the first object comes in'''
        
        print "*** Begin job: TMVA training"
        print "  * Running with ROOT version", gROOT.GetVersionInt()
        print "  * Tested with ROOT version 5.32"
        print "  * The TMVA interface changes frequently; if example does not work, please use a compatible ROOT version"
        print "  * Reading parameters"
        self.variable_names = self.__module.getStringOption('variable_names').split()
        print "  * Variable_names = " , self.variable_names

        # ROOT histogram style
        gROOT.Reset()
        gROOT.SetStyle("Plain")
        gStyle.SetOptStat(0)
        gStyle.SetOptFit(0)
        gStyle.SetStatX(0.98)
        gStyle.SetStatY(1.0)
        gStyle.SetStatW(0.1)
        gStyle.SetStatH(0.3)
        gStyle.SetNdivisions(505,"X")
        gStyle.SetNdivisions(508,"Y")
        gStyle.SetLabelOffset(0.008,"X")
        gStyle.SetLabelSize(0.08,"X")
        gStyle.SetTitleSize(0.1,"X")
        gStyle.SetTitleOffset(0.8,"X")
        gStyle.SetLabelOffset(0.02,"Y")
        gStyle.SetLabelSize(0.08,"Y")
        gStyle.SetTitleSize(0.12,"Y")
        gStyle.SetTitleOffset(0.9,"Y")
        gStyle.SetPadTopMargin(0.15)
        gStyle.SetPadBottomMargin(0.2)
        gStyle.SetPadLeftMargin(0.22)
        gStyle.SetPadRightMargin(0.1)
        gStyle.SetLineWidth(1)
        gStyle.SetMarkerStyle(22)
        gStyle.SetMarkerSize(0.7)
        gStyle.SetMarkerColor(2)

        print '  * Begin TMVA training job'

        self.outputFile = TFile.Open('TMVA.root', "RECREATE")
    
        self.factory = TMVA.Factory('tmva_training', self.outputFile, "!V:!Color")
        v = vector("TString")()
        for name in self.variable_names:
            v.push_back(name)
        self.factory.SetInputVariables(v)

    #----------------------------------------------------------------------
    def process(self,object,sink):
        '''Executed on every object'''
        # only process pxl::Events
        event = core.toEvent(object)
        if not event:
            return

        vars = std.vector('double')()
        for name in self.variable_names:
            vars.push_back(event.getUserRecord(name))

        if sink == "SignalTraining":
            self.factory.AddSignalTrainingEvent (vars)
        elif sink == "SignalTest":
            self.factory.AddSignalTestEvent (vars)
        elif sink == "BackgroundTraining":
            self.factory.AddBackgroundTrainingEvent (vars)
        elif sink == "BackgroundTest":
            self.factory.AddBackgroundTestEvent (vars)

        return "Black hole"

    #----------------------------------------------------------------------
    def endJob(self):
        '''Executed after the last object'''
    
        # if gROOT.GetVersionInt()>=52400 and gROOT.GetVersionInt()<52600:
        #     print 'ERROR: In ROOT versions > 5.24 (and < 5.26), TMVA has incompatibilities with PyROOT. Try ROOT 5.22 for the full output of this example.'
        #     return

        print '*** TMVA training being finalized:'
	
        self.factory.PrepareTrainingAndTestTree( TCut(""), TCut(""), "SplitMode=BLOCK:NormMode=NumEvents:!V" )
        self.factory.BookMethod( "MLP", "MLP", "" )
        self.factory.TrainAllMethods()
        self.factory.TestAllMethods()

        if gROOT.GetVersionInt()<52102:
            print 'ERROR: You need ROOT version > 5.21.02 to run TMVA.'
        else:
            self.factory.EvaluateAllMethods()
        self.outputFile.Close()

        print '*** Training end job'
    
        print '*** Plotting histograms from root file'
    
        file1 = TFile("TMVA.root")

        signal_test = file1.Get("Method_MLP/MLP/MVA_MLP_S")
        bkg_test = file1.Get("Method_MLP/MLP/MVA_MLP_B")
        signal_train = file1.Get("Method_MLP/MLP/MVA_MLP_Train_S")
        bkg_train = file1.Get("Method_MLP/MLP/MVA_MLP_Train_B")
        rhoepsilon = file1.Get("Method_MLP/MLP/MVA_MLP_rejBvsS")

        c = TCanvas("TMVA training", "TMVA training", 300, 600)
        c.Divide(1,3)
        myrebin = 10

        signal_test.UseCurrentStyle()
        signal_test.SetTitle("Test")
        signal_test.GetXaxis().SetTitle("MLP")
        signal_test.GetYaxis().SetTitle("N")
        signal_test.SetLineColor(2)
        signal_test.SetLineStyle(1)
        signal_test.SetLineWidth(3)
        signal_test.SetMarkerColor(2)
        signal_test.SetMarkerStyle(20)
        signal_test.SetMarkerSize(0.9)
        signal_test.Rebin(myrebin)

        signal_train.UseCurrentStyle()
        signal_train.SetTitle("Training")
        signal_train.GetXaxis().SetTitle("MLP")
        signal_train.GetYaxis().SetTitle("N")
        signal_train.SetLineColor(2)
        signal_train.SetLineStyle(1)
        signal_train.SetLineWidth(3)
        signal_train.SetMarkerColor(2)
        signal_train.SetMarkerStyle(20)
        signal_train.SetMarkerSize(0.9)
        signal_train.Rebin(myrebin)
        
        bkg_test.UseCurrentStyle()
        bkg_test.SetTitle("Test")
        bkg_test.GetXaxis().SetTitle("MLP")
        bkg_test.GetYaxis().SetTitle("N")
        bkg_test.SetLineColor(4)
        bkg_test.SetLineStyle(1)
        bkg_test.SetLineWidth(3)
        bkg_test.SetMarkerColor(4)
        bkg_test.SetMarkerStyle(20)
        bkg_test.SetMarkerSize(0.9)
        bkg_test.Rebin(myrebin)
    
        bkg_train.UseCurrentStyle()
        bkg_train.SetTitle("Training")
        bkg_train.GetXaxis().SetTitle("MLP")
        bkg_train.GetYaxis().SetTitle("N")
        bkg_train.SetLineColor(4)
        bkg_train.SetLineStyle(1)
        bkg_train.SetLineWidth(3)
        bkg_train.SetMarkerColor(4)
        bkg_train.SetMarkerStyle(20)
        bkg_train.SetMarkerSize(0.9)
        bkg_train.Rebin(myrebin)
    
        rhoepsilon.UseCurrentStyle()
        rhoepsilon.SetTitle("ROC")
        rhoepsilon.GetXaxis().SetTitle("Efficiency")
        rhoepsilon.GetYaxis().SetTitle("Purity")
        rhoepsilon.SetLineColor(2)
        rhoepsilon.SetLineStyle(1)
        rhoepsilon.SetLineWidth(3)
        rhoepsilon.SetMarkerColor(2)
        rhoepsilon.SetMarkerStyle(20)
        rhoepsilon.SetMarkerSize(0.9)
        rhoepsilon.SetMaximum(1)
        rhoepsilon.SetMinimum(0)
    
        c.cd(1)
        signal_train.Draw("e")
        bkg_train.Draw("histsame")
        c.cd(2)
        signal_test.Draw("e")
        bkg_test.Draw("histsame")
        c.cd(3)
        rhoepsilon.Draw()
    
        c.Print("tmva_training.pdf")
    
        print "*** histogram filling finished. ***"
        
        print " Closing all ROOT Canvases will finish the analysis"
        c.WaitPrimitive()
    
        print '*************************************'
        print 'You can inspect all histograms using:'
        print 'root -x $ROOTSYS/tmva/test/TMVAGui.C'
        print '*************************************'

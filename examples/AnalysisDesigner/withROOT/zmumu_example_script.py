#
# Purpose: Reconstruct Z -> mu mu events and plot the invariant mass of the Z
#
# Author : Andreas Hinzmann
# Date   : 02-June-2009
#
from ROOT import gROOT, gStyle, TH1F, TCanvas

from pxl import modules

try:
    from pxl import root
    rootEnabled = True
except:
    print "Using PXL version < 3.3 or PXL compiled without ROOT"
    rootEnabled = False

class ZMuMu(modules.PythonModule):
    #----------------------------------------------------------------------
    def initialize(self, module):
        # ROOT histogram style
        gROOT.Reset()
        gROOT.SetStyle("Plain")
        
        if rootEnabled:
            root.PxlStyle.initRootPlotStyle()

    #----------------------------------------------------------------------
    def beginJob(self, parameters=None):
        print "*** starting analysis "
    
        # histogram booking
    
        self.h_rec_pt = TH1F ("h_rec_pt" , "muon transverse momentum", 40, 0, 100)
        self.h_rec_eta = TH1F ("h_rec_eta", "muon pseudo-rapidity" , 40, - 4, 4)
    
        self.h_gen_pt = TH1F ("h_gen_pt" , "muon transverse momentum", 40, 0, 100)
        self.h_gen_eta = TH1F ("h_gen_eta", "muon pseudo-rapidity" , 40, - 4, 4)
    
        self.h_rec_Zmass = TH1F ("h_rec_Zmass", "reconstructed Z mass", 50, 0, 150)
        self.h_gen_Zmass = TH1F ("h_gen_Zmass", "reconstructed Z mass", 50, 0, 150)
    
        # histogram axis specification
    
        self.h_rec_pt.UseCurrentStyle()
        self.h_rec_eta.UseCurrentStyle()
        self.h_rec_Zmass.UseCurrentStyle()
    
        self.h_rec_pt.GetXaxis().SetTitle("pt [GeV]")
        self.h_rec_eta.GetXaxis().SetTitle("\\eta")
        self.h_rec_Zmass.GetXaxis().SetTitle("m [GeV]")
        
        self.h_rec_pt.Sumw2()
        self.h_rec_eta.Sumw2()
        self.h_rec_Zmass.Sumw2()
    
        self.h_rec_pt .GetYaxis().SetTitle("N")
        self.h_rec_eta.GetYaxis().SetTitle("N")
        self.h_rec_Zmass.GetYaxis().SetTitle("N")
    
    #----------------------------------------------------------------------
    def analyse(self, object):
        # only process pxl::Events
        event = core.toEvent(object)
        if not event:
            return
            
    # get event views of the event
    
        eventviews = event.getEventViews()
        for eventview in eventviews:
    
    # analyse reconstructed particles
    
            if eventview.getName() == "Reconstructed":
                ptmax1 = 0
                ptmax2 = 0
                muon1 = 0
                muon2 = 0
                
                weight = 1.
                if eventview.hasUserRecord("weight"):
                    weight = eventview.getUserRecord("weight")
                
                particles = eventview.getParticles()
                for particle in particles:
                    if particle.getDaughterRelations().size() == 0 and particle.getName() == "Muon":
                        self.h_rec_pt.Fill (particle.getPt(), weight)
                        self.h_rec_eta.Fill(particle.getEta(), weight)
    
                        if particle.getPt() > ptmax1:
                            ptmax1 = particle.getPt()
                            muon2 = muon1
                            muon1 = particle
                        elif particle.getPt() > ptmax2:
                            ptmax2 = particle.getPt()
                            muon2 = particle
    
                if muon1 and muon2:
                    Z = eventview.createParticle()
                    Z.setName('Z')
                    Z.linkDaughter(muon1)
                    Z.linkDaughter(muon2)
                    Z.setP4FromDaughters()
                    self.h_rec_Zmass.Fill(Z.getMass(), weight)
    
    # analyse generator particles
    
            if eventview.getName() == "Generator":
    
                ptmax1 = 0
                ptmax2 = 0
                muon1 = 0
                muon2 = 0
    
                particles = eventview.getParticles()
                for particle in particles:
                    if particle.getDaughterRelations().size() == 0 and particle.getName() == "mu":
                        self.h_gen_pt.Fill (particle.getPt())
                        self.h_gen_eta.Fill(particle.getEta())
    
                        if particle.getPt() > ptmax1:
                            ptmax1 = particle.getPt()
                            muon2 = muon1
                            muon1 = particle
                        elif particle.getPt() > ptmax2:
                            ptmax2 = particle.getPt()
                            muon2 = particle
    
                if muon1 and muon2:
                    Zmass = muon1.getE()*muon2.getE() - muon1.getPx()*muon2.getPx() - muon1.getPy()*muon2.getPy() - muon1.getPz()*muon2.getPz()
                    Zmass = (2 * Zmass) ** 0.5
                    self.h_gen_Zmass.Fill(Zmass)
    
    #----------------------------------------------------------------------
    def endJob(self):
    
        print "*** finishing analysis: write histograms"
    
        c1 = TCanvas('c1', 'muon', 800, 700)
        c1.Divide(2, 2)
        c1.cd(1)
        self.h_rec_pt.UseCurrentStyle()
        self.h_rec_pt.Draw("e")
        self.h_gen_pt.Draw("same")
        c1.cd(2)
        self.h_rec_eta.Draw("e")
        self.h_gen_eta.Draw("same")
        c1.cd(3)
        self.h_rec_Zmass.Draw("e")
        self.h_gen_Zmass.Draw("same")
        c1.Print("zmumu_example_output.ps")
    
        print " Closing all ROOT Canvases will finish the analysis"
        c1.WaitPrimitive()

#
# Purpose: apply efficiency correction
#
# Author : Jan Steggemann
# Date   : Aug 2012


import ROOT
from pxl import core, root
from pxl import modules

class EfficiencyCorrection(modules.PythonModule):
    def initialize(self, module):
        pass
    
    def beginJob (self, parameters=None):
        print "*** Efficiency correction: beginJob"
        print "*** Efficiency correction: Read efficiency histogram from pxlio"
        pxlioFile = core.InputFile("eff_histogram.pxlio")
        rootSer = pxlioFile.readNextObject()
        
        # takeTObject transferse ownership to Python
        self._hist = rootSer.takeTObject()
        
        # Note: Using getTObject is also possible, but the TObject
        # is by default tied to the RootSerializable and the
        # TObject is hence deleted when the RootSerializable goes
        # out of scope

    def analyse (self, object):
        # only process pxl::Events
        event = core.toEvent(object)
        if not event:
            return
        
        eventviews = event.getEventViews()
        for eventview in eventviews:
            if eventview.getName() == "Reconstructed":

                particles = eventview.getParticles()
                for particle in particles:
                    if particle.getName() == "Muon":
                        # The histogram represents a 1D efficiency parametrisation in muon eta
                        weight = self._hist.GetBinContent(self._hist.GetXaxis().FindBin(particle.getEta()))
                        
                        # Reweight the event according to the efficiency histogram
                        if not eventview.hasUserRecord("weight"):
                            eventview.setUserRecord("weight", weight)
                        else:
                            eventview.setUserRecord("weight", weight * eventview.getUserRecord("weight"))

    def endJob(self):
        pass

if __name__ == "__main__":
    effcorr = EfficiencyCorrection()
    effcorr.beginJob()
### TMVA evaluate script
# created by VISPA
# Thu Sep 17 17:56:52 2009
# Plots classifier output distribution after previous TMVA training

import os


from ROOT import TMVA
from math import sqrt
from array import array
from ROOT import gROOT, gStyle, TH1F, TCanvas, std, TString, vector

from pxl import modules
try:
    from pxl import root
    rootEnabled = True
except:
    print "Using PXL version < 3.3 or PXL compiled without ROOT"
    rootEnabled = False

class TmvaEvaluate(modules.PythonModule):
    #----------------------------------------------------------------------
    def initialize(self, module):
        self.__module = module
        self.__module.addStringOption("variable_names", "variable_names", "sum_jets_mass delta_jets_eta jet1_pt jet1_eta jet1_phi")

    #----------------------------------------------------------------------
    def beginJob(self, parameters=None):
        '''Executed before the first object comes in'''
        print "*** Begin job TMVA evaluate"
        print "  * reading parameters"
        print "  * Note: Variable names must be the same as in the TMVA training"
        self.variable_names = self.__module.getStringOption('variable_names').split()
        print "  * variable_names = " , self.variable_names

        # ROOT histogram style
        gROOT.Reset()
        gROOT.SetStyle("Plain")
        
        if rootEnabled:
            root.PxlStyle.initRootPlotStyle()

        # particle histograms
        self.h_MLP = TH1F ("h_MLP", "Data", 20, -1, 1)
        self.h_MLP.UseCurrentStyle()
        self.h_MLP.GetXaxis().SetTitle("MLP")
        self.h_MLP.GetYaxis().SetTitle("N")
        self.h_MLP.SetLineColor(2)
        self.h_MLP.SetLineStyle(1)
        self.h_MLP.SetLineWidth(3)
        self.h_MLP.SetMarkerColor(2)
        self.h_MLP.SetMarkerStyle(20)
        self.h_MLP.SetMarkerSize(0.9)

        v = vector("TString")()
        for name in self.variable_names:
            v.push_back(name)

        self.tmva_weightFilename = 'weights/tmva_training_MLP.weights.xml'
        self.reader = None

        '''Check if tmva training was already done and that weight file exists.'''
        if os.path.isfile(self.tmva_weightFilename):
            print "  * MVA weight file '%s' was found." % (self.tmva_weightFilename)

            '''Book TMVA Reader if file was found'''
            self.reader = TMVA.Reader(v, "!Color")
            self.reader.BookMVA("MLP method", self.tmva_weightFilename)
            print "  * tmva_reader started."
        else:
            print "  * ERROR: Weight file was not found. Please run the MVA training before trying to evaluate!"

    #----------------------------------------------------------------------
    def analyse(self, object):
        '''Executed on every object'''
        # only process pxl::Events
        event = core.toEvent(object)
        if not event:
            return

        if not self.reader == None:
            vars = std.vector('double')()
            for name in self.variable_names:
                vars.push_back(event.getUserRecord(name))
            value = self.reader.EvaluateMVA(vars, "MLP method")
            self.h_MLP.Fill(value)

    #----------------------------------------------------------------------
    def endJob(self):
        '''Executed after the last object'''

        print '*** End job evaluate'

        if not self.reader == None:
            c1 = TCanvas('TMVA evaluate', 'TMVA evaluate', 300, 200)
            c1.Divide(1, 1)
            c1.cd(1)
            self.h_MLP.Draw()
            c1.Print("tmva_evaluate.pdf")

            print "*** histogram filling finished. ***"

            print " Closing all ROOT Canvases will finish the analysis"
            c1.WaitPrimitive()
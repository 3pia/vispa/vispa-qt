#
# Purpose: smear particle momenta
#
# Author : Andreas Hinzmann
# Date   : 26-Oct-2009
#
import os.path
import random
from ROOT import TLorentzVector

from pxl import modules

class Smearer(modules.PythonModule):
    #----------------------------------------------------------------------
    def initialize(self, module):
        pass
    
    #----------------------------------------------------------------------
    def beginJob (self, parameters=None):
        print "*** smearing particle momenta"
        self.rel_resolutions_pt={"Jet":0.1,"Muon":0.01,"Electron":0.01,"Photon":0.01}
        self.rel_resolutions_eta={"Jet":0.01,"Muon":0.001,"Electron":0.001,"Photon":0.01}
        self.rel_resolutions_phi={"Jet":0.01,"Muon":0.001,"Electron":0.001,"Photon":0.01}
        random.seed(0)

    #----------------------------------------------------------------------
    def analyse (self, object):
        # only process pxl::Events
        event = core.toEvent(object)
        if not event:
            return
        
        eventviews = event.getEventViews()
        for eventview in eventviews:
            particles = eventview.getParticles()
            for particle in particles:
                name=particle.getName()
                (pt,eta,phi,m)=(particle.getPt(),particle.getEta(),particle.getPhi(),particle.getMass())
                if name in self.rel_resolutions_pt.keys():
                    pt*=random.gauss(1,self.rel_resolutions_pt[name])
                if name in self.rel_resolutions_eta.keys():
                    eta*=random.gauss(1,self.rel_resolutions_eta[name])
                if name in self.rel_resolutions_eta.keys():
                    phi*=random.gauss(1,self.rel_resolutions_phi[name])
                v=TLorentzVector()
                v.SetPtEtaPhiM(pt,eta,phi,m)
                if name in self.rel_resolutions_pt.keys():
                    print "Smearing",(particle.getPx(),particle.getPy(),particle.getPz(),particle.getE()),"to",(v.Px(),v.Py(),v.Pz(),v.E())
                particle.setP4(v.Px(),v.Py(),v.Pz(),v.E())

    #----------------------------------------------------------------------
    def endJob(self):
        pass

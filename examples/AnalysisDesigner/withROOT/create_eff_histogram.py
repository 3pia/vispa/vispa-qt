#
# Create a muon efficiency histogram and save to PXLIO file
#

import ROOT

from pxl import core, root

def main():
    h1 = ROOT.TH1F("mu_efficiency", "mu_efficiency", 10, -2.5, 2.5)
    h1.SetBinContent(1, 0.82)
    h1.SetBinContent(2, 0.90)
    h1.SetBinContent(3, 0.87)
    h1.SetBinContent(4, 0.93)
    h1.SetBinContent(5, 0.94)
    h1.SetBinContent(6, 0.94)
    h1.SetBinContent(7, 0.92)
    h1.SetBinContent(8, 0.88)
    h1.SetBinContent(9, 0.90)
    h1.SetBinContent(10, 0.82)
    
    pxlioFile = core.OutputFile("eff_histogram.pxlio")
    
    rootSer = root.RootSerializable()
    rootSer.setTObject(h1)
    
    pxlioFile.streamObject(rootSer)
    
    pxlioFile.close()
    
if __name__ == "__main__":
    main()
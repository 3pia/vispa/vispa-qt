### Skeleton for astroIsotropyGenerator.py
# created by VISPA
# Mon Oct 11 13:52:29 2010
### PyGenerate skeleton script

from pxl import modules

class Example(modules.PythonModule):
    def __init__(self):
        ''' Initialize private variables '''
        modules.PythonModule.__init__(self)
        self.counter = 0
        self.random = core.Random()

    def initialize(self, module):
        ''' Initialize module options '''
        module.addOption("NumberOfUHECRs", "Number of UHECRs to add to the container", 5000)
        module.addOption("SpectralIndex1", "Spectral index of the energy spectrum of the UHECRs below the breaking point", -2.7)
        module.addOption("SpectralIndex2", "Spectral index of the energy spectrum of the UHECRs above the breaking point", -4.7)
        module.addOption("BreakingPoint", "Energy of the breaking point of the power law [EeV]", 100)
        module.addOption("MinimumEnergy", "Minimum energy of the UHECRs [EeV]", 5)
        module.addOption("MaximumEnergy", "Maximum energy of the UHECRs [EeV]", 1000)
        self.__module = module


    def beginJob(self, parameters=None):
        '''Executed before the first object comes in'''
        print '*** Begin job'
        self.NumberOfUHECRs = self.__module.getOption("NumberOfUHECRs")
        self.SpectralIndex1 = self.__module.getOption("SpectralIndex1")
        self.SpectralIndex2 = self.__module.getOption("SpectralIndex2")
        self.BreakingPoint = self.__module.getOption("BreakingPoint")
        self.MinimumEnergy = self.__module.getOption("MinimumEnergy")
        self.MaximumEnergy = self.__module.getOption("MaximumEnergy")

    def beginRun(self):
        '''Executed before each run'''
        pass

    def generate(self):
        '''Executed on every object'''
        self.counter+=1
        if self.counter > 1:
          return None
        bc = core.BasicContainer()
        for i in range(self.NumberOfUHECRs):
          s = astro.UHECR(self.random.randUnitVectorOnSphere())
          s.setEnergy(self.random.randBrokenPowerLaw(self.SpectralIndex1,self.SpectralIndex2,self.BreakingPoint,self.MinimumEnergy,1000))
          bc.insertObject(s)
        return bc

    def endRun(self):
        '''Executed after each run'''
        pass

    def endJob(self):
        '''Executed after the last object'''
        print '*** End job'

#
# Purpose: common event selection for data from two different streams
#          data streams are kept separated
#         
# Author : Martin Erdmann
# Date   : 5-Jul-2008
#
# Modified : Robert Fischer
# Date     : 23-Jun-2010
#
# Modified : Andreas Hinzmann
# Date     : 27-Sep-2010

#from pxl import modules

class Selection(modules.PythonModule):
    #----------------------------------------------------------------------
    def initialize(self, module):
        module.addSink("Input1", "1st channel in")
        module.addSink("Input2", "2nd channel in")
        module.addSource("Output1", "1st channel pass")
        module.addSource("Output2", "2nd channel pass")
        module.addSource("Fail", "Fail event selection")
    
    #----------------------------------------------------------------------
    def beginJob(self, parameters=None):
        print "*** Begin job selection script ***"
        # event counting
        self._ievent = 0
        
        # accepted event counting
        self._jevent = 0
    
    #----------------------------------------------------------------------
    def process(self, object, sink):
    	# only process pxl::Events
        event = core.toEvent(object)
        if not event:
            return sink
    
        # count all events
        self._ievent += 1
    
        # reject event not fulfilling the selection criteria below
        accept = False
    
        # access to the event views of the event
        eventviews = event.getObjects()
    
        # check for reconstructed muons with large transverse momentum
        for eventview in eventviews:
            if eventview.getName() == "Reconstructed":
                particles = eventview.getObjects()
                for particle in particles:
                    if particle.getName() == "Muon" and particle.getPt() > 20:
                        accept = True
    
        # Pass accepted event to the corresponding output stream
        if accept:
            self._jevent += 1
            # The output source is a string given above; sink is either Input1 or Input2 
            source = "Output"+sink[-1]
            print "Event from", sink, "passed to", source
            return source
        else:
            return "Fail"
    
    #----------------------------------------------------------------------
    def endJob(self):
        print "*** End job selection script ***"
        print "  * data selection finished. ", self._jevent, "events passed out of", self._ievent, "events."


#
# Purpose: rename Particles according to the default name in
#          a ParticleDataList.
#

import os.path
from pxl import modules

class Rename(modules.PythonModule):
    #----------------------------------------------------------------------
    def initialize(self, module):
        pass
    
    #----------------------------------------------------------------------
    def beginJob (self, parameters=None):
        print "*** Rename particles according to ParticleDataList:"
        print hep.partonParticleDataList.getList()
    
    #----------------------------------------------------------------------
    def analyse(self, object):
        # only process pxl::Events
        event = core.toEvent(object)
        if not event:
            return

        eventviews = event.getEventViews()
        for eventview in eventviews:
            particles = eventview.getParticles()
            for particle in particles:
                # use default name from ParticleDataList for final state objects
                if particle.numberOfDaughters() == 0: # Alternative: if particle.getDaughterRelations().size() == 0: 
                    name = hep.partonParticleDataList.getDefaultName(particle.getName())
                    print "Rename", particle.getName(), "to", name
                    particle.setName(name)
    
    #----------------------------------------------------------------------
    def endJob(self):
        pass

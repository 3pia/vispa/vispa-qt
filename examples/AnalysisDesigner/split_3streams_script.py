#
# Purpose: split 1 input event data stream into 3 output data streams, each containing 1/3 of the data
#         
# Author : Martin Erdmann
# Date   : 5-Jul-2008
#
# Modified : Robert Fischer
# Date     : 23-Jun-2010

from pxl import modules

class Splitter(modules.PythonModule):
    #----------------------------------------------------------------------
    def initialize(self, module):
        print "*** Initialize split streams ***"
        
        module.addSink("in","input stream")
        module.addSource("out1","1st channel")
        module.addSource("out2","2nd channel")
        module.addSource("out3","3rd channel")
    
    #----------------------------------------------------------------------
    def beginJob(self, parameters=None):
        # maximum number of streams
        self._out_streams = 3
        
        # stream counting
        self._jstream = 9999
        
        # event counting
        self._ievent = 0
        print "*** Begin job split streams"
        print "  * start splitting data into", self._out_streams , "streams ***"
    
    #----------------------------------------------------------------------
    def switch(self, event):
        # increment by one for each event
        self._ievent += 1
    
        # increment stream and verify
        self._jstream += 1
    
        if self._jstream >= self._out_streams: 
            self._jstream = 0
    
        # return to pass this event to the corresponding output stream
        source="out"+str(self._jstream+1)
        print "Directing event to",source
        return source
    
    #----------------------------------------------------------------------
    def endJob(self):
        print "*** End job split streams"
        print "  * Data splitting finished. ", self._ievent, "events splitted into", self._out_streams, "streams."


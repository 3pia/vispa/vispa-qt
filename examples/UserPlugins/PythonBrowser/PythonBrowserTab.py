import logging
import os.path

from Vispa.Plugins.Browser.BrowserTab import BrowserTab

from Vispa.Main.GuiFacade import guiFacade

class PythonBrowserTab(BrowserTab):
    """
    """
    def __init__(self, plugin, parent=None):
        logging.debug(__name__ + ": __init__")
        BrowserTab.__init__(self, plugin, parent)
        self.setEditable(False)

    #@staticmethod
    def staticSupportedFileTypes():
        """ Returns supported file type: py.
        """
        return [('py', 'Python file')]
    staticSupportedFileTypes = staticmethod(staticSupportedFileTypes)
    
    def readFile(self, filename):
        """ Reads in the file.
        """
        return self.dataAccessor().open(filename)


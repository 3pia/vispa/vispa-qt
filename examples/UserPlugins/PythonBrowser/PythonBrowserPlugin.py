import os.path
import logging

from Vispa.Plugins.Browser.BrowserPlugin import BrowserPlugin
from Vispa.Plugins.Browser.BrowserTab import BrowserTab
from Vispa.Views.BoxDecayView import BoxDecayView
from PythonBrowserTab import PythonBrowserTab
from PythonDataAccessor import PythonDataAccessor
from Vispa.Main.GuiFacade import guiFacade

class PythonBrowserPlugin(BrowserPlugin):
    """ The PythonBrowserPlugin opens python files in a PythonBrowserTab.
    """
    
    def __init__(self):
        logging.debug(__name__ + ": __init__")
        BrowserPlugin.__init__(self)
        self.registerFiletypesFromTab(PythonBrowserTab)
        guiFacade.createStartupScreenEntry(verifyingFiletypes=self.filetypes())

    def startUp(self):
        BrowserPlugin.startUp(self)
        self.addCenterView(BoxDecayView)
                
    def newTab(self):
        """ Create PythonBrowserTab and add to MainWindow.
        """
        tab = PythonBrowserTab(self)
        tab.treeView().setMaxDepth(3)
        tab.setDataAccessor(PythonDataAccessor())
        tab.boxContentDialog().addButton("&Label", "object.label")
        guiFacade.addTab(tab)
        return tab

import sys
import os.path
import logging
import inspect

from Vispa.Share.BasicDataAccessor import BasicDataAccessor
from Vispa.Share.RelativeDataAccessor import RelativeDataAccessor
from Vispa.Main.Exceptions import exception_traceback

imported_modules = {}
file_dict = {}

class ObjectWithLabel(object):
    def __init__(self, label, obj):
        self.label = label
        self.object = obj

class PythonDataAccessor(BasicDataAccessor,RelativeDataAccessor):
    def __init__(self):
        logging.debug(__name__ + ": __init__")

        self._topLevelObjects = []
        self._file = None
    
    def topLevelObjects(self):
        return self._topLevelObjects 

    def label(self, obj):
        """ Get label of an object """
        text = obj.label
        if hasattr(obj.object, "__class__"):
            text += " (" + "".join(str(obj.object.__class__).split("'")[1: - 1]) + ")"
        return text

    def children(self, obj):
        """ Get children of an object """
        objects = []
        for entry in dir(obj.object):
            if not entry.startswith("_") and not entry.startswith("im_"):
                o = getattr(obj.object, entry)
                if self._property(entry, o) == None:
                    objects += [ObjectWithLabel(entry, o)]
        return objects

    def isContainer(self, obj):
        """ Return if the object is a container object.
        """
        return len(self.children(obj))>0

    def daughterRelations(self, obj):
        """ Return a list of the daughter relations of an object.
        """
        return []
        
    def motherRelations(self, obj):
        """ Return a list of the mother relations of an object.
        """
        return []

    def fullFilename(self, obj):
        """ Get full filename """
        text = ""
        if obj.label in file_dict.keys():
            text = file_dict[obj.label]
        root = os.path.splitext(text)[0]
        if root != "":
            text = root + ".py"
        return text

    def _property(self, name, obj):
        value = obj
        if callable(obj) and (inspect.ismethod(obj) or inspect.isfunction(obj)) and len(inspect.getargspec(obj)[0]) == 1:
            try:
                value = obj()
            except Exception:
                pass
        if isinstance(value, (bool)):
            return ("Boolean", name, value)
        elif isinstance(value, (int, long)):
            return ("Integer", name, value)
        elif isinstance(value, (float)):
            return ("Double", name, value)
        elif isinstance(value, (complex, str, unicode, list, tuple, dict)):
            try:
                strvalue = str(value)
            except Exception, e:
                strvalue = "ERROR: " + str(e)
            return ("String", name, strvalue)
        else:
            return None

    def properties(self, obj):
        """ Make list of all properties """
        properties = []
        properties += [("Category", "Object info", "")]
        if self.label(obj) != "":
            properties += [("String", "label", self.label(obj), None, True)]
        if self.fullFilename(obj) != "":
            properties += [("String", "full filename", self.fullFilename(obj), None, True)]
        properties += [("Category", "Content", "")]
        for entry in dir(obj.object):
            if not entry.startswith("_") and not entry.startswith("im_"):
                value = getattr(obj.object, entry)
                property = self._property(entry, value)
                if property != None:
                    properties += [property]
        return tuple(properties)
    
    def open(self, filename=None):
        """ Open module file and read it.
        """
        logging.debug(__name__ + ": open")
        if filename != None:
            self._filename = filename
        global imported_modules
        (module_path, fileName) = os.path.split(str(self._filename))
        self._moduleName = os.path.splitext(fileName)[0]

        # import input-module and make list of all imported modules
        for i in imported_modules.iterkeys():
            if i in sys.modules.keys():
                del sys.modules[i]
        sys.path.insert(0, module_path)
        common_imports = sys.modules.copy()

        import imp
        theFile = open(str(self._filename))
        self._file = imp.load_module(self._moduleName.replace(".", "_"), theFile, str(self._filename), ("py", "r", 1))
        theFile.close()
        
        imported_modules = sys.modules.copy()
        for i in common_imports.iterkeys():
            del imported_modules[i]
        
        # make dictionary that connects every object with the file in which it is defined
        for j in imported_modules.itervalues():
          setj = set(dir(j))
          for entry in setj:
              if entry[0] != "_":
                source = 1
                for k in imported_modules.itervalues():
                    if hasattr(k, entry):
                      setk = set(dir(k))
                      if len(setk) < len(setj) and setk < setj:
                        source = 0
                if source == 1:
                    filen = self._filename
                    if hasattr(j, "__file__"):
                        filen = j.__file__
                    file_dict[entry] = filen

        self._topLevelObjects = self.children(ObjectWithLabel(self._moduleName, self._file))
        return True

@ECHO OFF
heat dir "..\..\Vispa" -nologo  -dr INSTALLDIR -var var.SourceDir -cg VispaFiles -gg -sfrag -template:fragment -out directory.wxs
candle -nologo -dVispaRoot=..\..\ vispa.wxs
candle -nologo -dSourceDir=..\..\Vispa directory.wxs
light -nologo  -ext WixUIExtension vispa.wixobj directory.wixobj -o Vispa-0.6.0.msi

/*
 *  loader.cpp
 *  Simple program to load Vispa on Mac OS X.
 *  Makes sure necessary environment variables are set before Python is invoked.
 *
 *  Author: Robert Fischer
 *  Last modified: 2010-07-12
 *
 */

#include <iostream>
#include <string>

std::string& replaceAll(std::string& context, const std::string& from, const std::string& to)
{
	// found at http://www.cppreference.com/wiki/string/replace on 2010-07-11
    size_t lookHere = 0;
    size_t foundHere;
    while((foundHere = context.find(from, lookHere)) != std::string::npos)
    {
          context.replace(foundHere, from.size(), to);
          lookHere = foundHere + to.size();
    }
    return context;
}

std::string getExecutablePath(const std::string& executable) 
{
	std::string raw_path(executable.substr(0, executable.rfind("/")));
	if (raw_path[0] == *"/")
	{
		// absolute path
		//std::cout << "getExecutablePath(): absolute path" << std::endl;
		return raw_path;
	}
	
	// relative path
	//std::cout << "getExecutablePath(): relative path" << std::endl;
	char buf[BUFSIZ];
	std::string cwd(getcwd(buf, BUFSIZ));
	return cwd +"/"+ raw_path;
}

std::string getNormalizedPath(const std::string& path)
{
	std::string _path(path);
	//std::cout << "getNormalizedPath(): " << _path << std::endl;

	// "/./"
	replaceAll(_path, "/./", "/");

	// "/.."
	int pos(_path.find("/.."));
	int previousSlashPosistion(0);
	while(pos != std::string::npos)
	{
		previousSlashPosistion = _path.substr(0, pos).rfind("/");
		_path = _path.substr(0, previousSlashPosistion) + _path.substr(pos+3);
		pos = _path.find("/..");
		//std::cout << "getNormalizedPath(): " << _path << std::endl;
	}
	return _path;
}

void appendToEnvironmentVariable(const std::string name, const std::string appendValue)
{
	const char* cname = name.c_str();
	//std::cout << "appendToEnvironmentVariable: ";
	char * oldValue = getenv(cname);
	if(oldValue != 0)
	{
		// append
		setenv(cname, (std::string(oldValue) + ":" + getNormalizedPath(appendValue)).c_str(), 1);
	} else
	{
		// overwrite
		setenv(cname, getNormalizedPath(appendValue).c_str(), 1);
	}

	//std::cout << name << " == " << getenv(cname) << std::endl;
}

void printEnvironmentVariable(const std::string& variable_name)
{
	printf("export %s=\"%s\"\n", variable_name.c_str(), getenv(variable_name.c_str()));
}

int main(int argc, char** argv)
{
	std::cout << "___________________ Begin of OS X Vispa loader ____________________\n" << std::endl;

	// define variables
	std::string executable_path(getNormalizedPath(getExecutablePath(argv[0])));
	std::string command(executable_path + std::string("/python"));
	std::string vispa_script(getNormalizedPath(executable_path + "/../Resources/vispa"));
	
	// when application bundle is started by double click 
	// os x adds psn id argument that should not be forwarded to vispa
	//for(int i=1; i<argc; i++)
	//	command += std::string(" ") + std::string(argv[i]);

	// ROOT variables
	if(getenv("ROOTSYS") == 0)
	{
		std::string rootsys(getNormalizedPath(executable_path + "/../Resources/root"));
		std::string root_lib(rootsys + "/lib");
		std::cout << "rootsys_lib: " << root_lib.c_str() << std::endl;
		setenv("ROOTSYS", rootsys.c_str(), 1);
		appendToEnvironmentVariable("PYTHONPATH", root_lib);
		appendToEnvironmentVariable("LD_LIBRARY_PATH", root_lib);
	}
	
	// set environment variables
	//setenv("VERSIONER_PYTHON_VERSION", "2.5", 1);
	appendToEnvironmentVariable("PYTHONPATH", (executable_path + "/../Resources/site-packages"));
	appendToEnvironmentVariable("QT_PLUGIN_PATH", (executable_path + "/../Resources/qt-plugins"));
	appendToEnvironmentVariable("PATH", executable_path);
	appendToEnvironmentVariable("DYLD_FRAMEWORK_PATH", (executable_path + "/../Frameworks"));

	// print status info
	std::cout << "executable_path: " << executable_path << std::endl;
	std::cout << "command: " << command << std::endl;
	std::cout << "vispa_script: " << vispa_script << std::endl;

	std::cout << "\n__environment:__" << std::endl;
	const char* variables[] = {"PATH", "PYTHONPATH", "QT_PLUGIN_PATH", "LD_LIBRARY_PATH", "DYLD_FRAMEWORK_PATH", "ROOTSYS", "VERSIONER_PYTHON_VERSION"};
	for (int i=0; i < 6; i++)
		printEnvironmentVariable(variables[i]);
	
	std::cout << "\n___________________ End of OS X Vispa loader ____________________\n\n" << std::endl;

	// initiate python / vispa
	return execl(command.c_str(), command.c_str(), vispa_script.c_str(), NULL);
	//return system((command.c_str() + std::string(" ") + vispa_script.c_str()).c_str());
	
	//return execl("/usr/bin/arch", "/usr/bin/arch", "-x86_64", command.c_str(), vispa_script.c_str(), NULL);
	//return system((std::string("/usr/bin/arch -x86_64 ") + command.c_str() + " " + vispa_script.c_str()).c_str());
}


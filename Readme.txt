------------------------------------------------------------------------------

    VISPA - Visual Physics Analysis
    Copyright (C) 2006-2013  M. Erdmann
    Web:     http://vispa.physik.rwth-aachen.de      
    
    The VISPA program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This software is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301 USA

------------------------------------------------------------------------------

INSTALL INSTRUCTIONS
------------------------

# Required software

  - Python (>= 2.4.0 and < 3) 

  - PyQt4


# Recommended software

  - PXL, version >= 3.0.0 (http://vispa.physik.rwth-aachen.de), with pypxl 

  - ROOT with PyROOT (http://root.cern.ch)
    Required for some examples, e.g. histogramming
    # For ROOT to work, you need to setup some environment variables
    source ${YOUR_ROOTSYS}/bin/thisroot.sh
	
	- matplotlib, version >= 1.1.0 (http://http://matplotlib.sourceforge.net/)
		Required for additional views usefull for astroparticle physics

   
# Optional software for creating documentation

  - doxygen (optional, recommended version >= 1.5)

  - graphviz (optional, recommended version >= 2.20)


# using setup.py

  The setup.py from the distutils in python works similar as "./configure
  and make" works in C++. You can install the software in a directory
  with e.g. 'python setup.py install --prefix=$HOME/local' or make tar
  ball distribution with 'python setup.py sdist'. For an overview on the
  capabilities of the setup.py look at 'python setup.py --help'.

# Python

  make sure all software packages use the same Python installation/version!
	 
RUN INSTRUCTIONS
----------------

  In your local directory, run

	bin/vispa
	
  or

	python bin/vispa
	
  to start the VISPA GUI.


# Hint

  If VISPA does not run properly after an update of an
  already checked out version, try delete all .pyc files.
  (This is because python does not realize when files have
  been moved or renamed)
  Pydev can do this: right click on any folder
  -> Pydev -> remove *.pyc,...

import sys
import os
import logging
import ConfigParser

import Vispa.__init__

from PyQt4 import QtCore

logging.debug(__name__ + " Start preferences system")

########################################################################  
# Functions to determine the system settings needed to point to
# specific directories

def setBaseDirectory(dir):
    global baseDirectory, mainDirectory, pluginDirectory, docDirectory
    baseDirectory = dir
    logging.debug(__name__ +': baseDirectory - '+baseDirectory)
    mainDirectory = os.path.join(baseDirectory, "Vispa/Main")
    logging.debug(__name__ +': mainDirectory - '+mainDirectory)
    pluginDirectory = os.path.join(baseDirectory, "Vispa/Plugins")
    logging.debug(__name__ +': pluginDirectory - '+pluginDirectory)
    docDirectory = os.path.join(baseDirectory, "doc/pxldoc")
    logging.debug(__name__ +': docDirectory - '+docDirectory)

def setHomeDirectory(dir):
    global homeDirectory, preferencesDirectory, vispaIniFileName, preferencesIniFileName, logDirectory, userPluginDirectory
    homeDirectory = dir
    logging.debug(__name__ +': homeDirectory - '+homeDirectory)
    preferencesDirectory = os.path.abspath(os.path.join(homeDirectory,".vispa"))
    if not os.path.isdir(preferencesDirectory):
      os.mkdir(preferencesDirectory)
    logging.debug(__name__ +': preferencesDirectory - '+preferencesDirectory)
    vispaIniFileName = os.path.abspath(os.path.join(preferencesDirectory,"vispa.ini"))
    logging.debug(__name__ +': vispaIniFileName - '+vispaIniFileName)
    preferencesIniFileName = os.path.abspath(os.path.join(preferencesDirectory,"preferences.ini"))
    logging.debug(__name__ +': preferencesIniFileName - '+preferencesIniFileName)
    logDirectory = os.path.abspath(preferencesDirectory)
    logging.debug(__name__ +': logDirectory - '+logDirectory)
    userPluginDirectory = os.path.abspath(os.path.join(preferencesDirectory,"UserPlugins"))
    if not os.path.isdir(userPluginDirectory):
      os.mkdir(userPluginDirectory)
    logging.debug(__name__ +': userPluginDirectory - '+userPluginDirectory)
########################################################################  


def setWebsiteUrl(url):
    global websiteUrl
    websiteUrl=url

def find_in_path(file, path=None):
  """find_in_path(file[, path=os.environ['PATH']]) -> list

  Finds all files with a specified name that exist in the operating system's
  search path (os.environ['PATH']), and returns them as a list in the same
  order as the path.  Instead of using the operating system's search path,
  the path argument can specify an alternative path, either as a list of paths
  of directories, or as a single string seperated by the character os.pathsep.

  If you want to limit the found files to those with particular properties,
  use filter() or which().
  
  Adapted from http://bugs.python.org/file8185/find_in_path.py
  """
  if path is None:
    path = os.environ.get('PATH', '')
  if type(path) is type(''):
    path = path.split(os.pathsep)
  return filter(os.path.exists,
                map(lambda dir, file=file: os.path.join(dir, file), path))


# the preferences holding the config file
__preferences = ConfigParser.ConfigParser()
__vispaIni = ConfigParser.ConfigParser()
# Holds the settings
__settings = {}

def loadPreferences():
  '''Load the preferences from the preferencesIniFileName'''
  global __preferences
  __preferences.read(preferencesIniFileName)

def loadVispaIni():
  '''Load the ini from the vispaIniFileName'''
  global __vispaIni
  __vispaIni.read(vispaIniFileName)


def writePreferences():
  '''Write preferences to preferencesIniFileName - loop over settings first, to create
  the appropriate entries in the __preferences file'''
  global __preferences, __settings, preferencesIniFileName
  for section,settings in __settings.iteritems():
    for label, setting in settings.iteritems():
      __preferences.set(section,label,setting.getValue())
  try:
      configfile = open(preferencesIniFileName, "w")
      __preferences.write(configfile)
      configfile.close()
  except IOError:
    logging.error("Error writing configfile")

def writeVispaIni():
  '''Write preferences to vispaIniFileName'''
  global __vispaIni, __settings, vispaIniFileName
  try:
      configfile = open(vispaIniFileName, "w")
      __vispaIni.write(configfile)
      configfile.close()
  except IOError:
    logging.error("Error writing configfile")

class Setting:
  '''Container class to store informatuions about a setting'''
  def __init__(self, section, label, defaultValue, description,
      settingChangedFunction = None):
    self.__section = section
    self.__label = label
    self.__defaultValue = defaultValue
    self.__currentValue = defaultValue
    self.__editWidget = None
    self.__description = description
    self.__settingChangedFunction = settingChangedFunction 
    self.__enabled = True

  def getSection(self):
    return self.__section

  def getLabel(self):
    return self.__label

  def getType(self):
    return type(self.__defaultValue)

  def setValue(self, value):
    if not self.__enabled:
      logging.warning("Trying to set disabled setting: %s %s" %(self.__section, self.__label) )
    if type(value)!=self.getType():
      logging.warning("Set %s to different type (%s) than default Value (%s)" % (self.__label, str(self.getType()), str(type(value))))
    self.__currentValue = value

    if self.__settingChangedFunction:
      self.__settingChangedFunction()

  def getValue(self):
    return self.__currentValue

  def setEditWidget(self, widget):
    """Sets a special; widget as editwidget for this setting, which is
    shown in the preferences editor"""
    self.__editWidget = widget

  def getDefaultValue(self):
    return self.__defaultValue

  def getEditWidget(self):
    return self.__editWidget

  def getDescription(self):
    return self.__description


  def setEnabled(self, value):
    """
    If setting is not enabled, it cannot be modified
    """
    self.__enabled = value

  def isEnabled(self):
    return self.__enabled


def getVispaIni():
  global __vispaIni
  return __vispaIni


def getPreferences():
  global __preferences
  # commented warning for vispa release 0.4
  #logging.warning("Direct access to ConfigParser object will be removed at some point")
  return __preferences


def addSetting(section, label, defaultValue, description,
    settingChangedFunction = None):
  """ Adds a setting to the setting dict, and sets it to the value
  stored in the preferences file"""
  global __settings, __preferences
  if not __settings.has_key(section):
    __settings[section] = {}
  section_dict = __settings[section]
  if section_dict.has_key(label):
    logging.warning("Cannot add setting %s in section %s as I already have a Setting with this name" % (label, section))
  else:
    section_dict[label] = Setting(section,label,defaultValue,
        description, settingChangedFunction)

  if not __preferences.has_section(section):
    __preferences.add_section(section)

  if __preferences.has_option(section, label):
    #automatically convert from the string in the preferences file to
    #the type of the default value
    section_dict[label].setValue(section_dict[label].getType()(__preferences.get(section,label)))
  return __settings[section][label] 

def getSetting(section, label):
  """Returns the value of the Setting"""

  global __settings
  if not __settings.has_key(section):
    logging.warning("Section %s unknown" % (section))
    return None
  if not __settings[section].has_key(label):
    logging.warning("Setting %s unknown in section %s" % (label,section))
    return None
  return __settings[section][label].getValue()


def getSection(section):
  global __settings
  v = __settings[section].values()
  return v


def getSettings():
  global __settings
  return __settings

logging.debug(__name__+" Set directories")
setBaseDirectory(os.path.abspath(os.path.dirname(Vispa.__path__[0])))
setHomeDirectory(os.path.expanduser("~"))
setWebsiteUrl("http://vispa.physik.rwth-aachen.de")
applicationName=os.path.splitext(os.path.basename(sys.argv[0]))[0]

#Load Preferences on Startup
logging.debug(__name__ + " Loading application state from vispa.ini")
loadVispaIni()
logging.debug(__name__ + " Loading preferences from preferences.ini")
loadPreferences()

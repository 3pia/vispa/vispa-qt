import os
import sys
import string
import commands
import platform
import logging
import logging.handlers
import ConfigParser
import webbrowser
import subprocess
import locale

from PyQt4.QtCore import SIGNAL,qVersion,QString,QVariant, Qt
from PyQt4.QtGui import QApplication,QMenu,QPixmap,QAction,QFileDialog,QIcon, QFileOpenEvent
from PyQt4.QtGui import QMessageBox # TODO: remove when removing deprecated functions that are moved to GuiFacade

from Vispa.Main.Preferences import *
from Vispa.Main.MainWindow import MainWindow
from Vispa.Main.AbstractTab import AbstractTab
from Vispa.Main.Filetype import Filetype
from Vispa.Main.Exceptions import *
from Vispa.Main.AboutDialog import AboutDialog
from Vispa.Main.RotatingIcon import RotatingIcon
from PreferencesEditor import PreferencesEditor
from Vispa.Main.GuiFacade import guiFacade
from Vispa.Main.PluginManager import pluginmanager
import Vispa.__init__

import Resources


class Application(QApplication):

    MAX_RECENT_FILES = 30
    MAX_VISIBLE_RECENT_FILES = 10
    MAX_VISIBLE_UNDO_EVENTS = 10
    TAB_PREMATURELY_CLOSED_WARNING = "Tab was closed before user request could be handled."
    NO_PROCESS_EVENTS = False

    def __init__(self, argv):
        QApplication.__init__(self, argv)
        # set locale for numerics to "C" to avoid bug #950
        # See e.g. http://www.riverbankcomputing.co.uk/static/Docs/PyQt4/html/qcoreapplication.html
        locale.setlocale(locale.LC_NUMERIC,"C")
        #sys.modules['Vispa.Main.GuiFacade']=GuiFacade()
        self._version = None
        self._closeAllFlag = False
        self._knownFiltersList = []
        self._knownExtensionsDictionary = {}
        self._pluginMenus = []
        self._pluginToolBars = []
        self._recentFiles = []
        self._zoomToolBar = None
        self._undoToolBar = None
        self._messageId=0
        self._logFile = None

        self._initLogging()

        logging.debug('Running with Qt-Version ' + str(qVersion()))

        self._loadRecentFiles()

        self.setVersion(Vispa.__init__.__version__)

        self.__mainWindow = MainWindow(self, applicationName)
        self.__mainWindow.show()

        guiFacade.initApplication()
        guiFacade.setMainWindow(self.__mainWindow)

        pluginmanager.loadPlugins()
        self._collectFileExtensions()
        self._fillFileMenu()
        self._fillEditMenu()
        self._fillHelpMenu()

        guiFacade.createUndoToolBar()
        guiFacade.createZoomToolBar()
        guiFacade.hidePluginMenus()
        guiFacade.hidePluginToolBars()
        self.createStatusBar()
        self.updateMenu()

        self._connectSignals()
        self.__preferencesEditor = None
        
        if self.__mainWindow.startupScreen():
            self.__mainWindow.startupScreen().fillMenus()

    def commandLineOptions(self):
        return self._commandLineOptions

    def setVersion(self, version):
        self._version = version

    def version(self):
        """ Returns version string.
        """
        return self._version

    def atLeastQtVersion(self, versionString):
        """ Returns True if given versionString is newer than current used version of Qt.
        """
        [majorV, minorV, revisionV] = versionString.split(".")
        [majorQ, minorQ, revisionQ] = str(qVersion()).split(".")
        if majorV > majorQ:
            return True
        elif majorV < majorQ:
            return False
        elif majorV == majorQ:
            if minorV > minorQ:
                return True
            elif minorV < minorQ:
                return False
            elif minorV == minorQ:
                if revisionV > revisionQ:
                    return True
                elif revisionV < revisionQ:
                    return False
                elif revisionV == revisionQ:
                    return True
        return False



    def _checkFile(self, filename):
        """ Check if logfile is closed correctly
        """
        finished = True
        file = open(filename, "r")
        for line in file.readlines():
            if "INFO Start logging" in line:
                finished = False
            if "INFO Stop logging" in line:
                finished = True
        return finished

    def _initLogging(self):
        """ Add logging handlers for a log file as well as stderr.
        """
        instance = 0
        done = False
        while not done:
            # iterate name of log file for several instances of vispa
            instance += 1
            logfile = os.path.join(logDirectory, "log" + str(instance) + ".txt")
            # do not create more than 10 files
            if instance > 10:
                instance = 1
                logfile = os.path.join(logDirectory, "log" + str(instance) + ".txt")
                done = True
                break
            if not os.path.exists(logfile):
                done = True
                break
            done = self._checkFile(logfile)

        # clean up old logs
        nextlogfile = os.path.join(logDirectory, "log" + str(instance + 1) + ".txt")
        if os.path.exists(nextlogfile):
            if not self._checkFile(nextlogfile):
                file = open(nextlogfile, "a")
                file.write("Cleaning up logfile after abnormal termination: INFO Stop logging\n")

        if os.path.exists(logDirectory):
            handler1 = logging.handlers.RotatingFileHandler(logfile, maxBytes=100000, backupCount=1)
            formatter1 = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
            handler1.setFormatter(formatter1)
            self._logFile = logfile

        handler2 = logging.StreamHandler(sys.stderr)
        formatter2 = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
        handler2.setFormatter(formatter2)

        logging.root.handlers = []
        if os.path.exists(logDirectory):
            logging.root.addHandler(handler1)
        logging.root.addHandler(handler2)
        #logging.root.setLevel(logging.INFO)

        self._infologger = logging.getLogger("info")
        self._infologger.setLevel(logging.INFO)
        self._infologger.handlers = []
        if self._logFile:
            self._infologger.info("Start logging to " + self._logFile)


    def run(self):
        """ Show the MainWindow and run the application.
        """
        #logging.debug('Application: run()')
        exitCode = 0
        self.__mainWindow.show()
        self.__mainWindow.setStartupScreenVisible(self.__mainWindow.isVisible() and self.__mainWindow.tabWidget().count() == 0)
        exitCode = self.exec_()

        if self._logFile:
            self._infologger.info("Stop logging to " + self._logFile)

        #sys.exit(exitCode)
        return exitCode

    def _connectSignals(self):
        """ Connect signal to observe the TabWidget in the MainWindow.
        """
        logging.debug('Application: _connectSignals()')
        self.connect(self.__mainWindow.tabWidget(), SIGNAL("currentChanged(int)"), self.tabChanged)
        self.connect(self.__mainWindow, SIGNAL("windowActivated()"), self.tabChanged)
        self.connect(self.__mainWindow.tabWidget(), SIGNAL("tabCloseRequested(int)"), self.tabCloseRequest)
        self.connect(self.__mainWindow, SIGNAL("lastWindowClosed()"), self.quit)

    def tabControllers(self):
        return self.__mainWindow.tabWidgets()
#        controllers=[self.__mainWindow.tabWidget().widget(i).controller() for i in range(0, self.__mainWindow.tabWidget().count())]
#        controllers+=[tab.controller() for tab in self.__mainWindow.tabWidgets()]
#        return controllers

    def setCurrentTab(self, controller):
        self.__mainWindow.activateWindow()
        self.__mainWindow.tabWidget().setCurrentIndex(self.tabControllers().index(controller))

    def currentTab(self):
        """ Return the Tab that belongs to the tab selected in the MainWindow.
        """
        #logging.debug('Application: currentTab()')
        
        currentTab = self.__mainWindow.tabWidget().currentWidget()
        if not currentTab:
            raise NoCurrentTabException
        return currentTab

    def mainWindow(self):
        return self.__mainWindow

    def editPreferences(self):
        if not self.__preferencesEditor:
            self.__preferencesEditor = PreferencesEditor()
        self.__preferencesEditor.show()

    def _fillFileMenu(self):
        """Called for the first time this function creates the file menu and fill it.

        The function is written in a way that it recreates the whole menu, if it
        is called again later during execution. So it is possible to aad new
        plugins and use them  (which means they appear in the menus) without
        restarting the program.
        """
        logging.debug('Application: _fillFileMenu()')
        self._fileMenuItems = {}
        if not self.__mainWindow.fileMenu().isEmpty():
            self.__mainWindow.fileMenu().clear()

        # New
        newFileActions = []
        for plugin in pluginmanager.plugins():
            newFileActions += plugin.getNewFileActions()

        if len(newFileActions) == 1:
            newFileActions[0].setShortcut('Ctrl+N')

        self.__mainWindow.fileMenu().addActions(newFileActions)

        # Open
        openFileAction = guiFacade.createAction('&Open File', self.openFileDialog, 'Ctrl+O', "fileopen")
        # openFileAction needs a parent (works with mainWindow), otherwise it won't be shown in menu nor toolbar
        self.__mainWindow.fileMenu().addAction(openFileAction)
        self.__mainWindow.fileToolBar().addAction(openFileAction)

        # Reload
        self._fileMenuItems['reloadFileAction'] = guiFacade.createAction('&Reload File', self.reloadFile, ['Ctrl+R', 'F5'], "reload")
        self.__mainWindow.fileMenu().addAction(self._fileMenuItems['reloadFileAction'])
        #self.__mainWindow.fileToolBar().addAction(self._fileMenuItems['reloadFileAction'])

        # Recent files
        if not hasattr(self, 'recentFilesMenu'):
            self._recentFilesMenu = QMenu('&Recent Files', self.__mainWindow)
            self._recentFilesMenuActions = []
            for i in range(0, self.MAX_VISIBLE_RECENT_FILES):
                action = guiFacade.createAction("recent file " + str(i), self.openRecentFileSlot)
                action.setVisible(False)
                self._recentFilesMenu.addAction(action)
                self._recentFilesMenuActions.append(action)
            self._recentFilesMenu.addSeparator()
            self._fileMenuItems['clearMissingRecentFilesAction'] = guiFacade.createAction("Clear missing files", self.clearMissingRecentFiles)
            self._recentFilesMenu.addAction(self._fileMenuItems['clearMissingRecentFilesAction'])
            self._fileMenuItems['clearRecentFilesAction'] = guiFacade.createAction("Clear list", self.clearRecentFiles)
            self._recentFilesMenu.addAction(self._fileMenuItems['clearRecentFilesAction'])

        self.__mainWindow.fileMenu().addMenu(self._recentFilesMenu)

        self.__mainWindow.fileMenu().addSeparator()

        # Close
        self._fileMenuItems['closeFileAction'] = guiFacade.createAction('&Close', self.closeFile, 'Ctrl+W', "closefile")
        self.__mainWindow.fileMenu().addAction(self._fileMenuItems['closeFileAction'])

        # Close all
        self._fileMenuItems['closeAllAction'] = guiFacade.createAction('Close All', self.closeAllFiles, 'Ctrl+Shift+W', "closefileall")
        self.__mainWindow.fileMenu().addAction(self._fileMenuItems['closeAllAction'])

        self.__mainWindow.fileMenu().addSeparator()

        # Save
        self._fileMenuItems['saveFileAction'] = guiFacade.createAction('&Save', self.saveFile, 'Ctrl+S', "filesave")
        self.__mainWindow.fileMenu().addAction(self._fileMenuItems['saveFileAction'])
        self.__mainWindow.fileToolBar().addAction(self._fileMenuItems['saveFileAction'])

        # Save as
        self._fileMenuItems['saveFileAsAction'] = guiFacade.createAction('Save As...', self.saveFileAsDialog, 'Ctrl+Shift+S', image="filesaveas")
        self.__mainWindow.fileMenu().addAction(self._fileMenuItems['saveFileAsAction'])

        # Save all
        self._fileMenuItems['saveAllFilesAction'] = guiFacade.createAction('Save &All', self.saveAllFiles, "Ctrl+Alt+S", "filesaveall")
        self.__mainWindow.fileMenu().addAction(self._fileMenuItems['saveAllFilesAction'])

        self.__mainWindow.fileMenu().addSeparator()

        exit = guiFacade.createAction('&Exit', self.exit, "Ctrl+Q", "exit")      
        self.__mainWindow.fileMenu().addAction(exit)

    def _fillEditMenu(self):
        """Called for the first time this function creates the edit menu and fills it.
        
        The function is written in a way that it recreates the whole menu, if it
        is called again later during execution. So it is possible to aad new
        plugins and use them  (which means they appear in the menus) without
        restarting the program.
        """
        logging.debug('Application: _fillEditMenu()')
        self._editMenuItems = {}
        if not self.__mainWindow.editMenu().isEmpty():
            self.__mainWindow.editMenu().clear()

        # Undo / Redo
        self._editMenuItems["undoAction"] = guiFacade.createAction("Undo", self.undoEvent, "Ctrl+Z", "edit-undo")
        self._editMenuItems["redoAction"] = guiFacade.createAction("Redo", self.redoEvent, "Ctrl+Y", "edit-redo")
        self._editMenuItems["undoAction"].setData(QVariant(1))
        self._editMenuItems["redoAction"].setData(QVariant(1))
        self._editMenuItems["undoAction"].setEnabled(False)
        self._editMenuItems["redoAction"].setEnabled(False)
        self.__mainWindow.editMenu().addAction(self._editMenuItems["undoAction"])
        self.__mainWindow.editMenu().addAction(self._editMenuItems["redoAction"])
        #self._editMenuItems["undoAction"].menu().addAction(createAction("test"))
        #self._editMenuItems["undoAction"].menu().setEnabled(False)
        #self._editMenuItems["undoAction"].menu().setVisible(False)

        self._undoActionsMenu = QMenu(self.__mainWindow)
        self._undoMenuActions = []
        for i in range(0, self.MAX_VISIBLE_UNDO_EVENTS):
            action = guiFacade.createAction("undo " + str(i), self.undoEvent)
            action.setVisible(False)
            self._undoActionsMenu.addAction(action)
            self._undoMenuActions.append(action)

        self._redoActionsMenu  = QMenu(self.__mainWindow)
        self._redoMenuActions = []
        for i in range(0, self.MAX_VISIBLE_UNDO_EVENTS):
            action = guiFacade.createAction("redo " + str(i), self.redoEvent)
            action.setVisible(False)
            self._redoActionsMenu.addAction(action)
            self._redoMenuActions.append(action)

        # Cut
        self._editMenuItems['cutAction'] = guiFacade.createAction('&Cut', self.cutEvent, 'Ctrl+X', 'editcut')
        self.__mainWindow.editMenu().addAction(self._editMenuItems['cutAction'])
        self._editMenuItems['cutAction'].setEnabled(False)

        # Copy
        self._editMenuItems['copyAction'] = guiFacade.createAction('C&opy', self.copyEvent, 'Ctrl+C', 'editcopy')
        self.__mainWindow.editMenu().addAction(self._editMenuItems['copyAction'])
        self._editMenuItems['copyAction'].setEnabled(False)

        # Paste
        self._editMenuItems['pasteAction'] = guiFacade.createAction('&Paste', self.pasteEvent, 'Ctrl+V', 'editpaste')
        self.__mainWindow.editMenu().addAction(self._editMenuItems['pasteAction'])
        self._editMenuItems['pasteAction'].setEnabled(False)

        # Select all
        self._editMenuItems['selectAllAction'] = guiFacade.createAction("Select &all", self.selectAllEvent, "Ctrl+A", "selectall")
        self.__mainWindow.editMenu().addAction(self._editMenuItems['selectAllAction'])
        self._editMenuItems['selectAllAction'].setVisible(False)

        self.__mainWindow.editMenu().addSeparator()

        # Find
        self._editMenuItems['findAction'] = guiFacade.createAction('&Find', self.findEvent, 'Ctrl+F', "edit-find")
        self.__mainWindow.editMenu().addAction(self._editMenuItems['findAction'])
        self._editMenuItems['findAction'].setEnabled(False)

        self.__mainWindow.editMenu().addSeparator()
        self._editMenuItems['editPreferencesAction'] = guiFacade.createAction('Preferences',self.editPreferences,None, 'preferences-system')
        self.__mainWindow.editMenu().addAction(self._editMenuItems['editPreferencesAction'])

        # Exit
    def _fillHelpMenu(self):
        logging.debug('Application: _fillHelpMenu()')
        self._helpMenuItems = {}

        # About
        self._helpMenuItems['aboutAction'] = guiFacade.createAction('&About', self.aboutBoxSlot, 'F1')
        self.__mainWindow.helpMenu().addAction(self._helpMenuItems['aboutAction'])

        # open log file
        if self._logFile:
            self._helpMenuItems['openLogFile'] = guiFacade.createAction("Open log file", self.openLogFileSlot)
            self.__mainWindow.helpMenu().addAction(self._helpMenuItems['openLogFile'])

        # Offline Documentation
        if os.path.exists(os.path.join(docDirectory,"index.html")):
            self.__mainWindow.helpMenu().addAction(guiFacade.createAction('Offline Documentation', self._openDocumentation, "CTRL+F1"))

        # Vispa Website
        self.__mainWindow.helpMenu().addAction(guiFacade.createAction('Website', self._openWebsite, "Shift+F1"))

    def updateMenu(self):
        """ Update recent files and enable disable menu entries in file and edit menu.
        """
        logging.debug('Application: updateMenu()')
        if self.__mainWindow.startupScreen():
            self.__mainWindow.startupScreen().update()
        # Recent files
        num_recent_files = min(len(self._recentFiles), self.MAX_VISIBLE_RECENT_FILES)
        for i in range(0, num_recent_files):
            filename = self._recentFiles[i]
            self._recentFilesMenuActions[i].setText(os.path.basename(filename))
            self._recentFilesMenuActions[i].setToolTip(filename)
            self._recentFilesMenuActions[i].setStatusTip(filename)
            self._recentFilesMenuActions[i].setData(QVariant(filename))
            self._recentFilesMenuActions[i].setVisible(True)

        for i in range(num_recent_files, self.MAX_VISIBLE_RECENT_FILES):
            self._recentFilesMenuActions[i].setVisible(False)

        if num_recent_files == 0:
            self._fileMenuItems['clearRecentFilesAction'].setEnabled(False)
            self._fileMenuItems['clearMissingRecentFilesAction'].setEnabled(False)
        else:
            self._fileMenuItems['clearRecentFilesAction'].setEnabled(True)
            self._fileMenuItems['clearMissingRecentFilesAction'].setEnabled(True)

        # Enabled / disable menu entries depending on number of open files
        at_least_one_flag = False
        at_least_two_flag = False
        if len(self.tabControllers()) > 1:
            at_least_one_flag = True
            at_least_two_flag = True
        elif len(self.tabControllers()) > 0:
            at_least_one_flag = True

        self._fileMenuItems['saveFileAction'].setEnabled(at_least_one_flag)
        self._fileMenuItems['saveFileAsAction'].setEnabled(at_least_one_flag)
        self._fileMenuItems['reloadFileAction'].setEnabled(at_least_one_flag)
        self._fileMenuItems['closeFileAction'].setEnabled(at_least_one_flag)

        self._fileMenuItems['saveAllFilesAction'].setEnabled(at_least_two_flag)
        self._fileMenuItems['closeAllAction'].setEnabled(at_least_two_flag)

        try:
            if at_least_one_flag:
                if not self.currentTab().isEditable():
                    self._fileMenuItems['saveFileAction'].setEnabled(False)
                    self._fileMenuItems['saveFileAsAction'].setEnabled(False)
                if not self.currentTab().isModified():
                    self._fileMenuItems['saveFileAction'].setEnabled(False)

            # Copy / Cut / Paste
            copy_paste_enabled_flag = at_least_one_flag and self.currentTab().isCopyPasteEnabled()
            self._editMenuItems['cutAction'].setEnabled(copy_paste_enabled_flag)
            self._editMenuItems['copyAction'].setEnabled(copy_paste_enabled_flag)
            self._editMenuItems['pasteAction'].setEnabled(copy_paste_enabled_flag)

            currentTabExists = self.currentTab() != None
            self._editMenuItems['selectAllAction'].setVisible(currentTabExists and self.currentTab().allowSelectAll())

            self._editMenuItems['findAction'].setEnabled(at_least_one_flag and self.currentTab().isFindEnabled())

            # Undo / Redo
            undo_supported_flag = at_least_one_flag and (currentTabExists and self.currentTab().supportsUndo())
            self._editMenuItems["undoAction"].setEnabled(undo_supported_flag)
            self._editMenuItems["undoAction"].setVisible(undo_supported_flag)
            self._editMenuItems["redoAction"].setEnabled(undo_supported_flag)
            self._editMenuItems["redoAction"].setVisible(undo_supported_flag)
            guiFacade.showPluginToolBar(self._undoToolBar, undo_supported_flag)

            if undo_supported_flag:
                undo_events = self.currentTab().undoEvents()
                num_undo_events = min(len(undo_events), self.MAX_VISIBLE_UNDO_EVENTS)
                self._editMenuItems["undoAction"].setEnabled(num_undo_events > 0)
                if num_undo_events > 1:
                    self._editMenuItems["undoAction"].setMenu(self._undoActionsMenu)
                else:
                    self._editMenuItems["undoAction"].setMenu(None)
                for i in range(0, num_undo_events):
                    undo_event = undo_events[num_undo_events - i - 1]   # iterate backwards
                    self._undoMenuActions[i].setText(undo_event.LABEL)
                    self._undoMenuActions[i].setToolTip(undo_event.description())
                    self._undoMenuActions[i].setStatusTip(undo_event.description())
                    self._undoMenuActions[i].setData(QVariant(i+1))
                    self._undoMenuActions[i].setVisible(True)
                for i in range(num_undo_events, self.MAX_VISIBLE_UNDO_EVENTS):
                    self._undoMenuActions[i].setVisible(False)

                redo_events = self.currentTab().redoEvents()
                num_redo_events = min(len(redo_events), self.MAX_VISIBLE_UNDO_EVENTS)
                self._editMenuItems["redoAction"].setEnabled(num_redo_events > 0)
                if num_redo_events > 1:
                    self._editMenuItems["redoAction"].setMenu(self._redoActionsMenu)
                else:
                    self._editMenuItems["redoAction"].setMenu(None)
                for i in range(0, num_redo_events):
                    redo_event = redo_events[num_redo_events - i - 1]   # iterate backwards
                    self._redoMenuActions[i].setText(redo_event.LABEL)
                    self._redoMenuActions[i].setToolTip(redo_event.description())
                    self._redoMenuActions[i].setStatusTip(redo_event.description())
                    self._redoMenuActions[i].setData(QVariant(i+1))
                    self._redoMenuActions[i].setVisible(True)
                for i in range(num_redo_events, self.MAX_VISIBLE_UNDO_EVENTS):
                    self._redoMenuActions[i].setVisible(False)
                    
            # tab-specific edit menu entries
            allEditActions = self.__mainWindow.editMenu().actions()
            applicationEditActions = self._editMenuItems.values()
            tabsEditActions = self.currentTab().editMenuActions()
            for action in tabsEditActions:
                if not action in allEditActions:
                    self.__mainWindow.editMenu().addAction(action)
            for action in allEditActions:
                if not action in applicationEditActions:
                    if action in tabsEditActions:
                        action.setVisible(True)
                    else:
                        action.setVisible(False)

        except NoCurrentTabException:
            pass

    def _openDocumentation(self):
        """ Opens Vispa Offline Documentation
        """
        webbrowser.open(os.path.join(docDirectory,"index.html"), 2, True)

    def _openWebsite(self):
        """ Open new browser tab and opens Vispa Project Website.
        """
        webbrowser.open(websiteUrl, 2, True)

    def clearRecentFiles(self):
        """ Empties list of recent files and updates main menu.
        """
        self._recentFiles = []
        self._saveRecentFiles()
        self.updateMenu()

    def clearMissingRecentFiles(self):
        """ Removes entries from recent files menu if file does no longer exist.
        """
        newList = []
        for file in self._recentFiles:
            if os.path.exists(file):
                newList.append(file)
        self._recentFiles = newList
        self._saveRecentFiles()
        self.updateMenu()

    def addRecentFile(self, filename):
        """ Adds given filename to list of recent files.
        """
        logging.debug('Application: addRecentFile() - ' + filename)
        if isinstance(filename, QString):
            filename = str(filename)    # Make sure filename is a python string not a QString
        # full path of filenames
        filename = os.path.abspath(filename)
        if filename in self._recentFiles:
          self._recentFiles.remove(filename)

        self._recentFiles.insert(0, filename)
        if len(self._recentFiles) > self.MAX_RECENT_FILES:
          f = self._recentFiles.pop()
          logging.debug("Removing %s from recent file list" % f)
        self._saveRecentFiles()

    def recentFiles(self):
        """ Returns list of recently opened files.
        """
        return self._recentFiles

    def getLastOpenLocation(self):
        """ Returns directory name of first entry of recent files list.
        """
        # if current working dir is vispa directory use recentfile or home
        if os.path.abspath(os.getcwd()) in [os.path.abspath(baseDirectory),os.path.abspath(os.path.join(baseDirectory,"bin"))] or platform.system() == "Darwin":
            if len(self._recentFiles) > 0:
                return os.path.dirname(self._recentFiles[0])
            elif platform.system() == "Darwin":
                # Mac OS X
                return homeDirectory + "/Documents"
            else:
                return homeDirectory
        # if user navigated to another directory use this
        else:
            return os.getcwd()

    def recentFilesWithFiletype(self, filetype):
        files=[]
        for file in self._recentFiles:
            if os.path.splitext(os.path.basename(file))[1][1:].lower()==filetype.extension().lower():
                files+=[file]
        return files

    def exit(self, status=0):
        self.__mainWindow.close()
        
    def quit(self):
        """ debug """
        QApplication.quit(self)

    def _collectFileExtensions(self):
        """ Loop over all plugins and collect their file extensions.
        """
        self._knownExtensionsDictionary = {}
        self._knownFiltersList = []
        self._knownFiltersList.append('All files (*.*)')
        for plugin in pluginmanager.plugins():
            for ft in plugin.filetypes():
                self._knownExtensionsDictionary[ft.extension()] = plugin
                self._knownFiltersList.append(ft.fileDialogFilter())
        if len(self._knownFiltersList) > 0:
            allKnownFilter = 'All known files (*.' + " *.".join(self._knownExtensionsDictionary.keys()) + ')'
            self._knownFiltersList.insert(1, allKnownFilter)
            logging.debug('Application: _collectFileExtensions() - ' + allKnownFilter)
        else:
            logging.debug('Application: _collectFileExtensions()')


    def openFileDialog(self, defaultFileFilter=None):
        """Displays a common open dialog for all known file types.
        """
        logging.debug('Application: openFileDialog()')

        if not defaultFileFilter:
            if len(self._knownFiltersList) > 1:
                # Set defaultFileFilter to all known files
                defaultFileFilter = self._knownFiltersList[1]
            else:
                # Set dfaultFileFilter to any file type
                defaultFileFilter = self._knownFiltersList[0]

        try:
            openLocation = self.currentTab().filename()
            if type(openLocation) != str or not os.path.exists(openLocation):
                openLocation = self.getLastOpenLocation()
        except NoCurrentTabException:
            openLocation = self.getLastOpenLocation()

        # Dialog
        filename = QFileDialog.getOpenFileName(
                                               self.__mainWindow,
                                               'Select a file',
                                               openLocation,
                                               ";;".join(self._knownFiltersList),
                                               defaultFileFilter)
        if not filename.isEmpty():
            self.openFile(filename)

    def openFile(self, filename):
        """ Decides which plugin should handle opening of the given file name.
        """
        logging.debug('Application: openFile()')
        statusMessage = guiFacade.statusBarStartMessage("Opening file " + filename)
        if isinstance(filename, QString):
            filename = str(filename)  # convert QString to Python String

        # Check whether file is already opened
        for controller in self.tabControllers():
            if filename == controller.filename():
                self.setCurrentTab(controller)
                guiFacade.statusBarStopMessage(statusMessage, "already open")
                return

        baseName = os.path.basename(filename)
        ext = os.path.splitext(baseName)[1].lower().strip(".")
        errormsg = None

        if self._knownExtensionsDictionary == {}:
            self._collectFileExtensions()

        foundCorrectPlugin = False
        if os.path.exists(filename):
            if ext in self._knownExtensionsDictionary:
                foundCorrectPlugin = True
                try:
                    if self._knownExtensionsDictionary[ext].openFile(filename):
                        self.addRecentFile(filename)
                    else:
                        logging.error(self.__class__.__name__ + ": openFile() - Error while opening '" + str(filename) + "'.")
                        guiFacade.showErrorMessage("Failed to open file.")
                except Exception:
                    logging.error(self.__class__.__name__ + ": openFile() - Error while opening '" + str(filename) + "' : " + exception_traceback())
                    guiFacade.showErrorMessage("Exception while opening file. See log for details.")

            if not foundCorrectPlugin:
                errormsg = 'Unknown file type (.' + ext + '). Aborting.'
        else:
            errormsg = 'File does not exist: ' + filename

        self.updateMenu()

        # Error messages
        if not errormsg:
            guiFacade.statusBarStopMessage(statusMessage)
        else:
            logging.error(errormsg)
            guiFacade.statusBarStopMessage(statusMessage, "failed")
            guiFacade.showWarningMessage(errormsg)

    def reloadFile(self):
        """ Tells current tab to reload its file.
        """
        logging.debug('Application: reloadFile()')
        try:
            if self.currentTab().filename() and self.currentTab().allowClose():
                self.currentTab().setModified(False)
                self.currentTab().refreshFileModificationTimestamp()
                self.currentTab().reloadFile()
        except NoCurrentTabException:
            pass
        # call tabChanged instead of updateMenu to be qt 4.3 compatible
        self.tabChanged()
    
    def closeFile(self):
        """ Tells current tab controller to close.
        """
        logging.debug('Application: closeCurrentFile()')
        try:
            tab = self.currentTab()
            if tab.allowClose():
                tabWidget = self.__mainWindow.tabWidget()
                tabWidget.removeTab(tabWidget.indexOf(tab))
        except NoCurrentTabException:
            pass
        # call tabChanged instead of updateMenu to be qt 4.3 compatible
        self.tabChanged()

    def closeAllFiles(self):
        """ Closes all open tabs unless user aborts closing.
        """
        logging.debug('Application: closeAllFiles()')
        # to prevent unneeded updates set flag
        self._closeAllFlag = True
        while len(self.tabControllers())>0:
            tab=self.tabControllers()[0]
            if tab.allowClose() == True:
                tab.close()
                tabWidget = self.__mainWindow.tabWidget()
                tabWidget.removeTab(tabWidget.indexOf(tab))
            else:
                break
        self._closeAllFlag = False

        # call tabChanged instead of updateMenu to be qt 4.3 compatible
        self.tabChanged()

    def saveFile(self):
        """ Tells current tab controller to save its file.
        """
        logging.debug('Application: saveFile()')
        try:
            self.currentTab().save()
        except NoCurrentTabException:
            logging.warning(self.__class__.__name__ + ": " + self.TAB_PREMATURELY_CLOSED_WARNING)

    def saveFileAsDialog(self):
        """This functions asks the user for a file name. 
        """
        logging.debug('Application: saveFileAsDialog()')
        try:
            currentTab = self.currentTab()
        except NoCurrentTabException:
            logging.warning(self.__class__.__name__ + ": " + self.TAB_PREMATURELY_CLOSED_WARNING)
            return

        if currentTab.filename():
            startDirectory = currentTab.filename()
        else:
            startDirectory = self.getLastOpenLocation()

        filetypesList = []
        for filetype in currentTab.supportedFileTypes():
            filetypesList.append(Filetype(filetype[0], filetype[1]).fileDialogFilter())
        filetypesList.append('Any (*.*)')

        selectedFilter = QString("")
        filename = str(QFileDialog.getSaveFileName(
                                            self.__mainWindow,
                                            'Select a file',
                                            startDirectory,
                                            ";;".join(filetypesList), selectedFilter))
        if filename != "":
            # add extension if necessary
            if os.path.splitext(filename)[1].strip(".") == "" and str(selectedFilter) != 'Any (*.*)':
                ext = currentTab.supportedFileTypes()[filetypesList.index(str(selectedFilter))][0]
                filename = os.path.splitext(filename)[0] + "." + ext
            return currentTab.save(filename)
        return False

    def saveAllFiles(self):
        """ Tells tab controllers of all tabs to save.
        """
        logging.debug('Application: saveAllFiles()')

        for controller in self.tabControllers():
            if controller.filename() or controller == self.currentTab():
                controller.save()

    def cutEvent(self):
        """ Called when cut action is triggered (e.g. from menu entry) and forwards it to current tab controller.
        """
        try:
            self.currentTab().cut()
        except NoCurrentTabException:
            logging.warning(self.__class__.__name__ + ": " + self.TAB_PREMATURELY_CLOSED_WARNING)

    def copyEvent(self):
        """ Called when copy action is triggered (e.g. from menu entry) and forwards it to current tab controller.
        """
        try:
            self.currentTab().copy()
        except NoCurrentTabException:
            logging.warning(self.__class__.__name__ + ": " + self.TAB_PREMATURELY_CLOSED_WARNING)

    def pasteEvent(self):
        """ Called when paste action is triggered (e.g. from menu entry) and forwards it to current tab controller.
        """
        try:
            self.currentTab().paste()
        except NoCurrentTabException:
            logging.warning(self.__class__.__name__ + ": " + self.TAB_PREMATURELY_CLOSED_WARNING)

    def selectAllEvent(self):
        """ Called when selectAll action is triggered (e.g. from menu entry) and forwards it to current tab controller.
        """
        try:
            self.currentTab().selectAll()
        except NoCurrentTabException:
            logging.warning(self.__class__.__name__ + ": " + self.TAB_PREMATURELY_CLOSED_WARNING)

    def findEvent(self):
        """ Called when find action is triggered (e.g. from menu entry) and forwards it to current tab controller.
        """
        try:
            self.currentTab().find()
        except NoCurrentTabException:
            logging.warning(self.__class__.__name__ + ": " + self.TAB_PREMATURELY_CLOSED_WARNING)

    def zoomUserEvent(self):
        """ Handles button pressed event from zoom tool bar and forwards it to current tab controller.
        """
        try:
            self.currentTab().zoomUser()
        except NoCurrentTabException:
            logging.warning(self.__class__.__name__ + ": " + self.TAB_PREMATURELY_CLOSED_WARNING)

    def zoomHundredEvent(self):
        """ Handles button pressed event from zoom tool bar and forwards it to current tab controller.
        """
        try:
            self.currentTab().zoomHundred()
        except NoCurrentTabException:
            logging.warning(self.__class__.__name__ + ": " + self.TAB_PREMATURELY_CLOSED_WARNING)

    def zoomAllEvent(self):
        """ Handles button pressed event from zoom tool bar and forwards it to current tab controller.
        """
        try:
            self.currentTab().zoomAll()
        except NoCurrentTabException:
            logging.warning(self.__class__.__name__ + ": " + self.TAB_PREMATURELY_CLOSED_WARNING)

    def undoEvent(self):
        """ Handles undo action for buttons in undo tool bar and edit menu.
        """
        try:
            num = 1
            sender = self.sender()
            if sender:
                num = sender.data().toInt()
                if len(num) > 1:
                    # strange: toInt returns tuple like (1, True), QT 4.6.0, Mac OS X 10.6.4, 2010-06-28
                    num = num[0]
            self.currentTab().undo(num)
        except NoCurrentTabException:
            logging.warning(self.__class__.__name__ + ": "+ self.TAB_PREMATURELY_CLOSED_WARNING)

    def redoEvent(self):
        """ Handles redo action for buttons in undo tool bar and edit menu.
        """
        try:
            num = 1
            sender = self.sender()
            if sender:
                num = sender.data().toInt()
                if len(num) > 1:
                    # strange: toInt returns tuple like (1, True), QT 4.6.0, Mac OS X 10.6.4, 2010-06-28
                    num = num[0]
            self.currentTab().redo(num)
        except NoCurrentTabException:
            logging.warning(self.__class__.__name__ + ": "+ self.TAB_PREMATURELY_CLOSED_WARNING)

    def aboutBoxSlot(self):
        """ Displays about box. 
        """
        logging.debug('Application: aboutBoxSlot()')
        about = AboutDialog(self)
        about.onScreen()

    def openLogFileSlot(self):
        if self._logFile:
            self.doubleClickOnFile(self._logFile)
        else:
            logging.warning("%s: openLogFileSlot(): _logFile not set. Aborting..." % self.__class__.__name__)

    def openRecentFileSlot(self):
        """ Slot for opening recent file.
        
        Called from recent file menu action. Filename is set as data object (QVariant) of action.
        """
        filename = self.sender().data().toString()
        logging.debug('Application: openRecentFileSlot() - ' + filename)
        self.openFile(filename)

    def tabChanged(self, tab=None):
        """ when a different tab is activated update menu
        """
        logging.debug('Application: tabChanged()')
        # only update once when closing all files at once
        if not self._closeAllFlag:
            guiFacade.hidePluginMenus()
            guiFacade.hidePluginToolBars()
            guiFacade.hidePropertyValueChangedError()
            self.updateWindowTitle()
            self.updateMenu()
            try:
                self.currentTab().activated()
                self.currentTab().checkModificationTimestamp()
            except NoCurrentTabException:
                pass

        self.__mainWindow.setStartupScreenVisible(self.__mainWindow.isVisible() and self.__mainWindow.tabWidget().count() == 0)

    def windowTitle(self):
        return str(self.__mainWindow.windowTitle()).split("-")[0].strip()

    def updateWindowTitle(self):
        """ update window caption
        """
        #logging.debug('Application: updateWindowTitle()')
        name = self.windowTitle()

        try:
            filename = self.currentTab().filename()
        except NoCurrentTabException:
            filename = None

        if filename:
            dirName = os.path.dirname(sys.argv[0])
            if os.path.abspath(dirName) in filename:
                filename = filename[len(os.path.abspath(dirName)) + 1:]
            name = name + " - " + filename
        self.__mainWindow.setWindowTitle(name)

    def _loadRecentFiles(self):
        """ Save the list of recent files.
        """
        logging.debug('Application: _loadIni()')
        ini = getVispaIni()
        self._recentFiles = []
        if ini.has_section("history"):
            for i in range(0, self.MAX_RECENT_FILES):
                if ini.has_option("history", str(i)):
                    self._recentFiles+=[ini.get("history", str(i))]

    def _saveRecentFiles(self):
        """ Load the list of recent files.
        """
        logging.debug('Application: _saveIni()')
        ini = getVispaIni()
        if ini.has_section("history"):
            ini.remove_section("history")
        ini.add_section("history")
        for i in range(len(self._recentFiles)):
            ini.set("history", str(i), self._recentFiles[i])

        writeVispaIni()

    def errorMessage(self, message):
        """ Displays error message.
        """
        logging.warning("Deprecated: Application.errorMessage called, use GuiFacade instead")
        guiFacade.showErrorMessage(message)

    def warningMessage(self, message):
        """ Displays warning message.
        """
        logging.warning("Deprecated: Application.showInfoMessage called, use GuiFacade instead")
        guiFacade.showWanringMessage(message)

    def infoMessage(self, message):
        """ Displays info message.
        """
        logging.warning("Deprecated: Application.showInfoMessage called, use GuiFacade instead")
        guiFacade.showInfoMessage(message)

    def showMessageBox(self, text, informativeText="", standardButtons=QMessageBox.Ok | QMessageBox.Cancel | QMessageBox.Ignore, defaultButton=QMessageBox.Ok, extraButtons=None):
        """ Shows a standardized message box and returns the pressed button.
        
        See documentation on Qt's QMessageBox for a list of possible standard buttons.
        """
        logging.warning("Deprecated: Application.showMessageBox called, use GuiFacade instead")
        return guiFacade.showMessageBox(text, informativeText, standardButtons, defaultButton, extraButtons)

    def doubleClickOnFile(self, filename):
        """ Opens file given as argument if possible in Vispa.
        
        If Vispa cannot handle the file type the file will be opened in it's default application.
        """
        logging.debug(self.__class__.__name__ + ": doubleClickOnFile() - " + str(filename))

        if filename == "":
            return

        baseName = os.path.basename(filename)
        ext = os.path.splitext(baseName)[1].lower().strip(".")
        if self._knownExtensionsDictionary == {}:
            self._collectFileExtensions()
        if os.path.exists(filename):
            if ext in self._knownExtensionsDictionary:
                return self.openFile(filename)

        # open file in default application
        try:
          if 'Windows' in platform.system():
              os.startfile(filename)
          elif 'Darwin' in platform.system():
            if os.access(filename, os.X_OK):
              logging.warning("It seems that executing the python program is the default action on this system, which is processed when double clicking a file. Please change that to open the file witrh your favourite editor, to use this feature.")
            else:
              subprocess.call(("open", filename))
          elif 'Linux' in platform.system():
          # Linux comes with many Desktop Enviroments
            if os.access(filename, os.X_OK):
              logging.warning("It seems that executing the python program is the default action on this system, which is processed when double clicking a file. Please change that to open the file witrh your favourite editor, to use this feature.")
            else:
              try:
                  #Freedesktop Standard
                  subprocess.call(("xdg-open", filename))
              except:
                try:
                   subprocess.call(("gnome-open", filename))
                except:
                   logging.error(self.__class__.__name__ + ": doubleClickOnFile() - Platform '" + platform.platform() + "'. Cannot open file. I Don't know how!")
        except:
          logging.error(self.__class__.__name__ + ": doubleClickOnFile() - Platform '" + platform.platform() + "'. Error while opening file: " + str(filename))

    def createStatusBar(self):
        self._workingMessages = {}

        self._progressWidget = RotatingIcon(":/resources/vispabutton.png")
        self.__mainWindow.statusBar().addPermanentWidget(self._progressWidget)

    def tabCloseRequest(self, i):
        self.__mainWindow.tabWidget().setCurrentIndex(i)
        self.closeFile()

    def showStatusBarMessage(self, message, timeout=0):
        logging.warning("Deprecated: Application.showStatusBarMessage called, use GuiFacade instead")
        guiFacade.showStatusBarMessage(message, timeout)

    def cancel(self):
        """ Cancel operations in current tab.
        """
        logging.debug(__name__ + ": cancel")
        try:
            self.currentTab().cancel()
        except NoCurrentTabException:
            pass

    def event(self, event):
        if isinstance(event, QFileOpenEvent):
            logging.debug("%s: event() - QFileOpenEvent: '%s'." %  (self.__class__.__name__, event.file()))
            self.openFile(str(event.file()))
            event.accept()
            return True
        else:
            return QApplication.event(self, event)

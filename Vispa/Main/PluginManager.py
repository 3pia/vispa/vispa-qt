# PluginManager is a singleton used to create and manage plugins

import logging
import os.path

from Vispa.Main.Preferences import *
from Vispa.Main.Exceptions import *
from Vispa.Main.GuiFacade import guiFacade

class PluginManager(object):
    FAILED_LOADING_PLUGINS_ERROR = "Errors while loading plugins. For details see error output or log file.\n\nThe following plugins won't work correctly:\n\n"
    def __init__(self):
        self._plugins = []
        self._loadablePlugins = {}

    def loadPlugins(self):
        """ Search all subfolders of the plugin directory for vispa plugins and registers them.
        """
        logging.debug('Application: _loadPlugins()')
        dirs = ["Vispa.Plugins." + str(f) for f in os.listdir(pluginDirectory)
                if os.path.isdir(os.path.join(pluginDirectory, f)) and not f.startswith(".") and not f.startswith("CVS")]
        dirs += ["UserPlugins." + str(f) for f in os.listdir(userPluginDirectory)
                if os.path.isdir(os.path.join(pluginDirectory, f)) and not f.startswith(".") and not f.startswith("CVS")]

        failedToLoad = []
        for di in sorted(dirs):
            try:
                if "UserPlugins." in di:
                    module = __import__(di, globals(), locals(), "UserPlugins")
                else:
                    module = __import__(di, globals(), locals(), "Vispa.Plugins")
                self._loadablePlugins[module.plugin.__name__] = module.plugin
            except ImportError:
                logging.warning('Application: cannot load plugin ' + di + ': ' + exception_traceback())
                failedToLoad.append(di)
            except PluginIgnoredException, e:
                logging.info('Application: plugin ' + di + ' cannot be loaded and is ignored: ' + str(e))
            except AttributeError, e:
                logging.info('Application: plugin ' + di + ' is deactivated (define plugin in __init__.py to activate): ' + str(e))

        for pluginName in sorted(self._loadablePlugins.keys()):
            # loop over all loadable plugins
            # this mechanism enables plugins to call initializePlugin() for plugins they depend on
            if not self.initializePlugin(pluginName):
                failedToLoad.append(pluginName)

        if len(failedToLoad) > 0:
            guiFacade.showErrorMessage(self.FAILED_LOADING_PLUGINS_ERROR + "\n".join(failedToLoad))

    def initializePlugin(self, name):
        if name in [plugin.__class__.__name__ for plugin in self._plugins]:
            logging.info("%s: initalizePlugin(): Plugin '%s' already loaded. Aborting..." % (self.__class__.__name__, name))
            return True
        if not name in self._loadablePlugins.keys():
            logging.error("%s: initalizePlugin(): Unknown plugin '%s'. Aborting..." % (self.__class__.__name__, name))
            return False

        try:
            pluginObject = self._loadablePlugins[name]()
            self._plugins.append(pluginObject)
            logging.debug('Application: added plugin ' + name)
            return True
        except ValueError:
            logging.warning('Application: ' + name + ' is not a plugin: ' + exception_traceback())
        return False

    def plugins(self):
        return self._plugins

    def plugin(self, name):
        """ Returns plugin with given name or None if there is no such one.
        """
        if not name.endswith("Plugin"):
            name += "Plugin"

        for plugin in self._plugins:
            if name == plugin.__class__.__name__:
                return plugin
        return None

    def shutdownPlugins(self):
        logging.debug('Application: shutting down plugins')
        for plugin in self._plugins:
            plugin.shutdown()


pluginmanager = PluginManager()

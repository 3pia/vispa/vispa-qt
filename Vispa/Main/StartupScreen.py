import os.path
import logging

from PyQt4.QtCore import SIGNAL,QRect,QSize,QPoint
from PyQt4.QtGui import QToolButton,QIcon,QPixmap,QGridLayout,QLabel,QListWidget,QWidget
from PyQt4.QtSvg import QSvgRenderer, QSvgWidget

from Vispa.Gui.VispaWidget import VispaWidget

import Resources

class StartupScreen(VispaWidget):
    
    # inherited parameters
    BACKGROUND_SHAPE = 'ROUNDRECT'
    SELECTABLE_FLAG = False
    AUTOSIZE = True
    AUTOSIZE_KEEP_ASPECT_RATIO = False
    
    PROTOTYPING_DESCRIPTION = """Prototyping"""
    
    EXECUTING_DESCRIPTION = """Executing"""
    
    VERIFYING_DESCRIPTION = """Verifying"""
        
    def __init__(self, parent, application):
        self._menusFilled=False
        VispaWidget.__init__(self, parent)
        self._filenewIcon = QIcon(QPixmap(":/resources/filenew.svg"))
        self._fileopenIcon = QIcon(QPixmap(":/resources/fileopen.svg"))
        self.setImage(QSvgRenderer(":/resources/startup_development_cycle.svg"))
        self.setDragable(False)
        self.setMouseTracking(True)     # receive mouse events even if no button is pressed
        self._hideDescriptions = False
        self.__application = application

        self._prototypingActions = []
        self._executionActions = []
        self._verifyingActions = []
        self._executionFiletypes = []
        self._verifyingFiletypes = []

    def fillMenus(self):
        self._menusFilled=True
        self._descriptionWidgets = []
        self._descriptionActiveRects = [QRect(), QRect(), QRect()]   # descriptions will be visible if mouse cursor is in the rect
        self.createPrototypingWidget()
        self.createExecutionWidget()
        self.createVerifyingWidget()
        self.rearangeDescriptionWidgets()
        
    def createDescriptionWidget(self, arrowDirection, description):
        widget = VispaWidget(self.parent())
        widget.enableAutosizing(True, False)
        widget.setSelectable(False)
        widget.setArrowShape(arrowDirection)
        widget.setVisible(not self._hideDescriptions)
        widget.setDragable(False)
        self._descriptionWidgets.append(widget)
        return widget
    
    def createPrototypingWidget(self):
        self._prototypingDescriptionWidget = self.createDescriptionWidget(VispaWidget.ARROW_SHAPE_BOTTOM, self.PROTOTYPING_DESCRIPTION)
        
        bodyWidget = QWidget(self._prototypingDescriptionWidget)
        bodyWidget.setLayout(QGridLayout())
        bodyWidget.layout().setContentsMargins(0, 0, 0, 0)
        
        countEntries=0
        for action in self._prototypingActions:
            label=QLabel(action.text().replace("&",""))
            bodyWidget.layout().addWidget(label, countEntries, 0)
            toolButton=QToolButton()
            toolButton.setDefaultAction(action)
            bodyWidget.layout().addWidget(toolButton, countEntries, 1)
            countEntries+=1
    
        self._prototypingDescriptionWidget.setBodyWidget(bodyWidget)
        
    def createExecutionWidget(self):
        self._executionDescriptionWidget = self.createDescriptionWidget(VispaWidget.ARROW_SHAPE_RIGHT, self.EXECUTING_DESCRIPTION)
        
        bodyWidget = QWidget(self._executionDescriptionWidget)
        bodyWidget.setLayout(QGridLayout())
        bodyWidget.layout().setContentsMargins(0, 0, 0, 0)
        
        countEntries=0
        width=50
        for action in self._executionActions:
            label=QLabel(action.text().replace("&",""))
            bodyWidget.layout().addWidget(label, countEntries, 0)
            toolButton=QToolButton()
            toolButton.setDefaultAction(action)
            bodyWidget.layout().addWidget(toolButton, countEntries, 1)
            width=max(width,label.sizeHint().width()+toolButton.sizeHint().width())
            countEntries+=1
        label=QLabel("Open and run existing analysis:")
        bodyWidget.layout().addWidget(label, countEntries, 0)
        toolButton = QToolButton()
        toolButton.setText("Open analysis file")
        toolButton.setIcon(self._fileopenIcon)
        self.connect(toolButton, SIGNAL("clicked(bool)"), self.openExecutionFileSlot)
        bodyWidget.layout().addWidget(toolButton, countEntries, 1)
        width=max(width,label.sizeHint().width()+toolButton.sizeHint().width())
        countEntries+=1
        self._executionRecentFilesList=QListWidget()
        self._executionRecentFilesList.setFixedSize(width,150)
        self.connect(self._executionRecentFilesList, SIGNAL("doubleClicked(QModelIndex)"), self.openExecutionFileSlot)
        bodyWidget.layout().addWidget(self._executionRecentFilesList, countEntries, 0, 1, 2)
        
        self._executionDescriptionWidget.setBodyWidget(bodyWidget)

    def createVerifyingWidget(self):
        self._verifyingDescriptionWidget = self.createDescriptionWidget(VispaWidget.ARROW_SHAPE_LEFT, self.VERIFYING_DESCRIPTION)
        
        bodyWidget = QWidget(self._verifyingDescriptionWidget)
        bodyWidget.setLayout(QGridLayout())
        bodyWidget.layout().setContentsMargins(0, 0, 0, 0)
        
        countEntries=0
        width=50
        for action in self._verifyingActions:
            label=QLabel(action.text().replace("&",""))
            bodyWidget.layout().addWidget(label, countEntries, 0)
            toolButton=QToolButton()
            toolButton.setDefaultAction(action)
            bodyWidget.layout().addWidget(toolButton, countEntries, 1)
            width=max(width,label.sizeHint().width()+toolButton.sizeHint().width())
            countEntries+=1
        label=QLabel("Browse an existing data file:")
        bodyWidget.layout().addWidget(label, countEntries, 0)
        toolButton = QToolButton()
        toolButton.setText("Open data file")
        toolButton.setIcon(self._fileopenIcon)
        self.connect(toolButton, SIGNAL("clicked(bool)"), self.openVerifyingFileSlot)
        bodyWidget.layout().addWidget(toolButton, countEntries, 1)
        width=max(width,label.sizeHint().width()+toolButton.sizeHint().width())
        countEntries+=1
        self._verifyingRecentFilesList=QListWidget()
        self._verifyingRecentFilesList.setFixedSize(width,150)
        self.connect(self._verifyingRecentFilesList, SIGNAL("doubleClicked(QModelIndex)"), self.openVerifyingFileSlot)
        bodyWidget.layout().addWidget(self._verifyingRecentFilesList, countEntries, 0, 1, 2)
        
        self._verifyingDescriptionWidget.setBodyWidget(bodyWidget)
        
    def mouseMoveEvent(self, event):
        if bool(event.buttons()):
            VispaWidget.mouseMoveEvent(self, event)
        elif self._hideDescriptions:
            for i in range(len(self._descriptionWidgets)):
                self._descriptionWidgets[i].setVisible(self._descriptionActiveRects[i].contains(event.pos()))
                
    def moveEvent(self, event):
        VispaWidget.moveEvent(self, event)
        self.rearangeDescriptionWidgets()
        
    def rearangeContent(self):
        VispaWidget.rearangeContent(self)
        self.rearangeDescriptionWidgets()
        
    def rearangeDescriptionWidgets(self):
        if not self._menusFilled: return
        self._activeSize = QSize(0.3 * self.width(), 0.1 * self.height())
        self._prototypingRect = QRect(QPoint(0.5 * (self.width() - self._activeSize.width()), 0), self._activeSize)
        self._executionRect = QRect(QPoint(0, 0.635 * self.height()), self._activeSize)
        self._verifyingRect = QRect(QPoint(self.width() -self._activeSize.width(), 0.635 * self.height()), self._activeSize)
        self._descriptionActiveRects[0] = self._prototypingRect
        self._descriptionActiveRects[1] = self._executionRect 
        self._descriptionActiveRects[2] = self._verifyingRect
        
        self._prototypingDescriptionWidget.move(self.mapToParent(self._prototypingRect.topLeft()) + QPoint((self._prototypingRect.width() - self._prototypingDescriptionWidget.width()) * 0.5, - self._prototypingDescriptionWidget.height()))
        self._executionDescriptionWidget.move(self.mapToParent(self._executionRect.topLeft()) - QPoint(self._executionDescriptionWidget.width(), - 0.5 * (self._executionRect.height() - self._executionDescriptionWidget.height())))
        self._verifyingDescriptionWidget.move(self.mapToParent(self._verifyingRect.topRight()) - QPoint(0, - 0.5 * (self._verifyingRect.height() - self._verifyingDescriptionWidget.height())))
        
    def boundingRect(self):
        br = VispaWidget.boundingRect(self)
        if not self._menusFilled: return br
        for w in self._descriptionWidgets:
            br = br.united(w.boundingRect())
        return br

    def setVisible(self, visible):
        VispaWidget.setVisible(self, visible)
        if not self._menusFilled: return
        self._executionDescriptionWidget.setVisible(visible and not self._hideDescriptions)
        self._prototypingDescriptionWidget.setVisible(visible and not self._hideDescriptions)
        self._verifyingDescriptionWidget.setVisible(visible and not self._hideDescriptions)

    def update(self):
        if not self._menusFilled: return
        self._executionRecentFilesList.clear()
        self._executionRecentFilesList.addItem("...")
        self._executionRecentFilesList.setCurrentRow(0)
        for filetype in self._executionFiletypes:
            files = self.__application.recentFilesWithFiletype(filetype)
            for file in files:
                self._executionRecentFilesList.addItem(os.path.basename(file))

        self._verifyingRecentFilesList.clear()
        self._verifyingRecentFilesList.addItem("...")
        self._verifyingRecentFilesList.setCurrentRow(0)
        for filetype in self._verifyingFiletypes:
            files = self.__application.recentFilesWithFiletype(filetype)
            for file in files:
                self._verifyingRecentFilesList.addItem(os.path.basename(file))

    def openExecutionFileSlot(self, checked=False):
        currentRow=self._executionRecentFilesList.currentRow()
        if currentRow!=0:
            row=0
            for filetype in self._executionFiletypes:
               files = self.__application.recentFilesWithFiletype(filetype)
               for file in files:
                   row+=1
                   if currentRow==row:
                       self.__application.openFile(file)
        else:
            if len(self._executionFiletypes)>0:
                self.__application.openFileDialog(self._executionFiletypes[0].fileDialogFilter())
        
    def openVerifyingFileSlot(self, checked=False):
        currentRow=self._verifyingRecentFilesList.currentRow()
        if currentRow!=0:
            row=0
            for filetype in self._verifyingFiletypes:
               files = self.__application.recentFilesWithFiletype(filetype)
               for file in files:
                   row+=1
                   if currentRow==row:
                       self.__application.openFile(file)
        else:
            if len(self._verifyingFiletypes)>0:
                self.__application.openFileDialog(self._verifyingFiletypes[0].fileDialogFilter())

    def addPrototypingAction(self, action):
        self._prototypingActions += [action]

    def addExecutionAction(self, action):
        self._executionActions += [action]

    def addVerifyingAction(self, action):
        self._verifyingActions += [action]

    def addExecutionFiletype(self, Filetype):
        self._executionFiletypes += [Filetype]

    def addVerifyingFiletype(self, Filetype):
        self._verifyingFiletypes += [Filetype]

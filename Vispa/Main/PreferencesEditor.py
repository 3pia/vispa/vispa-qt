#This is a very rudimentary editor, which has to be beautyfied in
#the future

import Preferences
import logging
from PyQt4 import QtGui, QtCore

class SettingWidget(QtGui.QWidget):
  """ Abstract base class to implement edit widgets for settings"""
  def __init__(self, setting, parent = None):
    QtGui.QWidget.__init__(self,parent)
    self._setting = setting
    self.setToolTip("<FONT COLOR=black>" + setting.getDescription() + "</FONT>" )
  def apply(self):
    """Apply the contents of the widget to the setting"""
    raise NotImplementedError
  def showDefault(self):
    """Show the default value of the setting in the widget"""
    raise NotImplementedError


class GenericStringSettingWidget(SettingWidget):
  def __init__(self, setting, parent = None):
    SettingWidget.__init__(self, setting, parent)
    self.__lineEdit = QtGui.QLineEdit(self)
    self.__lineEdit.setText(self._setting.getValue())
    layout = QtGui.QHBoxLayout()
    layout.addWidget(self.__lineEdit)
    self.setLayout(layout)
    self.__lineEdit.setReadOnly(not setting.isEnabled())
  def apply(self):
    self._setting.setValue(str(self.__lineEdit.text()))
  def showDefault(self):
    self.__lineEdit.setText(self._setting.getDefaultValue())

class GenericBoolSettingWidget(SettingWidget):
  def __init__(self, setting, parent = None):
    SettingWidget.__init__(self, setting, parent)
    self.__checkBox= QtGui.QCheckBox(self)
    self.__checkBox.setChecked(setting.getValue())
    layout = QtGui.QHBoxLayout()
    layout.addWidget(self.__checkBox)
    self.setLayout(layout)
    self.__checkBox.setCheckable(setting.isEnabled())
  def apply(self):
    self._setting.setValue((self.__checkBox.isChecked()))
  def showDefault(self):
    self.__checkBox.setChecked(self._setting.getDefaultValue())

class GenericIntSettingWidget(SettingWidget):
  def __init__(self, setting, parent = None):
    SettingWidget.__init__(self, setting, parent)
    self.__spinBox= QtGui.QSpinBox(self)
    self.__spinBox.setValue(setting.getValue())
    layout = QtGui.QHBoxLayout()
    layout.addWidget(self.__spinBox)
    self.setLayout(layout)
    self.__spinBox.setReadOnly(not setting.isEnabled())
    
  def apply(self):
    self._setting.setValue((self.__spinBox.value()))
  def showDefault(self):
    self.__spinBox.setValue(self._setting.getDefaultValue())







class PreferencesEditor(QtGui.QDialog):
  """ Editor for the preferences"""
  def __init__(self, parent = None):
    QtGui.QDialog.__init__(self, parent)
    self.setMinimumSize(640,480)
    self.setModal(True)
    self.setWindowTitle('VISPA Preferences Editor')

    layout = QtGui.QGridLayout(self)
    self.__toolbox = QtGui.QToolBox(self)
    layout.addWidget(self.__toolbox,1,1,QtCore.Qt.AlignTop)

    self.__settingWidgets = []
    for section,settings in Preferences.getSettings().iteritems():
      #layout.addRow(QtGui.QLabel(section))
      W = QtGui.QWidget(self.__toolbox)
      localLayout = QtGui.QFormLayout()
      enabled = True
      for label,setting in settings.iteritems():
        S = setting.getEditWidget()
        # set default editors if S is not defined
        if not S:
          if setting.getType() == str:
            S = GenericStringSettingWidget(setting)
          elif setting.getType() == bool:
            S = GenericBoolSettingWidget(setting)
          elif setting.getType() == int:
            S = GenericIntSettingWidget(setting)
          else:
            logging.error(__name__ + "Do not know how to edit type %s for %s " % (str(setting.getType()), label))
            continue
        enabled = enabled and setting.isEnabled()
        localLayout.addRow(label,S)
        self.__settingWidgets.append(S)
      W.setLayout(localLayout)
      i = self.__toolbox.addItem(W, section)
      self.__toolbox.setItemEnabled(i, enabled)

    self.__buttonBox = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok | QtGui.QDialogButtonBox.Cancel | QtGui.QDialogButtonBox.RestoreDefaults)
    layout.addWidget(self.__buttonBox, 1,1,QtCore.Qt.AlignBottom)

    self.connect(self.__buttonBox, QtCore.SIGNAL("clicked(QAbstractButton*)"), self.__buttonClicked)

    
  def __buttonClicked(self, button):
    if button == self.__buttonBox.button(QtGui.QDialogButtonBox.Cancel):
      logging.debug(__name__ + " Cancel Action")
      self.close()
    elif button == self.__buttonBox.button(QtGui.QDialogButtonBox.Ok):
      logging.debug(__name__ + " Ok Action")
      self.__applyChanges()
      self.close()
      Preferences.writePreferences()
    elif button == self.__buttonBox.button(QtGui.QDialogButtonBox.RestoreDefaults):
      self.__restoreDefaults()
    else:
      logging.error(__name__ + "Unknown Button pressed")


  def __restoreDefaults(self):
    """Restores the defautl values, but only in the view"""
    logging.debug(__name__ + " Restore Defaults ")
    for S in self.__settingWidgets:
      S.showDefault()


  def __applyChanges(self):
    """Apply the settings"""
    logging.debug(__name__ + " Apply Settings")

    for S in self.__settingWidgets:
      S.apply()

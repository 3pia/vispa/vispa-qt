import logging

from PyQt4.QtCore import QObject
from PyQt4 import QtCore

from Vispa.Main.Filetype import Filetype
from Vispa.Main.GuiFacade import guiFacade



class VispaPlugin(QtCore.QObject):
    """Interface for all VispaPlugins"""

    def __init__(self):
        QtCore.QObject.__init__(self)

        self._createNewFileActions = []
        self._filetypes = []

    def application(self):
        """ Returns application object.
        """
        logging.warning("Maybe deprecated::VispaPlugin.__application__ called, use GuiFacade instead if possible")
        return QtCore.QCoreApplication.instance()

    def registerFiletype(self, ext, description):
        """ Registers Filetype object for given extension with description.
        
        Description will be shown in open and save dialogs.
        """
        self._filetypes.append(Filetype(ext,description))

    def filetypes(self):
        """ Returns local list of Filetype objects.
        """
        return self._filetypes

    def registerFiletypesFromTab(self, TabClass):
        """Adds supported file types from TabClass.
        
        Evaluates the static function staticSupportedFileTypes() of class TabClass."""
        for (ext, description) in TabClass.staticSupportedFileTypes():
            self.registerFiletype(ext, description)

    def openFile(self, filename):
        """This function has to be implemented by each plugin which can open files.
        
        On success it should return True
        """
        logging.warning('VispaPlugin: openFile() method not implemented by '+ self.__class__.__name__ +'.')
        guiFacade.showStatusBarMessage('Opening of desired file type not implemented.', 10000)
        return False

    def addNewFileAction(self, label, slot=None):
        """Creates a new file action with label and optionally with a callable slot set and appends it to local new file actions list. """
        action = guiFacade.createAction(label, slot,image='filenew')
        self._createNewFileActions.append(action)
        return action

    def getNewFileActions(self):
        """ Returns local list of new file actions.
        """
        return self._createNewFileActions

    def shutdown(self):
        """ Cleanup actions for the plugins
        """
        pass

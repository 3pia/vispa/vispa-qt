# GuiFacade is a singleton used as facade providing access to the
# Application and the MainWindow

import logging
import inspect
from PyQt4 import QtCore
from PyQt4 import QtGui

from PyQt4.QtGui import QMenu, QPixmap, QIcon, QAction, QMessageBox, QColor
from PyQt4.QtCore import SIGNAL, QObject, Qt, QPoint, QSize

from Vispa.Gui.VispaWidget import VispaWidget


class GuiFacade:
    def __init__(self):
        self.__application = None
        self.__mainWindow = None
        self._propertyValueChangedErrorWidget = None

    def initApplication(self):
        self.__application = QtCore.QCoreApplication.instance()

    def setMainWindow(self, window):
        self.__mainWindow = window 

    @classmethod
    def getIconFromImageName(cls, imagename):
      """
      Returns an icon with a pixmap fromm the resources with name
      imagename
      """
      image0 = QPixmap()
      image0.load(":/resources/" + imagename + ".svg")
      return QIcon(image0)


    def createAction(self, name, slot=None, shortcut=None, image=None, enabled=True):
        """ create an action with name and icon and connect it to a slot.
        """
        #logging.debug('Application: createAction() - ' + name)
        if image:
            icon = self.getIconFromImageName(image)
            action = QAction(icon, name, self.__mainWindow)
        else:
            action = QAction(name, self.__mainWindow)
        action.setEnabled(enabled)
        if slot:
            QObject.connect(action, SIGNAL("triggered()"), slot)
        if shortcut:
            if isinstance(shortcut, list):
                action.setShortcuts(shortcut)
            else:
                action.setShortcut(shortcut)
        return action


    def createPluginToolBar(self, name):
        """ Creates tool bar in main window and adds it to _pluginToolBars list.
        """
        toolBar = self.__mainWindow.addToolBar(name)
        self.__application._pluginToolBars.append(toolBar)
        return toolBar

    def showPluginToolBar(self, toolBarObject, show=True):
        """ Shows given toolbar if it is in _pluginToolBars list.
        """
        if toolBarObject in self.__application._pluginToolBars:
            toolBarObject.setVisible(show)

    def hidePluginToolBars(self):
        """ Hides all toolbars in _toolBarMenus list.
        """
        for toolBar in self.__application._pluginToolBars:
            toolBar.hide()

    def statusBarStartMessage(self, message=""):
        if len(self.__application._workingMessages.keys()) == 0:
            self.__application._progressWidget.start()
        self.__mainWindow.statusBar().showMessage(message + "...")
        self.__application._messageId += 1
        self.__application._workingMessages[self.__application._messageId] = message
        self.__application._progressWidget.setToolTip("<FONT COLOR=black>" + message + "</FONT>" )
        return self.__application._messageId

    def statusBarStopMessage(self, id, postfix="done"):
        if not id in self.__application._workingMessages.keys():
            logging.error(self.__application.__class__.__name__ + ": statusBarStopMessage() - Unknown id %s. Aborting..." % str(id))
            return
        if len(self.__application._workingMessages.keys()) > 1:
            self.__mainWindow.statusBar().showMessage(self.__application._workingMessages[self.__application._workingMessages.keys()[0]] + "...")
            self.__application._progressWidget.setToolTip("<FONT COLOR=black>" + self.__application._workingMessages[self.__application._workingMessages.keys()[0]] + "</FONT>" )
        else:
            self.__application._progressWidget.stop()
            self.__application._progressWidget.setToolTip("")
            self.__mainWindow.statusBar().showMessage(self.__application._workingMessages[id] + "... " + postfix + ".")
        del self.__application._workingMessages[id]

    def addTab(self, tab):
        self.__mainWindow.addTab(tab)

    def currentTab(self):
        return self.__application.currentTab()

    def createPluginMenu(self, name):
        """ Creates menu in main window's menu bar before help menu and adds it to _pluginMenus list.
        """
        menu = QMenu(name)
        self.__mainWindow.menuBar().insertMenu(self.__mainWindow.helpMenu().menuAction(), menu)
        self.__application._pluginMenus.append(menu)
        return menu

    def showPluginMenu(self, menuObject, show=True):
        """ Shows given menu if it is in _pluginMenus list.
        """
        if menuObject in self.__application._pluginMenus:
            # show all actions and activate their shortcuts
            if show:
                for action in menuObject.actions():
                    if hasattr(action, "_wasVisible") and action._wasVisible != None:
                        action.setVisible(action._wasVisible)
                        action._wasVisible = None
                    else:
                        action.setVisible(True)     # has to be after actions() loop to prevent permanant invisibility on Mac OS X
            menuObject.menuAction().setVisible(show)

    def hidePluginMenu(self, menuObject):
        """ Hides given menu object if it it in _pluginMenus list.
        """
        self.__application.showPluginMenu(menuObject, False)

    def hidePluginMenus(self):
        """ Hides all menus in _pluginMenus list.
        """
        for menuObject in self.__application._pluginMenus:
            # hide all actions and deactivate their shortcuts
            menuObject.menuAction().setVisible(False)
            for action in menuObject.actions():
                if not hasattr(action, "_wasVisible") or action._wasVisible == None:
                    action._wasVisible = action.isVisible()
                action.setVisible(False)    # setVisible() here hides plugin menu forever on Mac OS X (10.5.7), Qt 4.5.

    def createZoomToolBar(self):
        """ Creates tool bar with three buttons "user", "100 %" and "all".
        
        See Tab's documentation of zoomUser(), zoomHundred() and zoomAll() to find out more on the different zoom levels.
        """
        self.__application._zoomToolBar = self.createPluginToolBar('Zoom ToolBar')
        self.__application._zoomToolBar.addAction(self.createAction('Revert Zoom', self.__application.zoomUserEvent, image='zoomuser'))
        self.__application._zoomToolBar.addAction(self.createAction('Zoom to 100 %', self.__application.zoomHundredEvent, image='zoom100'))
        self.__application._zoomToolBar.addAction(self.createAction('Zoom to all', self.__application.zoomAllEvent, image='zoomall'))

    def showZoomToolBar(self):
        """ Makes zoom tool bar visible.
        
        Should be called from Tab's selected() function, if the controller wants to use the tool bar.
        """
        self.showPluginToolBar(self.__application._zoomToolBar)

    def hideZoomToolBar(self):
        """ Makes zoom tool bar invisible.
        """
        self.__application._zoomToolBar.hide()

    def createUndoToolBar(self):
        """ Creates tool bar with buttons to invoke undo and redo events.
        
        Needs to be called after _fillEditMenu() as actions are defined there.
        """
        self.__application._undoToolBar = self.createPluginToolBar("Undo ToolBar")
        self.__application._undoToolBar.addAction(self.__application._editMenuItems["undoAction"])
        self.__application._undoToolBar.addAction(self.__application._editMenuItems["redoAction"])

    def showUndoToolBar(self):
        """ Makes undo tool bar visible.
        """
        self.showPluginToolBar(self._undoToolBar)

    def hideUndoToolBar(self):
        """ Hides undo tool bar.
        """
        self.__application._undoToolBar.hide()

    def showStatusBarMessage(self, message, timeout=0):
        self.__mainWindow.statusBar().showMessage(message, timeout)

    def showErrorMessage(self, message):
        """ Displays error message.
        """
        QMessageBox.critical(self.__mainWindow, 'Error', message)

    def showWarningMessage(self, message):
        """ Displays warning message.
        """
        QMessageBox.warning(self.__mainWindow, 'Warning', message)

    def showInfoMessage(self, message):
        """ Displays info message.
        """
        QMessageBox.about(self.__mainWindow, 'Info', message)
        
        
    def showMessageBox(self, text, informativeText="", standardButtons=QMessageBox.Ok | QMessageBox.Cancel | QMessageBox.Ignore, defaultButton=QMessageBox.Ok, extraButtons=None):
        """ Shows a standardized message box and returns the pressed button.
        
        See documentation on Qt's QMessageBox for a list of possible standard buttons.
        """

        msgBox = QMessageBox(self.__mainWindow)
        msgBox.setParent(self.__mainWindow, Qt.Sheet)     # Qt.Sheet: Indicates that the widget is a Macintosh sheet.
        msgBox.setText(text)
        msgBox.setInformativeText(informativeText)
        msgBox.setStandardButtons(standardButtons)
        if extraButtons!=None:
            for button,role in extraButtons:
                msgBox.addButton(button,role)
        msgBox.setDefaultButton(defaultButton)
        return msgBox.exec_()

    def doubleClickOnFile(self, filename):
        self.__application.doubleClickOnFile(filename)

    def showDialog(self,dialog,*args):
        """ Show the dialog as a child of the MainWindow.
        """
        if inspect.isclass(dialog):
            d=dialog(self.__mainWindow, *args)
        else:
            d=dialog
        d.show()
        d.raise_()
        #d.activateWindow()
        d.setFocus()
        return d

    def showFileOpenDialog(self, filename="", filter=QtCore.QString(), selectedFilter=None, options=QtGui.QFileDialog.Options()):
        caption = "Select file"
        if filename=="":
            filename = self.__application.getLastOpenLocation()
        return str(QtGui.QFileDialog.getOpenFileName(self.__mainWindow, caption, filename, filter, selectedFilter, options))

    def showDirectoryOpenDialog(self, filename="", options=QtGui.QFileDialog.Options()):
        caption = "Select directory"
        if filename=="":
            filename = self.__application.getLastOpenLocation()
        return str(QtGui.QFileDialog.getExistingDirectory(self.__mainWindow, caption, filename, options))

    def showFilesOpenDialog(self, filename="", filter=QtCore.QString(), selectedFilter=None, options=QtGui.QFileDialog.Options()):
        caption = "Select list of files"
        if filename=="":
            filename = self.__application.getLastOpenLocation()
        return [str(f) for f in QtGui.QFileDialog.getOpenFileNames(self.__mainWindow, caption, filename, filter, selectedFilter, options)]

    def showFileSaveDialog(self, filename="", filter=QtCore.QString(), selectedFilter=None, options=QtGui.QFileDialog.Options()):
        caption = "Select file"
        if filename=="":
            filename = self.__application.getLastOpenLocation()
        return str(QtGui.QFileDialog.getSaveFileName(self.__mainWindow, caption, filename, filter, selectedFilter, options))

    def showFilesSaveDialog(self, filename="", filter=QtCore.QString(), selectedFilter=None, options=QtGui.QFileDialog.Options()):
        caption = "Select list of files"
        if filename=="":
            filename = self.__application.getLastOpenLocation()
        # QFileDialog does not have getSaveFileNames. Use getOpenFileNames instead.
        return [str(f) for f in QtGui.QFileDialog.getOpenFileNames(self.__mainWindow, caption, filename, filter, selectedFilter, options)]

    def showPropertyValueChangedError(self, message=None, pos=None):
        """" Shows error message as vispa widget pointing at the corresponding row of the PropertyView.
        
        If both message and pos are None, the previous error message is shown again, if there is such a message.
        """
        
        parent = self.__mainWindow
        if not self._propertyValueChangedErrorWidget:
            self._propertyValueChangedErrorWidget = VispaWidget(parent)
            self._propertyValueChangedErrorWidget.setShape("ROUNDRECT")
            self._propertyValueChangedErrorWidget.setArrowShape(VispaWidget.ARROW_SHAPE_RIGHT)
            self._propertyValueChangedErrorWidget.setDragable(False)
            pen_color = QColor(255, 255, 255)
            fill_color1 = QColor(200, 0, 0)
            fill_color2 = QColor(100, 0, 0)
            self._propertyValueChangedErrorWidget.setColors(pen_color, fill_color1, fill_color2)
            self._propertyValueChangedErrorWidget.enableAutosizing(True, False)
            self._propertyValueChangedErrorWidget.enableBackgroundGradient(False)
            self._propertyValueChangedErrorWidget.setMaximumSize(QSize(600, 500))
        elif message == None and pos == None:
            # show previous error message
            self._propertyValueChangedErrorWidget.show()
            self._propertyValueChangedErrorWidget.raise_()
            return
        
        self._propertyValueChangedErrorWidget.setText(message)
        posOffset = QPoint(self._propertyValueChangedErrorWidget.width(), -0.5 * self._propertyValueChangedErrorWidget.height())
        self._propertyValueChangedErrorWidget.move( parent.mapFromGlobal(pos - posOffset))
        self._propertyValueChangedErrorWidget.show()
        self._propertyValueChangedErrorWidget.raise_()
        
    def hidePropertyValueChangedError(self):
        if self._propertyValueChangedErrorWidget:
            self._propertyValueChangedErrorWidget.hide()
            
    def clipboard(self):
        return self.__application.clipboard()

    def createStartupScreenEntry(self, prototypingActions=None, executionActions=None, verifyingActions=None, executionFiletypes=None, verifyingFiletypes=None):
        if self.__mainWindow.startupScreen():
            if prototypingActions:
                for action in prototypingActions:
                    self.__mainWindow.startupScreen().addPrototypingAction(action)
            if executionActions:
                for action in executionActions:
                    self.__mainWindow.startupScreen().addExecutionAction(action)
            if verifyingActions:
                for action in verifyingActions:
                    self.__mainWindow.startupScreen().addVerifyingAction(action)
            if executionFiletypes:
                for Filetype in executionFiletypes:
                    self.__mainWindow.startupScreen().addExecutionFiletype(Filetype)
            if verifyingFiletypes:
                for Filetype in verifyingFiletypes:
                    self.__mainWindow.startupScreen().addVerifyingFiletype(Filetype)

    def setPrintCommandLineHelpMessageFlag(self, enabled=True):
        self.__application.setPrintCommandLineHelpMessageFlag(enabled)

        
guiFacade = GuiFacade()


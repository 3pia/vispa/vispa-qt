import logging

#from PyQt4.QtCore import *
#from PyQt4.QtGui import *
from PyQt4 import QtGui, QtCore

from Vispa.Views.AstroObjectView import AstroObjectView
from Vispa.Main.Exceptions import exception_traceback
from Vispa.Share.BasicDataAccessor import BasicDataAccessor

import pxl.core, pxl.astro, pxl.hep, pxl.healpix

try:
    import matplotlib
    from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
    from matplotlib.backends.backend_qt4agg import NavigationToolbar2QTAgg as NavigationToolbar
    from matplotlib.figure import Figure
    from scipy import *
    from numpy import *
except ImportError, e:
    raise RuntimeError(e)

class SkyplotView(AstroObjectView):

    LABEL = "&Skyplot View"

    def __init__(self, parent=None):
        AstroObjectView.__init__(self, parent)
        self._crCollection = None
        self._sourceMarker = None
        #self.createFigure()


    def drawAxes(self):
        ''' Draws the axes for plotting the objects. Here we create 
        two subplots for cosmic rays and sources
        '''
        self.axes1 = self.fig.add_subplot(211, projection='hammer')
        self.axes1.clear()

        self.axes1.set_xticks([-pi, -3*pi/4, -pi/2., -pi/4, 0., 
                              pi/4, pi/2., 3*pi/4, pi])
        self.axes1.set_xticklabels([r'-$\pi$', '','','',
                                   '0','','','', r'-$\pi$'])
        self.axes1.set_yticks([-pi/2,-3*pi/8, -pi/4, -pi/8, 0, 
                              pi/8, pi/4, 3*pi/8, pi/2])
        self.axes1.set_yticklabels([r'$-\pi/2$', '',r'$-\pi/4$','',
                                   r'$0$','',r'$\pi/4$', '', 
                                   r'$\pi/2$'])
        self.axes1.grid()
        self.axes1.set_title("Cosmic Rays",
                            horizontalalignment='left')

        self.axes2 = self.fig.add_subplot(212, projection='hammer')
        self.axes2.clear()

        self.axes2.set_xticks([-pi, -3*pi/4, -pi/2., -pi/4, 0., 
                              pi/4, pi/2., 3*pi/4, pi])
        self.axes2.set_xticklabels([r'-$\pi$', '','','',
                                   '0','','','', r'-$\pi$'])
        self.axes2.set_yticks([-pi/2,-3*pi/8, -pi/4, -pi/8, 0, 
                              pi/8, pi/4, 3*pi/8, pi/2])
        self.axes2.set_yticklabels([r'$-\pi/2$', '',r'$-\pi/4$','',
                                   r'$0$','',r'$\pi/4$', '', 
                                   r'$\pi/2$'])
        self.axes2.grid()
        self.axes2.set_title("Sources",
                            horizontalalignment='left')

    def _plotscript(self, object):
        '''The plotting script, plots a default skymap of cosmic rays and
        sources and allows highlighting of individual objects
        '''
        # axes are redrawn for every plot
        self.drawAxes()
        if self._crCollection:
            self.axes1.add_collection( self._crCollection )
        if self._sourceMarker:
            for s in self._sourceMarker:
                self.axes2.add_line(s[0])

        if isinstance(object, pxl.core.BasicContainer):
            self._basiccontainer = object
            # plot cosmic rays in first subplot
            self._crs = object.getObjectsOfType(pxl.astro.UHECR)
            if len(self._crs) == 0:
                return False
            lon = zeros(len(self._crs))
            lat = zeros(len(self._crs))
            energies = zeros(len(self._crs))
            labels = ['' for i in range(len(self._crs))]
            for i, cr in enumerate(self._crs):
                lon[i] = cr.getGalacticLongitude()
                lat[i] = cr.getGalacticLatitude()
                energies[i] = cr.getEnergy()
                labels[i] = cr.getId().toString()

            indices = argsort(energies)
            lon = lon[indices]
            lat = lat[indices]
            energies = energies[indices]
            labels = take(labels, indices)

            if self._crCollection:
                try:
                    self.axes1.collections.pop()
                except IndexError:
                    logging.debug(__name__ + ": axes1.collection is empty.")
            self._crCollection = self.axes1.scatter(lon, lat, 
                                                   c=18+log10(energies),
                                                    label=labels.tolist(),
                                                   picker=True,
                                                   linewidths=0,
                                                   vmin=self._crEmin,
                                                   vmax=self._crEmax)
            if not hasattr(self, 'colorbar'):
                self.colorbar = self.fig.colorbar( self._crCollection,
                                                    ax=self.axes1,
                                                  orientation='horizontal')
                self.colorbar.set_label('Energy [log(eV/Energy)]')

            # plot sources in second subplot
            self._sources = object.getObjectsOfType(pxl.astro.UHECRSource)
            if len(self._sources) > 0:
                self._sourceMarker = []
                for s in self._sources:
                    d = s.getDistance()
                    if d > 0:
                        msize = 15.*50/d+5
                    else:
                        msize = 15.*50/20+5
                    self._sourceMarker.append(self.axes2.plot(
                        s.getGalacticLongitude(),
                        s.getGalacticLatitude(), 'g+',
                        label=s.getId().toString(),
                        picker=True,
                        markersize=msize,
                        markeredgewidth=2))

        elif isinstance(object, pxl.astro.UHECRSource):
            d = object.getDistance()
            if d > 0:
                msize = 15.*50/d+5
            else:
                msize = 15.*50/20+5
            self.axes2.plot(
                object.getGalacticLongitude(),
                object.getGalacticLatitude(), 'r+',
                label=object.getId().toString(),
                picker=True,
                markersize=msize,
                markeredgewidth=2)

        elif isinstance(object, pxl.astro.UHECR):
            self._CR = self.axes1.scatter(object.getGalacticLongitude(), 
                              object.getGalacticLatitude(),
                              c=18+log10(object.getEnergy()),
                                         edgecolors='r',
                                         label=object.getId().toString(),
                              vmin=self._crEmin, vmax=self._crEmax)
            if not hasattr(self, 'colorbar'):
                self.colorbar = self.fig.colorbar( self._CR, ax=self.axes1,
                                                 orientation='horizontal')
                self.colorbar.set_label('Energy [log(eV/Energy)]')

        self.canvas.draw()



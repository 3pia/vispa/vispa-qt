import logging

from PyQt4 import QtGui, QtCore

from Vispa.Views.AbstractView import AbstractView
from Vispa.Main.Exceptions import exception_traceback
from Vispa.Share.BasicDataAccessor import BasicDataAccessor
from Vispa.Share.ImageExporter import ImageExporter

import pxl.core, pxl.astro, pxl.hep, pxl.healpix

try:
    import matplotlib
    from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
    from matplotlib.backends.backend_qt4agg import NavigationToolbar2QTAgg as NavigationToolbar
    from matplotlib.figure import Figure
    from scipy import *
    from numpy import *
except ImportError, e:
    raise RuntimeError(e)

class AstroObjectView(AbstractView, QtGui.QWidget):

    LABEL = "&Astro Object View"

    def __init__(self, parent=None):
        AbstractView.__init__(self) 
        QtGui.QWidget.__init__(self, parent)
        self.parent = parent
        self._operationId = 0
        self._updatingFlag = 0
        self._astroCollection = None
        self._imageExporter = None

        self._crEmin = 18.5
        self._crEmax = 20.5
        self.createFigure()
        self._selectionChanged = False
        self._selected = None
        self.colorbar = None
    def createFigure(self):
        '''Creates the FigureCanvas (matplotlib QWidget) that contains the
        matplotlib figure.
        '''
        self.fig = Figure(( 5.0, 4.0), dpi=100)
        self.canvas = FigureCanvas(self.fig)
        self.canvas.setParent(self)
        self.fig.canvas.mpl_connect('pick_event', self.on_pick)

        vbox = QtGui.QVBoxLayout()
        vbox.addWidget(self.canvas)
        self.setLayout(vbox)

    def drawAxes(self):
        ''' Draws the axes in the matplotlib figure.
        '''
        self.axes = self.fig.add_subplot(111, projection='hammer')
        #self.axes.clear()
        ticks = linspace(-180,180,5)
        tickLabels = ['$%i^\circ$' %(v) for v in ticks] 
        self.axes.set_xticks(ticks/180*pi)
        self.axes.set_xticklabels(tickLabels)
        ticks = linspace(-90,90,5)
        tickLabels = ['$%i^\circ$' %(v) for v in ticks]
        self.axes.set_yticks(ticks/180*pi)
        self.axes.set_yticklabels(tickLabels)
        self.axes.grid(True)

    def setDataAccessor(self, accessor):
        """ Sets the DataAccessor from which the data is read 
        You need to call updateContent() in order to make the changes visible.   
        """
        if not isinstance(accessor, BasicDataAccessor):
            raise TypeError(__name__ + " requires data accessor of type BasicDataAccessor.")
        #if not isinstance(accessor, RelativeDataAccessor):
        #    raise TypeError(__name__ + " requires data accessor of type RelativeDataAccessor.")
        AbstractView.setDataAccessor(self, accessor)

    def updateContent(self):
        """ Update content of view.        
        """
        logging.debug(__name__ + ": updateContent")
        self._updatingFlag += 1
        operationId = self._operationId
        objects = self.applyFilter(self.dataObjects())
        for object in objects:
            if operationId != self._operationId:
                break
            self._plotscript(object) 

        self._updatingFlag -= 1
        return operationId == self._operationId

    def _plotscript(self, object):
        """ The plotting script, so far only plotting for astro objects is implemented
        """
        # axes are redrawn for every plot
        self.drawAxes()
        if self._astroCollection:
            self.axes.add_collection( self._astroCollection )
        logging.debug(__name__ + ": _plotscript")
        if isinstance(object, pxl.core.BasicContainer):
            self._basiccontainer = object
            self._abos = object.getObjectsOfType(pxl.astro.UHECR)
            if len(self._abos) == 0:
                return False
            lon = zeros(len(self._abos))
            lat = zeros(len(self._abos))
            ene = zeros(len(self._abos))
            labels = ['' for i in range(len(self._abos))]
            for i,ao in enumerate(self._abos):
                lon[i] = ao.getGalacticLongitude()
                lat[i] = ao.getGalacticLatitude()
                ene[i] = ao.getEnergy()
                labels[i] = ao.getId().toString()

            # and now plot the coordinates and save them
            #if self._astroCollection:
            #    try:
            #        self.axes.collections.pop()
            #    except IndexError:
            #        logging.debug(__name__ + ": axes.collection is
            #        empty.")
            self._astroCollection = self.axes.scatter(lon, lat,
                c=18+log10(ene), s=20,linewidth=0, label=labels, picker=True, vmin=self._crEmin, vmax=self._crEmax)
            if not self.colorbar:
              self.colorbar = self.fig.colorbar(self._astroCollection, orientation='horizontal')
              t = linspace(self._crEmin, self._crEmax, 5)
              self.colorbar.set_ticks(t)
              tl = ["$10^{%.1f}$" %(v) for v in t]
              self.colorbar.set_ticklabels(tl)
              self.colorbar.set_label('Energy [eV]')

            
        # ROIs
        elif isinstance(object, pxl.astro.RegionOfInterest):
            self.axes.plot( object.getGalacticLongitude() ,
                           object.getGalacticLatitude(), '*', markersize=3.5)
            logging.debug(__name__ + 'DrawROI with ' + str(len(object.getSoftRelations().getContainer().items())) + ' UHECR')
            for key, item in object.getSoftRelations().getContainer().items():
                uhecr = pxl.astro.toUHECR(self._basiccontainer.getById(item))
                if isinstance(uhecr,pxl.astro.UHECR):
                    self.axes.plot( uhecr.getGalacticLongitude(),
                                   uhecr.getGalacticLatitude(),'g.',
                                   markersize=3,linewidth=0, label=uhecr.getId().toString())
                else:
                    uhecr = pxl.astro.toAstroObject(self._basiccontainer.getById(item))
                    if isinstance(uhecr, pxl.astro.AstroObject):
                        self.axes.plot(uhecr.getGalacticLongitude(), 
                                       uhecr.getGalacticLatitude(),'r.',
                                      markersize=2,
                                      label=uhecr.getId().toString())
        # UHECRSource
        elif isinstance(object, pxl.astro.UHECRSource):
            d = object.getDistance()
            self.axes.plot(object.getGalacticLongitude(),
                           object.getGalacticLatitude(), 'g+', 
                           markersize=15.*50/d+15,
                           markeredgewidth=2,
                          label=object.getId().toString())

        # UHECR
        elif isinstance(object, pxl.astro.UHECR):
            self._CR = self.axes.scatter(object.getGalacticLongitude(), 
                              object.getGalacticLatitude(),
                              c=18+log10(object.getEnergy()),
                                         label=object.getId().toString(),
                              vmin=self._crEmin, vmax=self._crEmax)
                    ## HealpixMap to be implemented
        #elif isinstance(object, pxl.core.BasicNVector):
        #    logging.debug(__name__ + 
        #                  ": drawing BasicNVector:")
        #    # plot HealpixMap with BasicNVector
        #    hp = pxl.healpix.HealpixMap(6)
        #    for j in range(6):
        #        if len(object) == hp.getNumberOfPixels(j+1):
        #            pixelOrder = j+1
        #            break
        #    if pixelOrder < 6: #redefine pixelorder
        #        hp = pxl.healpix.HealpixMap(pixelOrder)
        #    hp.setVector(object)
        #    logging.debug(__name__ + ": plot Hammer Image now")
        #    PXLAstroTools.plotHammerImage(hp)

        elif isinstance(object, pxl.astro.AstroBasicObject):
            self.axes.plot(object.getGalacticLongitude(),
                           object.getGalacticLatitude(),
                           'r.',label=object.getId().toString())


        
        self.canvas.draw()
        
    def on_pick(self, event):
        """ Executed if an object is selected in the figure, calls _plotscript 
            to plot the object.
        """
        logging.debug(__name__ + ": called pick event handler")
        #ind = event.ind[0]
        #self._plotscript(self._abos[ind]) 
        #self._selectionChanged = True
        #self.select(self._abos[ind])

        object = None

        if isinstance(event.artist, matplotlib.lines.Line2D):
            try:
                id = pxl.core.Id(event.artist.get_label())
                object = pxl.core.upCast(self._basiccontainer.getById(id))
            except Exception, e:
                logging.debug(__name__ + ": found no label in artist Line2D") 

        elif isinstance(event.artist, matplotlib.collections.CircleCollection):
            #try:
            ind = event.ind[0]
            labels = event.artist.get_label()
            id = pxl.core.Id(labels[ind])
            object = pxl.core.upCast(self._basiccontainer.getById(id))
            #except Exception, e:
            #    logging.debug(__name__ + ": found no label in artist CircleColleciton") 
            #    logging.debug(__name__ + "index %d label length %d"%(ind,
                                                                     #len(labels)))

        if object:
            logging.debug(__name__ + ": on_pick -> is object")
            logging.debug(__name__ + ": %s"%type(object))
            logging.debug(__name__ + ": %s"%object.toString())
            self.emit(QtCore.SIGNAL("selected"), object)
            self._plotscript(object)
            #self.select(object)


    def select(self, object):
        """ Select an object in the view.
        """
        logging.debug(__name__ + ": select")
        self._selected = object
            #logging.debug(__name__ + ":ID %s\n"%object.getId()
            #              + "Coordinates %f %f\n"%(object.getGalacticLongitude(), 
            #                                    object.getGalacticLatitude())
            #              + "Type %s"%type(object))

    def selection(self):
        """ Return the last selected object in the view.
        """
        return self._selected 

    def restoreSelection(self):
        """ Select the last selected object in the view.
        """
        self.select(self.selection())

    def cancel(self):
        """ Stop all operations in view.
        """
        self._operationId += 1

    def isBusy(self):
        """ Return is operations are ongoing in view.
        """
        return self._updatingFlag>0

    def exportImage(self, filename=None):
        if not filename:
            defaultname = QtCore.QCoreApplication.instance().getLastOpenLocation()
            supportedFileTypeExtensions= self.fig.canvas.get_supported_filetypes().keys()
            fileFilters = [ext.upper() +" File (*."+ ext.lower() + ")" for ext in supportedFileTypeExtensions] 

            fileFiltersString = ";;".join(fileFilters)
            filter = QtCore.QString(fileFilters[0])
            filename = str(QtGui.QFileDialog.getSaveFileName(self.parent,
              "Save image...",defaultname, fileFiltersString, filter))
        self.fig.savefig(filename)

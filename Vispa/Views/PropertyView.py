import logging
import sys
import os
import os.path
import ConfigParser

from PyQt4.QtCore import Qt, SIGNAL, QCoreApplication, QSize, QPoint, QVariant
from PyQt4.QtGui import QTableWidget, QTableWidgetItem, QCheckBox, QWidget, QSpinBox, QHBoxLayout, QVBoxLayout, QLineEdit, QSizePolicy, QTextEdit, QTextOption, QFrame, QToolButton, QPalette, QComboBox, QFileDialog, QTextCursor, QInputDialog, QPushButton, QGridLayout, QIcon, QHeaderView, QMessageBox, QColor, QFont, QMenu, QDialog, QPixmap, QDialogButtonBox, QAbstractItemView

from Vispa.Main.Application import Application
from Vispa.Main.AbstractTab import AbstractTab
from Vispa.Main.GuiFacade import guiFacade
from Vispa.Share.BasicDataAccessor import BasicDataAccessor
from Vispa.Views.AbstractView import AbstractView
from Vispa.Share.ThreadChain import ThreadChain
from Vispa.Gui.TextDialog import TextDialog
from Vispa.Gui.VispaWidget import VispaWidget
from Vispa.Main.FileManager import is_relative_path, absolute_path, relative_path

class PropertyVectorDialog(QDialog):
    """ QDialog object to edit text by using an editor window.
    """
    def __init__(self, parent=None, title="Edit list...", vector=None, propertyType=None, callButton=False, relativePath=None, readonly=False, help=None):
        super(PropertyVectorDialog, self).__init__(parent)
        self.setWindowTitle(title)
        self.resize(600, 500)
        self._list = QTableWidget()
        self._list.setSortingEnabled(False)
        self._list.verticalHeader().hide()
        self._list.horizontalHeader().hide()
        self._list.setSelectionMode(QTableWidget.NoSelection)
        self._list.clear()        # sets header
        self._list.setRowCount(0)
        self._list.setColumnCount(1)
        self._propertyType = propertyType
        self._relativePath = relativePath
        self._callButton = callButton
        if vector != None:
            for v in vector:
                self.appendProperty(v)
        self._help = help
        self._add = QPushButton('&Add...', self)
        self.connect(self._add, SIGNAL('clicked()'), self.addProperty)
        self._ok = QPushButton('&Ok', self)
        self.connect(self._ok, SIGNAL('clicked()'), self.accept)
        self._remove = QPushButton('&Remove all', self)
        self.connect(self._remove, SIGNAL('clicked()'), self.removeAll)
        if not readonly:
            self._cancel = QPushButton('&Cancel', self)
            self.connect(self._cancel, SIGNAL('clicked()'), self.reject)
        if help:
            self._helpButton = QPushButton('&Help', self)
            self.connect(self._helpButton, SIGNAL('clicked()'), self.showHelp)
        layout = QGridLayout()
        layout.addWidget(self._list, 0, 0, 1, 6)
        layout.addWidget(self._ok, 1, 5)
        layout.addWidget(self._add, 1, 3)
        layout.addWidget(self._remove, 1, 2)
        if not readonly:
            layout.addWidget(self._cancel, 1, 0)
        if help:
            layout.addWidget(self._helpButton, 1, 1)
        self.setLayout(layout)

    def resizeEvent(self, event=None):
        """ Resize columns when table size is changed.
        """
        space = self._list.width() - 4
        if self._list.verticalScrollBar().isVisible():
            space -= self._list.verticalScrollBar().width()
        self._list.setColumnWidth(0, space)

    def removeAll(self):
        self._list.clear()
        self._list.setRowCount(0)

    def propertyWidgets(self):
        """ Return all property widgets in the right column.
        
        Closable as well as normal properties are returned.
        """
        widgets = []
        for i in range(self._list.rowCount()):
            widget = self._list.cellWidget(i, 0)
            if isinstance(widget, Property):
                widgets += [widget]
            elif hasattr(widget, "closableProperty"):
                widgets += [widget.closableProperty()]
        return widgets

    def appendProperty(self, value):
        self._list.setRowCount(self._list.rowCount() + 1)
        property = {}
        property["type"] = self._propertyType
        property["name"] = ""
        property["value"] = value
        propertyWidget = PropertyView.propertyWidgetFromProperty(property)
        if self._relativePath:
            propertyWidget.useRelativePaths(self._relativePath)
        widget = ClosableProperty(propertyWidget, False)
        self.connect(widget.closeButton(), SIGNAL('clicked(bool)'), self.removeProperty)
        self._list.setCellWidget(self._list.rowCount() - 1, 0, widget)
        self._list.verticalHeader().resizeSection(self._list.rowCount() - 1, propertyWidget.propertyHeight())
        return propertyWidget

    def removeProperty(self, bool=False):
        """ This function deletes a property.
        
        The DataAcessor is called to handle the property remove.
        """
        logging.debug("%s: removeProperty()" % (self.__class__.__name__))
        property = self.sender().parent() ## instance of ClosableProperty
        widgets = self.propertyWidgets()
        for i in range(len(widgets)):
            if widgets[i] == property:
                self._list.removeRow(i)

    def addProperty(self, bool=False):
        """ This function adds a property.
        
        The DataAcessor is called to add the property.
        """
        if hasattr(self.parent(), "openMulti"):
            for v in self.parent().openMulti():
                self.appendProperty(v)
        else:
            widget = self.appendProperty("")
            widget.setFocus()
            if self._callButton:
                widget.buttonClicked()

    def showHelp(self):
        QMessageBox.about(self, 'Info', self._help)

class ComboBoxReturn(QComboBox):
    def keyPressEvent(self, event):
        QComboBox.keyPressEvent(self, event)
        if event.key() == Qt.Key_Return:
            self.emit(SIGNAL("returnPressed()"))


class PropertyView(QTableWidget, AbstractView):
    """ Shows properties of an object in a QTableWidget using the DataAccessor.
    
    The view can be used readonly ('setReadOnly') or for editing.
    On editing the signals 'valueChanged', 'propertyDeleted', 'propertyAdded' are emitted.
    """

    LABEL = "&Property View"

    def __init__(self, parent=None, name=None):
        """ Constructor """
        #logging.debug(self.__class__.__name__ + ": __init__()")
        AbstractView.__init__(self)
        QTableWidget.__init__(self, parent)

        self._valueChangedErrorWidget = None
        self._operationId = 0
        self._updatingFlag = 0
        self.updateIni = False
        self._currentCategoryName = ""
        self._relativePath = None
        self._labelContextMenu = None
        self._lastContextMenuRequestPos = None
        self.setSortingEnabled(False)
        self.verticalHeader().hide()
        self.setSelectionMode(QTableWidget.NoSelection)
        self._valueChangedSuccessfulFlag = True
        self.clear()        # sets header

        self._readOnly = False
        self._showAddDeleteButtonFlag = False

        self.connect(self.horizontalHeader(), SIGNAL("sectionResized(int,int,int)"), self.sectionResized)
        self.connect(self, SIGNAL("itemDoubleClicked(QTableWidgetItem *)"), self.itemDoubleClickedSlot)
        #self.connect(self, SIGNAL("itemClicked(QTableWidgetItem *)"), self.itemClickedSlot)
        self.connect(self, SIGNAL("customContextMenuRequested ( QPoint  ) "), self.customContextMenuRequestSlot)
        self.connect(self, SIGNAL("propertyAdded"), self.propertyAddedSlot)
        self.setContextMenuPolicy(Qt.CustomContextMenu)

        #self.setVerticalScrollMode(1)
        self.setVerticalScrollMode(QAbstractItemView.ScrollPerPixel)

    def cancel(self):
        """ Stop all running operations.
        """
        self._operationId += 1

    def clear(self):
        """ Clear the table and set the header label.
        """
        QTableWidget.clear(self)
        self._valueChangedSuccessfulFlag = True
        guiFacade.hidePropertyValueChangedError()
        self.setRowCount(0)
        self.setColumnCount(2)
        self.setHorizontalHeaderLabels(['Property', 'Value'])

    def propertyWidgets(self):
        """ Return all property widgets in the right column.
        
        Closable as well as normal properties are returned.
        """
        widgets = []
        for i in range(self.rowCount()):
            widget = self.cellWidget(i, 1)
            if isinstance(widget, Property):
                widgets += [widget]
            elif hasattr(widget, "closableProperty"):
                widgets += [widget.closableProperty()]
        return widgets

    def updatePropertyRowNumbers(self):
        for i in range(self.rowCount()):
            widget = self.cellWidget(i, 1)
            if isinstance(widget, Property):
                widget.setRowNumber(i)

    def updatePropertyHeight(self, property):
        """ Update the height of the column that holds a certain property.
        """
        #logging.debug(self.__class__.__name__ + ": updatePropertyHeight()")
        self.verticalHeader().resizeSection(property.rowNumber(), property.propertyHeight())

    def append(self, property):
        """ Adds a property to the PropertyView and returns it.
        """
        row_number = self.lastRow() + 1
        property.setPropertyView(self)
        property.setRowNumber(row_number)
        if self._readOnly:
            property.setReadOnly(True)
        self.insertRow(row_number)
        self.setItem(self.lastRow(), 0, LabelItem(property))
        if not self._readOnly and self._showAddDeleteButtonFlag and property.deletable():
            widget = ClosableProperty(property)
            self.connect(widget.closeButton(), SIGNAL('clicked(bool)'), self.removeProperty)
            self.setCellWidget(self.lastRow(), 1, widget)
        else:
            self.setCellWidget(self.lastRow(), 1, property)
        self.updatePropertyHeight(property)
        self.connect(property, SIGNAL('updatePropertyHeight'), self.updatePropertyHeight)
        return property

    def lastRow(self):
        """ Return the last row holding a property.
        
        The row with the add new property field is not counted.
        The row with the save and load category is not counted.
        The row with the save and load button is not counted.
        """
        if not self._readOnly and self._showAddDeleteButtonFlag and not self._updatingFlag > 0:
            return self.rowCount() - 3 - 1 # -1 for load script rows
        else:
            return self.rowCount() - 1

    def addCategory(self, name, value=""):
        """ Add a category row to the table which consists of two gray LabelItems.
        """
        self.insertRow(self.lastRow() + 1)
        self.setItem(self.lastRow(), 0, LabelItem(name, Qt.lightGray))
        self.setItem(self.lastRow(), 1, LabelItem(value, Qt.lightGray))
        self.verticalHeader().resizeSection(self.rowCount() - 1, Property.DEFAULT_HEIGHT)
        return name


    def setReadOnly(self, readOnly):
        """ Sets all properties in the PropertyView to read-only.
        
        After calling this function all properties that are added are set to read-only as well.
        """
        #logging.debug('PropertyView: setReadOnly()')
        self._readOnly = readOnly
        for property in self.propertyWidgets():
            if property:
                property.setReadOnly(self._readOnly)

    def readOnly(self):
        return self._readOnly

    def setShowAddDeleteButton(self, show):
        self._showAddDeleteButtonFlag = show

    def showAddDeleteButton(self):
        return self._showAddDeleteButtonFlag

    def resizeEvent(self, event):
        """ Resize columns when table size is changed.
        """
        if event != None:
            QTableWidget.resizeEvent(self, event)
        space = self.width() - 4
        if self.verticalScrollBar().isVisible():
            space -= self.verticalScrollBar().width()
        space -= self.columnWidth(0)
        self.setColumnWidth(1, space)
        if self.updateIni:
            self.writeIni()

    def sectionResized(self, index, old, new):
        space = self.width() - 4
        if self.verticalScrollBar().isVisible():
            space -= self.verticalScrollBar().width()
        space -= self.columnWidth(0)
        self.setColumnWidth(1, space)
        if self.updateIni:
            self.writeIni()

    def setDataAccessor(self, accessor):
        """ Sets the DataAccessor from which the object properties are read.
        
        You need to call updateContent() in order to make the changes visible.
        """
        if not isinstance(accessor, BasicDataAccessor):
            raise TypeError(__name__ + " requires data accessor of type BasicDataAccessor.")
        AbstractView.setDataAccessor(self, accessor)

    def appendAddRow(self):
        """ Append a row with a field to add new properties.
        """
        self.insertRow(self.lastRow() + 1)
        lineedit = QLineEdit()
        lineedit.setFrame(False)
        lineedit.setContentsMargins(0, 0, 0, 0)
        self.setCellWidget(self.lastRow(), 0, lineedit)
        widget = QWidget()
        widget.setContentsMargins(0, 0, 0, 0)
        widget.setLayout(QHBoxLayout())
        widget.layout().setSpacing(0)
        widget.layout().setContentsMargins(0, 0, 0, 0)
        typelist = ComboBoxReturn()
        types = ["String", "StringVector", "Boolean", "Integer",
            "Double", "InputFile", "InputFileVector", "OutputFile",
            "OutputFileVector", "TextSelection"]
        for type in types:
            typelist.addItem(type)
        widget.layout().addWidget(typelist)
        addButton = QToolButton()
        addButton.setText("+")
        widget.layout().addWidget(addButton)
        self.setCellWidget(self.lastRow(), 1, widget)
        self.verticalHeader().resizeSection(self.lastRow(), Property.DEFAULT_HEIGHT)
        self.connect(addButton, SIGNAL('clicked(bool)'), self.addProperty)
        self.connect(lineedit, SIGNAL('returnPressed()'), self.addProperty)
        self.connect(typelist, SIGNAL('returnPressed()'), self.addProperty)
        addButton._lineedit = lineedit
        addButton._typelist = typelist
        lineedit._lineedit = lineedit
        lineedit._typelist = typelist
        typelist._lineedit = lineedit
        typelist._typelist = typelist

        """ Append a row with a field to save and load properties from file.
        """
#        self.addCategory("Save/Load", "'Options' and 'User Records'")
        self.addCategory("", "")
        self.insertRow(self.lastRow() + 1)

        widgetSaveLoad = QDialogButtonBox()
        #widgetSaveLoad.setStyleSheet("QWidget {background-color: #c0c0c0; }")
        widgetSaveLoad.setCenterButtons(True)
#        widgetSaveLoad.setContentsMargins(0, 0, 0, 0)
#        widgetSaveLoad.setLayout(QHBoxLayout())
#        widgetSaveLoad.layout().setSpacing(0)
#        widgetSaveLoad.layout().setContentsMargins(0, 0, 0, 0)
        loadButton = widgetSaveLoad.addButton(QDialogButtonBox.Open)
        loadButton.setText('')
        loadButton.setIcon(QIcon(QPixmap(":/resources/fileopen.svg")))
        loadButton.setStyleSheet("QToolButton {background-color: white; }")
        tooltip = "Load 'Options' and 'User Records' from file"
        loadButton.setToolTip("<FONT COLOR=black>" + tooltip + "</FONT>")
        loadButton.pressed.connect(self.loadScript)

        saveButton = widgetSaveLoad.addButton(QDialogButtonBox.Save)
        saveButton.setText('')
        saveButton.setIcon(QIcon(QPixmap(":/resources/filesave.svg")))
        saveButton.setStyleSheet("QToolButton {background-color: white; }")
        tooltip = "Save 'Options' and 'User Records' to file"
        saveButton.setToolTip("<FONT COLOR=black>" + tooltip + "</FONT>")
        saveButton.pressed.connect(self.saveScript)

        self.setCellWidget(self.lastRow(), 0, widgetSaveLoad)
        self.setItem(self.lastRow(), 1, LabelItem("Save/Load 'Options' and 'User Records'", Qt.white)) #lightGray
        #widgetSaveLoad.setBackground(color)

    def loadScript(self):
        fileName = QFileDialog.getOpenFileName(self, "Load 'Options' and 'User Records' from file")
        logging.debug("%s: loadScript() - Reading module options from inputfile '%s'" % (self.__class__.__name__, fileName))
        fileConfigParser = ConfigParser.ConfigParser()
        fileConfigParser.optionxform = str

        try:
            outputFile = open(fileName, "r")
            fileConfigParser.readfp(outputFile)
            outputFile.close()
        except IOError:
            logging.debug("%s: loadScript() - Error reading module options from inputfile '%s'" % (self.__class__.__name__, fileName))

        data_dict = {}
        available_sections = fileConfigParser.sections()

        for section in available_sections:
            data_dict[section] = fileConfigParser.items(section)

        for category in data_dict:
            if category == "-Types-":
                continue

            for (name, value) in data_dict[category]:
                object = self.dataObject()
                if not (self.dataAccessor().type(object) == None): #check if we have a pxlModule
                    property = {}
                    if name in data_dict["-Types-"]:
                        property["type"] = data_dict["-Types-"][1]
                    else:#default
                        property["type"] = "String"
                    property["name"] = name
                    property["value"] = value

                    new_property = PropertyView.propertyWidgetFromProperty(property, category)

                    if not (self.dataAccessor().propertyValue(object, new_property.name()) == None):
                        self.valueChanged(new_property)
                    else:
                        self.addPropertyHelper(object, property["type"], property["name"], category)
                        self.valueChanged(new_property)
        self.updateContent()

    def saveScript(self):
        fileName = QFileDialog.getSaveFileName(self, "Save 'Options' and 'User Records' to file")
        logging.debug("%s: saveScript() - Writing module options to outputfile '%s'" % (self.__class__.__name__, fileName))

        fileConfigParser = ConfigParser.ConfigParser()
        fileConfigParser.optionxform = str
        categoriesToSave = ["User Records", "Options"]
        optionsToSkip = []
        urToSkip = ["xPos", "yPos"]

        for category in categoriesToSave:
            fileConfigParser.add_section(category)

        fileConfigParser.add_section("-Types-")

        if self.dataAccessor() == None:
            return False

        currentCategoryName = ""
        for property in self.dataAccessor().properties(self.dataObject()):
            if type(property) == tuple:
                # compatibility to old format
                property = PropertyView.propertyTupleToDict(property)

            if property["type"] == "Category":
                currentCategoryName = property["name"]
                continue

            if (currentCategoryName in categoriesToSave):
                name = property["name"]
                value = property["value"]
                type_ = property["type"]

                if (currentCategoryName == "Options" and name in optionsToSkip):
                    continue
                elif (currentCategoryName == "User Records" and name in urToSkip):
                    continue
                else:
                    fileConfigParser.set(currentCategoryName, name, value)
                    fileConfigParser.set("-Types-", name, type_)

        try:
            outputFile = open(fileName, "w")
            fileConfigParser.write(outputFile)
            outputFile.close()
        except IOError:
            logging.debug("%s: saveScript() - Error writing module options to outputfile '%s'" % (self.__class__.__name__, fileName))

    def updateContent(self):
        """ Fill the properties of an object in the PropertyView using the DataAccessor.
        """
        logging.debug("%s: updateContent()" % (self.__class__.__name__))
        #self.hideValueChangedError()
        self.cancel()
        if self.dataAccessor() == None:
            return False
        self._updatingFlag += 1
        self.clear()
        if self.dataObject() == None:
            self._updatingFlag -= 1
            return True
        self._ignoreValueChangeFlag = True  # prevent infinite loop
        operationId = self._operationId
        # do not use threads here since this may lead to crashes
        for property in self.dataAccessor().properties(self.dataObject()):
            if type(property) == tuple:
                # compatibility to old format
                property = PropertyView.propertyTupleToDict(property)

            if property["type"] == "Category":
                self._currentCategoryName = self.addCategory(property["name"])
            else:
                propertyWidget = PropertyView.propertyWidgetFromProperty(property, self._currentCategoryName)
                if propertyWidget:
                    self.append(propertyWidget)
                if isinstance(propertyWidget, (FileProperty, FileVectorProperty)):
                    propertyWidget.useRelativePaths(self._relativePath)
                if isinstance(propertyWidget, QCheckBox):
                    propertyWidget.setChecked(property["value"], False)      # strange, QCheckBox forgets its state on append in Qt 4.4.4
        if not self._readOnly and self._showAddDeleteButtonFlag:
            self.appendAddRow()
        self.resizeEvent(None)
        self._ignoreValueChangeFlag = False
        self._updatingFlag -= 1
        return self._operationId == operationId

    #@staticmethod
    def propertyWidgetFromProperty(property, categoryName=None):
        """ Create a property widget from a property tuple.
        
        This function is static in order to be used by other view, e.g. TableView.
        """
        propertyWidget = None
        if property["type"] == "String":
            propertyWidget = StringProperty(property["name"], property["value"], categoryName)
        elif property["type"] == "MultilineString":
            propertyWidget = StringProperty(property["name"], property["value"], categoryName, True)
        elif property["type"] == "StringVector":
            propertyWidget = StringVectorProperty(property["name"], property["value"], categoryName)
        elif property["type"] == "InputFile":
            propertyWidget = FileProperty(property["name"], property["value"], categoryName, True)
        elif property["type"] == "OutputFile":
            propertyWidget = FileProperty(property["name"], property["value"], categoryName, False)
        elif property["type"] == "InputFileVector":
            propertyWidget = FileVectorProperty(property["name"], property["value"], categoryName, True)
        elif property["type"] == "OutputFileVector":
            propertyWidget = FileVectorProperty(property["name"], property["value"], categoryName, False)
        elif property["type"] == "Boolean":
            propertyWidget = BooleanProperty(property["name"], property["value"], categoryName)
        elif property["type"] == "Integer":
            propertyWidget = IntegerProperty(property["name"], property["value"], categoryName)
        elif property["type"] == "Double":
            propertyWidget = DoubleProperty(property["name"], property["value"], categoryName)
        elif property["type"] == "DropDown":
            propertyWidget = DropDownProperty(property["name"], property["value"], property["dropDownEntries"], categoryName)
        elif property["type"] == "TextSelection":
            propertyWidget = TextSelectionProperty(property["name"], property["value"], categoryName)
        else:
            logging.error(__name__ + ": propertyWidgetFromProperty() - Unknown property type " + str(property["type"]))
            return None
        if "userInfo" in property and property["userInfo"]:
            propertyWidget.setUserInfo(property["userInfo"])
        if "readOnly" in property and property["readOnly"]:
            propertyWidget.setReadOnly(True)
        if "deletable" in property and property["deletable"]:
            propertyWidget.setDeletable(True)
        if "highlightLabel" in property and property["highlightLabel"]:
            propertyWidget.enableHighlightLabel()

        return propertyWidget
    propertyWidgetFromProperty = staticmethod(propertyWidgetFromProperty)

    #@staticmethod
    def propertyTupleToDict(propertiesTuple):
        """ Converts old style properties tuple to new dictionaries.
        
        This functions is called at several places. Check entire repository when removing this.
        """
        logging.warning("%s.propertyTupleToDict() - Using deprecated tuple format for property (name=%s) definition. Use dictionary instead." % (__name__, propertiesTuple[1]))
        propertiesDict = {}
        propertiesDict["type"] = propertiesTuple[0]
        propertiesDict["name"] = propertiesTuple[1]
        propertiesDict["value"] = propertiesTuple[2]
        if len(propertiesTuple) > 3:
            propertiesDict["userInfo"] = propertiesTuple[3]
        if len(propertiesTuple) > 4:
            propertiesDict["readOnly"] = propertiesTuple[4]
        if len(propertiesTuple) > 5:
            propertiesDict["deletable"] = propertiesTuple[5]
        if len(propertiesTuple) > 6:
            propertiesDict["dropDownEntries"] = propertiesTuple[6]

        return propertiesDict
    propertyTupleToDict = staticmethod(propertyTupleToDict)

    def valueChangedSuccessful(self):
        return self._valueChangedSuccessfulFlag

    def valueChanged(self, property):
        """ This function is called when a property a changed.
        
        The DataAcessor is called to handle the property change.
        """
        logging.debug("%s: valueChanged()" % (self.__class__.__name__))

        if self.dataAccessor() and not self._ignoreValueChangeFlag:
            bad = False
            newvalue = property.value()
            oldValue = self.dataAccessor().propertyValue(self.dataObject(), property.name())
            if newvalue != oldValue:
                if isinstance(newvalue, ValueError):
                    result = str(newvalue)
                else:
                    result = self.dataAccessor().setProperty(self.dataObject(), property.name(), newvalue, property.categoryName())

                if result == True:
                    self.emit(SIGNAL('valueChanged'), property.name(), newvalue, oldValue, property.categoryName())
                    guiFacade.hidePropertyValueChangedError()
                else:
                    property.setToolTip("<FONT COLOR=black>" + result + "</FONT>")
                    #QMessageBox.critical(self.parent(), 'Error', result)
                    self.showValueChangedError(property, result)
                    bad = True
            else:
                # keep old value and make sure old errors are hidden
                guiFacade.hidePropertyValueChangedError()

            property.setHighlighted(bad)
            self._valueChangedSuccessfulFlag = not bad

    def showValueChangedError(self, property, message):
        rowNumber = property.rowNumber()
        if type(rowNumber) != int:
            rowNumber = 0
        cellWidgetPos = self.visualItemRect(self.item(rowNumber, 0)).topLeft()

        x_pos = cellWidgetPos.x()
        y_pos = cellWidgetPos.y() + 0.5 * self.rowHeight(rowNumber)# + self.rowHeight(0) # add rowHeight(0) for header
        guiFacade.showPropertyValueChangedError(message, self.mapToGlobal(QPoint(x_pos, y_pos)))

    def hideValueChangedError(self):
        guiFacade.hidePropertyValueChangedError()

    def removeProperty(self, bool=False):
        """ This function deletes a property.
        
        The DataAcessor is called to handle the property remove.
        """
        property = self.sender().parent()._property
        name = property.name()
        if self.dataAccessor():
            if self.dataAccessor().removeProperty(self.dataObject(), property.name()):
                self.removeRow(property.rowNumber())
                self.updatePropertyRowNumbers()
                self.emit(SIGNAL('propertyDeleted'), name)

    def addProperty(self, bool=False):
        """ This function adds a property.
        
        The DataAcessor is called to add the property.
        """
        logging.debug("%s: addProperty()" % (self.__class__.__name__))

        type = str(self.sender()._typelist.currentText())
        name = str(self.sender()._lineedit.text().toAscii())
        object = self.dataObject()
        self.addPropertyHelper(object, type, name, "User Records", True)

    def addPropertyHelper(self, object, type, name, category="User Records", fromLineEdit=False):
        logging.debug("%s: addPropertyHelper()" % (self.__class__.__name__))

        if type in ["String", "InputFile", "OutputFile"]:
            value = ""
        elif type in ["Integer", "Double"]:
            value = 0
        elif type in ["StringVector", "InputFileVector",
            "OutputFileVector", "TextSelection"]:
            value = []
        elif type in ["Boolean"]:
            value = False
        if name == None or name == "":
            QCoreApplication.instance().infoMessage("Please specify name of property.")
            return
        if self.dataAccessor():
            if self.dataAccessor().setModuleProperties(object, name, value, category):
                #property=self.propertyWidgetFromProperty((type,name,value,None,False,True), self._currentCategoryName)    # TODO: remove line if property dictionaries work
                property = self.propertyWidgetFromProperty({"type": type, "name": name, "value": value, "readOnly": False, "deletable": True}, self._currentCategoryName)
                if property:
                    self.append(property)
                if isinstance(property, (FileProperty, FileVectorProperty)):
                    property.useRelativePaths(self._relativePath)
                if fromLineEdit:
                    self.sender()._lineedit.setText("")
#                self.updateContent()
                property.setFocus()
                self.emit(SIGNAL('propertyAdded'))

    def propertyAddedSlot(self):
        pass
        #logging.debug("%s: propertyAddedSlot()" % (self.__class__.__name__))

    def itemDoubleClickedSlot(self, item):
        """ Slot for itemClicked() signal.
        
        Calls items's property's doubleClicked().
        """
        #logging.debug(self.__class__.__name__ + ": itemDoubleClickedSlot()")
        if item.property():
            item.property().labelDoubleClicked()

    def customContextMenuRequestSlot(self, pos):
        logging.debug("%s.customContextMenuRequestSlot()" % self.__class__.__name__)
        self._lastContextMenuRequestPos = pos

        if not self._labelContextMenu:
            self._labelContextMenu = QMenu(self)
            labelAction = guiFacade.createAction("Copy label", self.contextMenuActionSlot)
            labelAction.setData(QVariant("label"))
            valueAction = guiFacade.createAction("Copy value", self.contextMenuActionSlot)
            valueAction.setData(QVariant("value"))
            self._labelContextMenu.addAction(labelAction)
            self._labelContextMenu.addAction(valueAction)

        self._labelContextMenu.exec_(self.mapToGlobal(pos))

    def contextMenuActionSlot(self):
        logging.debug("%s.contextMenuActionSlot()" % self.__class__.__name__)
        actionType = self.sender().data().toString()
        requestLabelItem = self.itemAt(self._lastContextMenuRequestPos)
        if requestLabelItem:
            copyValue = None
            if actionType == "label":
                copyValue = requestLabelItem.property().name()
            elif actionType == "value":
                copyValue = requestLabelItem.property().value()

            if copyValue != None:
                guiFacade.clipboard().setText(str(copyValue))

    def useRelativePaths(self, path):
        self._relativePath = path
        for propertyWidget in self.propertyWidgets():
            if isinstance(propertyWidget, (FileProperty, FileVectorProperty)):
                propertyWidget.useRelativePaths(self._relativePath)


class LabelItem(QTableWidgetItem):
    """ A QTableWidgetItem with a convenient constructor. 
    """
    def __init__(self, argument, color=Qt.white):
        """ Constructor.
        
        Argument may be either a string or a Property object.
        If argument is the latter the property's user info will be used for the label's tooltip.
        """
        if isinstance(argument, Property):
            tooltip = argument.name() + " (" + argument.userInfo() + ")"
            name = argument.name()
            self._property = argument
        else:
            tooltip = argument
            name = argument
            self._property = None

        QTableWidgetItem.__init__(self, name)
        self.setToolTip("<FONT COLOR=black>" + tooltip + "</FONT>")
        self.setFlags(Qt.ItemIsEnabled)
        self.setBackground(color)

        if self._property and self._property.highlightLabelEnabled():
            font = QFont(self.font())
            font.setBold(True)
            self.setForeground(Qt.red)
            self.setFont(font)

        if isinstance(self._property, (FileProperty, FileVectorProperty)):
            tooltip += "\nCheck the box for relative paths. Otherwise absolute paths are used."
            self.setToolTip("<FONT COLOR=black>" + tooltip + "</FONT>")

    def property(self):
        return self._property

class Property(object):
    """ Mother of all properties which can be added to the PropertyView using its append() function.
    """

    USER_INFO = "General property"
    DEFAULT_HEIGHT = 20

    def __init__(self, name, categoryName=None):
        self.setName(name)
        self.setUserInfo(self.USER_INFO)
        self._propertyView = None
        self._deletable = False
        self._categoryName = categoryName
        self._rowNumber = None
        self._highlightLabelFlag = False

    def setRowNumber(self, number):
        """ E.g. for error messages.
        """
        self._rowNumber = number

    def rowNumber(self):
        return self._rowNumber

    def setName(self, name):
        """ Sets the name of this property.
        """
        self._name = name

    def name(self):
        """ Return the name of this property.
        """
        return self._name

    def enableHighlightLabel(self, enable=True):
        self._highlightLabelFlag = enable

    def highlightLabelEnabled(self):
        return self._highlightLabelFlag

    def categoryName(self):
        return self._categoryName

    def setDeletable(self, deletable):
        self._deletable = deletable

    def deletable(self):
        return self._deletable

    def setPropertyView(self, propertyView):
        """ Sets PropertyView object.
        """
        self._propertyView = propertyView

    def propertyView(self):
        """ Returns property view.
        """
        return self._propertyView

    def setUserInfo(self, info):
        """ Returns user info string containing information on type of property and what data may be insert.
        """
        self._userInfo = info

    def userInfo(self):
        """ Returns user info string containing information on type of property and what data may be insert.
        """
        return self._userInfo

    def setReadOnly(self, readOnly):
        """ Disables editing functionality.
        """
        pass

    def propertyHeight(self):
        """ Return the height of the property widget.
        """
        return self.DEFAULT_HEIGHT

    def setValue(self, value):
        """ Abstract function returning current value of this property.
        
        Has to be implemented by properties which allow the user to change their value.
        """
        raise NotImplementedError

    def value(self):
        """ Abstract function returning current value of this property.
        
        Has to be implemented by properties which allow the user to change their value.
        """
        raise NotImplementedError

    def valueChanged(self):
        """ Slot for change events. 
        
        The actual object which have changed should connect their value changed signal 
        (or similar) to this function to forward change to data accessor of PropertyView.
        """
        logging.debug('Property: valueChanged() ' + str(self.name()))
        if self.propertyView():
            self.propertyView().valueChanged(self)

    def labelDoubleClicked(self):
        """ Called by PropertyView itemDoubleClicked().
        """
        pass

    def setHighlighted(self, highlight):
        """ Highlight the property, e.g. change color.
        """
        pass

class BooleanProperty(Property, QCheckBox):
    """ Property holding a check box for boolean values.
    """

    USER_INFO = "Enable / Disable"

    def __init__(self, name, value, categoryName=None):
        """ Constructor.
        """
        Property.__init__(self, name, categoryName)
        QCheckBox.__init__(self)
        self.connect(self, SIGNAL('stateChanged(int)'), self.valueChanged)

    def setChecked(self, check, report=True):
        if not report:
            self.disconnect(self, SIGNAL('stateChanged(int)'), self.valueChanged)
        QCheckBox.setChecked(self, check)
        if not report:
            self.connect(self, SIGNAL('stateChanged(int)'), self.valueChanged)

    def setReadOnly(self, readOnly):
        """ Disables editing functionality.
        """
        if readOnly:
            self.setEnabled(False)
            self.disconnect(self, SIGNAL('stateChanged(int)'), self.valueChanged)
        else:
            self.setEnabled(True)
            self.connect(self, SIGNAL('stateChanged(int)'), self.valueChanged)

    def value(self):
        """ Returns True if check box is checked.
        """
        return self.isChecked()

class DropDownProperty(Property, QComboBox):
    """ Property holding a check box for boolean values.
    """

    USER_INFO = "Drop down field"

    def __init__(self, name, value, values, categoryName=None):
        """ Constructor.
        """
        Property.__init__(self, name, categoryName)
        QComboBox.__init__(self)
        self._values = values
        for v in values:
            self.addItem(str(v))
        if value in values:
            self.setCurrentIndex(values.index(value))
        self.connect(self, SIGNAL('currentIndexChanged(int)'), self.valueChanged)

    def setReadOnly(self, readOnly):
        """ Disables editing functionality.
        """
        if readOnly:
            self.setEnabled(False)
            self.disconnect(self, SIGNAL('currentIndexChanged(int)'), self.valueChanged)
        else:
            self.setEnabled(True)
            self.connect(self, SIGNAL('currentIndexChanged(int)'), self.valueChanged)

    def value(self):
        """ Returns True if check box is checked.
        """
        return self._values[self.currentIndex()]


class TextSelectionProperty(Property, QComboBox):
    """ Property holding a combo box for string values. The last item of
    the list is the current selected value
    """

    USER_INFO = "Drop down field"

    def __init__(self, name, value, categoryName=None):
        """ Constructor.
        """
        logging.debug('TextSelectionProperty created')
        Property.__init__(self, name, categoryName)
        QComboBox.__init__(self)
        # values is a string like "['a','b']"
        v = eval(value)

        self._values = v[:-1]
        self.__selectedValue = v[-1]
        for v in self._values:
            self.addItem(str(v))
        if self.__selectedValue in self._values:
            self.setCurrentIndex(self._values.index(self.__selectedValue))
        self.connect(self, SIGNAL('currentIndexChanged(int)'), self.valueChanged)

    def setReadOnly(self, readOnly):
        """ Disables editing functionality.
        """
        if readOnly:
            self.setEnabled(False)
            self.disconnect(self, SIGNAL('currentIndexChanged(int)'), self.valueChanged)
        else:
            self.setEnabled(True)
            self.connect(self, SIGNAL('currentIndexChanged(int)'), self.valueChanged)

    def value(self):
        """ Returns the list of values with the last item set to the
        selected value 
        """
        l = "','".join(self._values)
        l+="','"
        l+=self._values[self.currentIndex()]
        return "['" + l + "']"




class TextEdit(QTextEdit):
    def focusOutEvent(self, event):
        QTextEdit.focusOutEvent(self, event)
        self.emit(SIGNAL("editingFinished()"))

class TextEditWithButtonProperty(Property, QWidget):
    """ This class provides a PropertyView property holding an editable text and a button.
    
    It is possible to hide the button unless the mouse cursor is over the property. This feature is turned on by default. See setAutohideButton().
    If the button is pressed nothing happens. This functionality should be implemented in sub-classes. See buttonClicked().
    The text field can hold single or multiple lines. See setMultiline()
    """

    BUTTON_LABEL = ''
    AUTOHIDE_BUTTON = True

    def __init__(self, name, value, categoryName=None, multiline=False):
        """ The constructor creates a QHBoxLayout and calls createLineEdit(), createTextEdit() and createButton(). 
        """
        Property.__init__(self, name, categoryName)
        QWidget.__init__(self)
        self._lineEdit = None
        self._textEdit = None
        self._button = None
        self.setAutohideButton(self.AUTOHIDE_BUTTON)

        self.setLayout(QHBoxLayout())
        self.layout().setSpacing(0)
        self.layout().setContentsMargins(0, 0, 0, 0)

        self._readOnly = False
        self._multiline = False

        self.createLineEdit()
        self.createTextEdit()
        self.createButton()
        self.setMultiline(multiline)
        self.setValue(value)

    def setValue(self, value):
        """ Sets value of text edit.
        """
        #logging.debug("%s: setValue()" % (self.__class__.__name__))
        self._originalValue = value
        if value != None:
            strValue = str(value)
        else:
            strValue = ""
        if not self._readOnly:
            self.disconnect(self._lineEdit, SIGNAL('editingFinished()'), self.valueChanged)
            self.disconnect(self._textEdit, SIGNAL('editingFinished()'), self.valueChanged)
        self._lineEdit.setText(strValue)
        self._textEdit.setText(strValue)
        self.setToolTip("<FONT COLOR=black>" + strValue + "</FONT>")
        if not self._readOnly:
            self.connect(self._lineEdit, SIGNAL('editingFinished()'), self.valueChanged)
            self.connect(self._textEdit, SIGNAL('editingFinished()'), self.valueChanged)
        # TODO: sometimes when changing value the text edit appears to be empty when new text is shorter than old text
        #if not self._multiline:
        #    self._textEdit.setCursorPosition(self._textEdit.displayText().length())

    def setToolTip(self, text):
        self._lineEdit.setToolTip("<FONT COLOR=black>" + text + "</FONT>")
        self._textEdit.setToolTip("<FONT COLOR=black>" + text + "</FONT>")

    def setMultiline(self, multi):
        """ Switch between single and multi line mode.
        """
        self.setValue(self.strValue())
        self._multiline = multi
        if self._multiline:
            self._textEdit.show()
            self._lineEdit.hide()
            self.setFocusProxy(self._textEdit)
        else:
            self._lineEdit.show()
            self._textEdit.hide()
            self.setFocusProxy(self._lineEdit)

    def createLineEdit(self, value=None):
        """ This function creates the signle line text field and adds it to the property's layout. 
        """
        self._lineEdit = QLineEdit(self)
        self._lineEdit.setFrame(False)
        self.connect(self._lineEdit, SIGNAL('editingFinished()'), self.valueChanged)
        self._lineEdit.setContentsMargins(0, 0, 0, 0)
        self._lineEdit.setSizePolicy(QSizePolicy(QSizePolicy.Expanding, QSizePolicy.MinimumExpanding))
        self.layout().addWidget(self._lineEdit)

    def createTextEdit(self, value=None):
        """ This function creates the multi line text field and adds it to the property's layout. 
        """
        self._textEdit = TextEdit(self)
        self._textEdit.setWordWrapMode(QTextOption.NoWrap)
        self._textEdit.setFrameStyle(QFrame.NoFrame)
        self.connect(self._textEdit, SIGNAL('editingFinished()'), self.valueChanged)
        self._textEdit.setContentsMargins(0, 0, 0, 0)
        self._textEdit.setSizePolicy(QSizePolicy(QSizePolicy.Expanding, QSizePolicy.MinimumExpanding))
        self.layout().addWidget(self._textEdit)

    def propertyHeight(self):
        """ Return the estimated height of the property.
        
        The returned height covers the whole text, even if multiline.
        """
        if self._multiline:
            self._textEdit.document().adjustSize()
            height = self._textEdit.document().size().height() + 3
            if self._textEdit.horizontalScrollBar().isVisible():
                height += self._textEdit.horizontalScrollBar().height() + 3
            return max(height, 80)
        else:
            return self.DEFAULT_HEIGHT

    def lineEdit(self):
        """ Returns line edit.
        """
        return self._lineEdit

    def textEdit(self):
        """ Returns text edit.
        """
        return self._textEdit

    def createButton(self):
        """ Creates a button and adds it to the property's layout.
        """
        self._button = QToolButton(self)
        self._button.setText(self.BUTTON_LABEL)
        self._button.setContentsMargins(0, 0, 0, 0)
        self.connect(self._button, SIGNAL('clicked(bool)'), self.buttonClicked)
        self.layout().addWidget(self._button)

        if self.autohideButtonFlag:
            self._button.hide()

    def button(self):
        """ Return button.
        """
        return self._button

    def hasButton(self):
        """ Returns True if the button has been created, otherwise False is returned. 
        """
        return self._button != None

    def setReadOnly(self, readOnly):
        """ Switch between readonly and editable.
        """
        self._readOnly = readOnly
        if not self.lineEdit().isReadOnly() and readOnly:
            self.disconnect(self._lineEdit, SIGNAL('editingFinished()'), self.valueChanged)
            self.disconnect(self._textEdit, SIGNAL('editingFinished()'), self.valueChanged)
            if self.hasButton():
                self._button = None
        if self.lineEdit().isReadOnly() and not readOnly:
            self.connect(self._lineEdit, SIGNAL('editingFinished()'), self.valueChanged)
            self.connect(self._textEdit, SIGNAL('editingFinished()'), self.valueChanged)
            if not self.hasButton():
                self.createButton()
        self.lineEdit().setReadOnly(readOnly)
        self.textEdit().setReadOnly(readOnly)

    def readOnly(self):
        return self._readOnly

    def strValue(self):
        """ Returns value of text edit.
        """
        if not self._multiline:
            return str(self._lineEdit.text().toAscii())
        else:
            return str(self._textEdit.toPlainText().toAscii())
        return ""

    def value(self):
        """ Returns the value of correct type (in case its not a string).
        """
        return self.strValue()

    def setAutohideButton(self, hide):
        """ If hide is True the button will only be visible while the cursor is over the property. 
        """
        self.autohideButtonFlag = hide

    def buttonClicked(self, checked=False):
        """
        This function is called if the button was clicked. For information on the checked argument see documentation of QPushButton::clicked().
        This function should be overwritten by sub-classes.
        """
        pass

    def enterEvent(self, event):
        """ If autohideButtonFlag is set this function makes the button visible. See setAutohideButton(). 
        """
        if self.autohideButtonFlag and self.hasButton() and not self._readOnly:
            self._button.show()

    def leaveEvent(self, event):
        """ If autohideButtonFlag is set this function makes the button invisible. See setAutohideButton(). 
        """
        if self.autohideButtonFlag and self.hasButton() and not self._readOnly:
            self._button.hide()

    def valueChanged(self):
        """ Update tooltip and height when text is changed.
        """
        logging.debug("%s: valueChanged()" % (self.__class__.__name__))
        if self._multiline:
            self.emit(SIGNAL('updatePropertyHeight'), self)
        self.setToolTip("<FONT COLOR=black>" + self.strValue() + "</FONT>")
        # set property only if button is not being pressed
        if not self.button() or not self.button().isDown():
            Property.valueChanged(self)

    def setHighlighted(self, highlight, color=Qt.red):
        """ Highlight the property by changing the background color of the textfield.
        """
        p = QPalette()
        if highlight:
            p.setColor(QPalette.Active, QPalette.ColorRole(9), color)
        else:
            p.setColor(QPalette.Active, QPalette.ColorRole(9), Qt.white)
        self._lineEdit.setPalette(p)
        self._textEdit.viewport().setPalette(p)

    def keyPressEvent(self, event):
        """ Switch back to the original value on ESC.
        """
        QWidget.keyPressEvent(self, event)
        if event.key() == Qt.Key_Escape:
            self.setValue(self._originalValue)
            event.accept()
            self.valueChanged()


class StringProperty(TextEditWithButtonProperty):
    """ Property which holds an editable text.
    
    A button is provided to switch between single and multi line mode. 
    """

    BUTTON_LABEL = '...'
    USER_INFO = "Text field"

    AUTHIDE_BUTTON = False

    def __init__(self, name, value, categoryName=None, multiline=None):
        """ Constructor """
        TextEditWithButtonProperty.__init__(self, name, value, categoryName, (multiline or str(value).count("\n") > 0))

    def setMultiline(self, multiline):
        TextEditWithButtonProperty.setMultiline(self, multiline)
        icon = QIcon(":/resources/editor.svg")
        dummyicon = QIcon()
        self._button.setIcon(icon)
        self._button.setIconSize(QSize(15, 15))

    def buttonClicked(self):
        """ Switch to multiline mode if button is clicked.
        """
        dialog = TextDialog(self, "Edit property...", self.strValue())
        if dialog.exec_():
            if not self._multiline:
                self.setMultiline(True)
            textEdit = dialog.getText()
            self.setValue(textEdit)
            self.valueChanged()


class IntegerProperty(Property, QWidget):
    """ Property which hold editable integer numbers.
    
    A Spinbox is provided when the property is editable.
    """

    USER_INFO = "Integer field"

    def __init__(self, name, value, categoryName=None):
        """ Constructor
        """
        Property.__init__(self, name, categoryName)
        QWidget.__init__(self)
        self.setLayout(QHBoxLayout())
        self.layout().setSpacing(0)
        self.layout().setContentsMargins(0, 0, 0, 0)

        self._spinbox = QSpinBox()
        #self.maxint = sys.maxint     # does not work on Mac OS X (Snow Leopard 10.6.2), confusion between 32 and 64 bit limits
        self.maxint = 2 ** 31
        self._spinbox.setRange(-self.maxint + 1, self.maxint - 1)
        self._spinbox.setFrame(False)
        self.layout().addWidget(self._spinbox)
        self.setFocusProxy(self._spinbox)
        self._lineedit = QLineEdit()
        self._lineedit.setReadOnly(True)
        self._lineedit.setFrame(False)
        self._lineedit.setContentsMargins(0, 0, 0, 0)
        self.layout().addWidget(self._lineedit)
        self._lineedit.hide()
        self._readOnly = False

        self.setValue(value)
        self.connect(self._spinbox, SIGNAL('valueChanged(int)'), self.valueChanged)

    def setReadOnly(self, readOnly):
        """ Switches between lineedit and spinbox.
        """
        if readOnly:
            self._spinbox.hide()
            self._lineedit.show()
            self.setFocusProxy(self._lineedit)
            self._readOnly = True
        else:
            self._spinbox.show()
            self._lineedit.hide()
            self.setFocusProxy(self._spinbox)

    def value(self):
        """ Returns integer value.
        """
        return self._spinbox.value()

    def setValue(self, value):
        self.disconnect(self._spinbox, SIGNAL('valueChanged(int)'), self.valueChanged)
        self._spinbox.setValue(value)
        self._lineedit.setText(str(value))
        self.connect(self._spinbox, SIGNAL('valueChanged(int)'), self.valueChanged)

class DoubleProperty(TextEditWithButtonProperty):
    """ TextEditWithButtonProperty which holds float numbers.
    """

    USER_INFO = "Double field"

    AUTHIDE_BUTTON = False

    def __init__(self, name, value, categoryName=None):
        """ Constructor
        """
        TextEditWithButtonProperty.__init__(self, name, value, categoryName=None)

    def createButton(self):
        """ Do not create a button."""
        pass

    def _toString(self, object):
        if isinstance(object, float):
            return "%.10g" % object
        else:
            return str(object)

    def setValue(self, value):
        TextEditWithButtonProperty.setValue(self, self._toString(value))

    def value(self):
        """ Transform text to float and return.
        """
        try:
            return float(TextEditWithButtonProperty.value(self))
        except:
            try:
                return float.fromhex(TextEditWithButtonProperty.value(self))
            except:
                return ValueError("Entered value is not of type double.")

class FileProperty(TextEditWithButtonProperty):
    """ TextEditWithButtonProperty which holds file names.
    
    A button for opening a dialog allowing to choose a file is provided.
    """

    USER_INFO = "Select a file. Double click on label to open file."
    BUTTON_LABEL = '...'

    def __init__(self, name, value, categoryName=None, isInputFile=False):
        self._relativePath = None
        self._isInputFile = isInputFile
        TextEditWithButtonProperty.__init__(self, name, value, categoryName)
        self.button().setToolTip("<FONT COLOR=black>" + self.userInfo() + "</FONT>")
        self._checkbox = QCheckBox(self)
        self._checkbox.setContentsMargins(0, 0, 0, 0)
        self._checkbox.setToolTip("Check the box for relative paths. Otherwise absolute paths are used.")
        self.connect(self._checkbox, SIGNAL('stateChanged(int)'), self.checkStateChanged)
        self.layout().addWidget(self._checkbox)
        self._checkbox.hide()
        if self._isInputFile:
            self.checkExists()

    def buttonClicked(self, checked=False):
        """ Shows the file selection dialog. """
        if self.value() != "":
            if self._relativePath and self._checkbox.checkState() == Qt.Checked:
                dir = absolute_path(os.path.join(self._relativePath, self.value()))
            else:
                dir = self.value()
        else:
            dir = ""
        if self._isInputFile:
            filename = guiFacade.showFileOpenDialog(dir, '', None, QFileDialog.DontConfirmOverwrite)
        else:
            filename = guiFacade.showFileSaveDialog(dir, '', None, QFileDialog.DontConfirmOverwrite)
        if not filename == "":
            filename = str(filename)
            if self._relativePath and self._checkbox.checkState() == Qt.Checked:
                filename = relative_path(filename, self._relativePath)
            self.setValue(filename)
            self.textEdit().emit(SIGNAL('editingFinished()'))

    def labelDoubleClicked(self):
        """ Open selected file in default application.
        """
        if isinstance(self.propertyView().parent(), AbstractTab):
            self.propertyView().parent().mainWindow().application().doubleClickOnFile(self.value())

    def useRelativePaths(self, path):
        self._relativePath = path
        if self._relativePath == None:
            self._checkbox.setEnabled(False)
            self._checkbox.setCheckState(Qt.Unchecked)
            self._checkbox.setToolTip("No absolute path specified!!! Cannot calculate relative paths. Please save first.")
        else:
            self._checkbox.setEnabled(True)
            if is_relative_path(self._originalValue):
                self._checkbox.setCheckState(Qt.Checked)
            else:
                self._checkbox.setCheckState(Qt.Unchecked)
        if self._isInputFile:
            self.checkExists()

    def valueChanged(self):
        TextEditWithButtonProperty.valueChanged(self)
        if is_relative_path(self._originalValue):
            self._checkbox.setCheckState(Qt.Checked)
        else:
            self._checkbox.setCheckState(Qt.Unchecked)
        if self._isInputFile:
            self.checkExists()

    def checkExists(self):
        if self._relativePath and self._checkbox.checkState() == Qt.Checked:
            exists = os.path.exists(absolute_path(os.path.join(self._relativePath, self.value())))
        else:
            exists = os.path.exists(self.value())
        self.setHighlighted(not exists, Qt.yellow)

    def checkStateChanged(self):
        if self._checkbox.checkState() == Qt.Checked and not is_relative_path(self._originalValue):
            self.disconnect(self._checkbox, SIGNAL('stateChanged(int)'), self.checkStateChanged)
            self.setValue(relative_path(self._originalValue, self._relativePath))
            self.connect(self._checkbox, SIGNAL('stateChanged(int)'), self.checkStateChanged)
            self.textEdit().emit(SIGNAL('editingFinished()'))
        elif not self._checkbox.checkState() == Qt.Checked and is_relative_path(self._originalValue):
            self.disconnect(self._checkbox, SIGNAL('stateChanged(int)'), self.checkStateChanged)
            self.setValue(absolute_path(os.path.join(self._relativePath, self._originalValue)))
            self.connect(self._checkbox, SIGNAL('stateChanged(int)'), self.checkStateChanged)
            self.textEdit().emit(SIGNAL('editingFinished()'))

    def enterEvent(self, event):
        """ If autohideButtonFlag is set this function makes the button visible. See setAutohideButton(). 
        """
        TextEditWithButtonProperty.enterEvent(self, event)
        self._checkbox.show()

    def leaveEvent(self, event):
        """ If autohideButtonFlag is set this function makes the button invisible. See setAutohideButton(). 
        """
        TextEditWithButtonProperty.leaveEvent(self, event)
        self._checkbox.hide()

class FileVectorProperty(TextEditWithButtonProperty):
    """ TextEditWithButtonProperty which holds file names.
    
    A button for opening a dialog allowing to choose a list of files is provided.
    """

    USER_INFO = "Edit list of files."
    BUTTON_LABEL = '...'

    def __init__(self, name, value, categoryName=None, isInputFile=False):
        self._relativePath = None
        self._isInputFile = isInputFile
        TextEditWithButtonProperty.__init__(self, name, value, categoryName)
        self.button().setToolTip("<FONT COLOR=black>" + self.userInfo() + "</FONT>")
        self._checkbox = QCheckBox(self)
        self._checkbox.setContentsMargins(0, 0, 0, 0)
        self._checkbox.setToolTip("Check the box for relative paths. Otherwise absolute paths are used.")
        self.connect(self._checkbox, SIGNAL('stateChanged(int)'), self.checkStateChanged)
        self.layout().addWidget(self._checkbox)
        self._checkbox.hide()

    def buttonClicked(self, checked=False):
        """ Shows the file selection dialog. """
        value = self.value()
        try:
            value = eval(value)
        except:
            pass
        if self._isInputFile:
            type = "InputFile"
        else:
            type = "OutputFile"
        
        update = False
        if (value == [] or value == ()):
            value = self.openMulti()
            update = True
            
        dialog = PropertyVectorDialog(self, "Edit file list...", value, type, True, self._relativePath)
        if dialog.exec_():
            value = [p.value() for p in dialog.propertyWidgets()]
            update = True
        
        if update:
            self.setValue(value)
            self.textEdit().emit(SIGNAL('editingFinished()'))
        return

    def openMulti(self):
        value = self.value()
        try:
            value = eval(value)
        except:
            pass
        if type(value) == list and len(value) > 0:
            if self._relativePath and self._checkbox.checkState() == Qt.Checked:
                dir = absolute_path(os.path.join(self._relativePath, os.path.dirname(value[0])))
            else:
                dir = os.path.dirname(value[0])
        else:
            dir = ""
        if self._isInputFile:
            fileNames = guiFacade.showFilesOpenDialog(dir, '', None, QFileDialog.DontConfirmOverwrite)
        else:
            fileNames = guiFacade.showFilesSaveDialog(dir, '', None, QFileDialog.DontConfirmOverwrite)
        if self._relativePath and self._checkbox.checkState() == Qt.Checked:
            nfileNames = []
            for v in fileNames:
                nfileNames += [relative_path(v, self._relativePath)]
            fileNames = nfileNames
        return fileNames

    def isBusy(self):
        return self._updatingFlag > 0

    def useRelativePaths(self, path):
        value = self.value()
        try:
            value = eval(value)
        except:
            pass
        self._relativePath = path
        if self._relativePath == None:
            self._checkbox.setEnabled(False)
            self._checkbox.setCheckState(Qt.Unchecked)
            self._checkbox.setToolTip("No absolute path specified!!! Cannot calculate relative paths. Please save first.")
        else:
            self._checkbox.setEnabled(True)
            if len(value) > 0:
                if is_relative_path(value[0]):
                    self._checkbox.setCheckState(Qt.Checked)
                else:
                    self._checkbox.setCheckState(Qt.Unchecked)
        if self._isInputFile:
            self.checkExists()

    def valueChanged(self):
        TextEditWithButtonProperty.valueChanged(self)
        if len(self._originalValue) > 0:
            if is_relative_path(self._originalValue[0]):
                self._checkbox.setCheckState(Qt.Checked)
            else:
                self._checkbox.setCheckState(Qt.Unchecked)
        if self._isInputFile:
            self.checkExists()

    def value(self):
        """ Returns the value of correct type (in case its not a string).
        """
        TextEditWithButtonProperty.value(self)
        try:
            value = eval(self.strValue())
        except Exception, e:
            return ValueError(str(e))
        if not isinstance(value, (list, tuple)):
            return ValueError("Entered value is not a list.")
        return self.strValue()

    def checkExists(self):
        value = self.value()
        try:
            value = eval(value)
        except:
            pass
        exists = True
        for v in value:
            if self._relativePath and self._checkbox.checkState() == Qt.Checked:
                if not os.path.exists(absolute_path(os.path.join(self._relativePath, v))):
                    exists = False
            else:
                if not os.path.exists(v):
                    exists = False
        self.setHighlighted(not exists, Qt.yellow)

    def checkStateChanged(self):
        value = self.value()
        try:
            value = eval(value)
        except:
            pass
        
        if type(value) == list and len(value) > 0:
          if self._checkbox.checkState() == Qt.Checked and not is_relative_path(value[0]):
            self.disconnect(self._checkbox, SIGNAL('stateChanged(int)'), self.checkStateChanged)
            nfileNames = []
            for v in value:
                if not is_relative_path(v):
                    nfileNames += [relative_path(v, self._relativePath)]
                else:
                    nfileNames += [v]
            self.setValue(nfileNames)
            self.connect(self._checkbox, SIGNAL('stateChanged(int)'), self.checkStateChanged)
            self.textEdit().emit(SIGNAL('editingFinished()'))
          elif not self._checkbox.checkState() == Qt.Checked and is_relative_path(value[0]):
            self.disconnect(self._checkbox, SIGNAL('stateChanged(int)'), self.checkStateChanged)
            nfileNames = []
            for v in value:
                if is_relative_path(v):
                    nfileNames += [absolute_path(os.path.join(self._relativePath, v))]
                else:
                    nfileNames += [v]
            self.setValue(nfileNames)
            self.connect(self._checkbox, SIGNAL('stateChanged(int)'), self.checkStateChanged)
            self.textEdit().emit(SIGNAL('editingFinished()'))

    def enterEvent(self, event):
        """ If autohideButtonFlag is set this function makes the button visible. See setAutohideButton(). 
        """
        TextEditWithButtonProperty.enterEvent(self, event)
        self._checkbox.show()

    def leaveEvent(self, event):
        """ If autohideButtonFlag is set this function makes the button invisible. See setAutohideButton(). 
        """
        TextEditWithButtonProperty.leaveEvent(self, event)
        self._checkbox.hide()

class StringVectorProperty(TextEditWithButtonProperty):
    """ TextEditWithButtonProperty which holds a vector of strings.
    
    A button for opening a dialog allowing to choose a list of strings is provided.
    """

    USER_INFO = "Edit list of strings."
    BUTTON_LABEL = '...'

    def __init__(self, name, value, categoryName=None):
        TextEditWithButtonProperty.__init__(self, name, value, categoryName)
        self.button().setToolTip("<FONT COLOR=black>" + self.userInfo() + "</FONT>")

    def buttonClicked(self, checked=False):
        """ Shows the file selection dialog. """
        value = self.value()
        try:
            value = eval(value)
        except:
            pass
        type = "String"
        dialog = PropertyVectorDialog(self, "Edit string list...", value, type)
        if dialog.exec_():
            fileNames = [p.value() for p in dialog.propertyWidgets()]
            self.setValue(fileNames)
            self.textEdit().emit(SIGNAL('editingFinished()'))

    def value(self):
        """ Returns the value of correct type (in case its not a string).
        """
        TextEditWithButtonProperty.value(self)
        try:
            value = eval(self.strValue())
        except Exception, e:
            return ValueError(str(e))
        if not isinstance(value, (list, tuple)):
            return ValueError("Entered value is not a list.")
        return self.strValue()

class ClosableProperty(QWidget, Property):
    def __init__(self, property, autoHide=True):
        QWidget.__init__(self)
        Property.__init__(self, None, None)
        self.setContentsMargins(0, 0, 0, 0)
        self.setLayout(QHBoxLayout())
        self.layout().setSpacing(0)
        self.layout().setContentsMargins(0, 0, 0, 0)
        self.layout().addWidget(property)
        self._closeButton = QToolButton()
        self._closeButton.setText("x")
        self._autoHide = autoHide
        if self._autoHide:
            self._closeButton.hide()
        self._property = property
        self.layout().addWidget(self._closeButton)

    def name(self):
        return self._property.name()

    def categoryName(self):
        return self._property.categoryName()

    def rowNumber(self):
        return self._property.rowNumber()

    def setRowNumber(self, number):
        self._property.setRowNumber(number)

    def value(self):
        return self._property.value()

    def setValue(self, value):
        self._property.value(value)

    def closableProperty(self):
        return self._property
    def closeButton(self):
        return self._closeButton
    def enterEvent(self, event):
        if self._autoHide:
            self._closeButton.show()
    def leaveEvent(self, event):
        if self._autoHide:
            self._closeButton.hide()

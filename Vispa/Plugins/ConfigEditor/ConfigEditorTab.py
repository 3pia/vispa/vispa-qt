import sys
import logging
import os.path
import copy

from PyQt4.QtCore import SIGNAL,QString,QCoreApplication, Qt
from PyQt4.QtGui import QWidget, QListWidget, QVBoxLayout, QPushButton,QToolButton, QSplitter, QMessageBox,QFileDialog

from Vispa.Main.SplitterTab import SplitterToolBar
from Vispa.Main.Preferences import *
from Vispa.Main.Application import Application
from Vispa.Main.Exceptions import exception_traceback
from Vispa.Main.GuiFacade import guiFacade
from Vispa.Gui.ToolBoxContainer import ToolBoxContainer
from Vispa.Gui.TextDialog import TextDialog
from Vispa.Share.ThreadChain import ThreadChain
from Vispa.Views.WidgetView import WidgetView
from Vispa.Plugins.Browser.BrowserTab import BrowserTab
from Vispa.Plugins.ConfigEditor.CodeTableView import CodeTableView
from Vispa.Plugins.ConfigEditor.ConfigEditorBoxView import ConfigEditorBoxView,ConnectionStructureView,SequenceStructureView

from Vispa.Main.GuiFacade import guiFacade

try:
    from FWCore.GuiBrowsers.DOTExport import DotExport
    import_dotexport_error=None
except Exception,e:
    import_dotexport_error=(str(e),exception_traceback())

try:
    from Vispa.Plugins.EdmBrowser.EventContentDialog import EventContentDialog
    event_content_error=None
except Exception,e:
    event_content_error=(str(e),exception_traceback())

try:
    from ToolDataAccessor import ToolDataAccessor,ConfigToolBase,standardConfigDir
    from ToolDialog import ToolDialog
    import_tools_error=None
except Exception,e:
    import_tools_error=(str(e),exception_traceback())

class ConfigEditorTab(BrowserTab):
    """ This is the main frame of the Config Editor Plugin.
    
    The tab is split in three horizontal parts, from left to right: Tree View, Center View, Property View.
    The editor can be dispayed using createEditor(). The Property View is then shared with the ConfigBrowser.
    """
    
    def __init__(self, plugin, parent=None):
        """ constructor """
        logging.debug(self.__class__.__name__ +": __init__()")
        self._updateCenterView=False
        BrowserTab.__init__(self, plugin, parent, True)
        self._editorSplitter = None
    
        self._editorName = ""
        self._thread = None
        self._originalSizes=[100,1,200]
        self._toolDialog=None
        self.setEditable(False)
        
        self._configMenu = guiFacade.createPluginMenu('&Config')
        self._configToolBar = guiFacade.createPluginToolBar('&Config')
        openEditorAction = guiFacade.createAction('&Open in custom editor', self.openEditor, "F6")
        self._configMenu.addAction(openEditorAction)
        chooseEditorAction = guiFacade.createAction('&Choose custom editor...', self.chooseEditor, "Ctrl+T")
        self._configMenu.addAction(chooseEditorAction)
        self._configMenu.addSeparator()
        self._dumpAction = guiFacade.createAction('&Dump python config to single file...', self.dumpPython, "Ctrl+D")
        self._configMenu.addAction(self._dumpAction)
        self._dotExportAction = guiFacade.createAction('&Export dot graphic...', self.exportDot, "Ctrl+G")
        self._configMenu.addAction(self._dotExportAction)
        self._historyAction = guiFacade.createAction('&Show history...', self.history, "Ctrl+H")
        self._configMenu.addAction(self._historyAction)
        self._eventContentAction = guiFacade.createAction('&Browse event content...', self.eventContent, "Ctrl+Shift+C")
        self._configMenu.addAction(self._eventContentAction)
        self._configMenu.addSeparator()
        self._editorAction = guiFacade.createAction('&Edit using ConfigEditor', self.startEditMode, "F8")
        self._configMenu.addAction(self._editorAction)
        self._configToolBar.addAction(self._editorAction)
        
    def createEditor(self):
        self.createToolBar(0)
        self._editorSplitter = QSplitter(Qt.Horizontal)

        toolBarSectionId = self.toolBar().addSection(SplitterToolBar.ALIGNMENT_LEFT)

        self._editorTableView=CodeTableView(self._editorSplitter)
        
        self._editorSplitter.setSizes([100, 300])

        self._minimizeButton = QToolButton()
        self._minimizeButton.setText("v")
        self._minimizeButton.setCheckable(True)
        self._originalButton = QToolButton()
        self._originalButton.setText("-")
        self._originalButton.setCheckable(True)
        self._originalButton.setChecked(True)
        self._maximizeButton = QToolButton()
        self._maximizeButton.setText("^")
        self._maximizeButton.setCheckable(True)

        self._centerViewToolBarId = self.toolBar().addSection(SplitterToolBar.ALIGNMENT_CENTER)
        self.toolBar().addWidgetToSection(self._minimizeButton, self._centerViewToolBarId)
        self.toolBar().addWidgetToSection(self._originalButton, self._centerViewToolBarId)
        self.toolBar().addWidgetToSection(self._maximizeButton, self._centerViewToolBarId)
    
        self.verticalSplitter().insertWidget(0,self._editorSplitter)
        self.updateToolBarSizes()
            
        self.connect(self._minimizeButton, SIGNAL('clicked(bool)'), self.minimizeEditor)
        self.connect(self._originalButton, SIGNAL('clicked(bool)'), self.originalEditor)
        self.connect(self._maximizeButton, SIGNAL('clicked(bool)'), self.maximizeEditor)
        
    def editorSplitter(self):
        return self._editorSplitter
        
    def horizontalSplitterMovedSlot(self, pos, index):
        if self.toolBar(): 
            self.updateToolBarSizes()
        
    def updateToolBarSizes(self):
        self.toolBar().setSectionSizes(self.horizontalSplitter().sizes())

    def minimizeButton(self):
        return self._minimizeButton

    def originalButton(self):
        return self._originalButton

    def maximizeButton(self):
        return self._maximizeButton

    def editorTableView(self):
        return self._editorTableView
    
    #@staticmethod
    def staticSupportedFileTypes():
        """ Returns supported file type: py.
        """
        return [('py', 'Config file')]
    staticSupportedFileTypes = staticmethod(staticSupportedFileTypes)
    
    def dotExportAction(self):
        return self._dotExportAction
    
    def updateViewMenu(self):
        BrowserTab.updateViewMenu(self)
        self.disconnect(self.centerView(), SIGNAL("doubleClicked"), self.onCenterViewDoubleClicked)
        self.connect(self.centerView(), SIGNAL("doubleClicked"), self.onCenterViewDoubleClicked)

    def onCenterViewDoubleClicked(self,object):
        logging.debug(__name__ + ": onCenterViewDoubleClicked()")
        self.treeView().select(object)
        self.onTreeViewSelected(object)

    def updateCenterView(self, propertyView=True):
        """ Fill the center view from an item in the TreeView and update it """
        if not self._updateCenterView:
            # Do not update the very first time
            self._updateCenterView=True
            return
        statusMessage = self.statusBarStartMessage("Updating center view")
        if propertyView:
            self.selectDataAccessor(True)
        else:
            self.selectDataAccessor(self.propertyView().dataObject())
        if self._thread != None and self._thread.isRunning():
            self.dataAccessor().cancelOperations()
            while self._thread.isRunning():
                if not Application.NO_PROCESS_EVENTS:
                    QCoreApplication.instance().processEvents()
        objects = []
        select=self.treeView().selection()
        if select != None:
            if self.currentCenterViewClassId() == self.plugin().viewClassId(ConnectionStructureView):
                self.centerView().setArrangeUsingRelations(True)
                if self.centerView().checkNumberOfObjects():
                    if self.dataAccessor().isContainer(select):
                        self._thread = ThreadChain(self.dataAccessor().readConnections, [select]+self.dataAccessor().allChildren(select))
                    else:
                        self._thread = ThreadChain(self.dataAccessor().readConnections, [select], True)
                    while self._thread.isRunning():
                        if not Application.NO_PROCESS_EVENTS:
                            QCoreApplication.instance().processEvents()
                    self.centerView().setConnections(self._thread.returnValue())
                    self.centerView().setDataObjects(self.dataAccessor().nonSequenceChildren(select))
                else:
                    self.centerView().setDataObjects([])
            elif self.currentCenterViewClassId() == self.plugin().viewClassId(SequenceStructureView):
                self.centerView().setArrangeUsingRelations(False)
                self.centerView().setDataObjects([select])
                self.centerView().setConnections({})
        if (self.currentCenterViewClassId() == self.plugin().viewClassId(ConnectionStructureView) or self.currentCenterViewClassId() == self.plugin().viewClassId(SequenceStructureView)) and \
            self.centerView().updateContent(True):
            if not self.dataAccessor().isContainer(select) and self.currentCenterViewClassId() == self.plugin().viewClassId(ConnectionStructureView):
                    self.centerView().select(select,500)
            else:
                self.centerView().restoreSelection()
            select = self.centerView().selection()
            if select != None:
                if self.propertyView().dataObject() != select and propertyView:
                    self.propertyView().setDataObject(select)
                    self.propertyView().updateContent()
        if import_tools_error==None and self.editorSplitter():
            self.updateConfigHighlight()
        self.statusBarStopMessage(statusMessage)
        
    def activated(self):
        """ Shows plugin menus when user selects tab.
        """
        logging.debug(__name__ + ": activated()")
        BrowserTab.activated(self)
        guiFacade.showPluginMenu(self._configMenu)
        guiFacade.showPluginToolBar(self._configToolBar)
        self._editorAction.setVisible(not self.editorSplitter())
        if self.editorSplitter():
            self._applyPatToolAction.setVisible(self.dataAccessor().process()!=None)
        guiFacade.showZoomToolBar()

    def openEditor(self):
        """ Call editor """
        logging.debug(__name__ + ": openEditor")
        selected_object = self.propertyView().dataObject()
        filename = self.dataAccessor().fullFilename(selected_object)
        if self._editorName != "" and selected_object != None and os.path.exists(filename):
            if os.path.expandvars("$CMSSW_RELEASE_BASE") in filename:
                QMessageBox.information(self, "Opening readonly file...", "This file is from $CMSSW_RELEASE_BASE and readonly") 
            command = self._editorName
            command += " " + filename
            command += " &"
            os.system(command)

    def chooseEditor(self, _editorName=""):
        """ Choose editor using FileDialog """
        logging.debug(__name__ + ": chooseEditor")
        if _editorName == "":
            _editorName = str(QFileDialog.getSaveFileName(self, "Choose editor", self._editorName, "Editor (*)", None , QFileDialog.DontConfirmOverwrite or QFileDialog.DontResolveSymlinks))
            if not os.path.exists(_editorName):
                _editorName = os.path.basename(_editorName)
        if _editorName != None and _editorName != "":
            self._editorName = _editorName
        self.saveIni()

    def dumpPython(self, fileName=None):
        """ Dump python configuration to file """
        logging.debug(__name__ + ": dumpPython")
        dump = self.dataAccessor().dumpPython()
        if dump == None:
            logging.error(self.__class__.__name__ +": dumpPython() - "+"Cannot dump this config because it does not contain a 'process'.\nNote that only 'cfg' files contain a 'process'.")
            self.showErrorMessage("Cannot dump this config because it does not contain a 'process'.\nNote that only 'cfg' files contain a 'process'.")
            return None
        filter = QString("")
        if not fileName:
            defaultname = os.path.splitext(self._filename)[0] + "_dump" + os.path.splitext(self._filename)[1]
            fileName = str(QFileDialog.getSaveFileName(self, "Save python config...", defaultname, "Python config (*.py)", filter))
        if fileName != "":
            name = fileName
            ext = "PY"
            if os.path.splitext(fileName)[1].upper().strip(".") == ext:
                name = os.path.splitext(fileName)[0]
                ext = os.path.splitext(fileName)[1].upper().strip(".")
            text_file = open(name + "." + ext.lower(), "w")
            text_file.write(dump)
            text_file.close()

    def history(self):
        """ Show config history """
        logging.debug(__name__ + ": history")
        history = self.dataAccessor().history()
        if history == None:
            logging.error(self.__class__.__name__ +": history() - "+"Cannot show config history because it does not contain a 'process'.\nNote that only 'cfg' files contain a 'process'.")
            self.showErrorMessage("Cannot show config history because it does not contain 'process'.\nNote that only 'cfg' files contain a 'process'.")
            return None
        dialog=TextDialog(self, "Configuration history", history, True, "This window lists the parameter changes and tools applied in this configuration file before it was loaded into ConfigEditor.")
        dialog.exec_()

    def eventContent(self):
        """ Open event content dialog """
        logging.debug(__name__ + ": eventContent")
        if event_content_error!=None:
            logging.error(__name__ + ": Could not import EventContentDialog: "+event_content_error[1])
            self.showErrorMessage("Could not import EventContentDialog (see logfile for details):\n"+event_content_error[0])
            return
        dialog=EventContentDialog(self,"This dialog let's you check if the input needed by your configuration file is in a dataformat or edm root file. You can compare either to a dataformat definition from a txt file (e.g. RECO_3_3_0) or any edm root file by selecting an input file.\n\nBranches that are used as input by your configuration but not present in the dataformat or file are marked in red.\nBranches that are newly created by your configuration are marked in green.")
        dialog.setConfigDataAccessor(self.dataAccessor())
        dialog.exec_()

    def loadIni(self):
        """ read options from ini """
        ini = getPreferences()
        if ini.has_option("config", "editor"):
            self._editorName = str(ini.get("config", "editor"))
        else:
            self._editorName = "emacs"
        if ini.has_option("config", "CurrentView"):
            proposed_view = ini.get("config", "CurrentView")
        else:
            proposed_view = self.plugin().viewClassId(ConnectionStructureView)
        self.switchCenterView(proposed_view)
        if ini.has_option("config", "box content script") and isinstance(self.centerView(),ConfigEditorBoxView):
            self.centerView().setBoxContentScript(str(ini.get("config", "box content script")))
            self._boxContentDialog.setScript(str(ini.get("config", "box content script")))

    def scriptChanged(self, script):
        BrowserTab.scriptChanged(self, script)
        self.saveIni()

    def saveIni(self):
        """ write options to ini """
        ini = getPreferences()
        if not ini.has_section("config"):
            ini.add_section("config")
        ini.set("config", "editor", self._editorName)
        if self.currentCenterViewClassId():
            ini.set("config", "CurrentView", self.currentCenterViewClassId())
        if isinstance(self.centerView(),ConfigEditorBoxView):
            ini.set("config", "box content script", self.centerView().boxContentScript())
        writePreferences()

    def exportDot(self, fileName=None):
        if import_dotexport_error!=None:
            logging.error(__name__ + ": Could not import DOTExport: "+import_dotexport_error[1])
            self.showErrorMessage("Could not import DOTExport (see logfile for details):\n"+import_dotexport_error[0])
            return
        dot = DotExport()
        if self.currentCenterViewClassId() == self.plugin().viewClassId(ConnectionStructureView):
            presets = {'seqconnect':False, 'tagconnect':True, 'seq':False, 'services':False, 'es':False, 'endpath':True, 'source':True, 'legend':False}
        else:
            presets = {'seqconnect':True, 'tagconnect':False, 'seq':True, 'services':False, 'es':False, 'endpath':True, 'source':True, 'legend':False}
        for opt, val in presets.items():
            dot.setOption(opt, val)
        types = ""
        for ft in dot.file_types:
            if types != "":
                types += ";;"
            types += ft.upper() + " File (*." + ft.lower() + ")"
        filter = QString("PDF File (*.pdf)")
        if not fileName:
            defaultname = os.path.splitext(self._filename)[0] + "_export"
            fileName = str(QFileDialog.getSaveFileName(self, "Export dot graphic...", defaultname, types, filter))
        if fileName != "":
            name = fileName
            ext = str(filter).split(" ")[0].lower()
            if os.path.splitext(fileName)[1].lower().strip(".") in dot.file_types:
                name = os.path.splitext(fileName)[0]
                ext = os.path.splitext(fileName)[1].lower().strip(".")
            try:
                dot.export(self.dataAccessor(), name + "." + ext, ext)
            except Exception:
                try:
                    dot.export(self.dataAccessor(), name + ".dot", "dot")
                    logging.error(self.__class__.__name__ +": exportDot() - "+"'dot' executable not found which is needed for conversion to '*." + ext + "'. Created '*.dot' file instead.")
                    self.showErrorMessage("'dot' executable not found which is needed for conversion to '*." + ext + "'. Created '*.dot' file instead.")
                except Exception,e:
                    logging.error(self.__class__.__name__ +": exportDot() - "+"Could not export dot graphic (see logfile for details): " + str(e))
                    self.showErrorMessage("Could not export dot graphic: " + exception_traceback())

    def readFile(self, filename):
        """ Reads in the file in a separate thread.
        """
        self._updateCenterView=False
        thread = ThreadChain(self.dataAccessor().open, filename)
        while thread.isRunning():
            if not Application.NO_PROCESS_EVENTS:
                QCoreApplication.instance().processEvents()
        if thread.returnValue():
            self._dumpAction.setEnabled(self.dataAccessor().process()!=None)
            self._historyAction.setEnabled(self.dataAccessor().process()!=None)
            self._eventContentAction.setEnabled(self.dataAccessor().process()!=None)
            self._editorAction.setEnabled(self.dataAccessor().process()!=None)
            if self.plugin().application().commandLineOptions().saveimage:
                self.centerView().updateConnections()
                self.saveImage(self.plugin().application().commandLineOptions().saveimage)
                print "Saved image to", self.plugin().application().commandLineOptions().saveimage, "."
                sys.exit(2)
            return True
        return False

    def save(self, filename=''):
        logging.debug(__name__ + ': save')
        self.startEditMode()
        if filename != "":
            if os.path.basename(filename) == os.path.basename(self.dataAccessor().configFile()):
                logging.error(self.__class__.__name__ +": save() - "+"Cannot use name of original configuration file: "+str(filename))
                self.showErrorMessage("Cannot use name of original configuration file.")
            elif BrowserTab.save(self, filename):
                self.dataAccessor().setIsReplaceConfig()
                return True
            else:
                return False
        elif self.dataAccessor().isReplaceConfig():
            return BrowserTab.save(self, filename)
        return self.plugin().application().saveFileAsDialog()

    def writeFile(self, filename):
        """ Write replace config file.
        """
        logging.debug(__name__ + ': writeFile')
        
        text_file = open(filename, "w")
        text_file.write(self.toolDataAccessor().topLevelObjects()[0].dumpPython()[1])
        if self.dataAccessor().process():
            text_file.write(self.dataAccessor().process().dumpHistory(False))
        text_file.close()
        return True

    def open(self, filename=None, update=True):
        if BrowserTab.open(self, filename, update):
            if self.dataAccessor().isReplaceConfig():
                self.startEditMode()
            return True
        return False

    def startEditMode(self):
        logging.debug(__name__ + ": startEditMode")
        if import_tools_error!=None:
            logging.error(__name__ + ": Could not import tools for ConfigEditor: "+import_tools_error[1])
            self.showErrorMessage("Could not import tools for ConfigEditor (see logfile for details):\n"+import_tools_error[0])
            return
        if self.editorSplitter():
            return
        if self._filename and not self.dataAccessor().process():
            logging.error(__name__ + ": Config does not contain a process and cannot be edited using ConfigEditor.")
            self.showErrorMessage("Config does not contain a process and cannot be edited using ConfigEditor.")
            return
        if self._filename and not self.dataAccessor().isReplaceConfig():
            self.setFilename(None)
            self.updateLabel()
        self.createEditor()
        self.setEditable(True)
        self.verticalSplitter().setSizes(self._originalSizes)

        self._importAction = guiFacade.createAction('&Import configuration...', self.importButtonClicked, "F2")
        self._configMenu.addAction(self._importAction)
        self._configToolBar.addAction(self._importAction)
        self._applyPatToolAction = guiFacade.createAction('&Apply tool...', self.applyButtonClicked, "F3")
        self._configMenu.addAction(self._applyPatToolAction)
        self._configToolBar.addAction(self._applyPatToolAction)
        self.activated()

        self._toolDataAccessor=ToolDataAccessor()
        self._toolDataAccessor.setConfigDataAccessor(self.dataAccessor())
        self.editorTableView().setDataAccessor(self._toolDataAccessor)
        self.connect(self.editorTableView(), SIGNAL('importButtonClicked'), self.importButtonClicked)
        self.connect(self.editorTableView(), SIGNAL('applyButtonClicked'), self.applyButtonClicked)
        self.connect(self.editorTableView(), SIGNAL('removeButtonClicked'), self.removeButtonClicked)
        self.connect(self.editorTableView(), SIGNAL('selected'), self.codeSelected)
        self.connect(self.propertyView(), SIGNAL('valueChanged'), self.valueChanged)
        self._updateCode()

    def toolDataAccessor(self):
        return self._toolDataAccessor

    def minimizeEditor(self):
        if self.originalButton().isChecked():
            self._originalSizes=self.verticalSplitter().sizes()
        self.minimizeButton().setChecked(True)
        self.originalButton().setChecked(False)
        self.maximizeButton().setChecked(False)
        self.verticalSplitter().setSizes([100, 1, 0])
    
    def originalEditor(self):
        self.minimizeButton().setChecked(False)
        self.originalButton().setChecked(True)
        self.maximizeButton().setChecked(False)
        self.verticalSplitter().setSizes(self._originalSizes)

    def maximizeEditor(self):
        if self.originalButton().isChecked():
            self._originalSizes=self.verticalSplitter().sizes()
        self.minimizeButton().setChecked(False)
        self.originalButton().setChecked(False)
        self.maximizeButton().setChecked(True)
        self.verticalSplitter().setSizes([0, 1, 100])
    
    def _updateCode(self,propertyView=True):
        logging.debug(__name__ + ": _updateCode")
        self.toolDataAccessor().updateToolList()
        self.editorTableView().setDataObjects(self.toolDataAccessor().topLevelObjects())
        if self.editorTableView().updateContent():
            self.editorTableView().restoreSelection()
        self.updateContent(False,propertyView)

    def importConfig(self,filename):
        logging.debug(__name__ + ": importConfig")
        statusMessage = self.statusBarStartMessage("Import python configuration in Editor")
        try:
            good=self.open(filename,False)
        except:
            logging.error(__name__ + ": Could not open configuration file: "+exception_traceback())
            self.showErrorMessage("Could not open configuration file (see log file for details).")
            self.statusBarStopMessage(statusMessage,"failed")
            return False
        if not good:
            logging.error(__name__ + ": Could not open configuration file.")
            self.showErrorMessage("Could not open configuration file.")
            self.statusBarStopMessage(statusMessage,"failed")
            return False
        if not self.dataAccessor().process():
            logging.error(__name__ + ": Config does not contain a process and cannot be edited using ConfigEditor.")
            self.showErrorMessage("Config does not contain a process and cannot be edited using ConfigEditor.")
            self.statusBarStopMessage(statusMessage,"failed")
            return False
        if self._filename and not self.dataAccessor().isReplaceConfig():
            self.setFilename(None)
            self.updateLabel()
        self.toolDataAccessor().setConfigDataAccessor(self.dataAccessor())
        self.propertyView().setDataObject(None)
        self._updateCode()
        self._applyPatToolAction.setVisible(True)
        self.statusBarStopMessage(statusMessage)
        return True

    def updateConfigHighlight(self):
        if self.editorTableView().selection() in self.toolDataAccessor().toolModules().keys():
            self.centerView().highlight(self.toolDataAccessor().toolModules()[self.editorTableView().selection()])
        else:
            self.centerView().highlight([])
            
    def importButtonClicked(self):
        logging.debug(__name__ + ": importButtonClicked")
        filename = QFileDialog.getOpenFileName(
            self,'Select a configuration file',standardConfigDir,"Python configuration (*.py)")
        if not filename.isEmpty():
            self.importConfig(str(filename))

    def applyButtonClicked(self):
        logging.debug(__name__ + ": applyButtonClicked")
        if not self._toolDialog:
            self._toolDialog=ToolDialog()
        self._toolDialog.setDataAccessor(self._toolDataAccessor)
        if not self._toolDialog.exec_():
            return
        if not self.toolDataAccessor().addTool(self._toolDialog.tool()):
            return
        self.setModified(True)
        self._updateCode()
        self.editorTableView().select(self.editorTableView().dataObjects()[-2])
        self.codeSelected(self.editorTableView().dataObjects()[-2])
            
    def removeButtonClicked(self,object):
        logging.debug(__name__ + ": removeButtonClicked")
        if not object or not self.dataAccessor().process() or\
            self._toolDataAccessor.label(object) in ["Import","ApplyTool"]:
            return
        if not self.toolDataAccessor().removeTool(object):
            self.showErrorMessage("Could not apply tool. See log file for details.")
            return
        self.setModified(True)
        self._updateCode()
        self.editorTableView().select(self.editorTableView().dataObjects()[-1])
        self.codeSelected(self.editorTableView().dataObjects()[-1])

    def onSelected(self, select):
        self.selectDataAccessor(select)
        BrowserTab.onSelected(self, select)

    def reloadFile(self):
        self.propertyView().setDataObject(None)
        BrowserTab.reloadFile(self)

    def updateContent(self, filtered=False, propertyView=True):
        if import_tools_error==None and isinstance(object,ConfigToolBase):
            propertyView=False
        else:
            self.propertyView().setDataAccessor(self.dataAccessor())
        BrowserTab.updateContent(self, filtered, propertyView)

    def select(self, object):
        self.selectDataAccessor(object)
        BrowserTab.select(self, object)
    
    def selectDataAccessor(self,object):
        if import_tools_error==None and isinstance(object,ConfigToolBase):
            self.propertyView().setDataAccessor(self.toolDataAccessor())
        else:
            self.propertyView().setDataAccessor(self.dataAccessor())
    
    def codeSelected(self,select):
        if self.propertyView().dataObject() != select:
            statusMessage = self.statusBarStartMessage("Updating property view")
            self.propertyView().setDataAccessor(self.toolDataAccessor())
            self.propertyView().setDataObject(select)
            self.propertyView().updateContent()
            self.statusBarStopMessage(statusMessage)
        self.updateConfigHighlight()

    def valueChanged(self,name):
        if isinstance(self.propertyView().dataObject(),ConfigToolBase):
            if self._toolDataAccessor.label(self.propertyView().dataObject())=="Import":
                filename=self.toolDataAccessor().propertyValue(self.propertyView().dataObject(),"filename")
                return self.importConfig(filename)
            else:
                self.toolDataAccessor().updateProcess()
                self.setModified(True)
                self._updateCode(False)
                self.codeSelected(self.editorTableView().selection())
        else:
            self._updateCode()

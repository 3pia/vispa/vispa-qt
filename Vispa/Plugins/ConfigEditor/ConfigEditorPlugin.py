import os.path
import logging

from PyQt4.QtCore import QCoreApplication

from Vispa.Plugins.Browser.BrowserPlugin import BrowserPlugin
from Vispa.Plugins.Browser.BrowserTab import BrowserTab
from Vispa.Main.Exceptions import NoCurrentTabException
from Vispa.Main.Exceptions import PluginIgnoredException

try:
    from Vispa.Plugins.ConfigEditor.ConfigDataAccessor import ConfigDataAccessor
except Exception,e:
    raise PluginIgnoredException("cannot import CMSSW: " + str(e))

from Vispa.Plugins.ConfigEditor.ConfigEditorTab import ConfigEditorTab
from Vispa.Plugins.ConfigEditor.ConfigEditorTab import ConfigEditorTab
from Vispa.Plugins.ConfigEditor.ConfigEditorBoxView import ConnectionStructureView
from Vispa.Plugins.ConfigEditor.ConfigEditorBoxView import SequenceStructureView
from Vispa.Views.AbstractView import NoneView

from Vispa.Main.GuiFacade import guiFacade

class ConfigEditorPlugin(BrowserPlugin):
    """ The ConfigEditorPlugin opens config files in a ConfigEditorTab.
    """
    
    def __init__(self, name=None):
        logging.debug(__name__ + ": __init__")
        BrowserPlugin.__init__(self)
        self.registerFiletypesFromTab(ConfigEditorTab)
        self.addNewFileAction("New configuration file", self.newFile)
        guiFacade.createStartupScreenEntry(prototypingActions=self._createNewFileActions, executionFiletypes=self.filetypes())

    def startUp(self):
        BrowserPlugin.startUp(self)
        self.addCenterView(NoneView)
        self.addCenterView(ConnectionStructureView,True)
        self.addCenterView(SequenceStructureView)

    def newTab(self):
        """ Create ConfigEditorTab and add to MainWindow.
        """
        tab = ConfigEditorTab(self)
        tab.setDataAccessor(ConfigDataAccessor())
        tab.boxContentDialog().addButton("&Type", "object.type")
        tab.boxContentDialog().addButton("&Classname", "object.classname")
        tab.boxContentDialog().addButton("&Filename", "object.filename")
        tab.boxContentDialog().addButton("&Package", "object.package")
        guiFacade.addTab(tab)
        return tab

    def newFile(self,new=True):
        """ Create ConfigEditorTab and add to MainWindow.
        """
        if self._startUp:
            self.startUp()
        if new:
            tab=self.newTab()
        else:
            try:
                if isinstance(guiFacade.currentTab(),ConfigEditorTab):
                    tab=guiFacade.currentTab()
            except NoCurrentTabException:
                tab=None
            if not tab:
                logging.error(__name__ + ": No configuration file was opened for editing.")
                guiFacade.showErrorMessage("No configuration file was opened for editing.")
                return None
        tab.startEditMode()

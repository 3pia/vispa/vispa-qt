import os.path
import logging

from PyQt4 import QtGui
from PyQt4 import  QtCore

from Vispa.Main.VispaPlugin import *
from Vispa.Main.SplitterTab import *
from Vispa.Main.PluginManager import pluginmanager
import Vispa.Main.Preferences

import Vispa.Plugins.BatchSystem
from Vispa.Plugins.BatchSystem.AbstractBatchManagerPlugin import AbstractBatchManagerPlugin, AbstractManagerStatusAndConfigurationWidget
from Vispa.Plugins.BatchSystem.AbstractBatchManagerPlugin import AbstractJobModel, AbstractJobListWidget, AbstractJobListSortFilterProxy

from BatchSystemManager.GridBatchManager.GridBatchManager import GridBatchManager
from ManagerStatusAndConfigurationWidget import ManagerStatusAndConfigurationWidget
from JobConfigurationWidget import JobConfigurationWidget
import os



class JobModel(AbstractJobModel):
#  def __init__(self, batchManager, parent=None):
#    AbstractJobModel.__init__(self, batchManager, parent)
  def data(self, index, role):
    if not index.isValid():
      return QtCore.QVariant()
    item = index.internalPointer()
    if role == QtCore.Qt.DecorationRole:
      if index.column() == self.getColumnNames().index('Status'):
        pixmap = QtGui.QPixmap(12, 12)
        if item.getStatus() in ["Crashed", "Aborted"]:
          pixmap.fill(QtCore.Qt.red)
          return pixmap
        elif item.getStatus() in ["Finished Abnormally"]:
          pixmap.fill(QtCore.Qt.yellow)
          return pixmap
        elif item.getStatus() in ["Finished Normally"]:
          pixmap.fill(QtCore.Qt.green)
          return pixmap

    # Default Fallback
    return AbstractJobModel.data(self, index, role)


class JobListWidget(AbstractJobListWidget):
  def __init__(self, batchmanager, parent):
    AbstractJobListWidget.__init__(self, parent)
    self.__batchManager = batchmanager
  def getContextMenu(self):
    indexes = self.selectionModel().selectedIndexes()
    menu = AbstractJobListWidget.getContextMenu(self)
    self.downloadOutputSandbox = menu.addAction('Download Outputsandbox')
    self.downloadOutputSandbox.triggered.connect(self.__downloadJobOutput)
    #self.downloadOutputSandbox.setEnabled(False)
    self.downloadJobOutput = menu.addAction('Download OutputData')
    self.downloadJobOutput.setEnabled(False)
#    self.readJobOutputAction.triggered.connect(self.__showJobOutput)
#    self.readJobErrorAction = menu.addAction('Show Job Error (stderr)')
#    self.readJobErrorAction.triggered.connect(self.__showJobError)
#    rows = set()
#    for i in indexes:
#      rows.add(self.model().mapToSource(i).row())
#    if len(rows)!=1:
#      self.readJobOutputAction.setEnabled(False)
#      self.readJobErrorAction.setEnabled(False)
#
#    self.stopAction.setEnabled(True)
    for index in indexes:
      item = self.model().mapToSource(index).internalPointer()
      if item.getStatus() not in ["Finished Normally", "Finished Abnormally"]:
        self.downloadOutputSandbox.setEnabled(False)
        break
    return menu

  def __downloadJobOutput(self):
    jobs = set()
    for i in self.selectionModel().selectedIndexes():
      item = self.model().mapToSource(i)
      jobs.add(item.internalPointer())
    for job in jobs:
      self.__batchManager.downloadOutputSandbox(job)

#  def __showJobOutput(self):
#    item = self.model().mapToSource(self.selectionModel().selectedIndexes()[0])
#    job = item.internalPointer()
#    data = job.getOutputData()
#    textdisplay = TextDisplayDialog(data, self)
#    textdisplay.exec_()
# 
#  def __showJobError(self):
#    item = self.model().mapToSource(self.selectionModel().selectedIndexes()[0])
#    job = item.internalPointer()
#    data = job.getErrorData()
#    textdisplay = TextDisplayDialog(data, self)
#    textdisplay.exec_()
# 
#  def __showJobLog(self):
#    item = self.model().mapToSource(self.selectionModel().selectedIndexes()[0])
#    job = item.internalPointer()
#    data = job.getLogData()
#    textdisplay = TextDisplayDialog(data, self)
#    textdisplay.exec_()





class GridBatchManagerPlugin(AbstractBatchManagerPlugin):
    """ basic Batch System, manage BatchManagers 
    
    """
    def __init__(self, name=None):
        logging.debug("GridBatchManagerPlugin: Initialize")
        self.__batchManager = None
        VispaPlugin.__init__(self)
        AbstractBatchManagerPlugin.__init__(self, 'Grid batch manager')

        PREFERENCES_SECTION = self.getManagerName()
        Vispa.Main.Preferences.addSetting(PREFERENCES_SECTION, 'OutputDirectory', 'Enter Directory on SE', '', None)
        Vispa.Main.Preferences.addSetting(PREFERENCES_SECTION, 'StorageElement', 'DefautlStorageElement', '', None)
        Vispa.Main.Preferences.addSetting(PREFERENCES_SECTION, 'VirtualOrganization', 'EnterVO', '', None)

        #Use internal check
        if GridBatchManager.checkSystemAvailability():
            try:
                self.__batchManager = GridBatchManager(self.getDataPath())
#                self._batchSystemPlugin.getPolling().add("GridBatchManagerUpdate", self.__batchManager.update)
                self.__batchManager.update()
                #FIXME: BM was thread
                #self.__batchManager.start()
                self._batchSystemPlugin.registerManager(self, self.__batchManager)
            except Exception, e:
                logging.error("Grid system crashed on startup. Not activating GridManagerPlugin")
                logging.error(" " + str(e))
                for action in self.getNewFileActions():
                    action.setDisabled(True)
        else:
            logging.info("Grid system not found. Not activating GridManagerPlugin")
            for action in self.getNewFileActions():
                action.setDisabled(True)
            prefs = Vispa.Main.Preferences.getSection(PREFERENCES_SECTION)
            for p in prefs:
              p.setEnabled(False)

        self.__jobListLoaded = False



    def loadJobList(self):
        # Load old jobs olny once
        if not self.__jobListLoaded:
            for filename in os.listdir(self.getDataPath()):
                if filename.endswith('.batchJob'):
                    self.__batchManager.loadJobFromFile(os.path.join(self.getDataPath(), filename))
            self.__jobListLoaded = True
            self.__batchManager.update()

    def getBatchManager(self):
        return self.__batchManager


    def getJobConfigurationWidget(self, parent):
      widget = JobConfigurationWidget(self.__batchManager, parent)
      return widget


    def getManagerStatusAndConfigurationWidget(self, parent):
      widget = ManagerStatusAndConfigurationWidget(self.__batchManager, parent)
      return widget


    def createJobModel(self, batchManager):
      return JobModel(batchManager, self)

    def createJobListSortFilterProxy(self, jobModel):
      myJobListSortFilterProxy = AbstractJobListSortFilterProxy(self) #Could also subclass  a JobListSortFilterProxy(AbstractJobListSortFilterProxy), but not needed yet
      myJobListSortFilterProxy.setSourceModel(jobModel)
      myJobListSortFilterProxy.setFilterKeyColumn(-1)
      myJobListSortFilterProxy.setDynamicSortFilter(False)
      return myJobListSortFilterProxy

    def getJobListWidget(self, parent):
      return JobListWidget(self.__batchManager, parent)

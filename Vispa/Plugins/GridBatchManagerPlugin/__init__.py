import logging

gridBatchManagerAvailable = False
try:
    from GridBatchManagerPlugin import GridBatchManagerPlugin as plugin
    logging.info("Successfully loaded '%s'" % ("GridBatchManagerPlugin"))
    gridBatchManagerAvailable = True
except ImportError:
    logging.info("Couldn't load '%s'. Did you install the BatchSystemManager Plugin ( https://forge.physik.rwth-aachen.de/projects/batchsystemmanager )? " % ("GridBatchManagerPlugin"))
    gridBatchManagerAvailable = False
from PyQt4 import QtGui
from PyQt4 import QtCore

import logging

from Vispa.Plugins.BatchSystem.AbstractBatchManagerPlugin import AbstractJobConfigWidget
import Vispa.Main.Preferences

class TableModel(QtCore.QAbstractTableModel):
  def __init__(self, data, parent):
    QtCore.QAbstractTableModel.__init__(self, parent)
    self.__data = data


  def rowCount(self, parent=None):
    return len(self.__data['rows'])

  def columnCount(self, parent=None):
    return len(self.__data['columns'])

  def data(self, index, role):
    if not index.isValid():
      return QtCore.QVariant()
    if role == QtCore.Qt.DisplayRole:
      d = self.__data['rows'][index.row()][index.column()]
      if d.isdigit():
        return int(d)
      else:
        return d

  def headerData(self, section, orientation, role):
    """
    The header data 
    """
    if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
      return self.__data['columns'][section]
    return QtCore.QVariant()

class TableSortFilterProxy(QtGui.QSortFilterProxyModel):
    def __init__(self, parent):
        QtGui.QSortFilterProxyModel.__init__(self, parent)

    def lessThan(self, left, right):
        leftData = self.sourceModel().data(left, QtCore.Qt.DisplayRole)
        rightData = self.sourceModel().data(right, QtCore.Qt.DisplayRole)

        if type(leftData) == int and type(rightData) == int:
            return leftData < rightData
        elif type(leftData) == int:
            return True
        elif type(rightData) == int:
            return False
        else:
            leftData_qstr = QtCore.QString(leftData)
            rightData_qstr = QtCore.QString(rightData)

            if (not leftData_qstr.trimmed().toUpper().toLong()[1] and not rightData_qstr.trimmed().toUpper().toLong()[1]):
                return (leftData_qstr.trimmed().toUpper() < rightData_qstr.trimmed().toUpper())
            elif (leftData_qstr.trimmed().toUpper().toLong()[1] and not rightData_qstr.trimmed().toUpper().toLong()[1]):
                return True
            elif (not leftData_qstr.trimmed().toUpper().toLong()[1] and rightData_qstr.trimmed().toUpper().toLong()[1]):
                return False
            else:
                return (leftData_qstr.trimmed().toUpper().toLong() < rightData_qstr.trimmed().toUpper().toLong())

class ElementSelectionDialog(QtGui.QDialog):
  """
  Dialog showing a table view to select the storage or computing element
  """
  def __init__(self, parent=None):
    QtGui.QDialog.__init__(self, parent)
    layout = QtGui.QVBoxLayout(self)
    filterWidget = QtGui.QWidget(self)
    label = QtGui.QLabel("Filter:", filterWidget)
    self.__filterStringWidget = QtGui.QLineEdit(filterWidget)
    self.__filterStringWidget.textChanged.connect(self.__newFilter)
    lay = QtGui.QHBoxLayout()
    lay.addWidget(label)
    lay.addWidget(self.__filterStringWidget)
    filterWidget.setLayout(lay)
    layout.addWidget(filterWidget)
    self.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Preferred)
    #self.__tableWidget = QtGui.QTableWidget(self)
    self.__tableWidget = QtGui.QTableView(self)
    self.__tableWidget.setSortingEnabled(True)
    self.__tableWidget.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
    self.__tableWidget.setSelectionMode(QtGui.QAbstractItemView.SingleSelection)

    self.__tableWidget.resizeRowsToContents()
    self.__tableWidget.resizeColumnsToContents()

    myHorizontalHeader = self.__tableWidget.horizontalHeader()
    myHorizontalHeader.setClickable(True)
    myHorizontalHeader.setSortIndicatorShown(True)

    #style of the table
    #myStyleSheet = QtCore.QString("QHeaderView::section { background-color: white ; padding-left: 4px}")#; border: px solid #6c6c6c;
    #self.__tableWidget.setStyleSheet(myStyleSheet)

    layout.addWidget(self.__tableWidget)
    buttonBox = QtGui.QDialogButtonBox(self)
    buttonBox.addButton(QtGui.QDialogButtonBox.Ok)
    buttonBox.addButton(QtGui.QDialogButtonBox.Cancel)
    self.connect(buttonBox, QtCore.SIGNAL("rejected()"), self.reject)
    self.connect(buttonBox, QtCore.SIGNAL("accepted()"), self.__storeSelectedRow)
    layout.addWidget(buttonBox)
    self.setLayout(layout)


  def setData(self, data):
    """
    Recieves data in form of a dictionary with elements:
    columns, rows
    """
    self.__model = TableModel(data, self)
    #self.__filterModel = QtGui.QSortFilterProxyModel(self)
    self.__filterModel = TableSortFilterProxy(self)
    self.__filterModel.setSourceModel(self.__model)
    self.__filterModel.setFilterKeyColumn(-1)
    self.__filterModel.setDynamicSortFilter(False)
    self.__tableWidget.setModel(self.__filterModel)
    self.__tableWidget.horizontalHeader().setResizeMode(len(data['columns']) - 1, QtGui.QHeaderView.Stretch)

  def __newFilter(self, text):
    self.__filterModel.setFilterRegExp(text)


  def __storeSelectedRow(self):
    items = self.__tableWidget.selectionModel().selectedIndexes()
    if len(items) == 0:
      logging.debug("No Element selected")
      return None
    self.__selectedRow = []
    for i in items:
      self.__selectedRow.append(str(self.__filterModel.data(i,
        QtCore.Qt.DisplayRole).toString()))
    self.accept()


  def getSelectedRow(self):
    return self.__selectedRow

  def __headerClickSlot(self, index):
    #by default: sort the column ascending
    if index == self.last_horizontalHeader_sortIndicatorSection:
        if self.last_horizontalHeader_sortIndicatorOrder == QtCore.Qt.AscendingOrder:
            self.__tableWidget.sortItems(index, QtCore.Qt.DescendingOrder)
            self.last_horizontalHeader_sortIndicatorOrder = QtCore.Qt.DescendingOrder
        else:
            self.__tableWidget.sortItems(index, QtCore.Qt.AscendingOrder)
            self.last_horizontalHeader_sortIndicatorOrder = QtCore.Qt.AscendingOrder
    else:
        self.__tableWidget.sortItems(index, QtCore.Qt.AscendingOrder)
        self.last_horizontalHeader_sortIndicatorOrder = QtCore.Qt.AscendingOrder

    self.last_horizontalHeader_sortIndicatorSection = index


#class PassWordDialog(QtGui.QInputDialog):
#  def __init__(self, parent=None):
#    QtGui.QDialog.__init(self, parent)



class JobConfigurationWidget(AbstractJobConfigWidget):
  # The Widget knows its batchmanager, as it needs to get the statuses
  def __init__(self, batchManager, parent=None):
    QtGui.QWidget.__init__(self, parent)
    self.__batchManager = batchManager

    PREFERENCES_SECTION = 'Grid batch manager'

    '''Create GridLayout and its information and add it to a viewport widget'''
    viewport_layout = QtGui.QFormLayout(self)
    self.__selectedComputingElement = None
    self.__selectedStorageElement = None

    self.__comuptingElementSelectionWidget = QtGui.QPushButton('Select Computing Element', self)
    viewport_layout.addRow(self.__comuptingElementSelectionWidget)
    self.__comuptingElementSelectionWidget.clicked.connect(self.__selectComputingElement)
    self.__computingElementName = QtGui.QLabel('None Selected')
    viewport_layout.addRow('Computing Element:', self.__computingElementName)
    self.__numberOfCPUs = QtGui.QLabel('--')
    viewport_layout.addRow('No. of. CPUs:', self.__numberOfCPUs)
    self.__freeCPUs = QtGui.QLabel('--')
    viewport_layout.addRow('Free CPUs:', self.__freeCPUs)
    self.__totalJobs = QtGui.QLabel('--')
    viewport_layout.addRow('Total Jobs:', self.__totalJobs)
    self.__runningJobs = QtGui.QLabel('--')
    viewport_layout.addRow('Running Jobs:', self.__runningJobs)
    self.__waitingJobs = QtGui.QLabel('--')
    viewport_layout.addRow('Waiting Jobs:', self.__waitingJobs)

    self.__storageElemetSelectioWidget = QtGui.QPushButton('Select Storage Element', self)
    viewport_layout.addRow(self.__storageElemetSelectioWidget)
    self.__storageElemetSelectioWidget.clicked.connect(self.__selectStorageElement)
    self.__storageElementName = QtGui.QLabel('None Selected')
    self.__storageElementName.setText(Vispa.Main.Preferences.getSetting(PREFERENCES_SECTION, 'StorageElement'))

    viewport_layout.addRow('Storage Element:', self.__storageElementName)
    self.__storageElementAvailableSpace = QtGui.QLabel('--')
    viewport_layout.addRow('Available Space (Kb):', self.__storageElementAvailableSpace)
    self.__storageElementUsedSpace = QtGui.QLabel('-- (--)')
    viewport_layout.addRow('Used Space (Kb) (%):', self.__storageElementUsedSpace)

    self.__datapathSelectionWidget = QtGui.QLineEdit(self)
    defaultDirectory = Vispa.Main.Preferences.getSetting(PREFERENCES_SECTION, 'OutputDirectory')
    self.__datapathSelectionWidget.setText(defaultDirectory)
    self.__datapathSelectionWidget.setToolTip('The job output will be copied to a subfolder named $JOBID in this folder on the Storage Element')
    viewport_layout.addRow('Datapath on SE:', self.__datapathSelectionWidget)

    self.__viewport = QtGui.QWidget()
    self.__viewport.setLayout(viewport_layout)

    '''Create the scroll bar and add the viewport widget'''
    self.__scrollArea = QtGui.QScrollArea()
    self.__scrollArea.setWidget(self.__viewport)

    '''Add the scroll bar to the QDialog layout'''

    self.__hboxlayout = QtGui.QHBoxLayout()
    self.__hboxlayout.addWidget(self.__scrollArea)
    self.setLayout(self.__hboxlayout)

  def __selectStorageElement(self, *args):
    selectionWidget = ElementSelectionDialog(self)
    data = self.__batchManager.getInfoSE()
    selectionWidget.setData(data)
    selectionWidget.setWindowTitle("Select Storage Element")
    selectionWidget.resize(640, 480)
    selectionWidget.exec_()
    if selectionWidget.result() == QtGui.QDialog.Accepted:
      row = selectionWidget.getSelectedRow()
      self.__storageElementName.setText(row[3])
      self.__storageElementAvailableSpace.setText(row[0])
      t = "%s (%.1f %%)" % (row[1], float(row[1]) / (float(row[0]) + float(row[1])) * 100)
      self.__storageElementUsedSpace.setText(t)
      self.__selectStorageElement = row[3]

  def __selectComputingElement(self, *args):
    selectionWidget = ElementSelectionDialog(self)
    data = self.__batchManager.getInfoCE()
    selectionWidget.setData(data)
    selectionWidget.setWindowTitle("Select Computing Element")
    selectionWidget.resize(640, 480)
    selectionWidget.exec_()
    if selectionWidget.result() == QtGui.QDialog.Accepted:
      row = selectionWidget.getSelectedRow()
      self.__computingElementName.setText(row[5])
      self.__numberOfCPUs.setText(row[0])
      self.__freeCPUs.setText(row[1])
      self.__totalJobs.setText(row[2])
      self.__runningJobs.setText(row[3])
      self.__waitingJobs.setText(row[4])
      self.__selectedComputingElement = row[5]


    columns = ["#CPU", "Free CPUs", "Total Jobs", "Running", "Waiting", "ComputingElement (CE)"]

  def getJobOptions(self):
    options = {}
    options['ComputingElement'] = self.__selectedComputingElement
    options['StorageElement'] = self.__selectedStorageElement
    options['DataOutputDirectory'] = str(self.__datapathSelectionWidget.text())
    return options

  def updateStatus(self, *args):
    """
    Updates thes tatus view 
    """
    pass

  def updateConfiguration(self):
    """
    Sets the configuration to the editor
    """
    logging.debug("LocalBatchManagerPlugin::ManagerStatusAndConfigurationWidget - update configuration")



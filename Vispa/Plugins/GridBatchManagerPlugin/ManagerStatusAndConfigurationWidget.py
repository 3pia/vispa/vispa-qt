from PyQt4 import QtGui
from PyQt4 import QtCore

from Vispa.Plugins.BatchSystem.AbstractBatchManagerPlugin import AbstractManagerStatusAndConfigurationWidget
import Vispa.Main.Preferences
import logging


class ManagerStatusAndConfigurationWidget(AbstractManagerStatusAndConfigurationWidget):
  # The Widget knows its batchmanager, as it needs to get the statuses

  def __init__(self, batchManager, parent=None):
    AbstractManagerStatusAndConfigurationWidget.__init__(self, parent)
    self.__batchManager = batchManager

    PREFERENCES_SECTION = 'Grid batch manager'

    '''Create GridLayout and its information and add it to a viewport widget'''
    viewport_layout = QtGui.QFormLayout(self)
    self.__virtualOrganizationEdit = QtGui.QLineEdit(self)
    vo = self.__batchManager.getVirtualOrganization()
    if vo != None:
      self.__virtualOrganizationEdit.setText(vo)
    else:
      self.__virtualOrganizationEdit.setText(Vispa.Main.Preferences.getSetting(PREFERENCES_SECTION, 'VirtualOrganization'))
    viewport_layout.addRow("Virtual Organization:", self.__virtualOrganizationEdit)

    self.__virtualOrganizationEdit.editingFinished.connect(self.__virtualOrganizationEntered)
    startProxyButton = QtGui.QPushButton('Start Proxy')
    viewport_layout.addRow('Start Proxy', startProxyButton)
    startProxyButton.clicked.connect(self.__startProxy)


    self.__proxySubject = QtGui.QLabel(' -- ')
    viewport_layout.addRow('Subject:', self.__proxySubject)
    self.__proxyTime = QtGui.QLabel('-- ')
    viewport_layout.addRow('Time Left:', self.__proxyTime)

    self.__viewport = QtGui.QWidget()
    self.__viewport.setLayout(viewport_layout)

    '''Create the scroll bar and add the viewport widget'''
    self.__scrollArea = QtGui.QScrollArea()
    self.__scrollArea.setWidget(self.__viewport)

    '''Add the scroll bar to the QDialog layout'''
    self.__hboxlayout = QtGui.QHBoxLayout()
    self.__hboxlayout.addWidget(self.__scrollArea)
    self.setLayout(self.__hboxlayout)

  def __virtualOrganizationEntered(self):
    logging.debug("Grid ManagerStatusAndConfigurationWidget:: VO Entered " + str(self.__virtualOrganizationEdit.text()))
    self.__batchManager.setVirtualOrganization(str(self.__virtualOrganizationEdit.text()))


  def __startProxy(self):
    passWD, ok = QtGui.QInputDialog.getText(self, "Enter GRID pass phrase:", "Enter GRID pass phrase:", QtGui.QLineEdit.Password, "")
    if ok:
      logging.debug("GridBatchManagerPlugin::Starting Proxy")
      ok = self.__batchManager.startVOMSProxyServer(str(passWD))

    while not ok:
      passWD, ok = QtGui.QInputDialog.getText(self, "Enter GRID pass phrase:", "WRONG PASSWORD: Enter GRID pass phrase:",
          QtGui.QLineEdit.Password, "")
      if ok:
        logging.debug("GridBatchManagerPlugin::Starting Proxy")
        ok = self.__batchManager.startVOMSProxyServer(str(passWD))
    if ok:
      self.__updateProxyInfo()


  def __updateProxyInfo(self):
    proxyInfo = self.__batchManager.getVOMSProxyInfo()
    self.__proxySubject.setText(proxyInfo['subject'])
    self.__proxyTime.setText(proxyInfo['timeleft'])


  def updateStatus(self, *args):
    """
    Updates thes tatus view 
    """
    pass

  def updateConfiguration(self):
    """
    Sets the configuration to the editor
    """
    logging.debug("LocalBatchManagerPlugin::ManagerStatusAndConfigurationWidget - update configuration")



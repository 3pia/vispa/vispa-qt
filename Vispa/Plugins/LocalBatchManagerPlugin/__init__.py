import logging

localBatchManagerAvailable = False
try:
    from LocalBatchManagerPlugin import LocalBatchManagerPlugin as plugin
    logging.info("Successfully loaded '%s'" % ("LocalBatchManagerPlugin"))
    localBatchManagerAvailable = True
except ImportError:
    logging.info("Couldn't load '%s'. Did you install the BatchSystemManager Plugin ( https://forge.physik.rwth-aachen.de/projects/batchsystemmanager )? " % ("LocalBatchManagerPlugin"))
    localBatchManagerAvailable = False
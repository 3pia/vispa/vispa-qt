import os.path
import logging

from PyQt4 import QtGui
from PyQt4 import QtCore

from Vispa.Main.VispaPlugin import *
from Vispa.Main.SplitterTab import *
from Vispa.Main.PluginManager import pluginmanager

from Vispa.Plugins.BatchSystem.AbstractBatchManagerPlugin import AbstractBatchManagerPlugin, AbstractManagerStatusAndConfigurationWidget
from Vispa.Plugins.BatchSystem.AbstractBatchManagerPlugin import AbstractJobModel, AbstractJobListWidget, AbstractJobListSortFilterProxy
from Vispa.Plugins.BatchSystem.TextDisplayDialog import TextDisplayDialog

import Vispa.Main.Preferences

from BatchSystemManager.LocalBatchManager.LocalBatchManager import LocalBatchManager
import os


class ManagerStatusAndConfigurationWidget(AbstractManagerStatusAndConfigurationWidget):
  # The Widget knows its batchmanager, as it needs to get the statuses
  def __init__(self, batchManager, parent):
    AbstractManagerStatusAndConfigurationWidget.__init__(self, parent)
    self.__batchManager = batchManager
    layout = QtGui.QFormLayout(self)
    self.__numberOfRunningJobsWidget = QtGui.QLabel("0")
    self.__numberOfWatchedJobsWidget = QtGui.QLabel("0")
    self.__numberOfPendingJobsWidget = QtGui.QLabel("0")
    self.__maxNumberOfJobsSpinBox = QtGui.QSpinBox(self)
    self.__maxNumberOfJobsSpinBox.setMinimum(0)
    #layout.addRow(QtGui.QLabel("Watched Jobs"), self.__numberOfWatchedJobsWidget)
    #layout.addRow(QtGui.QLabel("Waiting for execution"), self.__numberOfPendingJobsWidget)
    #layout.addRow(QtGui.QLabel("Currently Running Jobs"), self.__numberOfRunningJobsWidget)
    layout.addRow(QtGui.QLabel("Maximum Number of parallel running jobs"), self.__maxNumberOfJobsSpinBox)
    self.setLayout(layout)
    self.updateStatus()
    self.__maxNumberOfJobsSpinBox.valueChanged.connect(self.updateConfiguration)


  def updateStatus(self, *args):
    """
    Updates thes tatus view 
    """
    #self.__numberOfRunningJobsWidget.setText(str(self.__batchManager.getNumberOfCurrentlyRunningJobs()))
    #self.__numberOfWatchedJobsWidget.setText(str(len(self.__batchManager.getJobs())))
    #l = self.__batchManager.getNumberOfJobsWithStatus('Pending')
    #l+= self.__batchManager.getNumberOfJobsWithStatus('Created')
    #self.__numberOfPendingJobsWidget.setText(str(l))
    #self.__numberOfWatchedJobsWidget.setText(str(len(self.__batchManager.getJobs())))
    self.__maxNumberOfJobsSpinBox.setValue(self.__batchManager.getMaxRunning())

  def updateConfiguration(self):
    """
    Sets the configuration to the editor
    """
    logging.debug("LocalBatchManagerPlugin::ManagerStatusAndConfigurationWidget - update configuration")
    self.__batchManager.setMaxRunning(int(self.__maxNumberOfJobsSpinBox.value()))



class JobModel(AbstractJobModel):
#  def __init__(self, batchManager, parent=None):
#    AbstractJobModel.__init__(self, batchManager, parent)
  def data(self, index, role):
    if not index.isValid():
      return QtCore.QVariant()
    item = index.internalPointer()
    if role == QtCore.Qt.DecorationRole:
      if index.column() == self.getColumnNames().index('Status'):
        pixmap = QtGui.QPixmap(12, 12)
        if item.getStatus() in ["Crashed"]:
          pixmap.fill(QtCore.Qt.red)
          return pixmap
        elif item.getStatus() in ["Finished Abnormally"]:
          pixmap.fill(QtCore.Qt.yellow)
          return pixmap
        elif item.getStatus() in ["Finished Normally"]:
          pixmap.fill(QtCore.Qt.green)
          return pixmap

    # Default Fallback
    return AbstractJobModel.data(self, index, role)



class JobListWidget(AbstractJobListWidget):
  def getContextMenu(self):
    indexes = self.selectionModel().selectedIndexes()
    menu = AbstractJobListWidget.getContextMenu(self)
    #if len(indexes) > 1:
    #  self.stopAction.setEnabled(False)
    #  return menu

    self.readJobOutputAction = menu.addAction('Show Job Output (stdout)')
    self.readJobOutputAction.triggered.connect(self.__showJobOutput)
    self.readJobErrorAction = menu.addAction('Show Job Error (stderr)')
    self.readJobErrorAction.triggered.connect(self.__showJobError)
    rows = set()
    for i in indexes:
      rows.add(self.model().mapToSource(i).row())
    if len(rows) != 1:
      self.readJobOutputAction.setEnabled(False)
      self.readJobErrorAction.setEnabled(False)

    self.stopAction.setEnabled(True)
    for index in indexes:
      item = self.model().mapToSource(index).internalPointer()

      if item.getStatus() not in ['Running', "Pending", 'Created']:
        self.stopAction.setEnabled(False)
        break

    return menu


  def __showJobOutput(self):
    item = self.model().mapToSource(self.selectionModel().selectedIndexes()[0])
    job = item.internalPointer()
    data = job.getOutputData()
    textdisplay = TextDisplayDialog(data, self)
    textdisplay.exec_()

  def __showJobError(self):
    item = self.model().mapToSource(self.selectionModel().selectedIndexes()[0])
    job = item.internalPointer()
    data = job.getErrorData()
    textdisplay = TextDisplayDialog(data, self)
    textdisplay.exec_()


class LocalBatchManagerPlugin(AbstractBatchManagerPlugin):
    """ basic Batch System, manage BatchManagers 
    
    """
    def __init__(self, name=None):
      self.__batchManager = None
      AbstractBatchManagerPlugin.__init__(self, 'Local BatchManager')
      logging.debug("LocalBatchManagerPlugin: Initialize")

      self.__PREFERENCES_SECTION = self.getManagerName()
      self.__batchManager = LocalBatchManager(self.getDataPath())

      Vispa.Main.Preferences.addSetting(self.__PREFERENCES_SECTION, 'Parallel Processes', 2, 'Number of processes to run in parallel', self.__updateNumberOfJobs)

      if LocalBatchManager.checkSystemAvailability():
        self._batchSystemPlugin.registerManager(self, self.__batchManager)
        #self._batchSystemPlugin.getPolling().add("LocalBatchManagerUpdate", self.__batchManager.update)

        self.__batchManager.update()
        #FIXME: BatchManager class was thread before
        #self.__batchManager.start()
        self.__jobListLoaded = False
      else:
        logging.info("LocalBatchManagerPlugin disabled")
        for action in self.getNewFileActions():
            action.setDisabled(True)
        prefs = Vispa.Main.Preferences.getSection(self.__PREFERENCES_SECTION)
        for p in prefs:
          p.setEnabled(False)


    def loadJobList(self):
      """ Load old jobs only once """
      logging.debug('LocalBatchManagerPlugin:: loadJobList')
      if not self.__jobListLoaded:
        for filename in os.listdir(self.getDataPath()):
          if filename.endswith('.batchJob'):
            self.__batchManager.loadJobFromFile(os.path.join(self.getDataPath(), filename))
        self.__jobListLoaded = True
        self.__batchManager.update()

    def getBatchManager(self):
      return self.__batchManager

    def __updateNumberOfJobs(self):
      v = Vispa.Main.Preferences.getSetting(self.__PREFERENCES_SECTION, 'Parallel Processes')
      self.__batchManager.setMaxRunning(v)

    def getManagerStatusAndConfigurationWidget(self, parent):
      widget = ManagerStatusAndConfigurationWidget(self.__batchManager, parent)
      return widget

    def createJobModel(self, batchManager):
      return JobModel(batchManager, self)

    def createJobListSortFilterProxy(self, jobModel):
      myJobListSortFilterProxy = AbstractJobListSortFilterProxy(self) #Could also subclass  a JobListSortFilterProxy(AbstractJobListSortFilterProxy), but not needed yet
      myJobListSortFilterProxy.setSourceModel(jobModel)
      myJobListSortFilterProxy.setFilterKeyColumn(-1)
      myJobListSortFilterProxy.setDynamicSortFilter(False)
      return myJobListSortFilterProxy

    def getJobListWidget(self, parent):
      return JobListWidget(parent)
import logging

condorBatchManagerAvailable = False
try:
    from CondorBatchManagerPlugin import CondorBatchManagerPlugin as plugin
    logging.info("Successfully loaded '%s'" % ("CondorBatchManagerPlugin"))
    condorBatchManagerAvailable = True
except ImportError:
    logging.info("Couldn't load '%s'. Did you install the BatchSystemManager Plugin ( https://forge.physik.rwth-aachen.de/projects/batchsystemmanager )? " % ("CondorBatchManagerPlugin"))
    condorBatchManagerAvailable = False
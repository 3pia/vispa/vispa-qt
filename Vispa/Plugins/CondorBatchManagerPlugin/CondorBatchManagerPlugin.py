import os.path
import logging

from PyQt4 import QtGui
from PyQt4 import QtCore

from Vispa.Main.VispaPlugin import *
from Vispa.Main.SplitterTab import *
from Vispa.Main.PluginManager import pluginmanager

import Vispa.Plugins.BatchSystem

from Vispa.Plugins.BatchSystem.AbstractBatchManagerPlugin import AbstractBatchManagerPlugin, AbstractManagerStatusAndConfigurationWidget, AbstractJobModel, AbstractJobListWidget, AbstractJobListSortFilterProxy
from Vispa.Plugins.BatchSystem.TextDisplayDialog import TextDisplayDialog

from BatchSystemManager.CondorBatchManager.CondorBatchManager import CondorBatchManager
import os


class ManagerStatusAndConfigurationWidget(AbstractManagerStatusAndConfigurationWidget):
  def __init__(self, batchManager, parent):
    AbstractManagerStatusAndConfigurationWidget.__init__(self, parent)
    self.__batchManager = batchManager
    layout = QtGui.QFormLayout(self)
    layout.addRow(QtGui.QLabel("Jobs"))
    self.__numberOfRunningJobsWidget = QtGui.QLabel("0")
    layout.addRow(QtGui.QLabel("Currently Running Jobs"), self.__numberOfRunningJobsWidget)

    layout.addRow(QtGui.QLabel("Condor Status"))
    self.__availableCoresLabel = QtGui.QLabel('X')
    layout.addRow('Available Cores:', self.__availableCoresLabel)
    self.__coresClaimedByOwnerLabel = QtGui.QLabel('X')
    layout.addRow('Owner:', self.__coresClaimedByOwnerLabel)
    self.__coresClaimedByCondorLabel = QtGui.QLabel('X')
    layout.addRow('Claimed:', self.__coresClaimedByCondorLabel)
    self.__coresUnclaimedLabel = QtGui.QLabel('X')
    layout.addRow('Unclaimed:', self.__coresUnclaimedLabel)
    layout.addRow(QtGui.QLabel("User Priorities"))
    self.__userPrioritiesWidget = QtGui.QWidget()
    self.__userPrioritiesFormLayout = QtGui.QFormLayout(self.__userPrioritiesWidget)
    layout.addRow(self.__userPrioritiesWidget)
    self.updateStatus()

  def updateStatus(self):
    """
    Updates the status view 
    """
    self.__availableCoresLabel.setText(str(self.__batchManager.getAvailableCores()))
    self.__coresClaimedByOwnerLabel.setText(str(self.__batchManager.getCoresClaimedByOwner()))
    self.__coresClaimedByCondorLabel.setText(str(self.__batchManager.getCoresClaimedByCondor()))
    self.__coresUnclaimedLabel.setText(str(self.__batchManager.getUnclaimedCores()))

    #update self.__userPrioritiesFormLayout
    #delete old entries
    self.deleteItems(self.__userPrioritiesFormLayout)

    for entry_list in self.__batchManager.getUserPriorities():
        label = QtGui.QLabel("")
        value = QtGui.QLabel("")
        if len(entry_list) > 0:
            label.setText(str(entry_list[0]))
        if len(entry_list) > 1:
            value.setText(str(entry_list[1]))

        self.__userPrioritiesFormLayout.addRow(label, value)

  def deleteItems(self, layout):
    if layout is not None:
      while layout.count():
        item = layout.takeAt(0)
        widget = item.widget()
        if widget is not None:
          widget.deleteLater()
        else:
          deleteItems(item.layout())

  def updateConfiguration(self):
    """
    Sets the configuration to the editor
    """
    pass



class JobModel(AbstractJobModel):
#  def __init__(self, batchManager, parent=None):
#    AbstractJobModel.__init__(self, batchManager, parent)
  def data(self, index, role):
    if not index.isValid():
      return QtCore.QVariant()
    item = index.internalPointer()
    if role == QtCore.Qt.DecorationRole:
      if index.column() == self.getColumnNames().index('Status'):
        pixmap = QtGui.QPixmap(12, 12)
        if item.getStatus() in ["Crashed"]:
          pixmap.fill(QtCore.Qt.red)
          return pixmap
        elif item.getStatus() in ["Finished Abnormally"]:
          pixmap.fill(QtCore.Qt.yellow)
          return pixmap
        elif item.getStatus() in ["Finished Normally"]:
          pixmap.fill(QtCore.Qt.green)
          return pixmap

    # Default Fallback
    return AbstractJobModel.data(self, index, role)




class JobListWidget(AbstractJobListWidget):
  def getContextMenu(self):
    indexes = self.selectionModel().selectedIndexes()
    menu = AbstractJobListWidget.getContextMenu(self)
    #if len(indexes) > 1:
    #  self.stopAction.setEnabled(False)
    #  return menu

    self.readJobOutputAction = menu.addAction('Show Job Output (stdout)')
    self.readJobOutputAction.triggered.connect(self.__showJobOutput)
    self.readJobErrorAction = menu.addAction('Show Job Error (stderr)')
    self.readJobErrorAction.triggered.connect(self.__showJobError)
    self.readJobLogAction = menu.addAction('Show Job Log')
    self.readJobLogAction.triggered.connect(self.__showJobLog)
    rows = set()
    for i in indexes:
      rows.add(self.model().mapToSource(i).row())
    if len(rows) != 1:
      self.readJobOutputAction.setEnabled(False)
      self.readJobErrorAction.setEnabled(False)

    self.stopAction.setEnabled(True)
    for index in indexes:
      item = self.model().mapToSource(index).internalPointer()
      if item.getStatus() not in ['Running', "Pending", 'Created']:
        self.stopAction.setEnabled(False)
        break
    return menu


  def __showJobOutput(self):
    item = self.model().mapToSource(self.selectionModel().selectedIndexes()[0])
    job = item.internalPointer()
    data = job.getOutputData()
    textdisplay = TextDisplayDialog(data, self)
    textdisplay.exec_()

  def __showJobError(self):
    item = self.model().mapToSource(self.selectionModel().selectedIndexes()[0])
    job = item.internalPointer()
    data = job.getErrorData()
    textdisplay = TextDisplayDialog(data, self)
    textdisplay.exec_()

  def __showJobLog(self):
    item = self.model().mapToSource(self.selectionModel().selectedIndexes()[0])
    job = item.internalPointer()
    data = job.getLogData()
    textdisplay = TextDisplayDialog(data, self)
    textdisplay.exec_()




class CondorBatchManagerPlugin(AbstractBatchManagerPlugin):
    """ basic Batch System, manage BatchManagers 
    
    """

    def __init__(self, name=None):
      self.__batchManager = None
      logging.debug("CondorBatchManagerPlugin: Initialize")
      AbstractBatchManagerPlugin.__init__(self, 'CondorBatchManager')

      #Use internal check
      if CondorBatchManager.checkSystemAvailability():
        try:
          self.__batchManager = CondorBatchManager(self.getDataPath())
          #self._batchSystemPlugin.getPolling().add("CondorBatchManagerUpdate", self.__batchManager.update)
          self.__batchManager.update()
          #FIXME: BM was own thread
          #self.__batchManager.start()
          self._batchSystemPlugin.registerManager(self, self.__batchManager)
        except Exception, e:
          logging.error("Condor system crashed on startup. Not activating CondorManagerPlugin ")
          logging.error(" " + str(e))
          for action in self.getNewFileActions():
            action.setDisabled(True)

      else:
        logging.info("Condor system not found. Not activating CondorManagerPlugin")
        for action in self.getNewFileActions():
          action.setDisabled(True)
      self.__jobListLoaded = False


    def loadJobList(self):
      # Load old jobs only once
      if not self.__jobListLoaded:
        for filename in os.listdir(self.getDataPath()):
          if filename.endswith('.batchJob'):
            self.__batchManager.loadJobFromFile(os.path.join(self.getDataPath(), filename))
        self.__jobListLoaded = True
        self.__batchManager.update()


    def getBatchManager(self):
      return self.__batchManager


    def getManagerStatusAndConfigurationWidget(self, parent):
      widget = ManagerStatusAndConfigurationWidget(self.__batchManager, parent)
      return widget


    def createJobModel(self, batchManager):
      return JobModel(batchManager, self)

    def getJobListWidget(self, parent):
      return JobListWidget(parent)

    def createJobListSortFilterProxy(self, jobModel):
      myJobListSortFilterProxy = AbstractJobListSortFilterProxy(self) #Could also subclass  a JobListSortFilterProxy(AbstractJobListSortFilterProxy), but not needed yet
      myJobListSortFilterProxy.setSourceModel(jobModel)
      myJobListSortFilterProxy.setFilterKeyColumn(-1)
      myJobListSortFilterProxy.setDynamicSortFilter(False)
      return myJobListSortFilterProxy

import logging

from PyQt4 import QtCore
from PyQt4 import QtGui

from Vispa.Main.GuiFacade import guiFacade

class ScriptEditor(QtGui.QWidget):
  def __init__(self, prefixLabel=None, parent=None):
    QtGui.QWidget.__init__(self, parent)
    self.__editStatus = False
    self.__initialData = ""

    layout = QtGui.QGridLayout()

    if prefixLabel != None:
      label = QtGui.QLabel(prefixLabel)
      label.setWordWrap(True)
      layout.addWidget(label, 0, 0, 1, 0)

    self.__textDisplay = QtGui.QTextEdit(self)
    self.__textDisplay.setFontFamily("Monospace")
    layout.addWidget(self.__textDisplay, 1, 0)

    buttonBar = QtGui.QWidget(self)
    buttonlayout = QtGui.QVBoxLayout()
    saveButton = QtGui.QPushButton('save')
    saveButton.pressed.connect(self.saveScript)
    buttonlayout.addWidget(saveButton, QtCore.Qt.AlignTop)
    loadButton = QtGui.QPushButton('load')
    loadButton.pressed.connect(self.loadScript)
    buttonlayout.addWidget(loadButton, QtCore.Qt.AlignTop)
    resetButton = QtGui.QPushButton('reset')
    buttonlayout.addWidget(resetButton, QtCore.Qt.AlignTop)
    resetButton.pressed.connect(self.resetScript)
    buttonlayout.addStretch()
    buttonBar.setLayout(buttonlayout)

    layout.addWidget(buttonBar, 1, 1)
    #buttonBox = QtGui.QDialogButtonBox()
    #buttonBox.addButton(QtGui.QDialogButtonBox.Ok)
    ##buttonBox.addButton(QtGui.QDialogButtonBox.Apply)
    #buttonBox.addButton(QtGui.QDialogButtonBox.Cancel)
    #self.connect(buttonBox, QtCore.SIGNAL("accepted()"), self.accept);
    #self.connect(buttonBox, QtCore.SIGNAL("rejected()"), self.close);
    #layout.addWidget(buttonBox)

    self.setLayout(layout)
    #self.setMinimumSize(640,200)

  def resetScript(self):
    msgBox = QtGui.QMessageBox()
    msgBox.setText("Reset the script to its initial data and loosing all changes?")
    msgBox.setStandardButtons(QtGui.QMessageBox.Ok | QtGui.QMessageBox.Cancel)
    ret = msgBox.exec_()
    if ret == QtGui.QMessageBox.Ok:
      self.__textDisplay.setText(self.__initialData)


  def loadScript(self):
    fileName = QtGui.QFileDialog.getOpenFileName(self,
        "Open Script")
    if not fileName.isEmpty():
      oFile = open(fileName)
      data = oFile.read()
      oFile.close()
      self.__textDisplay.setText(data)

  def saveScript(self):
    fileName = QtGui.QFileDialog.getSaveFileName(self,
        "Save Script")
    if not fileName.isEmpty():
      oFile = open(fileName, "w")
      oFile.write(self.getData())
      oFile.close()


#  def getEditStatus(self):
#    """
#    True if the edit was ended with OK, False if canceld
#    """
#    return self.__editStatus

  def getData(self):
    return str(self.__textDisplay.toPlainText())

  def setData(self, data):
    self.__initialData = data
    self.__textDisplay.setText(data)


#  def accept(self):
#    logging.debug("ScriptEditor:: Apply data")
#    self.__editStatus = True
#    self.close()
#  def updateEditorGeometry(self, editor, option, index):
#    editor.setGeometry(option.rect)

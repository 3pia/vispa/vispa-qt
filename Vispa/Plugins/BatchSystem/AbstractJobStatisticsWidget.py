from PyQt4 import QtGui
from PyQt4 import QtCore

class AbstractJobStatisticsWidget(QtGui.QWidget):
  """
  Widget displaying statistics for all jobs in a manager
  """
  def __init__(self, parent = None):
    QtGui.QWidget.__init__(self,parent)
    self.__layout = QtGui.QFormLayout(self)
    self.__dataWidgets = {}
    self.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Fixed)

  def setModel(self, model):
    self.__model = model
    model.dataChanged.connect(self.updateStatus)
    model.rowsRemoved.connect(self.updateStatus)
    model.rowsInserted.connect(self.updateStatus)

  def initialize(self):
    for status in self.__model.getPossibleStatuses():
      self.__dataWidgets[status] = QtGui.QProgressBar(self)
      self.__dataWidgets[status].setFormat("%v / %m (%p%)")
      #range(0,0) defines busy bar!
      self.__dataWidgets[status].setRange(1,1)
      self.__dataWidgets[status].setValue(0)
      self.__layout.addRow(status , self.__dataWidgets[status])
    

  def updateStatus(self):
    for status in self.__model.getPossibleStatuses():
      if self.__model.rowCount() > 0:
        self.__dataWidgets[status].setRange(0,self.__model.rowCount())
        self.__dataWidgets[status].setValue(self.__model.getNumberOfJobsWithStatus(status))
      else:
        self.__dataWidgets[status].setRange(1,1)
        self.__dataWidgets[status].setValue(0)

        
      #print self.__dataWidgets[status].minimum(), self.__dataWidgets[status].maximum()
      

#  def addCategory(self, idString, label):
#    """
#    Adds a new category of job types to the widget
#    """
#
#  def updateCategory(self, idString, value):
#    """
#    Updates the number of jobs in the category
#    """
#    self.__dataWidgets[idString].setValue(value)
#
#  def updateJobNumer(self, value):
#    """
#    update the total number of jobs (for all categories)
#    """
#    for widget in self.__dataWidgets.iteritems:
#      widget.setmaximum(value)


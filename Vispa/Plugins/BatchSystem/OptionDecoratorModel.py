import logging

from PyQt4 import QtCore
from PyQt4 import QtGui

from OptionValueDecorator import OptionValueDecorator

class OptionDecoratorModel(QtCore.QAbstractListModel):
  """
  Provides a model to acess the decorators from the
  CommandLineDesigner.
  """

  def __init__(self, commandLineDesigner, parent=None):
    QtCore.QAbstractListModel.__init__(self, parent)
    self.__commandLineDesigner = commandLineDesigner

    if not hasattr(QtCore.QAbstractListModel, 'beginResetModel'):
      logging.warning("QAbstractListModel has no beginResetModel! Use QAbstractListModel::reset instead")
      self.beginResetModel = self.reset
      self.endResetModel = self.__dummy

  def __dummy(self):
    pass
 
  def setCommandLineDesigner(self, commandLineDesigner):
    self.beginResetModel()
    self.__commandLineDesigner = commandLineDesigner
    self.endResetModel()

  def rowCount(self, index = None):
    return len(self.__commandLineDesigner.getOptionDecorators())


  def headerData(self, section, orientation, role):
    if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
      return "Option Decorators"
    return QtCore.QVariant()


  def addOptionDecorator(self, name, script = ""):
    logging.debug("OptionDecorator:: Adding parameter range: %s" % (name))
    idx = len(self.__commandLineDesigner.getOptionDecorators())
    self.beginInsertRows(QtCore.QModelIndex(),idx, idx)
    optionDecorator = OptionValueDecorator(name)
    optionDecorator.setScript(script)
    self.__commandLineDesigner.addOptionDecorator(optionDecorator)
    self.endInsertRows()


  def index(self, row, column, parent):
    if not self.hasIndex(row, column, parent):
      return QtCore.QModelIndex()
    if len(self.__commandLineDesigner.getOptionDecorators()) == 0:
      return QtCore.QModelIndex()
    return self.createIndex(row, column, self.__commandLineDesigner.getOptionDecorators()[row])


  def flags(self, index):
    """
    Returns the flags for the item with index
    """
    if not index.isValid():
      return QtCore.Qt.NoItemFlags
    defaultFlags = QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEditable
    return defaultFlags


  def data(self, index, role):
    """
    Returns the data as QVariants to be displayed in the view
    """
    if not index.isValid():
      return QtCore.QVariant()
    item = index.internalPointer()
    if role==QtCore.Qt.DisplayRole:
        return item.getName()

  def _dataChanged(self, index):
    '''
    Has to be called when the data of the mdoel was changed using an
    internal pointer.
    '''
    self.emit(QtCore.SIGNAL('dataChanged(const QModelIndex &, const QModelIndex &)'), index, index)

  def setData(self,index, value, role):
    """
    The model is editable, thus we need this method to transform the
    edit actions to the data
    """
    if not index.isValid():
      return False
    item = index.internalPointer()
    if role == QtCore.Qt.EditRole:
      s = str(value.toString()).strip()
      if len(s) == 0:
        return False
      item.setName(s)
      return True
    return False

  def removeData(self, indexes):
    self.beginRemoveRows(QtCore.QModelIndex(), indexes[0].row(), indexes[-1].row())
    for index in indexes:
      if index.isValid():
        self.__commandLineDesigner.removeOptionDecorator(index.internalPointer())
    self.endRemoveRows()


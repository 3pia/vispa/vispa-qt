import logging
import os

from xml.etree import ElementTree as etree
from ParameterRange import ParameterRange
from CommandLineOption import CommandLineOption
from OptionValueDecorator import OptionValueDecorator


def replaceEnviromentVariables(valstring):
  '''Replaces all occuring enviroment varibales in the string with their
  current values'''
  for k, v in os.environ.items():
    valstring = valstring.replace('$' + k, v)
  return valstring



class CommandLineDesigner(object):
  """
  Holds a set of options and parameter ranges and 
  methods to generate the command lines from it.
  """
  def __init__(self):
    self.__options = []
    self.__parameterRanges = []
    self.__optionDecorators = []
    self.__command = "/insert/full/path/to/command"

  def setCommand(self, cmd):
    logging.debug("CommandLineDesigner:: Command set to: " + cmd)
    self.__command = cmd

  def getCommand(self):
    return self.__command

  def addOptionDecorator(self, decorator):
    logging.debug("CommandLineDesigner:: OptionDecorator added: " + decorator.getName())
    self.__optionDecorators.append(decorator)

  def removeOptionDecorator(self, optionDecorator):
    self.__optionDecorators.remove(optionDecorator)

  def getOptionDecorators(self):
    return self.__optionDecorators

  def addOption(self, option):
    logging.debug("CommandLineDesigner:: Option added: " + option.getName())
    self.__options.append(option)

  def getOptions(self):
    return self.__options

  def removeOption(self, option):
    self.__options.remove(option)

  def addParameterRange(self, parameterRange):
    logging.debug("CommandLineDesigner:: ParameterRange added: " +
        parameterRange.getName())
    self.__parameterRanges.append(parameterRange)

  def getParameterRanges(self):
    return self.__parameterRanges

  def removeParameterRange(self, parameterRange):
    self.__parameterRanges.remove(parameterRange)


  def _generateParameterDicts(self):
    """
      creates a lsit of dicts for every combination of the parameterranges
    """
    parametersetlists = []
    for p in [par for par in self.__parameterRanges if par.isActive()]:
      t = []
      for v in p.getValues():
        t.append([p, v])
      parametersetlists.append(t)

    # create a list of all possible permutations whith an element of
    # each sublist
    possiblepermutations = [[]]
    for x in parametersetlists:
      tmp = []
      for y in x:
        for el in possiblepermutations:
          tmp.append(el + [y])
      possiblepermutations = tmp
    # Create dict from lsit
    perm = []
    for pl in possiblepermutations:
      d = {}
      for p in pl:
        d[p[0].getName()] = p[1]
      perm.append(d)
    return perm


  def buildCommandLines(self):
    """
    Creates a commandline out of options and parameter ranges
    """
    parameterSets = self._generateParameterDicts()
    cmdls = []
    logging.debug("CommandLineDesigner: buildCommandLines from:")
    activeOptions = [o for o in self.__options if o.isActive()]
    logging.debug("  %i active options (%i options in total)" %
        (len(activeOptions), len(self.__options)))
    for parameterDict in parameterSets:
      cmdline = self.__command + " "
      for option in activeOptions:
        cmdline += option.getOptionString()
        #No decorator, just take option value
        if not option.hasDecorator():
          cmdline += str(option.getValue())
        else:
          d = option.getDecorator()
          cmdline += str(d.getValue(parameterDict)).replace(" ", "\ ")
        cmdline += " "
      cmdls.append(cmdline[:-1])
    return cmdls




class CommandLineXmlFileWriter(object):
  @classmethod
  def serialize(cls, commandLineDesigner, outFileName):
    """
    Creates xml file with all data contents of an CommandLineDesigner 
    """
    rootElement = etree.Element('CommandLineDesignerJob')
    rootElement.attrib['FileFormatVersion'] = '1.1'

    c = etree.SubElement(rootElement, 'Command')
    c.text = commandLineDesigner.getCommand()

    for p in commandLineDesigner.getParameterRanges():
      cls.serializeParameterRange(p, rootElement)
    for d in commandLineDesigner.getOptionDecorators():
      cls.serializeOptionDecorator(d, rootElement)
    for o in commandLineDesigner.getOptions():
      cls.serializeOption(o, rootElement)

    ofile = open(outFileName, "w")
    ofile.write(etree.tostring(rootElement))
    ofile.close()


  @classmethod
  def serializeParameterRange(cls, parameterRange, rootNode):
    element = etree.SubElement(rootNode, 'ParameterRange', Name=parameterRange.getName())
    if not parameterRange.getValues():
      valueType = 'None'
    elif isinstance(parameterRange.getValues()[0], float):
      valueType = 'float'
    elif isinstance(parameterRange.getValues()[0], str):
      valueType = 'string'
    elif any(isinstance(e, bool) for e in parameterRange.getValues()):
      valueType = 'bool'
    elif isinstance(parameterRange.getValues()[0], int):
      # note that 1 and 0 also evaluate as bool
      valueType = 'int'

    valuesElement = etree.SubElement(element, 'Values', attrib={'type':valueType})
    valuesElement.text = ", ".join(map(str, parameterRange.getValues()))
    scriptElement = etree.SubElement(element, 'Script')
    scriptElement.text = parameterRange.getScript()
    activeElement = etree.SubElement(element, 'IsActive')
    activeElement.text = str(parameterRange.isActive())


  @classmethod
  def serializeOptionDecorator(cls, item, rootNode):
    element = etree.SubElement(rootNode, 'OptionDecorator', Name=item.getName())
    scriptElement = etree.SubElement(element, 'Script')
    scriptElement.text = item.getScript()


  @classmethod
  def serializeOption(cls, item, rootNode):
    element = etree.SubElement(rootNode, 'Option', Name=item.getName(), Group=item.getGroup())
    valuesElement = etree.SubElement(element, 'OptionString')
    valuesElement.text = str(item.getOptionString())

    valuesElement = etree.SubElement(element, 'Value')
    valuesElement.text = str(item.getValue())

    valuesElement = etree.SubElement(element, 'OptionDecorator')
    if item.hasDecorator():
      valuesElement.text = str(item.getDecorator().getName())
    else:
      valuesElement.text = str(None)

    activeElement = etree.SubElement(element, 'IsActive')
    activeElement.text = str(item.isActive())



class CommandLineXmlFileReader(object):
  """
  Reads xml files with data content of an CommandLineDesigner
  """
  def __init__(self, dataFileName):
    self.__tree = etree.parse(dataFileName)
    self.__rootElement = self.__tree.getroot()
    if self.__rootElement.tag != 'CommandLineDesignerJob':
      raise RuntimeError("CommandLineDesignerJob::deserealizes - Unkown data format!")
    self.__version = self.__rootElement.attrib['FileFormatVersion']
    logging.debug("CommandLineXmlFileReader: Open file " + dataFileName + " with version: " + self.__version)
    if self.__rootElement.attrib['FileFormatVersion'] not in self.getKnownVersions():
      raise RuntimeError("CommandLineDesignerJob::deserealizes - Unkown Format Version!" + self.__version)

  @classmethod
  def getKnownVersions(cls):
    #Version 1.0 stores all value types as string, which makes
    #decorators not functionable if they rely on non string types
    #Version 1.1 stores the type, but is for now restricted to int,
    #float, strings and bools
    return ('1.0', '1.1')


  def deserialize(self):
    """
    deserealizes a commandLineDesigner from the data file and returns
    the new object
    """
    commandLineDesigner = CommandLineDesigner()
    command = self.__tree.findall('Command')[0].text
    commandLineDesigner.setCommand(command)

    for child in self.__tree.findall('ParameterRange'):
      p = self.deserializeParameterRange(child, self.__version)
      p.updateValue()
      commandLineDesigner.addParameterRange(p)

    for child in self.__tree.findall('OptionDecorator'):
      p = self.deserializeOptionDecorator(child)
      commandLineDesigner.addOptionDecorator(p)

    for child in self.__tree.findall('Option'):
      p = self.deserializeOption(child,
          commandLineDesigner.getOptionDecorators())
      commandLineDesigner.addOption(p)

    return commandLineDesigner


  @classmethod
  def deserializeParameterRange(cls, node, version=1.1):
    p = ParameterRange(node.attrib['Name'], "")
    for c in node.getchildren():
      if c.tag == 'Values':
        if version == '1.0':
          p.setValues(list(c.text.split(', ')))
        else:
          valueType = c.get('type')
          if valueType == 'None':
            p.setValues([])
            continue

          if c.text == None:
              c.text = ""
          l = c.text.split(', ')
          if valueType == 'float':
            p.setValues(map(float, l))
          elif valueType == 'int':
            p.setValues(map(int, l))
          elif valueType == 'bool':
            m = [x == 'True' for x in l]
            p.setValues(m)
          elif valueType == 'string':
            p.setValues(l)
          else:
            errorMessage = "Unknown datatype: '" + valueType + "' in deserialization of commandline. Aborting"
            logging.error(errorMessage)
            raise RuntimeError(errorMessage)

      elif c.tag == 'Script':
        if c.text == None:
          p.setScript("")
        else:
          p.setScript(c.text)
      elif c.tag == 'IsActive':
        if c.text == 'True':
          p.setActive(True)
        else:
          p.setActive(False)
    return p


  @classmethod
  def deserializeOptionDecorator(cls, node):
    p = OptionValueDecorator(node.attrib['Name'])
    for c in node.getchildren():
      if c.tag == 'Script':
        p.setScript(c.text)
    return p


  @classmethod
  def deserializeOption(cls, node, decorators=[]):
    """
    Need a list of decorators to choose from!
    """
    p = CommandLineOption(node.attrib['Name'], "", "", node.attrib['Group'])
    logging.debug("CommandLineXmlFileReader::deserializeOption " + p.getName())
    for c in node.getchildren():
      if c.tag == 'Value':
        p.setValue(c.text)
      elif c.tag == 'OptionString':
        p.setOptionString(c.text)
      elif c.tag == 'IsActive':
        if c.text == 'True':
          p.setActive(True)
        else:
          p.setActive(False)
      elif c.tag == 'OptionDecorator':
        if c.text == 'None':
          p.setDecorator(None)
        else:
          for i in decorators:
            if i.getName() == c.text:
              p.setDecorator(i)
              break
    return p







import logging

from PyQt4 import QtCore
from PyQt4 import QtGui

from ParameterRange import ParameterRange
from CommandLineDesigner import replaceEnviromentVariables




class ParameterRangeModel(QtCore.QAbstractTableModel):
  """
  Provides a model to acess the parameter ranges in the
  CommandLineDesigner.
  """

  class ColumnNames:
    """
    Stores the names of the columns. To be used as enum.
    """
    NAME = "Name"
    VALUES = "Values"
    ACTIVE = "Active"
    SCRIPT = "Script"


  __columnNames = [ColumnNames.NAME, ColumnNames.VALUES,
      ColumnNames.SCRIPT, ColumnNames.ACTIVE]


  def __init__(self, commandLineDesigner, parent=None):
    QtCore.QAbstractTableModel.__init__(self, parent)
    self.__commandLineDesigner = commandLineDesigner

    if not hasattr(QtCore.QAbstractTableModel, 'beginResetModel'):
      logging.warning("QAbstractTableModel has no beginResetModel! Use QAbstractTableModel::reset instead")
      self.beginResetModel = self.reset
      self.endResetModel = self.__dummy

  def __dummy(self):
    pass
 
  def setCommandLineDesigner(self, commandLineDesigner):
    self.beginResetModel()
    self.__commandLineDesigner = commandLineDesigner
    self.endResetModel()

  def rowCount(self, index = None):
    return len(self.__commandLineDesigner.getParameterRanges())


  def columnCount(self, parent):
    return len(self.__columnNames)


  def getColumnNames(self):
    return self.__columnNames


  def headerData(self, section, orientation, role):
    if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
      return self.__columnNames[section]
    return QtCore.QVariant()


  def addParameterRange(self, name, values, script="", active = True):
    logging.debug("ParameterRangeModel:: Adding parameter range: %s" % (name))
    idx = len(self.__commandLineDesigner.getParameterRanges())
    self.beginInsertRows(QtCore.QModelIndex(),idx, idx)
    parameterRange = ParameterRange(name, values)
    parameterRange.setScript(script)
    parameterRange.setActive(active)
    parameterRange.updateValue()
    self.__commandLineDesigner.addParameterRange(parameterRange)
    self.endInsertRows()


  def index(self, row, column, parent = QtCore.QModelIndex()):
    if not self.hasIndex(row, column, parent):
      return QtCore.QModelIndex()
    if len(self.__commandLineDesigner.getParameterRanges()) == 0:
      return QtCore.QModelIndex()
    return self.createIndex(row, column, self.__commandLineDesigner.getParameterRanges()[row])


  def flags(self, index):
    """
    Returns the flags for the item with index
    """
    if not index.isValid():
      return QtCore.Qt.NoItemFlags
    defaultFlags = QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEditable
    if index.column() == self.__columnNames.index(ParameterRangeModel.ColumnNames.ACTIVE):
      flags = defaultFlags | QtCore.Qt.ItemIsUserCheckable
    else:
      flags = defaultFlags
    return flags


  def data(self, index, role):
    """
    Returns the data as list of QVariants to be displayed in the view
    """
    if not index.isValid():
      return QtCore.QVariant()
    item = index.internalPointer()
    if role == QtCore.Qt.DisplayRole:
      if index.column() == self.__columnNames.index(ParameterRangeModel.ColumnNames.NAME):
        return item.getName()
      elif index.column() == self.__columnNames.index(ParameterRangeModel.ColumnNames.VALUES):
        return "[" +", ".join(map(str,item.getValues())) +  "]"

      elif index.column() == self.__columnNames.index(ParameterRangeModel.ColumnNames.SCRIPT):
        s = item.getScript()
        if s == "":
          return "-- No script --"
        else:
          return s
    elif role == QtCore.Qt.CheckStateRole:
      if index.column() == self.__columnNames.index(ParameterRangeModel.ColumnNames.ACTIVE):
        if item.isActive():
          return QtCore.Qt.Checked
        else:
          return QtCore.Qt.Unchecked

    if role == QtCore.Qt.DecorationRole:
      if index.column() == self.__columnNames.index(ParameterRangeModel.ColumnNames.SCRIPT):
        pixmap = QtGui.QPixmap( 12, 12 )
        if item.getScript() == "":
          pixmap.fill(QtCore.Qt.gray)
        elif item.getScriptStatus():
          pixmap.fill(QtCore.Qt.green)
        elif not item.getScriptStatus():
          pixmap.fill(QtCore.Qt.red)
        return pixmap

    return QtCore.QVariant()


  def setData(self,index, value, role):
    """
    The model is editable, thus we need this method to transform the
    edit actions to the data. Note that the value has to be of type
    QVariant!
    """
    if not index.isValid():
      return False
    item = index.internalPointer()
    if role == QtCore.Qt.EditRole:
      if index.column() == self.__columnNames.index(ParameterRangeModel.ColumnNames.NAME):
        s = str(value.toString()).strip()
        if len(s) == 0:
          return False
        item.setName(s)
      elif index.column() == self.__columnNames.index(ParameterRangeModel.ColumnNames.VALUES):
        try:
          #Need a native python string here
          valstring = str(value.toString())
          valstring = replaceEnviromentVariables(valstring)
          d = eval(valstring)
          item.setValues(d)
        except Exception, e:
          logging.error("Cannot eval python code: " + str(e))
          return False
      elif index.column() == self.__columnNames.index(ParameterRangeModel.ColumnNames.SCRIPT):
        item.setScript(str(value.toString()))
        item.updateValue()
        valueIndex = self.index(index.row(),
            self.__columnNames.index(ParameterRangeModel.ColumnNames.VALUES))
        self.emit(QtCore.SIGNAL('dataChanged(const QModelIndex &, const QModelIndex &)'), valueIndex, index)

    elif role == QtCore.Qt.CheckStateRole:
      if value == QtCore.Qt.Checked:
        item.setActive(True)
      else:
        item.setActive(False)
    self.emit(QtCore.SIGNAL('dataChanged(const QModelIndex &, const QModelIndex &)'), index, index)
    return True

  def getNumberOfJobs(self):
    N = 1 
    for item in self.__commandLineDesigner.getParameterRanges():
      if item.isActive():
        N*= len(item.getValues())
    return N


  def removeData(self, indexes):
    self.beginRemoveRows(QtCore.QModelIndex(), indexes[0].row(), indexes[-1].row())
    for index in indexes:
      if index.isValid():
        #Only work on one column
        if index.column() == 0:
          item = index.internalPointer()
          self.__commandLineDesigner.removeParameterRange(item)
    self.endRemoveRows()


  def _dataChanged(self, index):
    '''
    Has to be called when the data of the mdoel was changed using an
    internal pointer.
    '''
    self.emit(QtCore.SIGNAL('dataChanged(const QModelIndex &, const QModelIndex &)'), index, index)

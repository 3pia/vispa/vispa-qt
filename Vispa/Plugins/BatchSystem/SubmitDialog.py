import logging

from PyQt4 import QtCore
from PyQt4 import QtGui
from BatchJobsPreview import BatchJobsPreviewWidget
from ScriptEditor import ScriptEditor
from Vispa.Gui.Header import FrameWithHeader
from Vispa.Main.GuiFacade import GuiFacade

class SubmitDialog(QtGui.QDialog):
  '''Dialog to present and confirm the jobs to be submitted'''
  def __init__(self, batchSystemPlugin, parent=None):
    logging.debug("%s: __init__()" % self.__class__.__name__)
    QtGui.QDialog.__init__(self, parent)
    self.setWindowTitle("Submit")
    self.setWindowIcon(GuiFacade.getIconFromImageName('submit'))

    self.__plugin = batchSystemPlugin

    self.__managerConfigStatusWidgets = {}
    self.__managerJobConfigWidget = {}

    #self.__layout = QtGui.QGridLayout(self)
    self.__layout = QtGui.QVBoxLayout(self)
    self.__layout.addWidget(QtGui.QLabel("Select batch manager to submit jobs to:"))

    self.__batchManagerSelector = batchSystemPlugin.getBatchManagerSelectorWidget(self)
    self.__batchManagerSelector.currentIndexChanged.connect(self.__setManagerAccessor)
    self.__layout.addWidget(self.__batchManagerSelector)

    self.__managerDescription = QtGui.QLabel()
    self.__layout.addWidget(self.__managerDescription)

    self.__viewport_toolbox = QtGui.QToolBox(self)
    self.__layout.addWidget(self.__viewport_toolbox)
    self.__viewport_toolbox.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
    self.__viewport_toolbox.setMinimumSize(800, 400)

    #Preview
    self.__previewWidget = BatchJobsPreviewWidget()
    item = self.__viewport_toolbox.addItem(self.__previewWidget, 'Job Preview')
    self.__viewport_toolbox.setItemToolTip(item,"Preview of jobs to be submitted")

    #Configuration: Manager Status
    self.__managerConfigFrame = QtGui.QWidget(self)
    self.__managerConfigFrame.setLayout(QtGui.QVBoxLayout())

    #self.__managerConfigFrame.layout().addWidget(self.__batchManagerSelector)
    self.__configuration_toolboxIndex = self.__viewport_toolbox.addItem(self.__managerConfigFrame,
        "Batch-System Configuration")
    self.__viewport_toolbox.setItemToolTip(self.__configuration_toolboxIndex,
        "Configuration of the selected manager")

    #dummy widgets ... needed to add status information widgets ...
    self.__currentManagerConfigStatusWidget = QtGui.QWidget(self)
    self.__currentJobConfigStatusWidget = QtGui.QWidget(self)

    #Pre Execution Script
    self.__preExecutionScriptEditor = ScriptEditor("""
    Shell script executed before the actual command.
    The Job uuid will be made available as shell variable JOBID""")

    item = self.__viewport_toolbox.addItem(self.__preExecutionScriptEditor,
        'Pre Execution Script')
    self.__viewport_toolbox.setItemToolTip(item,
        "Specify script executed before job is started")

    #Post Execution Script
    self.__postExecutionScriptEditor = ScriptEditor("""
    Shell script executed after the actual command.
    The Job uuid will be made available as shell variable JOBID
    """)
    item = self.__viewport_toolbox.addItem(self.__postExecutionScriptEditor,
        'Post Execution Script')
    self.__viewport_toolbox.setItemToolTip(item,
        "Specify script executed after job is completed")

    #Button box: OK and CANCEL
    buttonBox = QtGui.QDialogButtonBox()
    buttonBox.addButton(QtGui.QDialogButtonBox.Ok)
    buttonBox.addButton(QtGui.QDialogButtonBox.Cancel)
    self.connect(buttonBox, QtCore.SIGNAL("accepted()"), self.accept)
    self.connect(buttonBox, QtCore.SIGNAL("rejected()"), self.reject)
    self.__layout.addWidget(buttonBox)

    #Initialize Manager configuration
    self.__setManagerAccessor()


  def getPreExecutionScript(self):
    return str(self.__preExecutionScriptEditor.getData())


  def getPostExecutionScript(self):
    return str(self.__postExecutionScriptEditor.getData())


  def getSelectedBatchManagerAccessor(self):
    return self.__currentManagerAccessor


  def getJobOptions(self):
    """
    Returns the selected job options
    """
    return self.__currentJobConfigStatusWidget.getJobOptions()


  def __setManagerAccessor(self, *args):
    """
    Makes the neccessary changes if a new manager has been selected
    """
    accessor = self.__batchManagerSelector.getSelectedBatchManagerAccessor()
    self.__currentManagerAccessor = accessor

    if not accessor:
      logging.warning("SubmitDialog:: No Manager selected!")
      return

    if not self.__managerConfigStatusWidgets.has_key(accessor):
      accessor.loadJobList()
      self.__managerConfigStatusWidgets[accessor] = accessor.getManagerStatusAndConfigurationWidget(self)
      self.__managerConfigFrame.layout().addWidget(self.__managerConfigStatusWidgets[accessor])

      self.__managerJobConfigWidget[accessor] = accessor.getJobConfigurationWidget(self)
      self.__managerConfigFrame.layout().addWidget(self.__managerJobConfigWidget[accessor])

    self.__preExecutionScriptEditor.setData(accessor.getDefaultPreExecutionScript())
    self.__postExecutionScriptEditor.setData(accessor.getDefaultPostExecutionScript())

    self.__currentManagerConfigStatusWidget.hide()
    self.__currentManagerConfigStatusWidget = self.__managerConfigStatusWidgets[accessor]
    self.__currentManagerConfigStatusWidget.show()

    self.__currentJobConfigStatusWidget.hide()
    self.__currentJobConfigStatusWidget = self.__managerJobConfigWidget[accessor]
    self.__currentJobConfigStatusWidget.show()

    self.__viewport_toolbox.setItemText(self.__configuration_toolboxIndex, "Batch-System Configuration: Active is " + str(accessor.getManagerName()))

    self.__managerDescription.setText(accessor.getManagerDescription())

  def setJobList(self, jobList):
    self.__previewWidget.setJobList(jobList)


from Vispa.Main.VispaPlugin import VispaPlugin
from Vispa.Main.PluginManager import pluginmanager
import Vispa.Main.Preferences

from PyQt4 import QtGui
from PyQt4 import QtCore

import time
import logging
import os
import shlex

from AbstractJobStatisticsWidget import AbstractJobStatisticsWidget

class AbstractJobModel(QtCore.QAbstractTableModel):
  __columnNames = ['Id', 'Command', 'Status', 'Creation Time', 'Finished Time']
  def __init__(self, batchManagerAccessor, parent=None):
    QtCore.QAbstractTableModel.__init__(self, parent)
    self.__batchManager = batchManagerAccessor

  def rowCount(self, index=None):
    return len(self.__batchManager.getJobs())

  def columnCount(self, parent):
    return len(self.__columnNames)

  def getColumnNames(self):
    return self.__columnNames

  def headerData(self, section, orientation, role):
    if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
      return self.__columnNames[section]
    return QtCore.QVariant()

  def index(self, row, column, parent=QtCore.QModelIndex()):
    if not self.hasIndex(row, column, parent):
      return QtCore.QModelIndex()
    if len(self.__batchManager.getJobs()) == 0:
      return QtCore.QModelIndex()
    return self.createIndex(row, column, self.__batchManager.getJobs()[row])

  def addJob(self, *args):
    idx = len(self.__batchManager.getJobs())
    self.beginInsertRows(QtCore.QModelIndex(), idx, idx)
    self.__batchManager.addJob(*args)
    self.endInsertRows()

  def data(self, index, role):
    if not index.isValid():
      return QtCore.QVariant()
    item = index.internalPointer()
    if role == QtCore.Qt.DisplayRole:
      if index.column() == 0:
        return str(item.getId())
      elif index.column() == 1:
        return item.getCommand()
      elif index.column() == 2:
        return item.getStatus()
      elif index.column() == 3:
        return time.ctime(item.getSubmissionTime())
      elif index.column() == 4:
        return time.ctime(item.getFinishedTime())
    elif role == QtCore.Qt.ToolTipRole:
      cl = shlex.split(item.getCommand())
      cmd= "<br>".join(cl)
      s = """<table>
      <tr><td align='right'><b>Id:</b></td> <td>%s</td></tr>
<tr><td align='right'><b>Command:</b></td><td>%s</td></tr>
</table>
      """ % (str(item.getId()), cmd)
      return s
    return QtCore.QVariant()

  def getNumberOfJobsWithStatus(self, status):
    return self.__batchManager.getNumberOfJobsWithStatus(status)

  def getPossibleStatuses(self):
    return self.__batchManager.getBatchJobClass().possibleStatuses

  def removeData(self, indexes):
    self.beginRemoveRows(QtCore.QModelIndex(), indexes[0].row(), indexes[-1].row())
    jobsToRemove = set()
    for index in indexes:
      if not index.isValid():
        continue
      else:
        jobsToRemove.add(index.internalPointer())
    self.__batchManager.removeJobs(list(jobsToRemove))
    self.endRemoveRows()

  def jobsChanged(self, jobs):
    """
    called when jobs are changed, so that the model can emit the
    appropriate signal
    """
    logging.debug("AbstractJobModel:: %i jobsChanged " % (len(jobs)))
    tl = self.index(0, 0)
    br = self.index(len(self.__batchManager.getJobs()), 0)
    self.emit(QtCore.SIGNAL('dataChanged(const QModelIndex &, const QModelIndex &)'), tl, br)


  def restartJobs(self, indexes):
    jobsToRestart = set() 
    for index in indexes:
      if not index.isValid():
        continue
      else:
        jobsToRestart.add(index.internalPointer())
    self.__batchManager.restartJobs(jobsToRestart)


  def stopJobs(self, indexes):
    jobsToStop = set()
    for index in indexes:
      if not index.isValid():
        continue
      else:
        jobsToStop.add(index.internalPointer())
    self.__batchManager.stopJobs(jobsToStop)

  def removeJobs(self, indexes):
    self.removeData(indexes)



class AbstractJobListWidget(QtGui.QTableView):
  def __init__(self, parent=None):
    QtGui.QTableView.__init__(self, parent)
    self.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)

    self.setSortingEnabled(True)
    self.resizeRowsToContents()
    self.resizeColumnsToContents()

    self.myHorizontalHeader = self.horizontalHeader()
    self.myHorizontalHeader.setClickable(True)
    self.myHorizontalHeader.setSortIndicatorShown(True)

  def initialize(self):
    self.horizontalHeader().setResizeMode(1, QtGui.QHeaderView.Stretch)
    self.horizontalHeader().setResizeMode(2,
        QtGui.QHeaderView.ResizeToContents)
    self.horizontalHeader().setResizeMode(3,
        QtGui.QHeaderView.ResizeToContents)
    self.horizontalHeader().setResizeMode(4,
        QtGui.QHeaderView.ResizeToContents)


  def getContextMenu(self):
    menu = QtGui.QMenu(self)
    self.removeAction = menu.addAction("Remove Job(s)")
    self.removeAction.triggered.connect(self.__removeJobs)
    menu.addSeparator()
    self.restartAction = menu.addAction("Restart Job(s)")
    self.restartAction.triggered.connect(self.__restartJobs)
    self.stopAction = menu.addAction("Stop Job(s)")
    self.stopAction.setEnabled(False)
    self.stopAction.triggered.connect(self.__stopJobs)
    menu.addSeparator()
    return menu


  def contextMenuEvent(self, event):
    index = self.indexAt(event.pos())
    if not index.isValid():
      return
    menu = self.getContextMenu()
    menu.popup(event.globalPos())


  def __stopJobs(self):
    indexes = self.selectedIndexes() 
    index_list = set() 
    for index in indexes:
        index_list.add(self.model().mapToSource(index))
    self.model().sourceModel().stopJobs(list(index_list))


  def __restartJobs(self):
    indexes = self.selectedIndexes()
    index_list = set() 
    for index in indexes:
        index_list.add(self.model().mapToSource(index))
    self.model().sourceModel().restartJobs(list(index_list))

  def __removeJobs(self):
    indexes = self.selectedIndexes()
    index_list = set() 
    for index in indexes:
        index_list.add(self.model().mapToSource(index))
    self.model().sourceModel().removeJobs(list(index_list))


class AbstractJobListSortFilterProxy(QtGui.QSortFilterProxyModel):
    def __init__(self, parent):
        QtGui.QSortFilterProxyModel.__init__(self, parent)


class AbstractBatchManagerPlugin(VispaPlugin):
  """
  Defines the interface for the BatchManagerImplementations
  """
  _batchSystemPlugin = None
  def __init__(self, managerName):
      """
      The managername is an human readable identifier for the manager system used e.g.
      tod efine the preferences section
      """
      VispaPlugin.__init__(self)
      self.__managerName = managerName
      #initialize plugin if not done already
      pluginmanager.initializePlugin("BatchSystemPlugin")
      self._batchSystemPlugin = pluginmanager.plugin("BatchSystemPlugin")

      bsp = os.path.join(Vispa.Main.Preferences.preferencesDirectory, "BatchSystem")
      defaultBatchSystemPath = os.path.join(bsp, "".join(self.__managerName.split()))
      Vispa.Main.Preferences.addSetting(self.__managerName, "Batchsystem-path", defaultBatchSystemPath , "Path to the storage location for batch manager data.", self.__batchSystempathChanged)

  def getDataPath(self):
    """
    Returns the apth where the data for this manage rplugin is stored
    """
    dataPath = Vispa.Main.Preferences.getSetting(self.__managerName, "Batchsystem-path")
    return dataPath

  def __batchSystempathChanged(self):
      """
      Called on change of the batch system path of the manager.
      """
      batchSystemPath = Vispa.Main.Preferences.getSetting(self.getManagerName(), 'Batchsystem-path')
      logging.debug("AbstractBatchManagerPlugin::"+self.getManagerName() +": Settint batch System Path to " + batchSystemPath)
      #Even if there is no manager (yet), the path can be set 
      if self.getBatchManager():
        self.getBatchManager().setDataPath(batchSystemPath)

  def getManagerStatusAndConfigurationWidget(self, parent=None):
    '''
    Returns a widget from the plugin in which the specific manager can
    be configured and which displayes the status of the manager. This
    Widget may be shown on various occasions, e.g. in the viewer and on
    job submission.
    '''
    return AbstractManagerStatusAndConfigurationWidget(parent)


  def getManagerName(self):
    """
    Returns the name of the manager
    """
    return self.__managerName


  def getJobConfigurationWidget(self, parent=None):
    '''
    Returns a widget from the plugin in which the jobs for the specific
    manager can be configured.
    '''
    return AbstractJobConfigWidget(parent)


  def loadJobList(self):
    """
    Load the joblist of the specific manager.
    """
    pass


  def getBatchManager(self):
    """
    Returns the manager of the plugin.
    """
    raise NotImplemented


  def getJobInfo(self):
    """
    Returns the job info class for the manager
    """
    return AbstractJobInfo

  def createJobModel(self, batchManager):
    """
    Creates the QtCore.QAbstractTableModel which holds the data
    """
    return AbstractJobModel(batchManager, self)

  def getJobListWidget(self, parent):
    """
    Creates a new QtGui.QTableView to view the data
    """
    return AbstractJobListWidget(parent)

  def getJobStatisticsWidget(self, parent):
    """
    Creates a new widet displaying statistics on the jobs of the
    batchManager
    """
    widget = AbstractJobStatisticsWidget(parent)
    return widget


  def createJobListSortFilterProxy(self, jobModel):
    """
    Creates the proxy which enables sorting and filtering of data in the table
    """
    myJobListSortFilterProxy = AbstractJobListSortFilterProxy(self) #Could also subclass  a JobListSortFilterProxy(AbstractJobListSortFilterProxy), but not needed yet
    myJobListSortFilterProxy.setSourceModel(jobModel)
    myJobListSortFilterProxy.setFilterKeyColumn(-1)
    myJobListSortFilterProxy.setDynamicSortFilter(False)
    return myJobListSortFilterProxy


class AbstractJobConfigWidget(QtGui.QWidget):
  def getJobOptions(self):
    """
    Returns a dictionary with the job specific options
    """
    return {}



class AbstractManagerStatusAndConfigurationWidget(QtGui.QWidget):

  def __init__(self, parent):
    QtGui.QWidget.__init__(self, parent)


  def updateStatus(self):
    """
    Updates thes tatus view 
    """
    pass

  def updateConfiguration(self):
    """
    Sets the configuration to the editor
    """
    pass





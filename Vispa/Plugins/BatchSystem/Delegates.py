import logging

from PyQt4 import QtCore
from PyQt4 import QtGui

from ParameterRangeScriptDialog import ParameterRangeScriptDialog
from OptionDecoratorEditor import OptionDecoratorEditor
#
# The Delegates are used in the views to perform the edit actions
#


class ScriptEditDelegate(QtGui.QStyledItemDelegate):
  """
  Edit delegate for small python scripts 
  """
  def __init__(self, parent=None):
    QtGui.QStyledItemDelegate.__init__(self, parent)


  def createEditor(self, parent, option, index):
    editor = ParameterRangeScriptDialog(parent)
    return editor


  def setEditorData(self, editor, index):
    data = index.model().data(index,QtCore.Qt.DisplayRole)
    logging.debug("ScriptEditor::setEditorData to " + data)
    editor.setScript(data)


  def setModelData(self, editor, model, index):
    logging.debug("ScriptEditDelegate:: setModelData")
    if editor.getEditStatus():
      logging.debug("ScriptEditDelegate:: setModelData Accepted")
      model.setData(index, QtCore.QVariant(editor.getScript()) , QtCore.Qt.EditRole)
    pass



class OptionDecoratorEditDelegate(QtGui.QStyledItemDelegate):
  """
  Edit delegate for option decorator 
  """
  def __init__(self, parent=None):
    QtGui.QStyledItemDelegate.__init__(self, parent)


  def createEditor(self, parent, option, index):
    editor = OptionDecoratorEditor(parent)
    return editor


  def setEditorData(self, editor, index):
    #data = index.model().data(index,QtCore.Qt.DisplayRole)
    name = index.internalPointer().getName()
    script = index.internalPointer().getScript()
    editor.setScript(script)
    editor.setName(name)


  def setModelData(self, editor, model, index):
    logging.debug("OptionDecoratorEditor:: setModelData")
    if editor.getEditStatus():
      logging.debug("OptionDecoratorEditor:: setModelData Accepted")
      model.setData(index, QtCore.QVariant(editor.getName()), QtCore.Qt.EditRole)
      index.internalPointer().setScript(editor.getScript())





class OptionDecoratorSelectionDelegate(QtGui.QStyledItemDelegate):
  """
  Edit delegate to select the option decorators.
  """
  def __init__(self, commandLineDesigner, parent=None):
    QtGui.QStyledItemDelegate.__init__(self, parent)
    self.__commandLineDesigner = commandLineDesigner


  def createEditor(self, parent, option, index):
    editor = QtGui.QComboBox(parent)
    editor.addItem(" -- ")
    decList = [l.getName() for l in self.__commandLineDesigner.getOptionDecorators()]
    editor.addItems(QtCore.QStringList(decList))
    return editor


  def setEditorData(self, editor, index):
    decorator = index.model().data(index,QtCore.Qt.EditRole)
    if decorator == None:
      editor.setCurrentIndex(0)
    else:
      idx = 1 + self.__commandLineDesigner.getOptionDecorators().index(value)
      editor.setCurrentIndex(idx)


  def setModelData(self, editor, model, index):
    model.setData(index, editor.currentIndex(), QtCore.Qt.EditRole)


  def updateEditorGeometry(self, editor, option, index):
    editor.setGeometry(option.rect)

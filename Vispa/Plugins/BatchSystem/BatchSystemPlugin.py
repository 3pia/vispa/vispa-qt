import os
import logging

from PyQt4 import QtCore, QtGui

from Vispa.Main.VispaPlugin import VispaPlugin

from BatchJobDesignerTab import BatchJobDesignerTab
from Vispa.Plugins.BatchSystem.BatchManagerViewTab import BatchManagerViewTab

from Vispa.Main.GuiFacade import guiFacade

from BatchManagerAccessor import ManagerAccessor

from Polling import Polling

class BatchManagerSelectorWidget(QtGui.QComboBox):
  """
  QComboBox with the available BatchManagerAccessors 
  """
  def __init__(self, batchSystemPlugin, parent=None):
    QtGui.QComboBox.__init__(self, parent)
    self.__plugin = batchSystemPlugin
    if self.__plugin.getBatchManagerAccessors():
      for managerAccessor in self.__plugin.getBatchManagerAccessors():
        self.addItem(managerAccessor.getManagerName())
    else:
      self.addItem("No batch manager available")
      self.setEnabled(False)

  def getSelectedBatchManagerAccessor(self):
    """
    Returns accessor to the currently selected manager. None if no
    manager is available.
    """
    if self.__plugin.getBatchManagerAccessors():
      currentManagerAccessor = self.__plugin.getBatchManagerAccessors()[self.currentIndex()]
      return currentManagerAccessor
    return None



class BatchSystemPlugin(VispaPlugin):
    """ The BatchSystemPlugin provides the main infrastructure for
    accessing batch job systems. It provides a command line designer, to
    easily produce many jobs and submit them to a batch system. Every
    individual batch sytem has to register to the BatchSystemPlugin.
    """
    __managerAccessors = []
    def __init__(self):
        logging.debug("%s: __init__()" % self.__class__.__name__)
        VispaPlugin.__init__(self)

        self.addNewFileAction("&New batch job", self.newBatchJobDesignerTab)

        self.__batchmenu = guiFacade.createPluginMenu("Batch System")
        guiFacade.showPluginMenu(self.__batchmenu)

        self.registerFiletypesFromTab(BatchJobDesignerTab)
        guiFacade.createStartupScreenEntry(prototypingActions=self._createNewFileActions, executionFiletypes=self.filetypes())
        self.__showBatchManagerViewerAction = self.addNewFileAction("Batch Manager Viewer", self.newViewTab)
        self.__showBatchManagerViewerAction.setEnabled(False)
        guiFacade.createStartupScreenEntry(executionActions=[self.__showBatchManagerViewerAction])
        self._polling = Polling()

        self._polling.add("updateBatchManagers", self.updateAllManagers)

    def shutdown(self):
      #Managers need cleanup as they are threads
      logging.debug("%s: Shuting down batch system" % self.__class__.__name__)
      if self._polling:
        self._polling.stopRunning()
        self._polling.join()

      for managerAccessor in self.__managerAccessors:
        managerAccessor.shutdown()

    def getPolling(self):
        return self._polling

    def getBatchManagerAccessors(self):
      return self.__managerAccessors

    def newBatchJobDesignerTab(self, filename=None):
        tab = BatchJobDesignerTab(self)
        guiFacade.addTab(tab)
        if filename:
          tab.setFilename(filename)
          tab.updateLabel("", "filename")
          return tab.open(filename)
        else:
          tab.updateLabel("", "New Batch Job")
          return tab

    def newViewTab(self):
      statusMessageId = guiFacade.statusBarStartMessage("Creating new batch manager viewer")
      #load job list for plugin
      # The view has to initiate the load, as we do not wan't to load
      # the list if not needed
      #self.__plugin.loadJobList()


      tab = BatchManagerViewTab(self)
      guiFacade.addTab(tab)
      guiFacade.statusBarStopMessage(statusMessageId)
      tab.updateLabel("", "Batch Manager Viewer")

      #self.connect(self, QtCore.SIGNAL("managerJobStatusChanged"), tab.noteJobStatusChange)
      #self.connect(self, QtCore.SIGNAL("jobRemoved"), tab.noteJobsRemoved)


    def openFile(self, filename=""):
        """  Call newBatchJobDesigner() with filename as argument.
        
        Also makes sure file extension is 'xml'.
        """
        logging.debug("BatchSystemPlugin: openFile()")
        #if self._startUp:
        #    self.startUp()
        base = os.path.basename(str(filename))
        ext = os.path.splitext(base)[1].lower().strip(".")
        if ext == "job":
            return self.newBatchJobDesignerTab(filename)
        return False

    def updateAllManagers(self):
      for managerAccessor in self.__managerAccessors:
          managerAccessor.updateJobs()

    def registerManager(self, plugin, batchManager):
      logging.debug("%s: Register batch manager" % batchManager.getName())
      #the BatchSystem plugin recieves the callbacks from the manager,
      #and translates them into Qt Signals
      managerAccessor = ManagerAccessor(plugin, batchManager)
      #self.addNewFileAction(batchManager.getName()+" Viewer", managerAccessor.newViewTab)
      self.__managerAccessors.append(managerAccessor)
      #we have at least one manager, thus the viewer should be enabled
      self.__showBatchManagerViewerAction.setEnabled(True)
      #and the polling to update should be started
      logging.debug("Start batch manager polling")

      if not (self._polling.isRunning()):
          self._polling.startRunning()
          self._polling.start()

    def getBatchManagerSelectorWidget(self, parent=None):
      """
      creates a BatchManagerSelectorWidget and returns it.
      """
      return BatchManagerSelectorWidget(self, parent)

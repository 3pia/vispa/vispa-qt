import logging

class OptionValueDecorator(object):
  """Holds a script which is executed for each parameter combination.
  The script gets the variables as parameter and returns an output"""
  def __init__(self, name):
    self.__name = name
    self.__script = ""

  def getName(self):
    return self.__name

  def setName(self, value):
    self.__name = value

  def setScript(self, script):
    self.__script = script


  def getScript(self):
    return self.__script


  def getValue(self, valueDict ={}):
    text = self.__script
    text = text.lstrip()
    text = text.rstrip()
    text = "\n"+text
    cmd = "def userScript(valueDict = {}):\n"
    cmd+="\n  ".join(text.split("\n"))

    data = None
    try:
      exec(cmd)
    except Exception, e:
      logging.error("Cannot exec python code: " + str(e))
    else:
      data = userScript(valueDict)
    return data


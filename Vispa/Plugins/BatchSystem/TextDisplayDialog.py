from PyQt4 import QtGui
from PyQt4 import QtCore

class TextDisplayDialog(QtGui.QDialog):
  """
  Dialog displaying some text
  """
  def __init__(self,text, parent = None):
    QtGui.QDialog.__init__(self,parent)
    self.resize(640,480)
    layout = QtGui.QVBoxLayout()
    self.__textDisplay = QtGui.QTextEdit(self)
    self.__textDisplay.setFontFamily("Monospace")
    self.__textDisplay.insertPlainText(text)
    self.__textDisplay.setReadOnly(True)
    self.__textDisplay.setLineWrapMode(QtGui.QTextEdit.FixedColumnWidth)
    self.__textDisplay.setLineWrapColumnOrWidth(80)
    layout.addWidget(self.__textDisplay)

    self.__closeButton = QtGui.QPushButton("Close",self)
    self.connect(self.__closeButton,QtCore.SIGNAL("clicked()"), self.close)
    layout.addWidget(self.__closeButton)
    self.setLayout(layout)



import logging

from PyQt4 import QtCore
from PyQt4 import QtGui

from ConfigParser import ConfigParser
import os

from Vispa.Main.SplitterTab import SplitterTab
from Vispa.Gui.Header import FrameWithHeader
from Vispa.Gui.ToolBoxContainer import ToolBoxContainer
from Vispa.Views.PropertyView import PropertyView
from Vispa.Main.GuiFacade import guiFacade

from CommandLineDesigner import CommandLineDesigner, replaceEnviromentVariables
from CommandLineDesigner import CommandLineXmlFileReader, CommandLineXmlFileWriter
from OptionModel import OptionModel
from OptionDecoratorModel import OptionDecoratorModel
from OptionDecoratorEditor import OptionDecoratorEditor
from Delegates import OptionDecoratorSelectionDelegate, ScriptEditDelegate, OptionDecoratorEditDelegate
from ScriptEditor import ScriptEditor
from ParameterRangeModel import ParameterRangeModel
from ParameterRange import ParameterRange

from ParameterRangeEditor import ParameterRangeEditor

from BatchJobsPreview import BatchJobsPreviewDialog
from SubmitDialog import SubmitDialog

from time import sleep


#
# GUI Widgets shown in the BatchJobDesignerTab
#

class OptionDecoratorListWidget(QtGui.QListView):
  """
  Displays the option decorators
  """
  def __init__(self, commandLineDesigner, parent=None):
    QtGui.QListView.__init__(self, parent)
    self.contextMenu = QtGui.QMenu(self)
    #Needs to know cmdldes. for edit action
    self.__commandLineDesigner = commandLineDesigner
    self.__removeOptionDecoratorAction = QtGui.QAction("Remove OptionDecorator",
        self)
    self.__editOptionDecoratorAction = QtGui.QAction("Edit OptionDecorator ",
        self)
    self.__editOptionDecoratorAction.triggered.connect(self.__editOptionDecorator)
    self.__removeOptionDecoratorAction.triggered.connect(self.removeSelectedOptionDecorators)
    self.contextMenu.addAction(self.__editOptionDecoratorAction)
    self.contextMenu.addAction(self.__removeOptionDecoratorAction)

    self.__optionDecoratorEditDelegate = OptionDecoratorEditDelegate(self)
    self.setItemDelegate(self.__optionDecoratorEditDelegate)

  def contextMenuEvent(self, event):
    logging.debug("OptionDecoratorListWidget: show menu")
    if len(self.selectedIndexes()) == 0:
      self.__editOptionDecoratorAction.setEnabled(False)
      self.__removeOptionDecoratorAction.setEnabled(False)
    elif len(self.selectedIndexes()) == 1:
      self.__editOptionDecoratorAction.setEnabled(True)
      self.__removeOptionDecoratorAction.setEnabled(True)
    else:
      self.__editOptionDecoratorAction.setEnabled(False)
      self.__removeOptionDecoratorAction.setEnabled(True)
    self.contextMenu.popup(event.globalPos())

  def removeSelectedOptionDecorators(self):
    self.model().removeData(self.selectedIndexes())

  def  __editOptionDecorator(self):
    item = self.selectedIndexes()[0].internalPointer()
    editor = OptionDecoratorEditor(self)
    #list all existing parameterRanges to assist the creation of new OptionDecorators
    for parameterRange in (self.__commandLineDesigner.getParameterRanges()):
      editor.addExistingParameterRange([parameterRange.getName(), parameterRange.getValues()]) #add a tuple key/name, value
    editor.setName(item.getName())
    editor.setScript(item.getScript())
    editor.setWindowTitle("Edit Option Decorator")
    editor.exec_()
    if editor.result() == QtGui.QDialog.Accepted:
      item.setScript(editor.getScript())
      item.setName(editor.getName())
      self.model()._dataChanged(self.selectedIndexes()[0])





class ParameterRangesWidget(QtGui.QTableView):
  """
  Widget showing the parameter ranges
  """
  def __init__(self, parent=None):
    QtGui.QTableView.__init__(self, parent)

  def initialize(self):
    self.__scriptEditDelegate = ScriptEditDelegate(self)
    columnNames = self.model().getColumnNames()

    self.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
    self.setColumnWidth(columnNames.index(ParameterRangeModel.ColumnNames.NAME),
        150)
    #self.setColumnWidth(columnNames.index(ParameterRangeModel.ColumnNames.VALUES),
    #    100)

    self.horizontalHeader().setResizeMode(columnNames.index(ParameterRangeModel.ColumnNames.SCRIPT), QtGui.QHeaderView.Stretch)
    self.setColumnWidth(columnNames.index(ParameterRangeModel.ColumnNames.ACTIVE),
        50)
    self.horizontalHeader().setResizeMode(columnNames.index(ParameterRangeModel.ColumnNames.ACTIVE),
        QtGui.QHeaderView.Fixed)
    self.setItemDelegateForColumn(columnNames.index(ParameterRangeModel.ColumnNames.SCRIPT),
      self.__scriptEditDelegate)

    self.contextMenu = QtGui.QMenu(self)
    self.__removeParameterRangeAction = QtGui.QAction("Remove ParameterRange",
        self)
    self.contextMenu.addAction(self.__removeParameterRangeAction)
    self.__editParameterRangeAction = QtGui.QAction("Edit ParameterRange ",
        self)
    self.__editParameterRangeAction.triggered.connect(self.__editParameterRange)
    self.__removeParameterRangeAction.triggered.connect(self.removeSelectedParameterRanges)
    self.contextMenu.addAction(self.__editParameterRangeAction)


  def contextMenuEvent(self, event):
    logging.debug("ParameterRangesWidget: show menu")

    rows = set()
    for index in self.selectedIndexes():
      rows.add(index.row())
#    print rows, len(rows)
    if len(rows) == 0:
      self.__editParameterRangeAction.setEnabled(False)
      self.__removeParameterRangeAction.setEnabled(False)
    elif len(rows) == 1:
      self.__editParameterRangeAction.setEnabled(True)
      self.__removeParameterRangeAction.setEnabled(True)
    else:
      self.__editParameterRangeAction.setEnabled(False)
      self.__removeParameterRangeAction.setEnabled(True)

    self.contextMenu.popup(event.globalPos())


  def removeSelectedParameterRanges(self):
    self.model().removeData(self.selectedIndexes())


  def  __editParameterRange(self):
    logging.debug("BatchManagerTab: Creating new ParameterRangeEditor")
    parameterRange = self.selectedIndexes()[0].internalPointer()
    editor = ParameterRangeEditor(self)
    editor.setParameterRange(parameterRange)
    editor.setWindowTitle("Edit Range")
    editor.exec_()
    if editor.result() == QtGui.QDialog.Accepted:
      p = editor.getParameterRange()
      p.updateValue()

      parameterRange.setName(p.getName())
      parameterRange.setScript(p.getScript())
      parameterRange.setValues(p.getValues())
      parameterRange.setActive(p.isActive())
#      self.model()._dataChanged(self.selectedIndexes()[0])



class AvailableOptionsWidget(QtGui.QTreeView):
  """
  TreeView of all availeable options
  """
  def __init__(self, parent=None):
    QtGui.QTreeView.__init__(self, parent)


  def initialize(self):
    columnNames = self.model().getColumnNames()
    self.hideColumn(columnNames.index(OptionModel.ColumnNames.DECORATOR_NAME))

    self.header().setResizeMode(1, QtGui.QHeaderView.Stretch)
    self.header().setResizeMode(3, QtGui.QHeaderView.Fixed)
    self.setColumnWidth(3, 50)

    self.contextMenu = QtGui.QMenu(self)
    self.removeOptionAction = QtGui.QAction("Remove Option",
        self)
    self.removeOptionAction.setEnabled(False)
    self.removeOptionAction.triggered.connect(self.removeSelectedOptions)
    self.contextMenu.addAction(self.removeOptionAction)
    self.editOptionAction = QtGui.QAction("Edit Option",
        self)
    self.editOptionAction.setEnabled(False)
    self.editOptionAction.triggered.connect(self.__editOption)
    self.contextMenu.addAction(self.editOptionAction)

    self.header().setResizeMode(columnNames.index(OptionModel.ColumnNames.NAME),
        QtGui.QHeaderView.ResizeToContents)
    self.header().setResizeMode(columnNames.index(OptionModel.ColumnNames.VALUE),
        QtGui.QHeaderView.Interactive)


  def removeSelectedOptions(self):
    logging.debug("Remove Options Triggered")
    self.model().removeData(self.selectedIndexes())


  def __editOption(self):
    item = self.selectedIndexes()[0].internalPointer()
    editor = OptionEditor(self)
    editor.setName(item.getName())
    editor.setActiveState(item.isActive())
    editor.setWindowTitle("Edit Option" + item.getName())
    editor.setGroup(item.getGroup())
    editor.setOptionString(item.getOptionString())
    editor.setOptionValue(str(item.getValue()))
    editor.exec_()
    if editor.result() == QtGui.QDialog.Accepted:
      item.setGroup(editor.getGroup())
      item.setName(editor.getName())
      item.setOptionString(editor.getOptionString())
      item.setValue(editor.getOptionValue())
      item.setActive(editor.getActiveState())
      self.model()._dataChanged(self.selectedIndexes()[0])


  def contextMenuEvent(self, event):
    logging.debug("ParameterRangesWidget: show menu")
    rows = set()
    for index in self.selectedIndexes():
      rows.add(index.row())
    if len(rows) == 0:
      self.editOptionAction.setEnabled(False)
      self.removeOptionAction.setEnabled(False)
    elif len(rows) == 1:
      self.editOptionAction.setEnabled(True)
      self.removeOptionAction.setEnabled(True)
    else:
      self.editOptionAction.setEnabled(False)
      self.removeOptionAction.setEnabled(True)

    self.contextMenu.popup(event.globalPos())



class ActiveOptionsWidget(QtGui.QTreeView):
  """
  View containing a filter on the active options
  """
  def __init__(self, parent=None):
    QtGui.QTreeView.__init__(self, parent)
    self.__activeStateFilterModel = ActiveStateFilterModel(self)


  def initialize(self):
    self.hideColumn(self.model().sourceModel().getColumnNames().index(OptionModel.ColumnNames.ACTIVE))
    self.hideColumn(self.model().sourceModel().getColumnNames().index(OptionModel.ColumnNames.VALUE))
    self.__decoratorSelection = OptionDecoratorSelectionDelegate(self.model().sourceModel().getCommandLineDesigner(), self)
    self.setItemDelegateForColumn(self.model().sourceModel().getColumnNames().index(OptionModel.ColumnNames.DECORATOR_NAME),
      self.__decoratorSelection)
    self.expandAll()

  def setModel(self, model):
    """
    Sets the model to the filtered one
    """
    self.__activeStateFilterModel.setSourceModel(model)
    QtGui.QTreeView.setModel(self, self.__activeStateFilterModel)
    self.header().setResizeMode(0,QtGui.QHeaderView.ResizeToContents)


  def modelUpdate(self, modelIndex):
    self.__activeStateFilterModel.invalidate()
    self.update()



class ActiveStateFilterModel(QtGui.QSortFilterProxyModel):
  """
  Filter on the active options fo,r the Modified Options Widget
  """
  def __init__(self, parent=None):
    QtGui.QSortFilterProxyModel.__init__(self, parent)


  def filterAcceptsRow(self, sourceRow, sourceParent):
    groupItem = sourceParent.internalPointer()
    if groupItem == None:
      if sourceRow == 0:
        # This is the ROOT Item
        return True
      else:
        item = self.sourceModel().getRootItem().children()[sourceRow]
        if len(item.getActiveItems()) == 0:
          return False
        else:
          return True
    item = groupItem.children()[sourceRow]
    return item.isActive()


class AddRemoveButtonWidget(QtGui.QWidget):
  """
  Widget holding an add and remove button
  """
  def __init__(self, parent=None):
    QtGui.QWidget.__init__(self, parent)
    l = QtGui.QHBoxLayout()
    l.setContentsMargins(2,2,2,2)
    self.addButton = QtGui.QToolButton(self)
    self.addButton.setText('+')
    l.addWidget(self.addButton)
    self.removeButton = QtGui.QToolButton(self)
    self.removeButton.setText('-')
    l.addWidget(self.removeButton)
    self.setLayout(l)


class AddRemoveButtonParameterRangesWidget(AddRemoveButtonWidget):
  """
  Widget holding an add and remove button with an additional fileopen
  dialog button
  """
  def __init__(self, parent=None):
    AddRemoveButtonWidget.__init__(self, parent)
    self.addFromFileListButton = QtGui.QToolButton(self)
    self.addFromFileListButton.setText('')
    self._fileopenIcon = QtGui.QIcon(QtGui.QPixmap(":/resources/fileopen.svg"))
    self.addFromFileListButton.setIcon(self._fileopenIcon)
    self.layout().addWidget(self.addFromFileListButton)





class BatchJobDesignerTab(SplitterTab):
    """
    The Batch Job Designer Tab holds the gui componentents to create
    Batch Jobs with multiple parameters etc. It uses the
    CommandLineDesigner to actually create the jobs.
    """
    #List of Acessors to each BatchSystem
    #__batchManagerAccessors = []

    def __init__(self, plugin, parent=None):
        logging.debug("%s: __init__()" % self.__class__.__name__)

        self.__commandLineDesigner = CommandLineDesigner()

        self.__optionModel = OptionModel(self.__commandLineDesigner)
        self.__parameterRangeModel = ParameterRangeModel(self.__commandLineDesigner)
        self.__optionDecoratorModel = OptionDecoratorModel(self.__commandLineDesigner)
        self.__optionModel.dataChanged.connect(self.__dataModified)
        self.__optionDecoratorModel.dataChanged.connect(self.__dataModified)
        self.__parameterRangeModel.dataChanged.connect(self.__dataModified)


        # Lsit of threads for job submission - list in case the user is
        # too fast :-)
        self.__submitThreads = {}
        #
        # Gui layout in columns
        #
        SplitterTab.__init__(self, parent)

        self.__plugin = plugin
        self.__availableOptions = []
        self.horizontalSplitter().setSizes([250, 250])

        self.__leftToolBoxContainer = ToolBoxContainer(self.horizontalSplitter())
        self.__commandFrame = FrameWithHeader()
        self.__commandFrame.header().setText("Command")
        self.__cmdLineEdit = QtGui.QLineEdit(self)
        self.__cmdLineEditCheck = QtGui.QLabel(self)
        contWidget = QtGui.QWidget(self.__commandFrame)
        contWidget.setLayout(QtGui.QHBoxLayout())
        contWidget.layout().addWidget(self.__cmdLineEdit)
        contWidget.layout().addWidget(self.__cmdLineEditCheck)
        self.setCommand(self.__commandLineDesigner.getCommand())
        self.connect(self.__cmdLineEdit, QtCore.SIGNAL("editingFinished ()"), self.__cmdLineEditChanged)
        self.__commandFrame.addWidget(contWidget)
        #self.__commandFrame.addWidget(self.__cmdLineEdit)
        self.__leftToolBoxContainer.addWidget(self.__commandFrame)

        self.__availableOptionsFrame = FrameWithHeader(self)
        self.__availableOptionsFrame.header().setText('Available Options')

        self.__availableOptionsWidget = AvailableOptionsWidget(self)
        self.__availableOptionsWidget.setModel(self.__optionModel)
        self.__availableOptionsWidget.initialize()
        self.__availableOptionsFrame.addWidget(self.__availableOptionsWidget)
        self.__addRemoveOptionWidget = AddRemoveButtonWidget(self.__availableOptionsFrame.header())
        self.__addRemoveOptionWidget.addButton.pressed.connect(self.__createOption)
        self.__addRemoveOptionWidget.addButton.setToolTip('Add Option')
        self.__addRemoveOptionWidget.removeButton.pressed.connect(self.__availableOptionsWidget.removeSelectedOptions)
        self.__addRemoveOptionWidget.removeButton.setToolTip('Remove Option')

        self.__leftToolBoxContainer.addWidget(self.__availableOptionsFrame)
        self.__leftToolBoxContainer.splitter().setSizes([20, 800])

        self.__centerToolBoxContainer = ToolBoxContainer(self.horizontalSplitter())
        self.__parameterRangesFrame = FrameWithHeader(self)
        self.__parameterRangesFrame.header().setText('Parameter Ranges')
        self.__parameterRangesWidget = ParameterRangesWidget(self)
        self.__parameterRangesWidget.setModel(self.__parameterRangeModel)
        self.__parameterRangesWidget.initialize()
        self.__parameterRangesFrame.addWidget(self.__parameterRangesWidget)
        self.__numberOfJobsCounter = QtGui.QLabel("Total Number of Jobs: --", self.__parameterRangesFrame)
        self.__parameterRangesFrame.addWidget(self.__numberOfJobsCounter)
        self.__parameterRangeModel.dataChanged.connect(self.__updateNumberOfJobsCounter)
        self.__parameterRangeModel.rowsInserted.connect(self.__updateNumberOfJobsCounter)
        self.__parameterRangeModel.rowsRemoved.connect(self.__updateNumberOfJobsCounter)

        self.__addRemoveParameterRangesWidget = AddRemoveButtonParameterRangesWidget(self.__parameterRangesFrame.header())
        self.__addRemoveParameterRangesWidget.addButton.pressed.connect(self.__createParameterRange)
        self.__addRemoveParameterRangesWidget.removeButton.pressed.connect(self.__parameterRangesWidget.removeSelectedParameterRanges)
        self.__addRemoveParameterRangesWidget.addFromFileListButton.pressed.connect(self.__createParameterRangeFromFileList)
        self.__addRemoveParameterRangesWidget.addButton.setToolTip('Add Parameter Range')
        self.__addRemoveParameterRangesWidget.removeButton.setToolTip('Remove Parameter Range')
        self.__addRemoveParameterRangesWidget.addFromFileListButton.setToolTip('Create a new ParameterRange using an InputFile-Browser')

        self.__centerToolBoxContainer.addWidget(self.__parameterRangesFrame)

        self.__optionDecoratorFrame = FrameWithHeader(self)
        self.__optionDecoratorFrame.header().setText('Option Decorators')
        self.__optionDecoratorWidget = OptionDecoratorListWidget(self.__commandLineDesigner, self)

        self.__optionDecoratorWidget.setModel(self.__optionDecoratorModel)
        self.__optionDecoratorFrame.addWidget(self.__optionDecoratorWidget)

        self.__addRemoveOptionDecoratorWidget = AddRemoveButtonWidget(self.__optionDecoratorFrame.header())
        self.__addRemoveOptionDecoratorWidget.addButton.pressed.connect(self.__createOptionDecorator)
        self.__addRemoveOptionDecoratorWidget.removeButton.pressed.connect(self.__optionDecoratorWidget.removeSelectedOptionDecorators)
        self.__addRemoveOptionDecoratorWidget.addButton.setToolTip('Add Option Decorator')
        self.__addRemoveOptionDecoratorWidget.removeButton.setToolTip('Remove Option Decorator')

        self.__centerToolBoxContainer.addWidget(self.__optionDecoratorFrame)

        self.__activeOptionsFrame = FrameWithHeader(self)
        self.__activeOptionsFrame.header().setText('Active Options')
        self.__activeOptionsWidget = ActiveOptionsWidget(self)
        self.__activeOptionsWidget.setModel(self.__optionModel)
        self.__activeOptionsWidget.initialize()
        self.__activeOptionsFrame.addWidget(self.__activeOptionsWidget)
        self.__centerToolBoxContainer.addWidget(self.__activeOptionsFrame)
        self.connect(self.__availableOptionsWidget,
            QtCore.SIGNAL("clicked(const QModelIndex &)"),
            self.__activeOptionsWidget.modelUpdate)

        self.__designerToolBar = guiFacade.createPluginToolBar("BatchDesignerToolbar")
        #self.__designermenu = guiFacade.createPluginMenu("")
        #
        # Create Actions
        #
        self.__previewAction = guiFacade.createAction('Preview',image =
            'preview')
        self.__designerToolBar.addAction(self.__previewAction)
        self.connect(self.__previewAction, QtCore.SIGNAL("triggered()"),
            self.__batchJobsPreview)

        self.__submitAction = guiFacade.createAction('Submit', image='submit')
        self.__designerToolBar.addAction(self.__submitAction)
        self.connect(self.__submitAction, QtCore.SIGNAL("triggered()"),
            self.__submitBatchJobs)
        if not self.__plugin.getBatchManagerAccessors():
          self.__submitAction.setEnabled(False)


        self.__designerToolBar.addSeparator()

        createOptionAction = guiFacade.createAction('New Option',
            image='newOption')
        self.__designerToolBar.addAction(createOptionAction)
        self.__availableOptionsWidget.contextMenu.addAction(createOptionAction)
        self.connect(createOptionAction, QtCore.SIGNAL("triggered()"),
            self.__createOption)

        createParameterRangeAction = guiFacade.createAction('New ParameterRange',
            image='newParameterRange')
        self.__designerToolBar.addAction(createParameterRangeAction)
        self.__parameterRangesWidget.contextMenu.addAction(createParameterRangeAction)
        self.connect(createParameterRangeAction, QtCore.SIGNAL("triggered()"),
            self.__createParameterRange)

        createParameterRangeFromFileListAction = guiFacade.createAction('Create ParameterRange using an InputFile-Browser')
        #self.__designerToolBar.addAction(createParameterRangeFromFileListAction)
        self.__parameterRangesWidget.contextMenu.addAction(createParameterRangeFromFileListAction)
        self.connect(createParameterRangeFromFileListAction, QtCore.SIGNAL("triggered()"),
            self.__createParameterRangeFromFileList)

        createOptionDecoratorAction = guiFacade.createAction('New Option Decorator',
            image='newOptionDecorator')
        self.__designerToolBar.addAction(createOptionDecoratorAction)
        self.__optionDecoratorWidget.contextMenu.addAction(createOptionDecoratorAction)
        self.connect(createOptionDecoratorAction, QtCore.SIGNAL("triggered()"),
            self.__createOptionDecorator)



    def __dataModified(self):
      """
      slot to manage actions if data is modified
      """
      self.setModified(True)


    def activated(self):
      """
      Calles when instance of tab is the current visible tab. Here the
      Toolbars etc haave to be made visible.
      """
      guiFacade.showPluginToolBar(self.__designerToolBar)


    def __updateNumberOfJobsCounter(self):
      self.__numberOfJobsCounter.setText("Total Number of Jobs: %i" %
          (self.__parameterRangeModel.getNumberOfJobs()))


    def addOption(self, *args):
      #simply pass the call to the OptionModel
      self.__optionModel.addOption(*args)
      self.setModified(True)


    def addParameterRange(self, *args):
      self.__parameterRangeModel.addParameterRange(*args)
      name = args[0] + "_SimpleDecorator"
      script = "return valueDict['%s']" % (args[0])
      self.__optionDecoratorModel.addOptionDecorator(name, script)
      self.setModified(True)


    def __cmdLineEditChanged(self):
      self.setCommand(str(self.__cmdLineEdit.text()))
      self.setModified(True)
      self.setModified(True)


    def setCommand(self, command):
      self.__commandLineDesigner.setCommand(command)
      self.__cmdLineEdit.setText(self.__commandLineDesigner.getCommand())
      pixmap = QtGui.QPixmap(12,12)
      if os.access(command, os.X_OK):
        pixmap.fill(QtCore.Qt.green)
        self.__cmdLineEditCheck.setToolTip("OK")
      else:
        pixmap.fill(QtCore.Qt.red)
        self.__cmdLineEditCheck.setToolTip("Command doesn't exist")

      self.__cmdLineEditCheck.setPixmap(pixmap)
      self.setModified(True)


    def __batchJobsPreview(self):
      """
      Opens a widget showing a preview of all commands that will be
      submitted
      """
      logging.debug("BatchJobDesignerTab: Opening preview")
      jobList = self.__commandLineDesigner.buildCommandLines()
      self.__previewWidget = BatchJobsPreviewDialog(jobList)
      self.__previewWidget.exec_()


    def __submitBatchJobs(self):
      """
      Opens the submit dialog
      """
      submitDialog = SubmitDialog(self.__plugin, self)
      jobList = self.__commandLineDesigner.buildCommandLines()
      submitDialog.setJobList(jobList)
      submitDialog.exec_()
      if submitDialog.result() == QtGui.QDialog.Accepted:
        logging.debug("BatchManagerTab: Accepted submission")


        submitThread = SubmitThread(submitDialog.getSelectedBatchManagerAccessor())
        submitThread.setJobList(jobList)
        submitThread.setPreExecutionScript(submitDialog.getPreExecutionScript())
        submitThread.setPostExecutionScript(submitDialog.getPostExecutionScript())
        submitThread.setJobOptions(submitDialog.getJobOptions())

        progressBarDialog = QtGui.QDialog(self)
        layout = QtGui.QVBoxLayout()
        progressBar = QtGui.QProgressBar()
        progressBar.setRange(0, len(jobList) - 1)
        layout.addWidget(progressBar)
        progressBarDialog.setLayout(layout)
        progressBarDialog.show()
        buttonBox = QtGui.QDialogButtonBox()
        buttonBox.addButton(QtGui.QDialogButtonBox.Abort)
        layout.addWidget(buttonBox)
        self.connect(buttonBox, QtCore.SIGNAL("rejected()"), submitThread.abortSubmission)
        self.connect(submitThread, QtCore.SIGNAL("finished()"), progressBarDialog.close)
        self.connect(submitThread, QtCore.SIGNAL("finished()"), self.__submissionFinished)
        self.connect(submitThread, QtCore.SIGNAL("jobSubmitted"), progressBar.setValue)
        progressBarDialog.show()
        submitThread.start()
        message = "Submitting %i jobs to %s" % (len(jobList),
            submitDialog.getSelectedBatchManagerAccessor().getManagerName())
        id = guiFacade.statusBarStartMessage(message)
        self.__submitThreads[id] = submitThread

    def __submissionFinished(self):
      for key in self.__submitThreads.keys():
        if not self.__submitThreads[key].isRunning():
          guiFacade.statusBarStopMessage(key)
          self.__submitThreads.pop(key)



    def __createOption(self):
      editor = OptionEditor(self)
      editor.setName("New Option")
      editor.setActiveState(True)
      editor.setWindowTitle("New Option")
      editor.exec_()
      if editor.result() == QtGui.QDialog.Accepted:
        logging.debug("BatchManagerTab: Creating new Option")
        groupName = editor.getGroup()
        optionName = editor.getName()
        optionString = editor.getOptionString()
        optionValue = editor.getOptionValue()
        activeState = editor.getActiveState()
        self.__optionModel.addOption(groupName, optionName, optionString,
            optionValue, activeState)
      self.setModified(True)



    def __createOptionDecorator(self):
      decoratorEditor = OptionDecoratorEditor(self)
      decoratorEditor.setModal(True)
      decoratorEditor.setName("New Option Decorator")
      decoratorEditor.setWindowTitle("New Option Decorator")

      #list all existing parameterRanges to assist the creation of new OptionDecorators
      for parameterRange in (self.__commandLineDesigner.getParameterRanges()):
          decoratorEditor.addExistingParameterRange([parameterRange.getName(), parameterRange.getValues()]) #add a tuple key/name, value

      decoratorEditor.exec_()
      if decoratorEditor.result() == QtGui.QDialog.Accepted:
        logging.debug("BatchManagerTab: Creating new decorator")
        name = decoratorEditor.getName()
        script = decoratorEditor.getScript()
        self.__optionDecoratorModel.addOptionDecorator(name, script)
      self.setModified(True)


    def __createParameterRange(self):
      #guiFacade.showErrorMessage("Not implemented yet")
      nParRAnges = len(self.__commandLineDesigner.getParameterRanges())
      name = "New Parameter Range %i" % (nParRAnges)
      p = ParameterRange(name)
      p.setActive(True)
      editor = ParameterRangeEditor(self)
      editor.setParameterRange(p)
      editor.setWindowTitle("New Parameter Range")
      editor.exec_()
      if editor.result() == QtGui.QDialog.Accepted:
        logging.debug("BatchManagerTab: Creating new ParameterRangeEditor")
        p = editor.getParameterRange()
        self.addParameterRange(p.getName(), p.getValues(), p.getScript(),
            p.isActive())
      self.setModified(True)

    def __createParameterRangeFromFileList(self):
      #guiFacade.showErrorMessage("Not implemented yet")
      filenames = guiFacade.showFilesOpenDialog("", "All files (*)")

      nParRAnges = len(self.__commandLineDesigner.getParameterRanges())
      name = "InputFiles"
      p = ParameterRange(name)
      p.setActive(True)
      p.setScript("return %s" % filenames)
      if not filenames == []:
          self.addParameterRange(p.getName(), p.getValues(), p.getScript(), p.isActive())
          self.setModified(True)

    def writeFile(self, filename):
      logging.debug('Writing BatchJob to ' + filename)
      CommandLineXmlFileWriter.serialize(self.__commandLineDesigner,
          filename)
      return True


    def updateContent(self):
      pass


    def load(self, filename):
      logging.debug('Reading BatchJob from ' + filename)
      reader = CommandLineXmlFileReader(filename)
      self.__commandLineDesigner = reader.deserialize()
      self.__optionModel.setCommandLineDesigner(self.__commandLineDesigner)
      self.__activeOptionsWidget.setModel(self.__optionModel)
      self.__activeOptionsWidget.initialize()
      self.__optionDecoratorModel.setCommandLineDesigner(self.__commandLineDesigner)
      self.__parameterRangeModel.setCommandLineDesigner(self.__commandLineDesigner)
      self.__updateNumberOfJobsCounter()
      self.setCommand(self.__commandLineDesigner.getCommand())
      return True


    def readFile(self, filename):
        return self.load(filename)

    @classmethod
    def staticSupportedFileTypes(cls):
        """ Returns supported file type: xml with ending .job.
        """
        return [('job', 'Batch Job Designer file')]






class OptionEditor(QtGui.QDialog):
  ''' Dialog to add an option to the BatchJobDesigner'''
  def __init__(self, parent=None):
    QtGui.QDialog.__init__(self, parent)
    self.setMinimumSize(340, 100)
    layout = QtGui.QVBoxLayout()
    self.__editStatus = False

    label = QtGui.QLabel("Group")
    layout.addWidget(label)
    self.__groupLineEdit = QtGui.QLineEdit()
    self.__groupLineEdit.setText("default")
    self.__groupLineEdit.setToolTip("Enter the option group. Only used for grouping options in the display.")
    layout.addWidget(self.__groupLineEdit)

    label = QtGui.QLabel("Name")
    layout.addWidget(label)
    self.__optionNameLineEdit = QtGui.QLineEdit(self)
    self.__optionNameLineEdit.setText("")
    self.__optionNameLineEdit.setToolTip("Name of the option.")
    layout.addWidget(self.__optionNameLineEdit)

    self.__activeCheckBox = QtGui.QCheckBox(self)
    self.__activeCheckBox.setText('Active')
    self.__activeCheckBox.setToolTip("Activates option for usage in Commandline")
    layout.addWidget(self.__activeCheckBox)



    label = QtGui.QLabel("Option String")
    layout.addWidget(label)
    self.__optionStringLineEdit = QtGui.QLineEdit(self)
    self.__optionStringLineEdit.setText("")
    self.__optionStringLineEdit.setToolTip("Option string to be added to the commandline,  e.g. --set=.")
    layout.addWidget(self.__optionStringLineEdit)

    label = QtGui.QLabel("Value")
    layout.addWidget(label)
    self.__valueLineEdit = QtGui.QLineEdit(self)
    self.__valueLineEdit.setText("")
    self.__valueLineEdit.setToolTip("Value to be passed behind the option name")
    layout.addWidget(self.__valueLineEdit)

    buttonBox = QtGui.QDialogButtonBox()
    buttonBox.addButton(QtGui.QDialogButtonBox.Ok)
    buttonBox.addButton(QtGui.QDialogButtonBox.Cancel)
    self.connect(buttonBox, QtCore.SIGNAL("accepted()"), self.accept);
    self.connect(buttonBox, QtCore.SIGNAL("rejected()"), self.reject);
    layout.addWidget(buttonBox)

    self.setLayout(layout)


  def setActiveState(self, value):
    if value:
      self.__activeCheckBox.setCheckState(QtCore.Qt.Checked)
    else:
      self.__activeCheckBox.setCheckState(QtCore.Qt.Unchecked)


  def getActiveState(self):
    return self.__activeCheckBox.checkState() == QtCore.Qt.Checked


  def setGroup(self, group):
    self.__groupLineEdit.setText(group)

  def getGroup(self):
    return str(self.__groupLineEdit.text())

  def setName(self, name):
    self.__optionNameLineEdit.setText(name)

  def getName(self):
    return str(self.__optionNameLineEdit.text())

  def setOptionString(self, value):
    self.__optionStringLineEdit.setText(value)

  def getOptionString(self):
    return str(self.__optionStringLineEdit.text())

  def setOptionValue(self, value):
    self.__valueLineEdit.setText(value)

  def getOptionValue(self):
    return str(self.__valueLineEdit.text())

  def getEditStatus(self):
    return self.__editStatus

  def accept(self):
    self.__editStatus = True
    QtGui.QDialog.accept(self)

  def reject(self):
    self.__editStatus = False
    QtGui.QDialog.reject(self)





class SubmitThread(QtCore.QThread):
  '''Thread handling the job submission'''
  def __init__(self, batchManagerAccessor):
    QtCore.QThread.__init__(self)
    self.__batchManager = batchManagerAccessor
    self.__jobs = None
    self.__options = None
    self.__running = True
    self.__timeDelay = 0
    self.__preScript = ""
    self.__postScript = ""


  def setPreExecutionScript(self, value):
    self.__preScript = value

  def setPostExecutionScript(self, value):
    self.__postScript = value

  def setTimeDelay(self, value):
    self.__timeDelay = value


  def setJobList(self, jobs):
    self.__jobs = jobs

  def setJobOptions(self, options):
    """
    Sets the per job options from the manager
    """
    self.__options = options


  def run(self):
    logging.debug("BatchSystem: Start job submission thread")
    for i, job in enumerate(self.__jobs):
      if not self.__running:
        break
      self.emit(QtCore.SIGNAL("jobSubmitted"), i)
      self.__batchManager.addJob(job, self.__preScript,
          self.__postScript, self.__options)
      sleep(self.__timeDelay)
    #self.exit()

  def abortSubmission(self):
    logging.debug("BatchSystem: Abort job Submission")
    self.__running = False


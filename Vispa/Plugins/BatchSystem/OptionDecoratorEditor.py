import logging

from PyQt4 import QtCore
from PyQt4 import QtGui

from ScriptEditor import ScriptEditor

class OptionDecoratorEditor(QtGui.QDialog):
  """
  Editor widget for OptionDecorators
  """
  def __init__(self, parent=None):
    QtGui.QDialog.__init__(self, parent)
    layout = QtGui.QVBoxLayout()
    label = QtGui.QLabel("Name:")
    layout.addWidget(label)
    self.__editStatus = False
    self.__nameLineEdit = QtGui.QLineEdit(self)
    self.__nameLineEdit.setToolTip("name of the decorator")
    layout.addWidget(self.__nameLineEdit)

    prefixLabel = "Enter a script passing the current value of a parameter range to a option via its return value.  The current values of the parameter ranges are available as members of the dictionary 'valueDict' with key as the name of the parameter range."
    l = QtGui.QLabel(prefixLabel)
    l.setWordWrap(True)
    layout.addWidget(l)

    self.__existingValueDicts = QtGui.QComboBox(self)
    self.__existingValueDicts.setToolTip("Values")
    self.__existingValueDicts.addItem("Select parameter range to insert return statement.")
    layout.addWidget(self.__existingValueDicts)
    self.__existingValueDicts.setEnabled(False)
    self.__existingValueDicts.currentIndexChanged.connect(self.addToScript)

    self.__scriptEditor = ScriptEditor("Script:", self)
    layout.addWidget(self.__scriptEditor)
    buttonBox = QtGui.QDialogButtonBox()

#    previewButton = QtGui.QPushButton('Preview')
#    buttonBox.addButton(previewButton, QtGui.QDialogButtonBox.ActionRole)
#    self.connect(previewButton, QtCore.SIGNAL("clicked()"), self.previewScript)

    buttonBox.addButton(QtGui.QDialogButtonBox.Ok)
    buttonBox.addButton(QtGui.QDialogButtonBox.Cancel)
    self.connect(buttonBox, QtCore.SIGNAL("accepted()"), self.accept)
    self.connect(buttonBox, QtCore.SIGNAL("rejected()"), self.reject)

    layout.addWidget(buttonBox)

    self.setLayout(layout)
    self.setWindowTitle("Option Decorator Editor")
    self.setMinimumSize(640,480)
    self.__existingParameterRanges = []


  def addToScript(self, index):
    if index != 0:
      valueDictToBeAdded = "return valueDict['%s']" % (self.__existingValueDicts.itemText(self.__existingValueDicts.currentIndex()))
      self.setScript(self.getScript() + "\n" + valueDictToBeAdded)
      self.__existingValueDicts.setCurrentIndex(0)
      return True

#  def previewScript(self):
#      script = self.evaluateScript()
#      print script
#      return True

  def getEditStatus(self):
    return self.__editStatus

  def getName(self):
    return str(self.__nameLineEdit.text())

  def getScript(self):
    return self.__scriptEditor.getData()

  def setName(self, name):
    self.__nameLineEdit.setText(name)

  def setScript(self, script):
    self.__scriptEditor.setData(script)

  def accept(self):
    self.__editStatus = True
    QtGui.QDialog.accept(self)

  def reject(self):
    self.__editStatus = False
    QtGui.QDialog.reject(self)

#  def evaluateScript(self):
#    """Tries to evaluate the script a user has entered - if this succeeds
#    and the the script returns a list, the return value of the script
#    is shown in the preview"""
#    text = self.getScript()
#    text = text.lstrip()
#    text = text.rstrip()
#    text = "\n" + text
#    cmd = "def userScript():\n"
#    cmd += "\n  ".join(text.split("\n"))
#
#    data = None
#    try:
#      exec(cmd)
#      data = userScript()
#    except Exception, e:
#      logging.error("Cannot exec python code: " + str(e))
#    return data

  def addExistingParameterRange(self, tuple): #tuple = [name, [values]]
    self.__existingParameterRanges.append(tuple)
    self.__existingValueDicts.setEnabled(True)
    if len(tuple) > 0:
        self.__existingValueDicts.addItem(tuple[0])

import logging

from PyQt4 import QtCore, QtGui

class ManagerAccessor(QtCore.QObject):
  '''
  Translates the manager callbacks into Qt Signals. 
  Thus the ManagerAccessor emits QtSignals managerStatusChanged,
  managerJobStatusChanged if the
  manager calls back.
  Gives access to the manager and the specific configuration widgets.
  All communication with the batch manager has to pass through this
  accessor, so that the accessor can emit the right signals!
  Hence the batchManager cannot be exposed by this widget!
  '''
  def __init__(self, plugin, batchManager):
    QtCore.QObject.__init__(self)
    self.__batchManager = batchManager
    self.__plugin = plugin
    self.__jobModel = plugin.createJobModel(self.__batchManager)
    self.__filterModel = plugin.createJobListSortFilterProxy(self.__jobModel)

  def setDataPath(self, dataPath):
    self.__batchManager.setDataPath(dataPath)

  def newFilter(self, text):
    self.__filterModel.setFilterRegExp(text)

  def stopAllJobs(self):
    self.__jobModel.beginResetModel()
    self.__batchManager.stopJobs(self.__batchManager.getJobs())
    self.__jobModel.endResetModel()

  def restartAllJobs(self):
    self.__jobModel.beginResetModel()
    self.__batchManager.restartJobs(self.__batchManager.getJobs())
    self.__jobModel.endResetModel()

  def removeAllJobs(self):
    self.__jobModel.beginResetModel()
    self.__batchManager.removeJobs(self.__batchManager.getJobs())
    self.__jobModel.endResetModel()

  def getJobModel(self):
    return self.__jobModel

  def addJob(self, *args):
    self.__jobModel.addJob(*args)

  def getManagerName(self):
    return self.__batchManager.getName()

  def getManagerDescription(self):
    return self.__batchManager.getDescription()

  def loadJobList(self):
    self.__plugin.loadJobList()

  def update(self):
    self.__batchManager.update()

  def getManagerStatusAndConfigurationWidget(self, parent=None):
    '''
    Returns a widget from the plugin in which the specific manager can
    be configured.
    Connects the widget updateStatusMethod with the managerStatusChange
    signal
    '''
    widget = self.__plugin.getManagerStatusAndConfigurationWidget(parent)
    self.connect(self, QtCore.SIGNAL("managerStatusChanged"), widget.updateStatus)
    return widget

  def getJobConfigurationWidget(self, parent=None):
    '''
    Returns a widget from the plugin in which the jobs for the specific
    manager can be configured.
    '''
    return self.__plugin.getJobConfigurationWidget(parent)

  def getJobInfo(self):
    return self.__plugin.getJobInfo()

  def shutdown(self):
    self.__batchManager.shutdown()

  def getJobListWidget(self, parent):
    widget = self.__plugin.getJobListWidget(parent)
    widget.setModel(self.__filterModel)
    widget.initialize()
    return widget

  def getJobStatisticsWidget(self, parent):
    widget = self.__plugin.getJobStatisticsWidget(parent)
    widget.setModel(self.__jobModel)
    self.connect(self, QtCore.SIGNAL("managerStatusChanged"), widget.updateStatus)
    widget.initialize()
    return widget

  def updateJobs(self):
    """
    Update the job info from the manager
    """
    self.__batchManager.update()
    modJobs = self.__batchManager.getModifiedJobs()
    self.__jobModel.jobsChanged(modJobs)

  def getDefaultPreExecutionScript(self):
    return self.__batchManager.getDefaultPreExecutionScript()

  def getDefaultPostExecutionScript(self):
    return self.__batchManager.getDefaultPostExecutionScript()

  def getNumberOfJobsWithStatus(self, status):
    return self.__jobStatistics[status]

  def getPossibleStatuses(self):
    return self.__batchManager.getBatchJobClass().possibleStatuses



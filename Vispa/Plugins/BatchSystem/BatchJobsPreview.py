import logging

from PyQt4 import QtCore
from PyQt4 import QtGui


class BatchJobsPreviewWidget(QtGui.QWidget):
  """
  Widget showing a preview of all jobs that will be submitted.
  """
  def __init__(self, parent = None):
    QtGui.QWidget.__init__(self, parent)
    layout = QtGui.QVBoxLayout()
    self.__label= QtGui.QLabel("Number of jobs created: 0") # + str(len(jobList)))
    layout.addWidget(self.__label)
    self.__listWidget = QtGui.QListWidget(self)
    layout.addWidget(self.__listWidget)
    self.setLayout(layout)

  def setJobList(self, jobList):
    self.__label.setText("Number of jobs created: " + str(len(jobList)))
    self.__listWidget.addItems(QtCore.QStringList(jobList))



class BatchJobsPreviewDialog(QtGui.QDialog):
  """
  Dialog showing the preview widget
  """
  def __init__(self, jobList, parent = None):
    QtGui.QDialog.__init__(self, parent)
    layout = QtGui.QVBoxLayout()
    self.__previewWidget = BatchJobsPreviewWidget(self)
    self.__previewWidget.setJobList(jobList)
    layout.addWidget(self.__previewWidget)
    buttonBox = QtGui.QDialogButtonBox(self)
    buttonBox.addButton(QtGui.QDialogButtonBox.Cancel)
    layout.addWidget(buttonBox)
    self.connect(buttonBox, QtCore.SIGNAL("rejected()"), self.reject)
    self.setLayout(layout)
    self.setWindowTitle("Batch Job Preview")
    self.setMinimumSize(640,480)




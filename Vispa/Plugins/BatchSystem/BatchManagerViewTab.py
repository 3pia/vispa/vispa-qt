import logging
import time

from PyQt4 import QtCore
from PyQt4 import QtGui

from Vispa.Main.SplitterTab import SplitterTab
from Vispa.Gui.Header import FrameWithHeader, Header
from Vispa.Views.PropertyView import PropertyView
from Vispa.Main.GuiFacade import guiFacade

class FilterBarWidget(QtGui.QWidget):
  def __init__(self, parent=None):
    QtGui.QWidget.__init__(self, parent)
    layout = QtGui.QHBoxLayout(self)
    self.setLayout(layout)
    self.lineEdit = QtGui.QLineEdit(self)
    layout.addWidget(self.lineEdit)

    self.__clearButton = QtGui.QPushButton(guiFacade.getIconFromImageName('remove'), 'Clear Filter', self)
    self.__clearButton.pressed.connect(self.lineEdit.clear)
    layout.addWidget(self.__clearButton)

    self.__hideButton = QtGui.QPushButton(guiFacade.getIconFromImageName('filter'), 'Hide Filterbar', self)
    self.__hideButton.pressed.connect(self.hide)
    layout.addWidget(self.__hideButton)




class BatchManagerViewTab(SplitterTab):
    """
    Tab showing the jobs of the managers and 
    """
    __statusAccessors = []
    def __init__(self, plugin, parent=None):
        logging.debug("%s: __init__()" % self.__class__.__name__)
        SplitterTab.__init__(self, parent)
        self.__plugin = plugin
        self.__viewerToolBar = guiFacade.createPluginToolBar("BatchViewerToolbar")

        self._centerFrameWithHeader = FrameWithHeader(self.horizontalSplitter())
        self._centerFrameWithHeader.header().setText("Watched Jobs")

        self.__currentJobListTree = QtGui.QWidget(self)
        self._centerFrameWithHeader.addWidget(self.__currentJobListTree)

        self.__filterBarWidget = FilterBarWidget(self)
        self.__filterBarWidget.hide()
        self.__filterBarWidget.lineEdit.textChanged.connect(self.__updateFilter)
        self.__filterBarWidget.lineEdit.textChanged.connect(self.__updateFilter)
        self.__hideFilterBarAction = guiFacade.createAction('Show/Hide Filterbar', self.__toggleFilterBarVisiility, image='filter')
        self.__viewerToolBar.addAction(self.__hideFilterBarAction)
        self._centerFrameWithHeader.addWidget(self.__filterBarWidget)

        self._rightSplitter = QtGui.QSplitter(QtCore.Qt.Vertical, self.horizontalSplitter())

        self.__statisticsFrameWithHeader = FrameWithHeader(self._rightSplitter)
        self.__statisticsFrameWithHeader.header().setText("Manager Status")
        self.__managerSelector = self.__plugin.getBatchManagerSelectorWidget(self)
        self.__managerSelector.currentIndexChanged.connect(self.__managerSelectionChanged)
        self.__statisticsFrameWithHeader.addWidget(self.__managerSelector)
        self.__currentManagerConfigStatusWidget = QtGui.QWidget()
        self.__currentManagerJobStatisticsWidget = QtGui.QWidget()
        self.__statisticsFrameWithHeader.addWidget(self.__currentManagerConfigStatusWidget)
        self.__statisticsFrameWithHeader.addWidget(self.__currentManagerJobStatisticsWidget)

        self.__watchedJobsWidgets = {}
        self.__managerConfigStatusWidgets = {}
        self.__managerJobStatisticsWidgets = {}
        # defines relative widths of manager view and right side
        self.horizontalSplitter().setSizes([700, 300])
        self.__managerSelectionChanged()

        self.__stopJobsAction = guiFacade.createAction('Stop all Jobs', self.__stopAllJobs, image="stop")
        self.__viewerToolBar.addAction(self.__stopJobsAction)
        self.__removeJobsAction = guiFacade.createAction('Remove all Jobs', self.__removeAllJobs, image='remove')
        self.__viewerToolBar.addAction(self.__removeJobsAction)
        self.__restartJobsAction = guiFacade.createAction('Restart all Jobs', self.__restartAllJobs, image="restart")
        self.__viewerToolBar.addAction(self.__restartJobsAction)

        self.__updateJobsAction = guiFacade.createAction('Update Jobs',
            self.__updateJobs, image='reload')
        self.__viewerToolBar.addAction(self.__updateJobsAction)

    def __toggleFilterBarVisiility(self):
      if self.__filterBarWidget.isVisible():
        self.__filterBarWidget.hide()
      else:
        self.__filterBarWidget.show()


    def __updateFilter(self, text):
      accessor = self.__managerSelector.getSelectedBatchManagerAccessor()
      accessor.newFilter(text)

    def __managerSelectionChanged(self, *args):
        accessor = self.__managerSelector.getSelectedBatchManagerAccessor()
        if not accessor:
          return

        if not self.__watchedJobsWidgets.has_key(accessor):
          accessor.loadJobList()
          self.__managerConfigStatusWidgets[accessor] = accessor.getManagerStatusAndConfigurationWidget(self)
          self.__managerJobStatisticsWidgets[accessor] = accessor.getJobStatisticsWidget(self)
          self.__watchedJobsWidgets[accessor] = accessor.getJobListWidget(self)
          self.__statisticsFrameWithHeader.addWidget(self.__managerConfigStatusWidgets[accessor])
          self.__statisticsFrameWithHeader.addWidget(self.__managerJobStatisticsWidgets[accessor])
          self._centerFrameWithHeader.insertWidget(1, self.__watchedJobsWidgets[accessor])
        else:
          accessor.update()

        self.__currentJobListTree.hide()
        self.__currentJobListTree = self.__watchedJobsWidgets[accessor]
        self.__currentJobListTree.show()

        self.__currentManagerConfigStatusWidget.hide()
        self.__currentManagerConfigStatusWidget = self.__managerConfigStatusWidgets[accessor]
        self.__currentManagerConfigStatusWidget.show()

        self.__currentManagerJobStatisticsWidget.hide()
        self.__currentManagerJobStatisticsWidget = self.__managerJobStatisticsWidgets[accessor]
        self.__currentManagerJobStatisticsWidget.show()

    def activated(self):
        """ 
        """
        guiFacade.showPluginToolBar(self.__viewerToolBar)
        self.updateLabel("", " Batch Manager Viewer")

    def __updateJobs(self):
      self.__managerSelector.getSelectedBatchManagerAccessor().updateJobs()


    def __stopAllJobs(self):
      result = guiFacade.showMessageBox("You are about to STOP ALL jobs!", "Do you want to continue?", QtGui.QMessageBox.Yes | QtGui.QMessageBox.No, QtGui.QMessageBox.Yes)
      if result == QtGui.QMessageBox.Yes:
        accessor = self.__managerSelector.getSelectedBatchManagerAccessor()
        accessor.stopAllJobs()


    def __removeAllJobs(self):
      result = guiFacade.showMessageBox("You are about to REMOVE ALL jobs!", "Do you want to continue?", QtGui.QMessageBox.Yes | QtGui.QMessageBox.No, QtGui.QMessageBox.Yes)
      if result == QtGui.QMessageBox.Yes:
        accessor = self.__managerSelector.getSelectedBatchManagerAccessor()
        accessor.removeAllJobs()


    def __restartAllJobs(self):
      result = guiFacade.showMessageBox("You are about to RESTART ALL jobs!", "Do you want to continue?", QtGui.QMessageBox.Yes | QtGui.QMessageBox.No, QtGui.QMessageBox.Yes)
      if result == QtGui.QMessageBox.Yes:
        accessor = self.__managerSelector.getSelectedBatchManagerAccessor()
        accessor.restartAllJobs()




import threading
from time import sleep
class Polling(threading.Thread):
  ''' Thread with a polling to update e.g. the BatchManagerPlugins'''

  def __init__(self, intervallSeconds=3):
    threading.Thread.__init__(self)
#    self.__condition = threading.Condition(threading.Lock())
    self.__intervall = intervallSeconds
    self.__methods = {}
    self.__running = False

  def add(self, key, callback):
      self.__methods[key] = callback

  def startRunning(self):
      self.__running = True

  def isRunning(self):
      return self.__running

  def stopRunning(self):
      self.__running = False

  def remove(self, key):
      if key in self.__methods:
          del self.__methods[key]

  def run(self):
      while self.__running:

          #create temporary dict to safely iterate through the dictionary
          self.__temp_methods = self.__methods
          for key in self.__temp_methods:
              self.__methods[key]()
          sleep(self.__intervall)
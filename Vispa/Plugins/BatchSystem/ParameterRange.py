import logging

class ParameterRange(object):
  """
  Represents a variable with a range of parameters, which can be
  generated from a python script  
  """

  def __init__(self, name, values=[]):
    self.__name = name
    self.__script = ""
    self._values = values
    self.__active = True

    self.__scriptStatus = None

  def setActive(self, state):
    self.__active = state


  def isActive(self):
    return self.__active


  def setValues(self, values):
    self._values = values


  def getValues(self):
    return self._values


  def setName(self, name):
    self.__name = name


  def getName(self):
    return self.__name


  def setScript(self, script):
    self.__script = script


  def getScript(self):
    return self.__script


  def getScriptStatus(self):
    """
    Returns wheter the last script execution was successfull
    """
    return self.__scriptStatus


  def _evaluateScript(self):
    """Tries to evaluate the script a user has entered - if this succeeds
    and the the script returns a list, the return value of the script
    become the new option value"""
    text = self.__script
    text = text.lstrip()
    text = text.rstrip()
    text = "\n" + text
    cmd = "def userScript():\n"
    cmd += "\n  ".join(text.split("\n"))

    data = None
    try:
      exec(cmd)
      data = userScript()
    except Exception, e:
      logging.error("Cannot exec python code: " + str(e))
    return data


  def updateValue(self):
    data = self._evaluateScript()
    try:
      data = list(data)
      self._values = data
      self.__scriptStatus = True
      return True
    except Exception, e:
      logging.error("Cannot interpret return value as list " + str(e))
      self.__scriptStatus = False
      return False


import logging

class CommandLineOption(object):
  """ Class to store a Command Line Option - not used to create a commandline, but
  used to replace values, and display the option
  """
  #def __init__(self, label, prefix,name, postfix):
  def __init__(self, name, optionString, value = "", group="default"):
    self.__optionString = optionString
    #Use setters to allow type checks
    self.setName(name)
    self.setOptionString(optionString)
    self.setValue(value)
    self.setGroup(group)
    self.__active = True
    self.__decorator = None

  def setActive(self, state):
    logging.debug("CommandLineOption " + self.__name + ": Setting active: " + str(state) )
    self.__active = state

  def isActive(self):
    return self.__active

  def getName(self):
    return self.__name

  def setName(self,name):
    self.__name = name

  def getOptionString(self):
    return self.__optionString

  def setOptionString(self, optionString):
    if optionString == None:
      optionString = ""
    self.__optionString = optionString

  def getValue(self):
    return self.__value

  def setValue(self, value):
    self.__value = value

  def getGroup(self):
    return self.__group

  def setGroup(self, group):
    self.__group = group

  def setDecorator(self, decorator):
    if decorator == None:
      logging.debug("CommandLineOption " + self.__name + ": Unsetting decorator" )
    else:
      logging.debug("CommandLineOption " + self.__name + ": Setting decorator to " + decorator.getName())
    self.__decorator = decorator

  def getDecorator(self):
    return self.__decorator

  def hasDecorator(self):
    return self.__decorator != None


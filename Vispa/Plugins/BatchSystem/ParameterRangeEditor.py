import logging

from copy import copy

from PyQt4 import QtCore
from PyQt4 import QtGui

from ScriptEditor import ScriptEditor

from CommandLineDesigner import replaceEnviromentVariables

class ParameterRangeEditor(QtGui.QDialog):
  """
  Editor for parameter ranges
  """
  def __init__(self, parent = None):
    QtGui.QDialog.__init__(self, parent)
    layout = QtGui.QVBoxLayout()
    self.__editStatus = False

    label= QtGui.QLabel("Name:")
    layout.addWidget(label)
    self.__nameLineEdit = QtGui.QLineEdit(self)
    self.__nameLineEdit.setToolTip("name of the ParameterRange")
    layout.addWidget(self.__nameLineEdit)

    self.__activeCheckBox= QtGui.QCheckBox(self)
    self.__activeCheckBox.setText('Active')
    self.__activeCheckBox.setToolTip("Activates option for usage in Commandline")
    layout.addWidget(self.__activeCheckBox)

    prefixLabel = "Enter user script to generate a parameter range. The script has to return a list of elements. No function header is allowed."

    self.__scriptEditor = ScriptEditor(prefixLabel, self)
    layout.addWidget(self.__scriptEditor)

    label= QtGui.QLabel("Values:")
    layout.addWidget(label)
    self.__valuesLineEdit = QtGui.QLineEdit(self)
    self.__valuesLineEdit.setToolTip("Values")
    layout.addWidget(self.__valuesLineEdit)

    buttonBox = QtGui.QDialogButtonBox()
    buttonBox.addButton(QtGui.QDialogButtonBox.Ok)
    buttonBox.addButton(QtGui.QDialogButtonBox.Cancel)
    self.connect(buttonBox, QtCore.SIGNAL("accepted()"), self.accept)
    self.connect(buttonBox, QtCore.SIGNAL("rejected()"), self.reject)
    layout.addWidget(buttonBox)

    self.setLayout(layout)
    self.setWindowTitle("ParameterRange Editor")

  def getEditStatus(self):
    return self.__editStatus


  def setParameterRange(self, parameterRange):
    """ 
    Sets the parameterRange with a copy. A copy is needed to preserve the
    original data if the editing is canceld.
    """
    self.__parameterRange = copy(parameterRange)
    if parameterRange.isActive():
      self.__activeCheckBox.setCheckState(QtCore.Qt.Checked)
    else:
      self.__activeCheckBox.setCheckState(QtCore.Qt.Unchecked)
    self.__nameLineEdit.setText(parameterRange.getName())
    self.__scriptEditor.setData(parameterRange.getScript())
    values = ", ".join(map(str, parameterRange.getValues()))
    self.__valuesLineEdit.setText(values)


  def getParameterRange(self):
    return self.__parameterRange

  def accept(self):
    self.__editStatus = True
    self.__parameterRange.setName(str(self.__nameLineEdit.text()))
    try:
      valstring = str(self.__valuesLineEdit.text())
      valstring = replaceEnviromentVariables(valstring)
      d = eval(valstring)
      self.__parameterRange.setValues(d)
    except Exception, e:
      logging.error("ParameterRangeEditor: Error setting values " + str(e))
    self.__parameterRange.setScript(self.__scriptEditor.getData())
    self.__parameterRange.setActive(self.__activeCheckBox.checkState() ==
        QtCore.Qt.Checked)

    QtGui.QDialog.accept(self)

  def reject(self):
    self.__editStatus = False
    QtGui.QDialog.reject(self)


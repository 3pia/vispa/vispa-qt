import logging

from PyQt4 import QtCore
from PyQt4 import QtGui

from ScriptEditor import ScriptEditor

class ParameterRangeScriptDialog(QtGui.QDialog):
  """
  Dialog for editing the scripts in parameter ranges
  """
  def __init__(self, parent = None):
    QtGui.QDialog.__init__(self, parent)
    layout = QtGui.QVBoxLayout()

    self.__editStatus = False
    prefixLabel = "def userScript():"
    self.__scriptEditor = ScriptEditor(prefixLabel, self)
    layout.addWidget(self.__scriptEditor)

    buttonBox = QtGui.QDialogButtonBox()
    buttonBox.addButton(QtGui.QDialogButtonBox.Ok)
    buttonBox.addButton(QtGui.QDialogButtonBox.Cancel)
    self.connect(buttonBox, QtCore.SIGNAL("accepted()"), self.__acceptEdit)
    self.connect(buttonBox, QtCore.SIGNAL("rejected()"), self.__rejectEdit)
    layout.addWidget(buttonBox)
    self.setLayout(layout)
    self.setWindowTitle("Edit ParameterRange Script")
    self.setMinimumSize(640,200)

  def getScript(self):
    return self.__scriptEditor.getData()

  def setScript(self,script):
    self.__scriptEditor.setData(script)

  def __acceptEdit(self):
    self.__editStatus = True
    self.close()

  def __rejectEdit(self):
    self.__editStatus = False
    self.close()

  def getEditStatus(self):
    return self.__editStatus


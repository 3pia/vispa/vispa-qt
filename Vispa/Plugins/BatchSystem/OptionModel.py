import logging

from PyQt4 import QtCore
from PyQt4 import QtGui

from CommandLineOption import CommandLineOption

class OptionModel(QtCore.QAbstractItemModel):
  """
  The OptiomModel is the accessor for the CommandLineDesigner. It
  provides an hirachical view for the individual options.
  """

  class GroupItem:
    """
    Stores the informationa bout a group of options. IT nows all options
    having this group tag 
    """
    def __init__(self, name, parent = None):
      self.__childItems = []
      self.__name = name
      self.__parentItem = parent
    def setName(self,  value):
      self.__name = name
    def appendChild(self, child):
      self.__childItems.append(child)
    def child(self, row):
      return self.__childItems[row]
    def children(self):
      return self.__childItems
    def childCount(self):
      return len(self.__childItems)
    def row(self):
      if(self.__parentItem):
        return self.__parentItem.children().index(self)
    def getName(self):
      return self.__name
    def getActiveItems(self):
      """
      Returns all items ahicha re active
      """
      activeItems = []
      for child in self.__childItems:
        if child.isActive():
          activeItems.append(child)
      return activeItems

    

  class ColumnNames:
    """
    Stores the names of the columns. To be used as enum.
    """
    NAME = "Name"
    VALUE = "Default Value"
    ACTIVE = "Active"
    DECORATOR_NAME= "Decorator Name"


  # Names of the columns in the data with ordering as in the views
  __columnNames = [ColumnNames.NAME, ColumnNames.VALUE,
      ColumnNames.ACTIVE, ColumnNames.DECORATOR_NAME]


  def __init__(self, commandLineDesigner, parent=None):
    QtCore.QAbstractItemModel.__init__(self, parent)
    self.__commandLineDesigner = commandLineDesigner
    # Dictionary of all groups with thier name as key and the groupitem
    # as value
    self.__groups = {}
    # The invisible root item of the model
    self.__rootItem = OptionModel.GroupItem("__INTERNAL_ROOT_GROUP_DO_NOT_NAME_ANY_GROUP_SO_OR_IT_WILL_CRASH__!")

    #Check if PyQt Version is up to date
    if not hasattr(QtCore.QAbstractItemModel, 'beginResetModel'):
      logging.warning("QAbstractItemModel has no beginResetModel! Use QAbstractItemModel::reset instead")
      self.beginResetModel = self.reset
      self.endResetModel = self.__dummy

  def __dummy(self):
    pass
  

  def setCommandLineDesigner(self, commandLineDesigner):
    self.beginResetModel()
    self.__commandLineDesigner = commandLineDesigner
    self.__groups = {}
    self.__rootItem = OptionModel.GroupItem("__INTERNAL_ROOT_GROUP_DO_NOT_NAME_ANY_GROUP_SO_OR_IT_WILL_CRASH__!")
    for option in self.__commandLineDesigner.getOptions():
      self.__addOptionToModel(option)

    self.endResetModel()

  def getColumnNames(self):
    return self.__columnNames


  def columnCount(self, parent):
    return len(self.__columnNames)


  def getRootItem(self):
    return self.__rootItem


  def getCommandLineDesigner(self):
    return self.__commandLineDesigner


  def index(self, row, column, parent):
    """
    Returns the QModelIndex of item stored in row,column, parent
    """
    if not self.hasIndex(row, column, parent):
      return QtCore.QModelIndex()

    parentItem = None
    if not parent.isValid():
      parentItem = self.__rootItem
    else:
      parentItem = parent.internalPointer()

    childItem = parentItem.child(row)
    if (childItem):
      return self.createIndex(row, column, childItem)
    else:
      return QtCore.QModelIndex()


  def rowCount(self, parent):
    """
    Returns the number of rows of the parent
    """
    # The items are only linked in column 0 (Name/Group)
    if parent.column() > self.__columnNames.index(OptionModel.ColumnNames.NAME):
      return 0
    if not parent.isValid():
      parentItem = self.__rootItem
    else:
      parentItem = parent.internalPointer()

    if isinstance(parentItem, CommandLineOption):
      return 0
    return parentItem.childCount()


  def headerData(self, section, orientation, role):
    """
    The header data 
    """
    if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
      return self.__columnNames[section]
    return QtCore.QVariant()


  def flags(self, index):
    """
    Returns the flags for the item with index
    """
    if not index.isValid():
      return QtCore.Qt.NoItemFlags

    flags = QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable 
    item = index.internalPointer()

    if index.column() == self.__columnNames.index(OptionModel.ColumnNames.ACTIVE):
      flags = flags | QtCore.Qt.ItemIsUserCheckable

    if isinstance(item, OptionModel.GroupItem):
      pass
    elif isinstance(item, CommandLineOption):
      flags = flags | QtCore.Qt.ItemIsEditable
    return flags


  def parent(self, index):
    """
    Returns the index of the parent of the item with index
    """
    if not index.isValid():
      return QtCore.QModelIndex()
    childItem = index.internalPointer()
    if isinstance(childItem, CommandLineOption):
      parentItem = self.__groups[childItem.getGroup()]
      return self.createIndex(parentItem.row(), 0 , parentItem)
    else:
      return QtCore.QModelIndex()


  def data(self, index, role):
    """
    Returns the data as list of QVariants to be displayed in the view
    """
    if not index.isValid():
      return QtCore.QVariant()
    item = index.internalPointer()

    if isinstance(item, OptionModel.GroupItem):
      if role==QtCore.Qt.DisplayRole:
        if index.column() == self.__columnNames.index(OptionModel.ColumnNames.NAME):
          return item.getName().replace('\ ',' ')

      elif role==QtCore.Qt.CheckStateRole:
        if index.column() == self.__columnNames.index(OptionModel.ColumnNames.ACTIVE):
          activeItems = item.getActiveItems()
          if len(activeItems) == 0:
            return QtCore.Qt.Unchecked
          elif len(activeItems) == item.childCount():
            return QtCore.Qt.Checked
          return QtCore.Qt.PartiallyChecked
      else:
        return QtCore.QVariant()

    elif isinstance(item, CommandLineOption):
      #How to display the options
      if role==QtCore.Qt.DisplayRole:
        if index.column() == self.__columnNames.index(OptionModel.ColumnNames.NAME):
          return item.getName().replace('\ ',' ')
        elif index.column() == self.__columnNames.index(OptionModel.ColumnNames.VALUE):
          return item.getValue()
        elif index.column() == self.__columnNames.index(OptionModel.ColumnNames.DECORATOR_NAME):
          decorator = item.getDecorator()
          if decorator == None:
            return str(" -- ")
          else:
            return decorator.getName()
#      elif role==QtCore.Qt.FontRole:
#        if index.column() == self.__columnNames.index(OptionModel.ColumnNames.VALUE):
#          if item.hasDecorator() and item.isActive():
#            font = QtGui.QFont()
#            font.setItalic(True)
#            return font
      elif role ==QtCore.Qt.ForegroundRole:
        if index.column() == self.__columnNames.index(OptionModel.ColumnNames.VALUE):
          if item.hasDecorator() and item.isActive():
            brush = QtGui.QBrush()
            brush.setColor(QtCore.Qt.gray)
            return brush



      elif role==QtCore.Qt.CheckStateRole:
        if index.column() == self.__columnNames.index(OptionModel.ColumnNames.ACTIVE):
          if item.isActive():
            return QtCore.Qt.Checked
          else:
            return QtCore.Qt.Unchecked
    else:
      logging.error("OptionModel: Unknown item in data??? %s" % str(type(item)))


  def setData(self,index, value, role):
    """
    The model is editable, thus we need this method to transform the
    edit actions to the data
    """
    if not index.isValid():
      return False
    item = index.internalPointer()
    if role == QtCore.Qt.EditRole:
      if isinstance(item, CommandLineOption):
        logging.debug("OptionModel: setData with editrole")
        if index.column() == self.__columnNames.index(OptionModel.ColumnNames.DECORATOR_NAME):
          idx = value.toInt()[0]
          if idx == 0:
            item.setDecorator(None)
          else:
            decorators = self.__commandLineDesigner.getOptionDecorators()
            item.setDecorator(decorators[idx-1])
          self.emit(QtCore.SIGNAL('dataChanged(const QModelIndex &, ' 'const QModelIndex &)'), index, index)
          return True
        elif index.column() == self.__columnNames.index(OptionModel.ColumnNames.NAME):
          if value.toString().isEmpty():
            return False
          item.setName(str(value.toString()))
          self.emit(QtCore.SIGNAL('dataChanged(const QModelIndex &, ' 'const QModelIndex &)'), index, index)
          return True
        elif index.column() == self.__columnNames.index(OptionModel.ColumnNames.VALUE):
          item.setValue(str(value.toString()))
          self.emit(QtCore.SIGNAL('dataChanged(const QModelIndex &, ' 'const QModelIndex &)'), index, index)
          return True

    elif role == QtCore.Qt.CheckStateRole:
      logging.debug("OptionModel: setData with checkstaterole")
      if isinstance(item, OptionModel.GroupItem):
        if value == QtCore.Qt.Checked:
          for child in item.children():
            child.setActive(True)
        else:
          for child in item.children():
            child.setActive(False)

        index2 = self.index(item.childCount(),self.__columnNames.index(OptionModel.ColumnNames.ACTIVE), index)
        self.emit(QtCore.SIGNAL('dataChanged(const QModelIndex &, const QModelIndex &)'), index, index2)
        return True

      elif isinstance(item, CommandLineOption):
        if value == QtCore.Qt.Checked:
          item.setActive(True)
        else:
          item.setActive(False)
        groupItem = self.__groups[item.getGroup()]
        row = self.__rootItem.children().index(groupItem)
        column = self.__columnNames.index(OptionModel.ColumnNames.ACTIVE)
        index2 = self.createIndex(row, column, self.__rootItem)
        #self.emit(QtCore.SIGNAL('dataChanged(const QModelIndex &, ' 'const QModelIndex &)'), index2, index2)
        self.emit(QtCore.SIGNAL('dataChanged(const QModelIndex &, ' 'const QModelIndex &)'), index2, index)
        return True
      return False


  def addOption(self,groupName,optionName, optionString, optionValue, active=False):
    """
    Creates an option and add it to the CommandLineDesigner
    """
    logging.debug("OptionModel:: Adding new Option")
    option = CommandLineOption(optionName, optionString, optionValue, groupName)
    option.setActive(active)
    self.__commandLineDesigner.addOption(option)

    self.__addOptionToModel(option)

  def __addOptionToModel(self, option):
    """
    Adds the option to the QtModel
    """
    self.beginResetModel()
    groupItem = None
    # Get or create group
    if self.__groups.has_key(option.getGroup()):
      groupItem = self.__groups[option.getGroup()]
    else:
      logging.debug("OptionModel:: Creating new Group " +
          option.getGroup())
      idx = self.index(0,0, QtCore.QModelIndex())
#      self.beginInsertRows(idx,self.__rootItem.childCount(),self.__rootItem.childCount())
      groupItem = OptionModel.GroupItem(option.getGroup(),  self.__rootItem)
      self.__groups[option.getGroup()] = groupItem
      self.__rootItem.appendChild(groupItem)
#      self.endInsertRows()

    idx = self.createIndex(self.__rootItem.childCount(), 0 , groupItem)
#    self.beginInsertRows(idx,groupItem.childCount(),groupItem.childCount())
    groupItem.appendChild(option)
#    self.endInsertRows()
    self.endResetModel()


  def _dataChanged(self, index):
    self.emit(QtCore.SIGNAL('dataChanged(const QModelIndex &, const QModelIndex &)'), index, index)


  def removeData(self, indexes):
    self.beginRemoveRows(QtCore.QModelIndex(), 0, 0)
    for index in indexes:
      if index.isValid():
        item = index.internalPointer()
        # Remove from cmdl model
        if item in self.__commandLineDesigner.getOptions():
          self.__commandLineDesigner.removeOption(item)
        p = self.parent(index)
        if item in p.internalPointer().children():
          p.internalPointer().children().remove(item)

    self.endRemoveRows()



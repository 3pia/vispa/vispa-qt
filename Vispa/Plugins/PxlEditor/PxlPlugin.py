import os.path
import logging

from Vispa.Plugins.EventBrowser.EventBrowserPlugin import EventBrowserPlugin
from Vispa.Plugins.EventBrowser.EventBrowserTab import EventBrowserTab

from PxlDataAccessor import PxlDataAccessor
from PxlTab import PxlTab
from Vispa.Main.Exceptions import NoCurrentTabException
from Vispa.Views.AbstractView import NoneView
from Vispa.Views.WidgetView import WidgetView
from Vispa.Views.BoxDecayView import BoxDecayView
from Vispa.Views.LineDecayView import LineDecayView
from Vispa.Views.TableView import TableView
from Vispa.Main.GuiFacade import guiFacade

class PxlPlugin(EventBrowserPlugin):
    """ The PxlPlugin opens pxlio files in the EventBrowserTab.
    """
    def __init__(self):
        logging.debug(__name__ + ": __init__")
        EventBrowserPlugin.__init__(self)
        self.registerFiletypesFromTab(PxlTab)
        self.addNewFileAction("&New physics event", self.newFile)
        guiFacade.createStartupScreenEntry(prototypingActions=self._createNewFileActions, verifyingFiletypes=self.filetypes())

    def startUp(self):
        EventBrowserPlugin.startUp(self)
        self.addCenterView(NoneView)
        try:
          from Vispa.Views.AstroObjectView import AstroObjectView
          from Vispa.Views.SkyplotView import SkyplotView
          self.addCenterView(AstroObjectView)
          self.addCenterView(SkyplotView)
        except Exception, e:
          logging.warning("Exception thrown while trying to import Astro views - probably missing dependencies")
          logging.warning("Exception: " + str(e))
        self.addCenterView(BoxDecayView)
        self.addCenterView(LineDecayView, True)
        try:
            from Vispa.Views.RootCanvasView import RootCanvasView
            self.addCenterView(RootCanvasView)
        except:
            logging.info("Exception thrown while trying to import RootCanvasView - probably root not installed?")
        self.addCenterView(TableView)
        self._eventMenu = guiFacade.createPluginMenu('&Event')
        self._pxlToolBar = guiFacade.createPluginToolBar("Event")
        self._switchToEditModeAction = guiFacade.createAction("Edit", self.switchToEditMode,"Ctrl+D",image="editEvent")
        self._autolayoutAction = guiFacade.createAction("Autolayout event", self.autolayout,"Ctrl+U",image="Autolayout")
        self._eventMenu.addAction(self._switchToEditModeAction)
        self._pxlToolBar.addAction(self._switchToEditModeAction)
        self._pxlToolBar.addAction(self._autolayoutAction)
        self._viewMenu.addSeparator()
        self._viewMenu.addAction(self._autolayoutAction)

    def editMenuActions(self):
        return [self._switchToEditModeAction]

    def switchToEditMode(self):
        """ Calls switchToEditMode() function of current tab controller.
        """
        try:
            guiFacade.currentTab().switchToEditMode()
        except NoCurrentTabException:
            logging.warning(self.__class__.__name__ + ": switchToEditMode() - No tab controller found.")

    def autolayout(self):
        """ Calls autolayout() function of current tab controller.
        """
        try:
            guiFacade.currentTab().autolayout()
        except NoCurrentTabException:
            logging.warning(self.__class__.__name__ + ": autolayout() - No tab controller found.")

    def newFile(self):
        if self._startUp:
            self.startUp()
        return self.newTab(True)

    def newTab(self,editable=False):
        """ Create PxlTab and add to MainWindow.
        """
        tab = PxlTab(self)
        tab.setDataAccessor(PxlDataAccessor())

        tab.boxContentDialog().addButton("&Type", "object.Type")
        tab.boxContentDialog().addButton("&Name", "object.Name")
        tab.boxContentDialog().addButton("&Pt", "str(object.Pt)")
        tab.boxContentDialog().addButton("&Eta", "str(object.Eta)")
        tab.setEditable(editable)
        if editable:
            tab.addEvent()

        guiFacade.addTab(tab)
        return tab

    def eventMenu(self):
        return self._eventMenu

    def pxlToolBar(self):
        return self._pxlToolBar

    def switchToEditModeAction(self):
        return self._switchToEditModeAction

    def autolayoutAction(self):
        return self._autolayoutAction


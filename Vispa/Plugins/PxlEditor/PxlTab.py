import logging

from PyQt4.QtCore import SIGNAL, Qt, QPoint, QChar
from PyQt4.QtGui import QTreeWidgetItem, QHBoxLayout, QVBoxLayout, QWidget, QPalette, QSizePolicy, QColor, QStandardItemModel, QHeaderView, QFrame,QMenu

from Vispa.Plugins.Browser.BrowserTab import BrowserTab
from Vispa.Plugins.EventBrowser.EventBrowserTab import EventBrowserTab
from Vispa.Main.GuiFacade import guiFacade
from Vispa.Gui.SimpleDraggableTreeWidget import SimpleDraggableTreeWidget
from Vispa.Gui.ToolBoxContainer import ToolBoxContainer
from Vispa.Gui.VispaWidget import VispaWidget
from Vispa.Views.LineDecayView import DecayNode, DecayLine, ParticleWidget
from Vispa.Views.TreeView import TreeView
from Vispa.Views.AbstractView import NoneView
from Vispa.Views.LineDecayView import LineDecayView
from Vispa.Main.Exceptions import NoCurrentTabException

from Vispa.Main.GuiFacade import guiFacade

class PxlTab(EventBrowserTab):
    def __init__(self, plugin, parent=None):
        EventBrowserTab.__init__(self, plugin, parent)
        self.updateEditComponentsVisiblity()
        self.connect(self.propertyView(), SIGNAL('valueChanged'), self.setModified)
        self.connect(self.propertyView(), SIGNAL('valueChanged'), self.propertyChanged)
        self.connect(self.propertyView(), SIGNAL('propertyDeleted'), self.setModified)
        self.connect(self.propertyView(), SIGNAL('propertyAdded'), self.setModified)
    
    def editMenuActions(self):
        return self.plugin().editMenuActions()
    
    def createTreeView(self):
        self._toolBoxContainer = ToolBoxContainer(self.horizontalSplitter(), ToolBoxContainer.TYPE_WIDGETS)
        
        self._toolBoxContainer.addWidget(self.createStandardModellToolBox())
        
        EventBrowserTab.createTreeView(self,self._toolBoxContainer)
        self._treeviewArea.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self._toolBoxContainer.addWidget(self._treeviewArea, 3)
        self.updateEditComponentsVisiblity()

    def updateEditComponentsVisiblity(self):
        #logging.debug(self.__class__.__name__ +": updateEditComponentsVisibility()")
        self._toolBoxContainer.showWidget(self._standardModellToolBox, self.isEditable())
                
    def particleRightClicked(self,point,particle):
        popup=QMenu(particle)
        popup.addAction(guiFacade.createAction('Insert in event', self.particleDoubleClicked))
        self._insertParticle=particle
        popup.exec_(point)
        
    def particleDoubleClicked(self,particle=None):
        if hasattr(self,"_insertParticle"):
            particle=self._insertParticle
            del self._insertParticle
        if not particle:
            return
        self.centerView().topLevelContainer().createObject(particle.dragData(),QPoint(20,50))
        if particle.dragData()!="Node":
            self.autolayout()

    def createStandardModellToolBox(self):
        self._standardModellToolBox = QFrame(self)
        self._standardModellToolBox.setFrameStyle(QFrame.Box|QFrame.Sunken)
        self._standardModellToolBox.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        self._standardModellToolBox.show()
        self._standardModellToolBox.setLayout(QVBoxLayout())
        self._standardModellToolBox.layout().setContentsMargins(1, 1, 1, 1)
        self._standardModellToolBox.layout().setSpacing(0)
        self._standardModellToolBox.setToolTip("drag & drop objects from here into the event")

        # header
        model = QStandardItemModel()
        model.setHorizontalHeaderLabels(["Available Objects"])
        header = QHeaderView(Qt.Horizontal)
        header.setModel(model)
        header.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        header.setStretchLastSection(True)
        header.setFixedHeight(25)
        self._standardModellToolBox.layout().addWidget(header)

        # body
        body = QWidget(self)
        body.setAutoFillBackground(True)
        body.setPalette(QPalette(Qt.white))
        body.show()
        bodyLayout = QVBoxLayout()
        body.setLayout(bodyLayout)
        body.layout().setContentsMargins(5, 5, 5, 5)
        self._standardModellToolBox.layout().addWidget(body)
        
        # layouts
        hBoxLayout = QHBoxLayout()  # main layout for particles widgets
        hBoxLayout.setContentsMargins(0, 0, 0, 0)
        decayObjectsLayout = QHBoxLayout()
        decayObjectsLayout.setSpacing(2)
        matterLayout = QVBoxLayout()
        matterLayout.setSpacing(0)
        interactionLayout = QVBoxLayout()
        interactionLayout.setSpacing(0)
        higgsLayout = QVBoxLayout()
        higgsLayout.setSpacing(0)

        extensionLayout = QHBoxLayout()
        extensionLayout.setSpacing(0)
        extensionLayout.addStretch(0)
        
        susyLayout = QHBoxLayout()
        susyLayout.setSpacing(0)

        hBoxLayout.addLayout(matterLayout)
        hBoxLayout.addLayout(interactionLayout)
        hBoxLayout.addLayout(higgsLayout)
        hBoxLayout.addStretch(1)


        bodyLayout.addLayout(decayObjectsLayout)
        bodyLayout.addLayout(hBoxLayout)
        bodyLayout.addLayout(extensionLayout)
        bodyLayout.addLayout(susyLayout)

        ## Node, Partice, EventView
        # EventView
        eventViewWidget = ParticleWidget(self, ParticleWidget.NONE, "", "EventView")
        eventViewWidget.setTitle("Event View")
        eventViewWidget.setColors(QColor(0, 75, 141), QColor('white'), QColor('white')) # copied from WidgetContainer
        eventViewWidget.setMinimumSize(0, 50)
        eventViewWidget.setToolTip("Event View")
        decayObjectsLayout.addWidget(eventViewWidget, 0, Qt.AlignHCenter)
        
        # Particle
        particleWidget = ParticleWidget(self, ParticleWidget.NONE, "", "Particle")
        particleWidget.setMinimumSize(DecayLine.DEFAULT_LENGTH, 40)
        particleWidget.setColors(QColor('white'), QColor('white'), QColor('white'))
        particleWidget.setToolTip("Particle")
        decayLine = DecayLine(particleWidget, QPoint(5, 20), QPoint(particleWidget.width() -5, 20))
        decayLine.setLabel("Particle")
        particleWidget.setDecayObject(decayLine)
        decayObjectsLayout.addWidget(particleWidget, 0, Qt.AlignHCenter)
        
        # Node
        nodeWidget = ParticleWidget(self, ParticleWidget.NONE, "", "Node")
        nodeWidget.TOP_MARGIN = 1
        nodeWidget.BOTTOM_MARGIN = 1
        nodeWidget.LEFT_MARGIN = 1
        nodeWidget.RIGHT_MARGIN = 1
        nodeWidget.setMinimumSize(30, 40)
        nodeWidget.setColors(QColor('white'), QColor('white'), QColor('white'))
        nodeWidget.setTitle("Node")
        nodeWidget.setToolTip("Node")
        nodeWidget.titleField().setPenColor(QColor('black'))
        decayNode = DecayNode(nodeWidget, QPoint(0.5 * nodeWidget.width(), 0.5 * nodeWidget.height()))
        nodeWidget.setDecayObject(decayNode)
        decayObjectsLayout.addWidget(nodeWidget, 0, Qt.AlignHCenter)
        decayObjectsLayout.addStretch(1)
        
        # matter sub-layouts
        leptonLayout = QHBoxLayout()
        neutrinoLayout = QHBoxLayout()
        upTypeLayout = QHBoxLayout()
        downTypeLayout = QHBoxLayout()
        matterLayout.addLayout(leptonLayout)
        matterLayout.addLayout(neutrinoLayout)
        matterLayout.addStretch(0.5)
        matterLayout.addLayout(upTypeLayout)
        matterLayout.addLayout(downTypeLayout)
        
        # standard model
        leptonLayout.addWidget(ParticleWidget(self, ParticleWidget.LEPTON, "e"))
        leptonLayout.addWidget(ParticleWidget(self, ParticleWidget.LEPTON, unichr(956), "muon"))      # mu
        leptonLayout.addWidget(ParticleWidget(self, ParticleWidget.LEPTON, unichr(964), "tau"))      # tau
        neutrinoLayout.addWidget(ParticleWidget(self, ParticleWidget.LEPTON, unichr(957), "electron_neutrino"))    # nu
        neutrinoLayout.addWidget(ParticleWidget(self, ParticleWidget.LEPTON, unichr(957), "muon_neutrino"))    # nu
        neutrinoLayout.addWidget(ParticleWidget(self, ParticleWidget.LEPTON, unichr(957), "tau_neutrino"))    # nu
        upTypeLayout.addWidget(ParticleWidget(self, ParticleWidget.QUARK, "u"))
        upTypeLayout.addWidget(ParticleWidget(self, ParticleWidget.QUARK, "c"))
        upTypeLayout.addWidget(ParticleWidget(self, ParticleWidget.QUARK, "t"))
        downTypeLayout.addWidget(ParticleWidget(self, ParticleWidget.QUARK, "d"))
        downTypeLayout.addWidget(ParticleWidget(self, ParticleWidget.QUARK, "s"))
        downTypeLayout.addWidget(ParticleWidget(self, ParticleWidget.QUARK, "b"))
        interactionLayout.addWidget(ParticleWidget(self, ParticleWidget.BOSON, unichr(947), "photon")) # gamma
        interactionLayout.addWidget(ParticleWidget(self, ParticleWidget.BOSON, "W"))
        interactionLayout.addWidget(ParticleWidget(self, ParticleWidget.BOSON, "Z"))
        interactionLayout.addWidget(ParticleWidget(self, ParticleWidget.BOSON, "g", "gluon"))
        
        # higgs
        higgsLayout.addWidget(ParticleWidget(self, ParticleWidget.HIGGS, "Higgs"))
        
        # gauge sector extensions
        extensionLayout.addWidget(ParticleWidget(self, ParticleWidget.BOSON, "Z'"), 0, Qt.AlignHCenter)
        extensionLayout.addWidget(ParticleWidget(self, ParticleWidget.BOSON, "Z''"), 0, Qt.AlignHCenter)
        extensionLayout.addWidget(ParticleWidget(self, ParticleWidget.BOSON, "W'"), 0, Qt.AlignHCenter)
        extensionLayout.addWidget(ParticleWidget(self, ParticleWidget.HIGGS, "H0"), 0, Qt.AlignHCenter)
        extensionLayout.addWidget(ParticleWidget(self, ParticleWidget.HIGGS, "A0"), 0, Qt.AlignHCenter)
        extensionLayout.addWidget(ParticleWidget(self, ParticleWidget.HIGGS, "H+"), 0, Qt.AlignHCenter)
        extensionLayout.addStretch(1)
        
#        # SUSY
#        susyLayout.addWidget(ParticleWidget(self, ParticleWidget.BOSON, "d~L"))
        
        return self._standardModellToolBox

    def decayObjectMimeType(self):
        LineDecayView.DECAY_OBJECT_MIME_TYPE

    #@staticmethod
    def staticSupportedFileTypes():
        """ Returns supported file type: pxlio.
        """
        return [('pxlio', 'PXL io file')]
    staticSupportedFileTypes = staticmethod(staticSupportedFileTypes)
    
    def writeFile(self, filename):
        """ Save pxlio file.
        """
        return self.dataAccessor().writeTopLevelEventsToFile(filename)

    def switchToEditMode(self):
        if self.dataAccessor().numberOfEvents():
            moreThanOneEventInFile = self.dataAccessor().numberOfEvents() > 1
        elif self.dataAccessor().eventNumber() > 1:
            moreThanOneEventInFile = True
        else:
            self.cancel()
            self.dataAccessor().goto(2)
            moreThanOneEventInFile = self.dataAccessor().eventNumber() > 1
            self.dataAccessor().goto(1)

        self.cancel()
        self.dataAccessor().close()
        self.setEditable(True)
                    
        if moreThanOneEventInFile:
            self.setFilename(None)
            self.setModified()      # calls self.updateLabel()

        self.updateContent()
        self.autolayout()

    def addEvent(self):
        event = self.dataAccessor().addEvent()
        self.updateContent()
        
    def addEventView(self):
        noEventsError = False
        if self.centerView().dataObjectsCount() < 1:
            noEventsError = True
            
        if not noEventsError:
            candidateWidgets = self.centerView().selectedWidgets()
            if len(candidateWidgets) < 1:
                candidateWidgets = self.centerView().children()
            selectedWidget = None
            for widget in candidateWidgets:
                if hasattr(widget, "object") and hasattr(widget.object(), "createEventView"):
                    selectedWidget = widget
                    break
                
        if noEventsError or selectedWidget == None:
             self.showWarningMessage("There is no event to which you could add an event view.")
             return
                 
        event = selectedWidget.object()
        eventView = event.createEventView()
        self.updateContent()
        
    def autolayout(self):
        statusMessage = self.statusBarStartMessage("Autolayouting")
        self.centerView().autolayout()
        self.statusBarStopMessage(statusMessage)
        
    def setEditable(self, editable):
        EventBrowserTab.setEditable(self, editable)
        if editable:
            lineDecayViewClassId = self.plugin().viewClassId(LineDecayView)
            self.switchCenterView(lineDecayViewClassId)
            self.enableCenterViewSelectionMenu(False, lineDecayViewClassId)
        else:
            self.enableCenterViewSelectionMenu(True)
        
        self.updateEditComponentsVisiblity()
        if isinstance(self.centerView(),LineDecayView):
            self.centerView().setEditable(editable)
        if self.propertyView():
            self.propertyView().setShowAddDeleteButton(editable)
            
        try:
            if guiFacade.currentTab() == self:
                self.plugin().application().tabChanged()
        except NoCurrentTabException:
            # happens if setEditable() is called in constructor, when there are no other tabs yet
            pass
        
    def switchCenterView(self, requestedViewClassId):
        EventBrowserTab.switchCenterView(self, requestedViewClassId)
        
        if isinstance(self.centerView(), LineDecayView):
            self.connect(self.centerView(), SIGNAL("updateTreeView"), self.updateTreeView)
            self.connect(self.centerView(), SIGNAL("clearPropertyView"), self.propertyView().clear)
        
    def activated(self):
        EventBrowserTab.activated(self)
        editable=self.isEditable()
        self.plugin().switchToEditModeAction().setVisible(not editable)
        self.plugin().autolayoutAction().setVisible(editable)
        if not editable:
            guiFacade.showPluginMenu(self.plugin().eventMenu())
        guiFacade.showPluginToolBar(self.plugin().pxlToolBar())

    def onTreeViewSelected(self,select):
        EventBrowserTab.onTreeViewSelected(self,select)
        if self.isEditable():
            self.autolayout()
        
    def propertyChanged(self,name):
        if name=="Name":
            self.updateTreeView()
            selection = self.treeView().selection()
            if selection:
                self.centerView().setDataObjects([selection])
            self.updateCenterView()
            self.centerView().autolayout()

    def centerViewMenuButtonClicked(self, point=None):
        if self.isEditable():
            popup=QMenu(self.centerViewMenuButton())
            popup.addAction(self.plugin()._boxContentAction)
            popup.addAction(self.plugin()._saveImageAction)
            popup.addAction(self.plugin()._zoomAction)
            popup.addAction(self.plugin()._autolayoutAction)
            if not isinstance(point,QPoint):
                point=self.centerViewMenuButton().mapToGlobal(QPoint(self.centerViewMenuButton().width(),0))
            popup.exec_(point)
        else:
            EventBrowserTab.centerViewMenuButtonClicked(self, point)

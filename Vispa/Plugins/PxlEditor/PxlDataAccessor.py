import logging

from PyQt4.QtGui import QColor

from Vispa.Main.Exceptions import exception_traceback
from Vispa.Main.Exceptions import PluginNotLoadedException
try:
    from pxl.core import *
    from pxl.hep import *
    from pxl.astro import *
    from pxl.healpix import *
    Healpix_initialize()
except Exception:
    raise PluginNotLoadedException("cannot import pxl: " + exception_traceback())

# Put this into separate try-except block: If healpix import fails, pxl.root would not be imported!
try:
    from pxl.root import *
    import ROOT
except Exception:
    pass	
    #raise PluginNotLoadedException("cannot load ROOT-based pxl objects: " + exception_traceback())

from Vispa.Share.BasicDataAccessor import BasicDataAccessor
from Vispa.Share.RelativeDataAccessor import RelativeDataAccessor
from Vispa.Share.ParticleDataAccessor import ParticleDataAccessor
from Vispa.Plugins.EventBrowser.EventFileAccessor import EventFileAccessor

class PxlDataAccessor(BasicDataAccessor, RelativeDataAccessor, ParticleDataAccessor, EventFileAccessor):
    def __init__(self):
        logging.debug(__name__ + ": __init__")

        self._dataObjects = []
        self._objectHashList = {}
        self._indexHashList = {}
        
        self._eventIndex = 1
        self._numEvents = 0
        self._file = None
        self._currentObject = None
        self._knowEventNumber = False
        
    def children(self, parent):
        #logging.debug(__name__ + ": children")
        if hasattr(parent, "getObjects"):
            if hasattr(parent, "getIndex"):
                for index, object in parent.getIndex().items():
                    self._indexHashList.setdefault(index, []).append(object.getId().toString())
            return parent.getObjects()
        else:
            return ()
        
    def motherRelations(self, object):
        if hasattr(object, "getMotherRelations"):
            motherRelations=object.getMotherRelations().getContainer()
            # pxl relations are not sorted and are sorted here
            return sorted(motherRelations,lambda x,y: cmp(x.getId().toString(),y.getId().toString()))
        else:
            return []

    def daughterRelations(self, object):
        if hasattr(object, "getDaughterRelations"):
            daughterRelations=object.getDaughterRelations().getContainer()
            # pxl relations are not sorted and are sorted here
            return sorted(daughterRelations,lambda x,y: cmp(x.getId().toString(),y.getId().toString()))
        else:
            return []

    def getType(self, object):
        splitted = str(object.__class__).split(".")
        return splitted[len(splitted) - 1].strip("<'>")

    def label(self, object):
        if hasattr(object, "getName"):
            return object.getName()
        else:
            return self.getType(object)

    def _property(self,name,value,description=None):
        if isinstance(value, (bool)):
            return {"type":"Boolean", "name":name, "value":value, "userInfo":description}
        elif isinstance(value, (int, long)):
            return {"type":"Integer", "name":name, "value":value, "userInfo":description}
        elif isinstance(value, (float)):
            return {"type":"Double", "name":name, "value":value, "userInfo":description}
        elif isinstance(value, (complex, str, unicode)):
            return {"type":"String", "name":name, "value":str(value), "userInfo":description}
        else:
            return None

    def properties(self, object):
        """ Make list of all properties """
        properties = []

        properties += [{"type":"Category", "name":"Object info", "value":""}]
        if hasattr(object,"getName"):
            properties += [self._property("Name",object.getName(),object.getName.__doc__)]
        properties += [{"type":"String", "name":"Type", "value":self.getType(object), "readOnly":True}]
        getter_list_info = ["Charge", "PdgNumber", "Locked", "Workflag"]
        # all items in getter_list_info
        for getter in getter_list_info:
            if hasattr(object, "get" + getter):
                properties += [self._property(getter, getattr(object, "get" + getter)(),getattr(object, "get" + getter).__doc__)]
        # id and index
        if hasattr(object,"getId"):
            properties += [{"type":"String", "name":"Id", "value":object.getId().toString(), "userInfo":object.getId.__doc__, "readOnly":True}]
            indexes = []
            for index, ids in self._indexHashList.items():
                if object.getId().toString() in ids:
                    indexes.append(index)
            if len(indexes) > 0:
                properties += [{"type":"String", "name":"Indexes", "value":", ".join(indexes)}]
        # all other getters
        getter_list_vector_edit = ["E", "Px", "Py", "Pz", "X", "Y", "Z", "Energy", "Latitude", "Longitude"]
        getter_list_vector_readonly = ["Mass", "Pt", "Eta", "Phi", "P", "Et", "Theta"]
        for property in dir(object):
            if property.startswith("get"):
                getter = property[3:]
                if not getter in ["Name","UserRecord"] + getter_list_info + getter_list_vector_edit + getter_list_vector_readonly:
                    try:
                        result = getattr(object, property)()
                        property=self._property(getter, result, getattr(object, property).__doc__)
                        if property!=None:
                            properties += [property]
                    except TypeError:
                            pass
                    except Exception, msg:
                        properties += [self._property(getter, "ERROR: " + str(msg))]

        if True in [hasattr(object, "get" + getter) for getter in getter_list_vector_edit+getter_list_vector_readonly]:
            properties += [{"type":"Category", "name":"Vector", "value":""}]
        # all items in getter_list_vector
        for getter in getter_list_vector_edit+getter_list_vector_readonly:
            if hasattr(object, "get" + getter):
                property=self._property(getter, getattr(object, "get" + getter)(),getattr(object, "get" + getter).__doc__)
                if getter in getter_list_vector_readonly:
                    property["readOnly"]=True
                if property!=None:
                    properties += [property]
        # hard relations
        if hasattr(object, "numberOfMothers"):
            properties += [{"type":"Category", "name":"HardRelations", "value":""}]
            properties += [{"type":"String", "name":"Number of mothers", "value":object.numberOfMothers(), "readOnly": True}]
            properties += [{"type":"String", "name":"Number of daughters", "value":object.numberOfDaughters(), "readOnly": True}]
        # soft relations
        if hasattr(object, "getSoftRelations"):
            softrelations=sorted(object.getSoftRelations().getContainer().items())
            properties += [{"type":"Category", "name":"SoftRelations", "value":""}]
            for key,value in softrelations:
                properties += [{"type":"String", "name":key, "value":value.toString(), "readOnly":True}]
            properties += [{"type":"String", "name":"Number of relations", "value":str(len(softrelations)), "readOnly":True}]
        # userrecords
        if hasattr(object, "getUserRecords"):
            properties += self.__class__.userRecordProperties(object)
        # array
        if hasattr(object, "getElement"):
            properties += [{"type":"Category", "name":"Elements", "value":""}]
            if hasattr(object,"getSize"):
                for i in range(object.getSize()):
                    properties += [self._property(str(i),object.getElement(i))]
            elif hasattr(object,"getSize1") and hasattr(object,"getSize2"):
                for i in range(object.getSize1()):
                    for j in range(object.getSize2()):
                        properties += [self._property(str(i)+","+str(j),object.getElement(i,j))]
        if hasattr(object, "getTObject"):
            if hasattr(object.getTObject(), "ClassName"):
                properties += [{"type":"String", "name":"ClassName", "value":object.getTObject().ClassName()}]
            if hasattr(object.getTObject(), "GetName"):
                properties += [{"type":"String", "name":"RootName", "value":object.getTObject().GetName()}]
            if hasattr(object.getTObject(), "GetTitle"):
                properties += [{"type":"String", "name":"RootTitle", "value":object.getTObject().GetTitle()}]
        return tuple(properties)

    @staticmethod
    def userRecordProperties(object, skipList=[]):
        properties = []
        
        properties += [{"type": "Category", "name": "User Records"}]
        for key, value in sorted(object.getUserRecords().getContainer().items()):
            if key in skipList:
                continue
            if value.getType() == Variant.TYPE_BOOL:
                properties += [{"type": 'Boolean', "name": key, "value": object.getUserRecord(key), "deletable": True}]
            elif value.getType() in [Variant.TYPE_CHAR,Variant.TYPE_UCHAR]:
                properties += [{"type": 'Integer', "name": key, "value": object.getUserRecord(key), "deletable": True}]
            elif value.getType() in [Variant.TYPE_INT16,Variant.TYPE_UINT16,
                                     Variant.TYPE_INT32,Variant.TYPE_UINT32,
                                     Variant.TYPE_INT64,Variant.TYPE_UINT64,]:
                properties += [{"type": 'Integer', "name": key, "value": object.getUserRecord(key), "deletable": True}]
            elif value.getType() in [Variant.TYPE_FLOAT,Variant.TYPE_DOUBLE]:
                properties += [{"type": 'Double', "name": key, "value": object.getUserRecord(key), "deletable": True}]
            elif value.getType() == Variant.TYPE_STRING:
                properties += [{"type": 'String', "name": key, "value": object.getUserRecord(key), "deletable": True}]
            else:
                properties += [{"type": 'String', "name": key, "value": object.getUserRecord(key), "deletable": True}]

        return properties
    
    def setProperty(self, object, name, value, categoryName):
        """ Set the property 'name' to 'value' """
        logging.debug(__name__ + ": setProperty("+name+")"+str(value))
        # index
        if name=="Index":
            if hasattr(object,"owner"):
                object.owner().setIndex(value,object,False)
                self._indexHashList.setdefault(value, []).append(object.getId().toString())
            return True
        list_p4 = ["Px", "Py", "Pz", "E"]
        if name in list_p4 and hasattr(object,"setP4"):
            v=[object.getPx(),object.getPy(),object.getPz(),object.getE()]
            v[list_p4.index(name)]=value
            object.setP4(v[0],v[1],v[2],v[3])
            return True
        # all setters
        elif "set"+name in dir(object):
            getattr(object,"set" + name)(value)
            return True
        # userrecords
        if hasattr(object, "setUserRecord"):
            object.setUserRecord(name,value)
            return True
        return "Cannot set property "+name+"."

    def addProperty(self, object, name, value, type):
        if hasattr(object, "hasUserRecord"):
            if object.hasUserRecord(name):
                return False
        if hasattr(object, "setUserRecord"):
            object.setUserRecord(name,value)
            return True
        return False

    def removeProperty(self, object, name):
        if hasattr(object, "hasUserRecord"):
            if not object.hasUserRecord(name):
                return False
        if hasattr(object, "eraseUserRecord"):
            object.eraseUserRecord(name)
            return True
        return False

    def setDataObjects(self, objects):
        #logging.debug(__name__ + ": setDataObjects " + str(objects))
        self._objectHashList = {}
        self._indexHashList = {}
        self._dataObjects = objects

    def id(self, object):
        if hasattr(object, "getId"):
            return object.getId().toString()
        return None
        
    def particleId(self, object):
        if hasattr(object, "getPdgNumber"):
            id = object.getPdgNumber()
            if id == 0:
                id = int(defaultParticleDataList.getIdFromName(object.getName()))
            return id
        else:
            return 0

    def charge(self, object):
        if hasattr(object, "getCharge"):
            charge=object.getCharge()
            if charge == 0 and hasattr(defaultParticleDataList,"getParticleDataFromId"):
                particleData = defaultParticleDataList.getParticleDataFromId(self.particleId(object))
                if particleData:
                    charge = particleData.charge
            return charge
        else:
            return None
    
    def goto(self, index):
        """ Goto event number index in file.
        """
        #logging.debug(__name__ + ": goto")
        if not self._file:
            return False
        if index > self._file.objectCount():
            self._currentObject = self._file.seekToObject(index)
        else:
            self._currentObject = None
            self.close()
            self._file = InputFile(self._filename)
            self._currentObject = self._file.seekToObject(index)
        if not self._file.good():
            if self._file.objectCount()>0 and self._file.objectCount()>self._numEvents and index>self._file.objectCount():
#            if self._file.eof():
                # remember last event if at end of file
                self._numEvents=self._file.objectCount()
                newindex=self._numEvents
            else:
                newindex=index
            self._currentObject = None
            self.close()
            self._file = InputFile(self._filename)
            self._knowEventNumber = True
            self._currentObject = self._file.seekToObject(newindex)
        self._eventIndex = self._file.objectCount()
        if self._currentObject:
            self.setDataObjects([self._currentObject])
        return self._currentObject
    
    def eventNumber(self):
        return self._eventIndex

    def numberOfEvents(self):
        if self._knowEventNumber:
            return self._numEvents
        else:
            return None

    def topLevelObjects(self):
        return self._dataObjects
    
    def addTopLevelObjects(self, object):
        self.appendObject(object)

    def open(self, filename=None):
        """ Open pxlio file and read first event.
        """
        logging.debug(__name__ + ": open")
        if filename != None:
            self._filename = filename
        self._file=None
        self._eventIndex = 1
        self._numEvents = 0
        self._knowEventNumber = False

        if self._filename == None:
            return False
        
        self._currentObject = None
        self._file = InputFile(self._filename)
        if self._file == None:
            return False
        self.goto(1)
        return True


    def close(self):
        """ Close pxlio file.
        """
        if not self._file:
            return False
        if self._file.good():
            self._file.close()
        self._file=None

    def addEvent(self):
        event = Event()
        self._dataObjects.append(event)

    def createEvent(self):
        return Event()
    
    def createParticle(self):
        return Particle()
    
    def openPxlOutputFile(self, filename):
        return OutputFile(filename)
    
    def writeTopLevelEventsToFile(self, filename):
        """ Returns True on success.
        
        TODO: Generalize this function for any kind of top level pxl objects. 
        """
        file = OutputFile(filename)
        for event in self._dataObjects:
            if isinstance(event, Event):
                file.writeEvent(event)
        file.close()
        return True
    
    def isContainer(self, object):
        return isinstance(object, (Event, EventView, BasicContainer))

    def isQuark(self, object):
        particleId = self.particleId(object)
        if not particleId:
            return False
        return defaultParticleDataList.isQuarkId(particleId)

    def isLepton(self, object):
        particleId = self.particleId(object)
        if not particleId:
            return False
        return defaultParticleDataList.isLeptonId(particleId)

    def isGluon(self, object):
        particleId = self.particleId(object)
        if not particleId:
            return False
        return defaultParticleDataList.isGluonId(particleId)

    def isBoson(self, object):
        particleId = self.particleId(object)
        if not particleId:
            return False
        return defaultParticleDataList.isBosonId(particleId)

    def isPhoton(self, object):
        particleId = self.particleId(object)
        if not particleId:
            return False
        if not hasattr(defaultParticleDataList,"isPhotonId"):
            return False
        return defaultParticleDataList.isPhotonId(particleId)
    
    def isHiggs(self, object):
        particleId = self.particleId(object)
        if not particleId:
            return False
        if not hasattr(defaultParticleDataList,"isHiggsId"):
            return False
        return defaultParticleDataList.isHiggsId(particleId)
    
    def lineStyle(self, object):
        particleId = self.particleId(object)
        if hasattr(defaultParticleDataList,"isPhotonId") and defaultParticleDataList.isPhotonId(particleId):
            return self.LINE_STYLE_WAVE
        elif defaultParticleDataList.isGluonId(particleId):
            return self.LINE_STYLE_SPIRAL
        elif defaultParticleDataList.isBosonId(particleId):
            return self.LINE_STYLE_DASH
        elif isinstance(object,Vertex):
            return self.LINE_VERTEX
        return self.LINE_STYLE_SOLID
    
    def color(self, object):
        particleId = self.particleId(object)
        if defaultParticleDataList.isLeptonId(particleId):
            return QColor(244, 164, 96)
        elif defaultParticleDataList.isQuarkId(particleId):
            return QColor(0, 100, 0)
        elif hasattr(defaultParticleDataList,"isHiggsId") and defaultParticleDataList.isHiggsId(particleId):
            return QColor(247, 77, 251)
        elif defaultParticleDataList.isBosonId(particleId):
            return QColor(253, 74, 74)
        return QColor(176, 179, 177)
    
    def defaultName(self, object):
        particleId = self.particleId(object)
        if not particleId:
            return None
        return defaultParticleDataList.getNameFromId(particleId)

    def hasRelations(self,object):
        return hasattr(object, "getDaughterRelations") or hasattr(object,"getObjectOwner")

    def linkMother(self, object, mother):
        if hasattr(object, "linkMother"):
            object.linkMother(mother)
        
    def linkDaughter(self, object, daughter):
        if hasattr(object, "linkDaughter"):
            object.linkDaughter(daughter)
        

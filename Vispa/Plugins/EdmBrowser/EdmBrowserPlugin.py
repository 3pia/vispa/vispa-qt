import os.path
import logging

from Vispa.Plugins.EventBrowser.EventBrowserPlugin import EventBrowserPlugin
from Vispa.Views.TableView import TableView
from Vispa.Share.ThreadChain import ThreadChain
from Vispa.Main.GuiFacade import guiFacade
from Vispa.Main.Exceptions import NoCurrentTabException,PluginIgnoredException,exception_traceback

try:
    from Vispa.Plugins.EdmBrowser.EdmDataAccessor import EdmDataAccessor
except Exception,e:
    raise PluginIgnoredException("cannot import CMSSW: " + str(e))

from Vispa.Plugins.EdmBrowser.EdmBrowserTab import EdmBrowserTab
from Vispa.Plugins.EdmBrowser.EdmBrowserBoxView import EdmBrowserBoxView
from Vispa.Plugins.EdmBrowser.EdmBrowserTab import EdmBrowserTab

class EdmBrowserPlugin(EventBrowserPlugin):
    """ The EdmBrowserPlugin opens edm root files in the EventBrowserTab.
    """
    
    def __init__(self, name=None):
        logging.debug(__name__ + ": __init__")
        EventBrowserPlugin.__init__(self)
        self.registerFiletypesFromTab(EdmBrowserTab)
        guiFacade.createStartupScreenEntry(verifyingFiletypes=self.filetypes())

    def startUp(self):
        EventBrowserPlugin.startUp(self)
        self.addCenterView(EdmBrowserBoxView)
        self.addCenterView(TableView)
                
    def newTab(self):
        """ Create EdmBrowserTab and add to MainWindow.
        """
        logging.debug(__name__ + ": newTab")
        tab = EdmBrowserTab(self)
        tab.setDataAccessor(EdmDataAccessor())
        tab.boxContentDialog().addButton("&Label", "str(object.Label)")
        tab.boxContentDialog().addButton("&Type", "str(object.Type)")
        tab.boxContentDialog().addButton("&Name", "str(object.Name)")
        tab.boxContentDialog().addButton("&Pt", "str(object.Pt)")
        
        guiFacade.addTab(tab)
        return tab

    def _expandToDepth(self):
        """ Calls expandToDepthDialog() function of current tab controller.
        """
        try:
            guiFacade.currentTab().expandToDepthDialog()
        except NoCurrentTabException:
            logging.warning(self.__class__.__name__ + ": _expandToDepth() - No tab controller found.")

    def _fillMenu(self):
        """ Fill specific menu.
        """
        EventBrowserPlugin._fillMenu(self)
        self._filterBranchesAction = guiFacade.createAction('&Filter invalid branches', self._filterBranches, "Ctrl+Shift+F")
        self._filterBranchesAction.setCheckable(True)
        self._filterBranchesAction.setChecked(True)
        self._viewMenu.addAction(self._filterBranchesAction)
        self._hideUnderscorePropertiesAction = guiFacade.createAction('&Hide _underscore properties', self._hideUnderscoreProperties, "Ctrl+Shift+U")
        self._hideUnderscorePropertiesAction.setCheckable(True)
        self._hideUnderscorePropertiesAction.setChecked(True)
        self._viewMenu.addAction(self._hideUnderscorePropertiesAction)
        self._viewMenu.addSeparator()
        self._eventContentAction = guiFacade.createAction('&Browse event content...', self._eventContent, "Ctrl+Shift+C")
        self._viewMenu.addAction(self._eventContentAction)
        self._viewMenu.addSeparator()

    def filterBranchesAction(self):
        return self._filterBranchesAction

    def _filterBranches(self):
        try:
            guiFacade.currentTab().toggleFilterBranches()
        except NoCurrentTabException:
            logging.warning(self.__class__.__name__ + ": _filterBranches() - No tab controller found.")

    def eventContentAction(self):
        return self._eventContentAction

    def _eventContent(self):
        try:
            guiFacade.currentTab().eventContent()
        except NoCurrentTabException:
            logging.warning(self.__class__.__name__ + ": _eventContent() - No tab controller found.")

    def hideUnderscorePropertiesAction(self):
        return self._hideUnderscorePropertiesAction

    def _hideUnderscoreProperties(self):
        try:
            guiFacade.currentTab().toggleUnderscoreProperties()
        except NoCurrentTabException:
            logging.warning(self.__class__.__name__ + ": _hideUnderscoreProperties() - No tab controller found.")

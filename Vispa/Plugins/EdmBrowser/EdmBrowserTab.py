import logging
import os.path

from PyQt4.QtCore import SIGNAL,QPoint
from PyQt4.QtGui import QInputDialog,QMenu, QWidget

from Vispa.Main.Preferences import *
from Vispa.Gui.Header import FrameWithHeader
from Vispa.Plugins.EdmBrowser.BranchTableView import BranchTableView
from Vispa.Plugins.EventBrowser.EventBrowserTab import EventBrowserTab
from Vispa.Plugins.EdmBrowser.EventContentDialog import EventContentDialog

from Vispa.Main.GuiFacade import guiFacade

class EdmBrowserTab(EventBrowserTab):
    def __init__(self, plugin, parent=None):
        EventBrowserTab.__init__(self, plugin, parent)
        self._treeDepth=1

    def setDataAccessor(self, accessor):
        EventBrowserTab.setDataAccessor(self,accessor)
        self.loadIniEdmSpecific()
    
    def createTreeView(self,parent=None):
        """ Create the tree view.
        """
        if not parent:
            parent=self.horizontalSplitter()
        
        self._treeviewArea = FrameWithHeader(parent)
        self._treeviewArea.header().setText("Branches")
        self._treeViewMenuButton = self._treeviewArea.header().createMenuButton()
        self._treeView = BranchTableView(self._treeviewArea)
        self._treeviewArea.addWidget(self._treeView)

    #@staticmethod
    def staticSupportedFileTypes():
        """ Returns supported file type: root.
        """
        return [('root', 'EDM root file')]

    staticSupportedFileTypes = staticmethod(staticSupportedFileTypes)
    
    def updateViewMenu(self):
        EventBrowserTab.updateViewMenu(self)
        self.plugin().expandToDepthAction().setVisible(True)
        self.plugin().boxContentAction().setVisible(False)
        self.disconnect(self.centerView(), SIGNAL("toggleCollapsed"), self.toggleCollapsed)
        self.connect(self.centerView(), SIGNAL("toggleCollapsed"), self.toggleCollapsed)

    def toggleCollapsed(self,object):
        logging.debug(__name__ + ": toggleCollapsed()")
        if not self.centerView().isUpdated(object):
            self.dataAccessor().read(object)
            self.updateCenterView()

    def onTreeViewSelected(self,select):
        EventBrowserTab.onTreeViewSelected(self,self.dataAccessor().read(select,self._treeDepth))

    def onSelected(self,select):
        EventBrowserTab.onSelected(self,self.dataAccessor().read(select))

    def updateCenterView(self,propertyView=True):
        if self.treeView().selection():
            self.dataAccessor().read(self.treeView().selection(),self._treeDepth)
        EventBrowserTab.updateCenterView(self,propertyView)

    def expandToDepthDialog(self):
        """ Show dialog and expand center view to depth.
        """
        if hasattr(QInputDialog, "getInteger"):
            # Qt 4.3
            (depth, ok) = QInputDialog.getInteger(self, "Expand to depth...", "Input depth:", self._treeDepth, 0)
        else:
            # Qt 4.5
            (depth, ok) = QInputDialog.getInt(self, "Expand to depth...", "Input depth:", self._treeDepth, 0)
        if ok:
            self._treeDepth=depth
            if self.treeView().selection():
                self.onTreeViewSelected(self.treeView().selection())

    def centerViewMenuButtonClicked(self, point=None):
        popup=QMenu(self.centerViewMenuButton())
        popup.addAction(self.plugin()._expandToDepthAction)
        popup.addAction(self.plugin()._saveImageAction)
        popup.addAction(self.plugin()._zoomAction)
        popup.addAction(self.plugin()._filterAction)
        popup.addSeparator()
        for action in self.plugin().viewMenu().actions():
            if action.data().toString()!="":
                popup.addAction(action)
        if not isinstance(point,QPoint):
            point=self.centerViewMenuButton().mapToGlobal(QPoint(self.centerViewMenuButton().width(),0))
        popup.exec_(point)

    def toggleFilterBranches(self):
        self.dataAccessor().setFilterBranches(not self.dataAccessor().filterBranches())
        self.updateContent()
        self.saveIniEdmSpecific()

    def toggleUnderscoreProperties(self):
        self.dataAccessor().setUnderscoreProperties(not self.dataAccessor().underscoreProperties())
        self.propertyView().setDataObject(None)
        self.updateCenterView()
        self.saveIniEdmSpecific()

    def loadIniEdmSpecific(self):
        """ read options from ini """
        ini = getPreferences()
        if ini.has_option("edm", "filter branches"):
            self.dataAccessor().setFilterBranches(ini.get("edm", "filter branches")=="True")
        if ini.has_option("edm", "underscore properties"):
            self.dataAccessor().setUnderscoreProperties(ini.get("edm", "underscore properties")=="True")
        self.plugin().hideUnderscorePropertiesAction().setChecked(not self.dataAccessor().underscoreProperties())

    def saveIniEdmSpecific(self):
        """ write options to ini """
        ini = getPreferences()
        if not ini.has_section("edm"):
            ini.add_section("edm")
        ini.set("edm", "filter branches", self.dataAccessor().filterBranches())
        ini.set("edm", "underscore properties", self.dataAccessor().underscoreProperties())
        writePreferences()

    def eventContent(self):
        """ Open event content dialog """
        logging.debug(__name__ + ": eventContent")
        dialog=EventContentDialog(self,"This dialog let's you compare the contents of your edm root file with another dataformat / edm root file. You can compare either to a dataformat definition from a txt file (e.g. RECO_3_3_0) or any edm root file by selecting an input file.")
        branches=[branch[0].split("_") for branch in self.dataAccessor().filteredBranches()]
        name = os.path.splitext(os.path.basename(self._filename))[0]
        dialog.setEventContent(name,branches)
        dialog.exec_()

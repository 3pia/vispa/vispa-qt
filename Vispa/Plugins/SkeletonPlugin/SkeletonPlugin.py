import os.path
import logging

from PyQt4 import QtGui
from PyQt4 import QtCore

from Vispa.Main.VispaPlugin import VispaPlugin
from Vispa.Main.SplitterTab import SplitterTab

from Vispa.Main.GuiFacade import guiFacade

class SkeletonPlugin(VispaPlugin):
    """ A skeleton for creating new plugins.
    
    It creates a menu entry "New Skeleton Tab" and on click a tab with a PropertyView.
    """
    def __init__(self, name=None):
        logging.debug("SkeletonPlugin: __init__")
        VispaPlugin.__init__(self)
        self.addNewFileAction("New Skeleton Tab", self.newTab)
        guiFacade.createStartupScreenEntry(prototypingActions=self._createNewFileActions)
                
    def newTab(self):
        statusMessage=guiFacade.statusBarStartMessage("Creating new skeleton tab")

        tab = SplitterTab(self)
        tab.createPropertyView()
        guiFacade.addTab(tab)

        toolbar = guiFacade.createPluginToolBar("Skeleton Plugin Toolbar")
        guiFacade.showPluginToolBar(toolbar)
        
        action = guiFacade.createAction("Skeleton Plugin Action")
        toolbar.addAction(action)
        
        guiFacade.showInfoMessage("Opened Skeleton Plugin Tab")

        guiFacade.statusBarStopMessage(statusMessage)

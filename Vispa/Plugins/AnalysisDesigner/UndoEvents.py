import logging

from Vispa.Share.UndoEvent import UndoEvent, MultiUndoEvent
from Vispa.Plugins.AnalysisDesigner.ModuleWidget import ModuleWidget
from Vispa.Plugins.AnalysisDesigner.ModuleController import PythonModuleController

class UndoMoveEvent(UndoEvent):
    LABEL = "Module move"

    def __init__(self, widgets):
        # cannot save widget directly, as it might be deleted and recreated by other UndoEvents
        # need to find widget by name and workspace

        if not isinstance(widgets, list):
            # received just one widget
            widgets = [widgets]

        self._data = {}
        for widget in widgets:
            if not isinstance(widget, ModuleWidget):
                continue
            zoom = widget.zoomFactor()
            moduleName = widget.name()
            oldPos = widget.previousDragPosition() * 1.0 / zoom
            newPos = widget.pos() * 1.0 / zoom
            self._data[moduleName] = [oldPos, newPos]
        if len(widgets) > 0:
            self._workspace = widgets[0].parent()

    def moveWidget(self, moduleName, pos):
        widget = self._workspace.getWidgetByName(moduleName)
        if widget:
            widget.move(pos * widget.zoomFactor())
        else:
            logging.error("%s: moveWidget(): Could not find module with name '%s'" % (self.__class__.__name__, moduleName))

    def undo(self):
        for moduleName, positions in self._data.iteritems():
            self.moveWidget(moduleName, positions[0])

    def redo(self):
        for moduleName, positions in self._data.iteritems():
            self.moveWidget(moduleName, positions[1])

    def combine(self, otherUndoEvent):
        if not isinstance(otherUndoEvent, self.__class__) or \
            self._workspace != otherUndoEvent.workspace() or \
            len(self._data) != len(otherUndoEvent.data()):
                return False
        localKeys = self._data.keys()
        otherKeys = otherUndoEvent.data().keys()
        for key in localKeys:
            if key not in otherKeys:
                return False
        for key in localKeys:
            self._data[key][1] = otherUndoEvent.data()[key][1]
        return True

    def workspace(self):
        return self._workspace

    def data(self):
        return self._data

    def moduleName(self):
        return self._moduleName

    def newPos(self):
        return self._newPos


class AbstractPortConnectionUndoEvent(UndoEvent):

    def __init__(self, connection):
        self._workspace = connection.parent()
        self._sourceModuleName = connection.sourcePort().parent().title()
        self._sourcePortName = connection.sourcePort().name()
        self._sinkModuleName = connection.sinkPort().parent().title()
        self._sinkPortName = connection.sinkPort().name()

    def sourcePort(self):
        return self._workspace.getWidgetByName(self._sourceModuleName).sourcePort(self._sourcePortName)

    def sinkPort(self):
        return self._workspace.getWidgetByName(self._sinkModuleName).sinkPort(self._sinkPortName)

    def deleteConnection(self):
        connection = self._workspace.portConnection(self.sourcePort(), self.sinkPort())
        connection.delete()

    def createConnection(self):
        connection = self._workspace.tab().createPortConnection(self.sourcePort(), self.sinkPort())
        self._workspace.tab().createPxlConnection(connection)

class UndoConnectModulesEvent(AbstractPortConnectionUndoEvent):
    LABEL = "Create connection"
    def undo(self):
        self.deleteConnection()

    def redo(self):
        self.createConnection()

class UndoDisconnectModulesEvent(AbstractPortConnectionUndoEvent):
    LABEL = "Delete connection"
    def undo(self):
        self.createConnection()

    def redo(self):
        self.deleteConnection()


class AbstractCreateModuleUndoEvent(MultiUndoEvent):
    FILENAME_OPTION_NAME = "filename"

    def __init__(self, widget):
        undoDisconnectEvents = []
        for connection in widget.attachedConnections():
            undoDisconnectEvents.append(UndoDisconnectModulesEvent(connection))
        MultiUndoEvent.__init__(self, undoDisconnectEvents, self.LABEL)
        self._widgetPos = widget.pos()
        self._moduleName = widget.title()   # use title() instead of name(), as pxl module might already have been deleted
        self._moduleType = widget.type()
        self._options = widget.options()
        self.filterEmptyFileOptions()
        self._tab = widget.tab()

    def deleteModule(self):
        logging.debug(self.__class__.__name__ + ": deleteModule()")
        widget = self._tab.workspace().getWidgetByName(self._moduleName)
        self._options = widget.options()
        self.filterEmptyFileOptions()
        self._tab.removePxlModule(widget.dataObject())
        widget.deleteLater()

    def filterEmptyFileOptions(self):
        # do not set empty file options

        options = self._options[:]  # copy
        for option in options:
            if (option["type"] == "InputFile" or option["type"] == "OutputFile") and not option["value"]:
                self._options.remove(option)

    def createModule(self):
        logging.debug("%s: createModule() of type '%s'" % (self.__class__.__name__, self._moduleType))
        dataObject = self._tab.createPxlModule(self._moduleType)
        widget = self._tab.createModuleWidget(dataObject)
        moduleController = widget.moduleController()
        widget.setOptions(self._options)
        widget.move(self._widgetPos)
        widget.setName(self._moduleName)
        if isinstance(moduleController, PythonModuleController):
            moduleController.reloadPxlModule()
        self._tab.workspace().moduleWidgetAdded(widget)


class UndoDropModuleEvent(AbstractCreateModuleUndoEvent):
    LABEL = "Drop module"

    def __init__(self, widget):
        AbstractCreateModuleUndoEvent.__init__(self, widget)

    def undo(self):
        self.deleteModule()

    def redo(self):
        self.createModule()

class UndoDeleteModuleEvent(AbstractCreateModuleUndoEvent):
    LABEL = "Delete module"

    def __init__(self, widget):
        AbstractCreateModuleUndoEvent.__init__(self, widget)

    def undo(self):
        self.createModule()
        AbstractCreateModuleUndoEvent.undo(self)

    def redo(self):
        AbstractCreateModuleUndoEvent.redo(self)
        self.deleteModule()

class UndoChangePropertyViewValueEvent(UndoEvent):
    LABEL = "Change property"

    def __init__(self, workspace, moduleName, propertyName, newValue, oldValue, categoryName):
        self._workspace = workspace
        self._moduleName = moduleName
        self._propertyName = propertyName
        self._newValue = newValue
        self._oldValue = oldValue
        self._categoryName = categoryName

    def workspace(self):
        return self._workspace

    def moduleName(self):
        return self._moduleName

    def propertyName(self):
        return self._propertyName

    def newValue(self):
        return self._newValue

    def combine(self, otherUndoEvent):
        if isinstance(otherUndoEvent, self.__class__):
            if self._workspace == otherUndoEvent.workspace() and \
                self._moduleName == otherUndoEvent.moduleName() and \
                self._propertyName == otherUndoEvent.propertyName():
                self._newValue = otherUndoEvent.newValue()
                return True
        return False

    def changeValue(self, value):
        dataObject = self._workspace.dataObjectByName(self._moduleName)
        self._workspace.tab().dataAccessor().setProperty(dataObject, self._propertyName, value, self._categoryName)
        self._workspace.tab().onSelected(dataObject)
        if self._propertyName == "Name":
            self._moduleName = value
        #self._workspace.tab().propertyView().updateContent()

    def undo(self):
        self.changeValue(self._oldValue)

    def redo(self):
        self.changeValue(self._newValue)

import os.path
import time
import getpass

from PyQt4 import QtCore

import pxl.core
import pxl.modules
import pxl.xml

import logging
from Vispa.Plugins.PxlEditor.PxlDataAccessor import PxlDataAccessor
from Vispa.Share.BasicDataAccessor import BasicDataAccessor
from Vispa.Main.Exceptions import exception_traceback


class AnalysisDataAccessor(BasicDataAccessor, QtCore.QObject):
    """ Data accessor for pxl analysis class.
    """

    XPOS_USER_RECORD_NAME = "xPos"
    YPOS_USER_RECORD_NAME = "yPos"

    def __init__(self, parent):
        """ Constructor.
        """
        #logging.debug(self.__class__.__name__ +": __init__()")
        QtCore.QObject.__init__(self, parent)
        BasicDataAccessor.__init__(self)

    def stringToBoolean(self, string):
        """ Returns True if string is either 'true' or '1'.
        """
        if string == "1" or string.lower() == "true":
            return True
        return False

    def booleanToString(self, boolean):
        """ Returns 'true' if boolean is True. Otherwise 'false' is returned.
        """
        if boolean:
            return "true"
        else:
            return "false"

    def pxlOptionTypeString(self, argument):
        """ Translates pxl OptionDescription.TYPE_* values to strings suitable for xml format.
        
        For example OptionDescription.TYPE_STRING is translated to 'string'.
        argument may either be an OptionDescription or an OptionDescription.Type.
        """

        if isinstance(argument, pxl.modules.OptionDescription):
            _type = argument.type
        elif type(argument) == type(0):
            _type = argument
        else:
            logging.error(self.__class__.__name__ + ": pxlOptionTypeString() - Unknown argument.")
            return None

        if _type == pxl.modules.OptionDescription.TYPE_STRING:
            return "string"
        elif _type == pxl.modules.OptionDescription.TYPE_DOUBLE:
            return "double"
        elif _type == pxl.modules.OptionDescription.TYPE_LONG:
            return "long"
        elif _type == pxl.modules.OptionDescription.TYPE_BOOLEAN:
            return "boolean"
        elif _type == pxl.modules.OptionDescription.TYPE_STRING_VECTOR:
            return "string_vector"
        return None

    def listToStringVector(self, list):
        return pxl.core.StringVector(list)

#    def pxlOptionUsageString(self, argument):
#        """ Translates pxl OptionDescription.USAGE_* values to strings suitable for xml format.
#        
#        For example OptionDescription.USAGE_TEXT is translated to 'text'.
#        argument may either be an OptionDescription or an OptionDescription.Usage.
#        """
#
#        if isinstance(argument, pxl.modules.OptionDescription):
#            usage = argument.usage
#        elif type(argument) == type(0):
#            usage = argument
#        else:
#            logging.error(self.__class__.__name__ + ": pxlOptionUsageString() - Unknown argument.")
#            return None
#
#        if usage == pxl.modules.OptionDescription.USAGE_TEXT_MULTILINE:
#            return "text_multiline"
#        elif usage == pxl.modules.OptionDescription.USAGE_FILE_OPEN:
#            return "file_open"
#        elif usage == pxl.modules.OptionDescription.USAGE_FILE_SAVE:
#            return "file_save"
#        elif usage == pxl.modules.OptionDescription.USAGE_TEXT_SELECTION:
#            return "text_selection"
#        #if usage == pxl.modules.OptionDescription.USAGE_TEXT:
#        else:
#            return "text"

    def createAnalysis(self):
        return pxl.modules.Analysis()

    def addSearchPath(self, analysis, path):
        analysis.addSearchPath(path)

    def setOutputPath(self, analysis, path):
        analysis.setOutputPath(path)

    def modules(self, analysis):
        return analysis.getModules()

    def readFile(self, analysis, filename):
        if not os.path.exists(filename):
            return False
        analysisImport = pxl.xml.AnalysisXmlImport()
        analysis.addSearchPath(os.path.dirname(filename))
        analysisImport.open(filename)
        try:
            defectModules = analysisImport.parseInto(analysis)
        except Exception, e:
            message = "Analysis could not be loaded with the following errors:\n"
            message += exception_traceback()
            return False, message
        analysisImport.close()
        if len(defectModules) > 0:
            if len(defectModules) == 1:
                message = "The following module could not be loaded with the following errors:\n"
            else:
                message = "The following modules could not be loaded with the following errors:\n"
            for module in defectModules:
                message += "\n".join(module.getStatusMessages())
            return True, message
        return True, None


    #method used to serialize pxl objects; connections will be serialized directly in the VISPA PortConnection python object
    def serialize(self, pxlObject):
        #for now this is a simple workaround as pxl does not allow to export xml code of modules/connections or to export a full analysis into a stream which is not a physical file
        # temp_analysis = self.createAnalysis()
        # self.addModule(temp_analysis, pxlModule.getType(), pxlModule.getName(), pxlModule.getRunIndex())
        # temp_analysisExport = pxl.xml.AnalysisXmlExport()
        # temp_analysisExport.open(filename)
        # temp_analysisExport.writeObject(analysis)
        # temp_analysisExport.close()
        #or
        # stream = pxl.xml.XmlStream(std::ostream & stream)
        # writer = pxl.xml.AnalysisXmlWriter()
        # writer.writeObject(temp_analysis, stream)
        #
        #there also exist no memory buffer object that would allow the following:
        # outputBuffer = pxl.core.BufferOutput()
        # pxlModule.serialize(outputBuffer)
        # string = outputBuffer.getData()
        #or
        # string = QtCore.QString(outputBuffer.getData2())

        pxlObjectDict = {}

        if isinstance(pxlObject, pxl.modules.Module):
            moduleType = self.type(pxlObject)
            moduleOptions = self.options(pxlObject)
            moduleProperties = self.properties(pxlObject)

            pxlObjectDict = {
                             "type": moduleType,
                             "options": moduleOptions,
                             "properties": moduleProperties
                             }
#        elif isinstance(pxlObject, pxl.modules.Module):
#            pass

        return pxlObjectDict

    def deserialize(self, encoded):
        #see comments above
        #for now not needed as unpickle is done in AnalysisDesignerTab to allow to append data to the clipboard 
        return encoded

    def writeFile(self, analysis, filename):
        analysis.setUserRecord("Modification date", time.strftime("%Y-%m-%d"))
        analysis.setUserRecord("Modification time", time.strftime("%H:%M:%S"))
        analysisExport = pxl.xml.AnalysisXmlExport()
        if not analysisExport.open(filename):
          return False
        analysisExport.writeObject(analysis)
        return analysisExport.close()
        

#    def userRecord(self, object, userRecordName):
#        try:
#            if object.hasUserRecord(userRecordName):
#                return object.getUserRecord(userRecordName)
#            return ""
#        except Exception, e:
#            logging.info("%s: userRecord() - Problem while accessing user record '%s': %s " % (self.__class__.__name__, userRecordName, str(e)))
#            return None
#        
#    def setUserRecord(self, object, userRecordName, value):
#        try:
#            object.setUserRecord(userRecordName, value)
#            return True
#        except Exception, e:
#            logging.info("%s: setUserRecord() - Could not set user record '%s': %s " % (self.__class__.__name__, userRecordName, str(e)))
#            return "Could not set user record '%s'" % userRecordName

    def author(self, pxlAnalysis):
        if not pxlAnalysis.hasUserRecord("Author"):
            logging.debug("AnalysisDataAccessor: Setting Author to username")
            pxlAnalysis.setUserRecord("Author", getpass.getuser())
        return pxlAnalysis.getUserRecord("Author")

    def modificationDate(self, pxlAnalysis):
        if pxlAnalysis.hasUserRecord("Modification date") :
            return pxlAnalysis.getUserRecord("Modification date")
        else:
            return ""

    def modificationTime(self, pxlAnalysis):
        if pxlAnalysis.hasUserRecord("Modification time") :
            return pxlAnalysis.getUserRecord("Modification time")
        else:
            return ""

    def isEnabled(self, pxlModule):
        return pxlModule.isEnabled()

    def properties(self, object):
        """ Returns list of properties for PropertyView.
        
        object can either be a pxl.module.Analysis or a pxl.module.Module.
        """
        #logging.debug("%s: properties()" % self.__class__.__name__)

        # pxl analysis
        if isinstance(object, pxl.modules.Analysis):
            return self.analysisProperties(object)

        # pxl module
        elif isinstance(object, pxl.modules.Module):
            return self.moduleProperties(object)

        return []

    def analysisProperties(self, object):
        properties = []

        properties += [{"type": 'Category', "name": 'Main'}]
        properties += [{"type": 'String', "name": 'Author', "value": self.author(object)}]
        properties += [{"type": 'String', "name": 'Modification date', "value": self.modificationDate(object), "readOnly": True}]
        properties += [{"type": 'String', "name": 'Modification time', "value": self.modificationTime(object), "readOnly": True}]
        if object.hasUserRecord("Comment"):
            comment = object.getUserRecord("Comment")
        else:
            comment = ""
        properties += [{"type": 'MultilineString', "name": 'Comment', "value": comment}]

        # userrecords
        if hasattr(object, "getUserRecords"):
            properties += PxlDataAccessor.userRecordProperties(object, ["Author", "Comment", "Modification date", "Modification time"])
        return properties

    def moduleProperties(self, object):
        sourceNames = [""] + [desc.name for desc in self.getSourceDescriptions(object)] # empty string to disable

        properties = []
        properties += [{"type": 'Category', "name": 'Main'}]
        properties += [{"type": 'String', "name": 'Name', "value": object.getName()}]
        if hasattr(object, "getVersion"):
            try:
                properties += [{"type": 'String', "name": 'Version', "value": object.getVersion()}]
            except: pass
        if hasattr(object, "getDescription"):
            try:
                properties += [{"type": 'String', "name": 'Description', "value": object.getDescription()}]
            except: pass
        properties += [{"type": 'String', "name": 'Type', "value": object.getType(), "readOnly": True}]
        properties += [{"type": 'Boolean', "name": 'isEnabled', "value": object.isEnabled()}]
        if hasattr(object, "getBypassSource"):
            properties += [{"type": 'DropDown', "name": 'Bypass to Source', "value": object.getBypassSource(), "dropDownEntries": sourceNames}]
        if object.hasUserRecord("Compilation Date"):
            properties += [{"type": 'String', "name": 'Compilation date', "value": object.getUserRecord("Compilation Date").strip(" \n"), "readOnly": True}]
        if object.hasUserRecord("Compilation Time"):
            properties += [{"type": 'String', "name": 'Compilation time', "value": object.getUserRecord("Compilation Time").strip(" \n"), "readOnly": True}]
        if object.isRunnable():
            properties += [{"type": 'Integer', "name": 'runIndex', "value": object.getRunIndex()}]
        properties += [{"type": 'String', "name": 'Status Message', "value": object.getStatusMessages(), "readOnly": True}]

        properties += [{"type": 'Category', "name": 'Options'}]
        properties += self.options(object)

        # userrecords
        if hasattr(object, "getUserRecords"):
            properties += PxlDataAccessor.userRecordProperties(object, ["Compilation Date", "Compilation Time"])

        return properties

    def findProperties(self, properties, category):
        """ returns all properties of a certain category
        """

        properties_list = []

        foundStart = False
        for entry_dict in properties:
            if not foundStart:
                if 'type' in entry_dict:
                    if entry_dict['type'] == "Category":
                        if entry_dict['name'] == category:
                            foundStart = True
                            #skip this as it is the header
            else:
                if 'type' in entry_dict:
                    if entry_dict['type'] == "Category":
                        #we found the end of the category
                        return properties_list
                    else:
                        #a valid entry of the category
                        properties_list.append(entry_dict)
        return properties_list

    def setProperty(self, object, name, value, categoryName):
        """ Sets option of pxl module.
        
        Is called by PropertyView.
        """
        name = str(name)
        logging.debug(__name__ + ': setProperty() ' + name + ' = ' + str(value) + ', categoryName = ' + str(categoryName))

        # pxl analysis
        if isinstance(object, pxl.modules.Analysis):
            return self.setAnalysisProperties(object, name, value, categoryName)

        # pxl module
        elif isinstance(object, pxl.modules.Module):
            return self.setModuleProperties(object, name, value, categoryName)

        return "Object neither pxl analysis nor pxl module. Cannot set property."

    def setAnalysisProperties(self, object, name, value, categoryName):
        if name != "Modification date" and name != "Modification time":
            if hasattr(object, "setUserRecord"):
                object.setUserRecord(name, value)
                return True
            else:
                return "This object does not support user records."
        else:
            return "Cannot set read-only property."

    def setModuleProperties(self, object, name, value, categoryName):
        analysis = object.getAnalysis()
        moduleName = object.getName()

        if categoryName == "Main":
            if name == "Name":
                try:
                    self.setName(object, value)
                    return True
                except:
                    return "Could not set module name. Please note, duplicate names for modules are not supported."

            elif name == "runIndex":
                object.setRunIndex(long(value))
                return True

            elif name == "Type":
                logging.warning(__name__ + ": setProperty() - Cannot change type of module.")
                return "Cannot change type of module."

            elif name == "isEnabled":
                object.setEnabled(value)
                self.emit(QtCore.SIGNAL("moduleEnabled"), object.getName(), bool(value))
                return True

            elif name == "Bypass to Source":
                analysis.setModuleBypass(moduleName, value)
                self.emit(QtCore.SIGNAL("moduleBypassChanged"), object.getName(), value)
                return True

        elif categoryName == "Options" and object.hasOption(name):
            return self.setOption(object, name, value)

        if categoryName == "User Records":
            object.setUserRecord(name, value)
            if name == "xPos" or name == "yPos":
                self.emit(QtCore.SIGNAL("updatedWidgetPos"))
            return True
        return "Property '%s' unknown: Cannot set value." % name

    def addProperty(self, object, name, value, type):
        if hasattr(object, "hasUserRecord"):
            if object.hasUserRecord(name):
                return False
        if hasattr(object, "setUserRecord"):
            object.setUserRecord(name, str(value))
            return True
        return False

    def removeProperty(self, object, name):
        if hasattr(object, "hasUserRecord"):
            if not object.hasUserRecord(name):
                return False
        if hasattr(object, "eraseUserRecord"):
            object.eraseUserRecord(name)
            return True
        return False

    def addModule(self, analysis, moduleType, moduleName, runIndex):
        try:
            return analysis.addModule(moduleType, moduleName, "")
        except Exception, e:
            logging.error("%s: addModule() - Could not add module of type %s. %s" % (self.__class__.__name__, moduleType, exception_traceback()))

    def removeModule(self, analysis, pxlModule):
        try:
            return analysis.removeModule(pxlModule.getName())
        except Exception, e:
            logging.error("%s: removeModule() - Could not remove module. %s" % (self.__class__.__name__, exception_traceback()))

    def connectModules(self, analysis, sourceModuleName, sourceName, sinkModuleName, sinkName):
        """ Connects two modules within given analysis.
        """
        try:
            analysis.connectModules(sourceModuleName, sourceName, sinkModuleName, sinkName)
        except Exception, e:
            logging.error("%s: connectModules() - Could not connect modules %s (source %s) and %s (sink %s). %s" % (self.__class__.__name__, sourceModuleName, sourceName, sinkModuleName, sinkName, exception_traceback()))

    def disconnectModules(self, analysis, sourceModuleName, sourceName, sinkModuleName, sinkName):
        """ Connects two modules within given analysis.
        """
        try:
            analysis.disconnectModules(sourceModuleName, sourceName, sinkModuleName, sinkName)
            connections = analysis.getConnections()
        except Exception, e:
            logging.error("%s: disconnectModules() - Could not disconnect modules %s (source %s) and %s (sink %s). %s" % (self.__class__.__name__, sourceModuleName, sourceName, sinkModuleName, sinkName, exception_traceback()))

    def hasLayers(self):
        return False    # disables LayersToolBox in AnalysisDesigner for VISPA 0.5
        #return hasattr(pxl.modules.Analysis,"addLayer")

    def addLayer(self, analysis, layerName):
        analysis.addLayer(layerName)

    def activateLayer(self, analysis, layerName):
        analysis.activateLayer(layerName)

    def renameLayer(self, analysis, oldName, newName):
        pass

    def removeLayer(self, analysis, layerName):
        pass

    def bypassSource(self, pxlModule):
        return None
        # TODO: reenable when pxl supports bypass
        #return pxlModule.getBypassSource()

    def type(self, pxlModule):
        if pxlModule:
            return pxlModule.getType()
        return None

    def name(self, object):
        #logging.debug("%s: name()" % self.__class__.__name__)
        if isinstance(object, pxl.modules.Analysis):
            return "Analysis - NAME NOT YET IMPLEMENTED. Does Analysis have a name?"

        if isinstance(object, pxl.modules.Module):
            return str(object.getName())
        else:
            logging.error("%s: name() - Could not retrieve name of object of type %s." % (self.__class__.__name__, type(object)))

    def setName(self, pxlModule, newName):
        """ Sets name of given pxl module and makes sure all connections are updated correspondingly.
        """
        oldName = pxlModule.getName()
        analysis = pxlModule.getAnalysis()

        pxlModule.setName(newName)
        if analysis:
            for connection in analysis.getConnections():
                if connection.sourceModuleName == oldName:
                    connection.sourceModuleName = newName
                if connection.sinkModuleName == oldName:
                    connection.sinkModuleName = newName
        self.emit(QtCore.SIGNAL("moduleNameChanged"), newName, oldName)

    def optionDescriptions(self, pxlModule):
        return pxlModule.getOptionDescriptions()

    def optionNames(self, pxlModule):
        options = []
        for optionDescription in pxlModule.getOptionDescriptions():
            options.append(optionDescription.name)
        return options

    def options(self, object):
        properties = []
        for optionDescription in object.getOptionDescriptions():
            name = optionDescription.name
            usage = "String"
            value = self.option(object, optionDescription)
            typeString = self.pxlOptionTypeString(optionDescription.type)
            description = optionDescription.description
            if value == None:
                value = "WARNING: TYPE NOT YET IMPLEMENTED!"
                logging.warning(self.__class__.__name__ + ": Type " + typeString + " not yet implemented.")

            if typeString == "boolean":
                usage = "Boolean"
            elif typeString == "long":
                usage = "Integer"
            elif typeString == "string_vector":
                usage = "StringVector"
                if optionDescription.usage == pxl.modules.OptionDescription.USAGE_FILE_OPEN:
                    usage = "InputFileVector"
                elif optionDescription.usage == pxl.modules.OptionDescription.USAGE_FILE_SAVE:
                    usage = "OutputFileVector"
                elif optionDescription.usage == pxl.modules.OptionDescription.USAGE_TEXT_SELECTION:
                    usage = "TextSelection"
                if value != "" and value != () and value != []:
                    value = "['" + "','".join(value) + "']"
                else:
                    value = "[]"
            elif optionDescription.usage == pxl.modules.OptionDescription.USAGE_FILE_OPEN:
                usage = "InputFile"
            elif optionDescription.usage == pxl.modules.OptionDescription.USAGE_FILE_SAVE:
                usage = "OutputFile"
            elif optionDescription.usage == pxl.modules.OptionDescription.USAGE_TEXT_MULTILINE:
                usage = "MultilineString"

            properties += [{"type": usage, "name": name, "value": value, "userInfo": description}]
        return properties

    def setOptions(self, pxlModule, options):
        try:
            for option in options:
                logging.debug("%s: setOptions() - %s" % (self.__class__.__name__, str(option)))
                self.setOption(pxlModule, option["name"], option["value"])
        except AttributeError, e:
            logging.warning("%s: setOptions() - %s" % (self.__class__.__name__, str(e)))

    def option(self, pxlModule, argument):
        """ Returns option value of pxl module belonging to given AnalysisDesignerModuleController module.
        
        'argument' may either be the name of the option or the corresponding pxl.modules.OptionDescription.
        For details see pxl.modules.OptionDescription from modules in the pxl package. 
        """
        optionDescription = None

        if isinstance(argument, pxl.modules.OptionDescription):
            # argument is OptionDescription
            optionDescription = argument
        elif isinstance(argument, str):
            # argument is option name
            try:
                optionDescription = pxlModule.getOptionDescription(argument)
            except RuntimeError:
                logging.error(self.__class__.__name__ + ": getOption() - " + exception_traceback())
                return None

        if not optionDescription:
            logging.error(self.__class__.__name__ + ": getOption() - Unknown argument.")
            return None

        value = None
        if pxlModule:
            value = pxlModule.getOption(optionDescription.name)
        #print __name__ +": getOption() - name, type, value;", optionDescription.name, self.pxlOptionTypeString(optionDescription.type), value
        return value

    def setOption(self, pxlModule, name, value):

        #analysis = pxlModule.getAnalysis()
        #moduleName = pxlModule.getName()
        #try:
        #    analysis.setModuleOption(moduleName, name, str(value))
        #except RuntimeError, e:
        #    return str(e)
        #return True

        # remove remainder once setModuleOption() works reliably hopefully in > PXL 3.0.2

        try:
            optionDescription = pxlModule.getOptionDescription(name)
        except RuntimeError, e:
            logging.error(self.__class__.__name__ + ": setOption() - Cannot get option description: " + exception_traceback())
            return "Cannot get option description (see logfile for details): " + str(e)

        try:
            if optionDescription.type == pxl.modules.OptionDescription.TYPE_STRING:
                pxlModule.setStringOption(name, str(value))
            elif optionDescription.type == pxl.modules.OptionDescription.TYPE_STRING_VECTOR:
                if type(value) == type(""):
                    if value == "":
                        value = "['']"
                    print value
                    value = eval(value)
                    value = self.listToStringVector(value)
                else:
                    return "Property is not of type string."
                pxlModule.setStringVectorOption(name, value)
            elif optionDescription.type == pxl.modules.OptionDescription.TYPE_DOUBLE:
                pxlModule.setDoubleOption(name, float(value))
            elif optionDescription.type == pxl.modules.OptionDescription.TYPE_LONG:
                if value != "":
                    pxlModule.setLongOption(name, long(value))
                else:
                    return "Property is not of type long."
            elif optionDescription.type == pxl.modules.OptionDescription.TYPE_BOOLEAN:
                if type(value) == type(""):
                    # when reading xml file value is most likely "true" / "false" (string)
                    # needs to be converted into boolean type before it can be used
                    value = self.stringToBoolean(value)
                pxlModule.setBooleanOption(name, bool(value))
        except Exception, e:
            logging.error(self.__class__.__name__ + ": setOption() - Cannot set module option: " + exception_traceback())
            return "Cannot set module option (see logfile for details):\n" + str(e)
        return True

    def updateModulePos(self, pxlModule, pos):
        if not isinstance(pxlModule, pxl.modules.Module):
            return False
        try:
            pxlModule.setUserRecord(self.XPOS_USER_RECORD_NAME, pos.x())
            pxlModule.setUserRecord(self.YPOS_USER_RECORD_NAME, pos.y())
        except Exception, e:
            logging.error("%s: updateModulePos() - Could not set coordinates: %s " % (self.__class__.__name__, str(e)))

#    def setModuleXPos(self, pxlModule, xPos):
#        if not isinstance(pxlModule, pxl.modules.Module):
#            return False
#        try:
#            pxlModule.setUserRecord(self.XPOS_USER_RECORD_NAME, xPos)
#        except Exception, e:
#            logging.error("%s: setModuleXPos() - Could not set x coordinate: %s " % (self.__class__.__name__, str(e)))
#    
#    def setModuleYPos(self, pxlModule, yPos):
#        if not isinstance(pxlModule, pxl.modules.Module):
#            return False
#        try:
#            pxlModule.setUserRecord(self.YPOS_USER_RECORD_NAME, yPos)
#        except Exception, e:
#            #logging.error("%s: setModuleYPos() - Could not set y coordinate: %s " % (self.__class__.__name__, str(e)))
#            # not necessarily and error, happens e.g. when creating modules by drop
#            return None

    def moduleXPos(self, pxlModule):
        if not isinstance(pxlModule, pxl.modules.Module):
            return None
        try:
            return pxlModule.getUserRecord(self.XPOS_USER_RECORD_NAME)
        except Exception, e:
            #logging.error("%s: getModuleXPos() - Could not retrieve x coordinate: %s " % (self.__class__.__name__, str(e)))
            # not necessarily and error, happens e.g. when creating modules by drop
            return None

    def moduleYPos(self, pxlModule):
        if not isinstance(pxlModule, pxl.modules.Module):
            return None
        try:
            return pxlModule.getUserRecord(self.YPOS_USER_RECORD_NAME)
        except Exception, e:
            #logging.error("%s: getModuleYPos() - Could not retrieve y coordinate: %s " % (self.__class__.__name__, str(e)))
            return None

    def getSinkDescriptions(self, pxlModule):
        return pxlModule.getSinkDescriptions()

    def getSourceDescriptions(self, pxlModule):
        return pxlModule.getSourceDescriptions()


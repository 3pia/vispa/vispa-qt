import logging
import os
import time
import inspect

from PyQt4 import QtCore
from PyQt4 import QtGui

from Vispa.Gui.ConnectableWidget import ConnectableWidget
from Vispa.Main.Exceptions import exception_traceback
from Vispa.Main.GuiFacade import guiFacade
from Vispa.Main.FileManager import is_relative_path, absolute_path, relative_path

class PxlModuleController(object):

    MODULE_NAME = ""
    ON_DOUBLECLICK_LOAD_OPTION_AS_FILE = "filename"

    def __init__(self, moduleWidget, analysisDesignerPlugin):
        """ Constructor.
        """
        self._moduleWidget = moduleWidget
        self._plugin = analysisDesignerPlugin
        self.moduleWidget().addMenuEntry("Copy", self.copyModuleToClipboardAction)
        self.moduleWidget().addMenuEntry("Cut", self.cutModuleAction)
        self.moduleWidget().addMenuEntry("Delete", self.deleteModuleAction)

    def copyModuleToClipboardAction(self):
        self.moduleWidget().parent().copySelectedModules()

    def cutModuleAction(self):
        self.moduleWidget().parent().copySelectedModules()
        self.moduleWidget().parent().deleteSelectedModules()

    def deleteModuleAction(self):
        self.moduleWidget().parent().deleteSelectedModules()

    def plugin(self):
        """ Returns plugin object of Analysis Designer.
        """
        return self._plugin

    def dataAccessor(self):
        """ Returns tab controllers data accessor object.
        """
        return self.moduleWidget().dataAccessor()

    def moduleWidget(self):
        """ Returns module widget. See setModuleWidget() and ModuleWidget.
        """
        return self._moduleWidget

#    def setAllowAutoRenaming(self, allow):
#        """ If allow is True auto renaming will be allowed, otherwise not.
#        
#        Auto renaming is a convenience feature which may disturb while reading from a file.
#        Auto renaming will for example set module name if a file is selected.
#        """
#        self._allowAutoRenaming = allow
#        
#    def allowAutoRenaming(self):
#        """ Returns True is auto renaming is enabled. See setAllowAutorenaming().
#        """
#        return self._allowAutoRenaming

    def arrangePorts(self):
        """ ModuleController can arrange ports instead of ModuleWidget.
        
        If the controller arranges the ports this function has to return True.
        In any other case the usual arrangePorts() method of ConnectableWidget is called by ModuleWidget.arrangePorts().
        The default behavior is to nothing and retain ConnectableWidget behavior.
        """
        pass

    def showFileDialog(self, type, filename="", filter=QtCore.QString(), selectedFilter=None):
        if type == "open":
            return guiFacade.showFileOpenDialog(filename, filter, selectedFilter)
        elif type == "multiopen":
            return guiFacade.showFilesOpenDialog(filename, filter, selectedFilter)
        else:
            return guiFacade.showFileSaveDialog(filename, filter, selectedFilter)

    @classmethod
    def supportsScript(cls, scriptContentsDir):
        """ Returns True if given script content is supported by this module controller.
        
        scriptContentsDir is a dictionary of of names contained in a python script, which is returned by dir(python.module). 
        """
        return False

    @staticmethod
    def supportsFile(filename):
        """ Returns True if the module class can handle the type of the given filename.
        
        Otherwise False will be returned.
        """
        return False


class FileModuleController(PxlModuleController):
    PXLIO_SUFFIX = ".pxlio"
    PXLIO_FILTER = "PXL io file (*.pxlio)"

    def __init__(self, moduleWidget, analysisDesignerPlugin):
        PxlModuleController.__init__(self, moduleWidget, analysisDesignerPlugin)

class FileInputModuleController(FileModuleController):
    MODULE_NAME = "File Input"

    def __init__(self, moduleWidget, analysisDesignerPlugin):
        FileModuleController.__init__(self, moduleWidget, analysisDesignerPlugin)
        self.moduleWidget().addMenuEntry("Select file", self.selectFileAction)

    def selectFileAction(self, filename=""):
        if filename == "":
            filenames = self.moduleWidget().option("File names")
        if len(filenames) > 0:
            filename = filenames[0]
        was_relative = is_relative_path(filename)
        if was_relative:
            filename = absolute_path(os.path.join(self.moduleWidget().parent().tab().analysisPath(), filename))
        filenames = self.showFileDialog("multiopen", filename, ";;".join(["Any (*.*)", self.PXLIO_FILTER]), self.PXLIO_FILTER)
        if len(filenames) > 0:
            if was_relative:
                nfileNames = []
                for v in filenames:
                    nfileNames += [relative_path(v, self.moduleWidget().parent().tab().analysisPath())]
                filenames = nfileNames
            self.moduleWidget().setOption('File names', str(filenames))
            self.moduleWidget().tab().propertyView().updateContent()
        return filenames

    def mouseDoubleClickEvent(self, event):
        """ Handle double click events of ModuleWidget.
        """
        filenames = self.moduleWidget().option("File names")
        if len(filenames) > 0:
            filename = filenames[0]
        else:
            self.selectFileAction()
            return
        if is_relative_path(filename):
            filename = absolute_path(os.path.join(self.moduleWidget().parent().tab().analysisPath(), filename))
        if filename.endswith(self.PXLIO_SUFFIX):
            guiFacade.doubleClickOnFile(filename)

    @staticmethod
    def supportsFile(filename):
        """ Returns True if the module class can handle the type of the given filename.
        
        Otherwise False will be returned.
        """
        if type(filename) != type(""):
            return False
        return filename.endswith(FileInputModuleController.PXLIO_SUFFIX)


class FileOutputModuleController(FileModuleController):
    MODULE_NAME = "File Output"
    SELECT_FILE_TYPE = "save"

    def __init__(self, moduleWidget, analysisDesignerPlugin):
        FileModuleController.__init__(self, moduleWidget, analysisDesignerPlugin)
        self.moduleWidget().addMenuEntry("Select file", self.selectFileAction)

    def selectFileAction(self, filename=""):
        if filename == "":
            filename = self.moduleWidget().option("filename")
        was_relative = is_relative_path(filename)
        if was_relative:
            filename = absolute_path(os.path.join(self.moduleWidget().parent().tab().analysisPath(), filename))
        filename = self.showFileDialog("save", filename, ";;".join(["Any (*.*)", self.PXLIO_FILTER]), self.PXLIO_FILTER)
        if was_relative:
            filename = relative_path(filename, self.moduleWidget().parent().tab().analysisPath())
        self.moduleWidget().setOption('filename', filename)
        self.moduleWidget().tab().propertyView().updateContent()
        return filename

    def mouseDoubleClickEvent(self, event):
        """ Handle double click events of ModuleWidget.
        """
        filename = self.moduleWidget().option("filename")
        if not filename:
            self.selectFileAction()
            return
        if is_relative_path(filename):
            filename = absolute_path(os.path.join(self.moduleWidget().parent().tab().analysisPath(), filename))
        if filename.endswith(self.PXLIO_SUFFIX):
            guiFacade.doubleClickOnFile(filename)


class PythonModuleController(PxlModuleController):

    PYTHON_SUFFIX = ".py"
    PYTHON_FILTER = "Python file (*.py)"
    HOOK_FUNCTION_NAME = None
    PYSKELETONFILE = None
    PYSKELETON = None

    def __init__(self, moduleWidget, analysisDesignerPlugin):
        PxlModuleController.__init__(self, moduleWidget, analysisDesignerPlugin)
        moduleWidget.addMenuEntry("Open script", self.openScriptAction)
        moduleWidget.addMenuEntry("Select script", self.selectFileAction)
        moduleWidget.addMenuEntry("Reload script", self.reloadPxlModule)
        moduleWidget.addMenuEntry("Create skeleton", self.createSkeleton)

    def openScriptAction(self):
        self.mouseDoubleClickEvent("placeholder")

    def reloadPxlModule(self):
        try:
            self.moduleWidget().dataObject().reload()
            self.moduleWidget().loadPxlPorts()
        except Exception, e:
            message = "Error while reloading module '%s'. See following error output.\n\n" % (self.moduleWidget().name())
            guiFacade.showErrorMessage(message + str(e))

    def selectFileAction(self, filename=""):
        if filename == "":
            filename = self.moduleWidget().option("filename")
        was_relative = is_relative_path(filename)
        if was_relative:
            filename = absolute_path(os.path.join(self.moduleWidget().parent().tab().analysisPath(), filename))
        filename = self.showFileDialog("open", filename, ";;".join([self.PYTHON_FILTER, "Any (*.*)"]))
        if filename:
            if was_relative:
                filename = relative_path(filename, self.moduleWidget().parent().tab().analysisPath())
            self.moduleWidget().setOption('filename', filename)
            self.moduleWidget().loadPxlPorts()
        self.moduleWidget().tab().propertyView().updateContent()
        return filename

    def mouseDoubleClickEvent(self, event):
        """ Handle double click events of ModuleWidget.
        """
        filename = self.moduleWidget().option("filename")
        if not filename:
            self.selectFileAction()
            return
        if is_relative_path(filename):
            filename = absolute_path(os.path.join(self.moduleWidget().parent().tab().analysisPath(), filename))
        if filename.endswith(self.PYTHON_SUFFIX):
            guiFacade.doubleClickOnFile(filename)

    def createSkeleton(self):
        filename = self.moduleWidget().option("filename")
        if not filename:
            was_relative = True
            filename = self.showFileDialog("save", filename, ";;".join([self.PYTHON_FILTER, "Any (*.*)"]))
            result = QtGui.QMessageBox.Ok
        else:
            was_relative = is_relative_path(filename)
            if was_relative:
                filename = absolute_path(os.path.join(self.moduleWidget().parent().tab().analysisPath(), filename))
            if os.path.exists(filename):
                result = guiFacade.showMessageBox("The file \n\n '%s', \n\nwill be overwritten with a basic skeleton script." % filename, "Do you want to create the skeleton file?", QtGui.QMessageBox.Cancel | QtGui.QMessageBox.Ok, QtGui.QMessageBox.Ok)
            else:
                result = QtGui.QMessageBox.Ok

        if result == QtGui.QMessageBox.Ok:
            try:
                skeleton = '### Skeleton for ' + filename.split('/')[ -1] + '\n# created by VISPA' + '\n# ' + time.ctime() + '\n'
                if not self.PYSKELETON:
                  fn = os.path.join(self._plugin.getPxlDataPath(), self.PYSKELETONFILE)
                  if os.path.isfile(fn):
                    self.PYSKELETON = open(fn).read()
                    skeleton += self.PYSKELETON
                  else:
                    skeleton += '#No skeleton available'
                else:
                    skeleton += self.PYSKELETON

                of = open(filename, 'w')
                of.write(skeleton)
                of.close()
                self.reloadPxlModule()
                if was_relative:
                    filename = relative_path(filename, self.moduleWidget().parent().tab().analysisPath())
                self.moduleWidget().setOption('filename', filename)
                self.moduleWidget().loadPxlPorts()
                self.moduleWidget().tab().propertyView().updateContent()
            except IOError, e:
                logging.warning("%s: setOption() - Could not write to file %s" % (self.__class__.__name__, filename))
            # TODO: add cumulated error message for user

    def setOption(self, name, value, typeString=None):
        """ Sets pxl option with given name to given value.
        """
        PxlModuleController.setOption(self, name, value, typeString)

        if name == "filename":
            #check if filename exists
            filename = value
            if filename.endswith('.py') and not os.path.isfile(filename):
                self.createSkeleton(filename)
        return True

    @classmethod
    def supportsScript(cls, script):
        """ Returns True if given script content is supported by this module controller.
        
        script is the module object (e.g. the object that corresponds to an imported file).
        This default implementation checks whether script contains a class object with member functions "beginJob", "end,Job"
        and a function defined in cls.HOOK_FUNCTION_NAME.
        If cls.HOOK_FUNCTION_NAME is None / False, False will be returned.
        """
        if not cls.HOOK_FUNCTION_NAME:
            return False

        for attribute_name in dir(script):
            attribute = getattr(script, attribute_name)
            if inspect.isclass(attribute) and \
                hasattr(attribute, "beginJob") and \
                hasattr(attribute, "endJob") and \
                hasattr(attribute, cls.HOOK_FUNCTION_NAME):
                    return True
        return False

class PyAnalyseModuleController(PythonModuleController):
    MODULE_NAME = "PyAnalyse"
    HOOK_FUNCTION_NAME = "analyse"
    PYSKELETONFILE = "skeletons/PyAnalyse.py"


class PyGeneratorModuleController(PythonModuleController):
    MODULE_NAME = "PyGenerator"
    HOOK_FUNCTION_NAME = "generate"
    PYSKELETONFILE = "skeletons/PyGenerator.py"

class PyDecideModuleController(PythonModuleController):
    MODULE_NAME = "PyDecide"
    HOOK_FUNCTION_NAME = "decide"
    PYSKELETONFILE = "skeletons/PyDecide.py"


class PySwitchModuleController(PythonModuleController):
    MODULE_NAME = "PySwitch"
    HOOK_FUNCTION_NAME = "switch"
    PYSKELETONFILE = "skeletons/PySwitch.py"


class PyModuleModuleController(PythonModuleController):
    MODULE_NAME = "PyModule"
    HOOK_FUNCTION_NAME = "process"
    PYSKELETONFILE = "skeletons/PyModule.py"



import subprocess
import sys
import os
import logging
import signal
import time
import shlex
import copy

from PyQt4.QtCore import QThread, SIGNAL

class AnalysisExecutionThread(QThread):

    MAX_LINES = 1000 # maximum number of lines read from stdout at once

    def __init__(self, command, pxlLogLevel, parent=None):
        QThread.__init__(self, parent)
        self._command = str(command)
        self._process = None
        self._aborted = False
        self.__pxlLogLevel = pxlLogLevel

    def __replaceAnsiEscapeSequences(self, s):
      s = s.replace("\033[0;31m", " <font color=red>")
      s = s.replace("\033[0;33m", " <font color=yellow>")
      s = s.replace("\033[m", "</font>")
      return s
      

    def run(self):
        logging.debug(self.__class__.__name__ + ": run(): " + str(self._command))
        #on windows hlex.split is not correctly supported, but command can be executed as string directly
        if sys.platform == 'win32':
            cmd=self._command
        else:
            cmd = shlex.split(self._command)
        env = copy.copy(os.environ)
        if self.__pxlLogLevel >= 0:
          env['PXL_LOG_LEVEL'] = str(self.__pxlLogLevel)
        try:
          self._process = subprocess.Popen(cmd,  stdout=subprocess.PIPE, stderr=subprocess.STDOUT, env=env)
        except Exception, e:
            self.emit(SIGNAL('newStdOut(PyQt_PyObject)'), "--- Could not execute pxlrun. Please verify pxlrun executable path in preference dialog.")
            self.emit(SIGNAL('newStdOut(PyQt_PyObject)'), str(e))

        while True:
          out = self._process.stdout.read(1024)
          if len(out) == 0:
            #if nothing is here, wait
            time.sleep(0.1)
          else:
            out+=self._process.stdout.readline()
            out = self.__replaceAnsiEscapeSequences(out)
            self.emit(SIGNAL('newStdOut(PyQt_PyObject)'), out)

          if not (self._process and (self._process.poll() == None)):
            break

        out = self._process.stdout.read()
        if len(out) != 0:
          out = self.__replaceAnsiEscapeSequences(out)
          self.emit(SIGNAL('newStdOut(PyQt_PyObject)'), out)

        if self._aborted:
            self.emit(SIGNAL('newStdOut(PyQt_PyObject)'), '--- process terminated')
        else:
          if self._process.returncode ==0:
            self.emit(SIGNAL('newStdOut(PyQt_PyObject)'), '<font color=green> --- process finished normally with exit status %i</font>' % (self._process.returncode))
          else:
            self.emit(SIGNAL('newStdOut(PyQt_PyObject)'), '<font color=red> --- process finished abnormally with exit status %i</font>' % (self._process.returncode))
        self._process = None


    def stop(self):
        logging.debug(__name__ +'::stop called.')
        self._aborted = True
        if self._process and self._process.poll() == None:
            if hasattr(self._process, "kill"):
                logging.debug(__name__ +'::Using python > 2.6 subprocess kill.')
                # Use kill in python >= 2.6
                self._process.kill()
            else:
                if sys.platform == 'win32':
                    self.kill_win32()
                else:
                    self.kill_unix()

        self.terminate()


    def kill_unix(self):
        logging.debug(__name__ +'::kill_unix called.')
        os.kill(self._process.pid, signal.SIGTERM)
        t = 2.5  # max wait time in secs
        while self._process and self._process.poll() < 0:
            if t > 0.5:
                t -= 0.25
                time.sleep(0.25)
            else:  # still there, force kill
                os.kill(self._process.pid, signal.SIGKILL)
                time.sleep(0.5)
                self._process.poll() # final try
                break


    def kill_win32(self):
        try:
            import ctypes
            PROCESS_TERMINATE = 1
            handle = ctypes.windll.kernel32.OpenProcess(PROCESS_TERMINATE, False, self._process.pid)
            ctypes.windll.kernel32.TerminateProcess(handle, -1)
            ctypes.windll.kernel32.CloseHandle(handle)
        except:
            os.popen('TASKKILL /PID ' + str(self._process.pid) + ' /F')

    def aborted(self):
        return self._aborted

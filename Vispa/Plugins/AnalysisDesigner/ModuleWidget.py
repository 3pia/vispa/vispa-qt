from PyQt4 import QtCore
from PyQt4 import QtSvg

import logging

from Vispa.Gui.ConnectableWidget import ConnectableWidget
from Vispa.Gui.PortWidget import PortWidget

class ModuleWidget(ConnectableWidget):
    """ Widget for visualizing pxl modules.
    """
    
    BACKGROUND_SHAPE = 'ROUNDRECT'
    SHOW_PORT_NAMES = True
    AUTOSIZE = True
    AUTOSIZE_KEEP_ASPECT_RATIO = False
    
    def __init__(self, parent=None, name=None):
        self._moduleController = None
        self._tab = None
        self._dataAccessor = None
        self._dataObject = None
        self._type = None
        self._options = None
        ConnectableWidget.__init__(self, parent, name)
        self.setPortNamesPosition(self.PORT_NAMES_ABOVE_PORTS)

    def setModuleController(self, controller):
        """ Sets controller. See AnalysisDesignerModuleController.
        """
        self._moduleController = controller
        
    def moduleController(self):
        """ Returns controller. See setController().
        """
        return self._moduleController
    
    def dataAccessor(self):
        return self._tab.dataAccessor()
        
    def setTab(self, tab):
        """ Sets tab controller.
        """
        self._tab = tab
        self.loadModuleConfiguration()
        
    def tab(self):
        """ Returns tab controller.
        """
        return self._tab
        
    def setDataObject(self, dataObject):
        """ Sets data object that will be associated with this widget.
        """
        self._dataObject = dataObject
        self.loadPxlPorts()
        self.loadModuleConfiguration()
        
    def dataObject(self):
        """ Returns the data object that is associated with this widget.
        
        If there is no associated object None will be returned.
        """
        return self._dataObject
        
    def enable(self, enable):
        self.enableGrayScaleMode(not enable)

    def loadPxlPorts(self, filter=None):
        """ Evaluates information on sinks and sources of pxl module.
        """
        logging.debug("%s: loadPxlPorts()" % self.__class__.__name__)
        if filter and (filter != "sink" and filter != "source"):
            filter = None

        if not self._dataObject:
            return

        #remove ports not needed any more
        sinkNames = [desc.name for desc in self.dataAccessor().getSinkDescriptions(self._dataObject)]
        sourceNames = [desc.name for desc in self.dataAccessor().getSourceDescriptions(self._dataObject)]
        
        ports = self.ports()[:]     # copy
        for port in ports:
            if (port.portType() == "sink" and not port.name() in sinkNames) or (port.portType() == "source" and not port.name() in sourceNames):
                self.removePort(port)
        
        if not filter or filter == "sink":
            for desc in self.dataAccessor().getSinkDescriptions(self._dataObject):
                if not self.sinkPort(desc.name):
                    self.addSinkPort(desc.name, desc.description)
        if not filter or filter == "source":
            for desc in self.dataAccessor().getSourceDescriptions(self._dataObject):
                if not self.sourcePort(desc.name):
                    self.addSourcePort(desc.name, desc.description)
    
    def mouseDoubleClickEvent(self, event):
        """ Forward event to ModuleController.
        """
        if self._moduleController:
            self._moduleController.mouseDoubleClickEvent(event)

    def arrangePorts(self):
        """ Override default port arrangement.
        """
        singleSink = len(self.sinkPorts()) == 1
        singleSource = len(self.sourcePorts()) == 1

        if not singleSink and not singleSource:
            ConnectableWidget.arrangePorts(self)
        elif not singleSink and singleSource:
            ConnectableWidget.arrangePorts(self, "sink")
        elif singleSink and not singleSource:
            ConnectableWidget.arrangePorts(self, "source")

        if singleSink:
            self.centerSinglePortVertically(self.sinkPorts(), self.getDistance("firstSinkX"))
        if singleSource:
            self.centerSinglePortVertically(self.sourcePorts(), self.getDistance("firstSourceX"))
        return True

    def loadModuleConfiguration(self):
        """ Applies module controller settings from configuration file to this controller.

        Default configuration is stored in AnalysisDesignerPlugin.py.
        """
        #logging.debug("%s.loadModuleConfiguration() - %s" % (self.__class__.__name__, self.title()))
        cp = self.tab().plugin().moduleControllerConfigParser()
        mn = None   # module name
        if self.dataAccessor():
            mn = self.dataAccessor().type(self.dataObject())
        if mn == None:
            mn = self.title()

        if cp and cp.has_section(mn):
            if cp.has_option(mn, "show_port_names"):
                self.setShowPortNames(cp.getboolean(mn, "show_port_names"))
            if cp.has_option(mn, "show_port_lines"):
                self.setShowPortLines(cp.getboolean(mn, "show_port_lines"))
            if cp.has_option(mn, "icon_file"):
                self.setImage(QtSvg.QSvgRenderer(cp.get(mn, "icon_file")))

    def setOption(self, optionName, value):
        if self.dataAccessor().setOption(self._dataObject, optionName, value) == True:
            self.tab().setModified()

    def option(self, optionName):
        return self.dataAccessor().option(self._dataObject, optionName)
    
    def setOptions(self, options):
        self.dataAccessor().setOptions(self._dataObject, options)

    def options(self):
        """ Returns options in a list of dictionaries in the style of PropertyView's properties.
        
        delete() reads all options and caches them in self._options list for UndoEvents.
        """
        if self._options:
            return self._options
        elif self.dataAccessor():
            return self.dataAccessor().options(self.dataObject())
        return []

    def type(self):
        """ Returns type of associated pxl object.
        
        delete() reads and caches the type in a  variable for UndoEvents.
        """
        if self._type:
            return self._type
        elif self.dataAccessor():
            return self.dataAccessor().type(self._dataObject)

    def name(self):
        return self.dataAccessor().name(self._dataObject)

    def setName(self, name):
        self.dataAccessor().setName(self._dataObject, name)

    def delete(self):
        if self.isDeletable():
            self._type = self.type()
            self._options = self.dataAccessor().options(self.dataObject())
        ConnectableWidget.delete(self)

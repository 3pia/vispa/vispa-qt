from PyQt4.QtCore import SIGNAL
from PyQt4.QtGui import QDialog, QGridLayout, QVBoxLayout, QHBoxLayout, QLabel, QLineEdit, QPushButton, QFileDialog

from Vispa.Main.Preferences import homeDirectory
from Vispa.Main.GuiFacade import guiFacade
from Vispa.Views.PropertyView import PropertyView
from Vispa.Share.BasicDataAccessor import BasicDataAccessor


class CreateModuleDialog(QDialog):
    
    def __init__(self, parent=None):
        QDialog.__init__(self, parent)
        
        self._nameLineEdit = QLineEdit(self)
        self._fileNameLineEdit = QLineEdit(self)
        self._selectFileButton = QPushButton("...", self)
        self._cancelButton = QPushButton("Cancel", self)
        self._clearButton = QPushButton("Clear", self)
        self._createModuleButton = QPushButton("Create module", self)
        
        self._optionsGroupBox = None
        self._optionElements = []
        self._propertyView = None
        self._properties = []
        
        self.initializeLayout()
        
        self.connect(self._selectFileButton, SIGNAL("clicked(bool)"), self.selectFileSlot)
        self.connect(self._cancelButton, SIGNAL("clicked(bool)"), self.cancelButtonPressedSlot)
        self.connect(self._clearButton, SIGNAL("clicked(bool)"), self.clearButtonPressedSlot)
        self.connect(self._createModuleButton, SIGNAL("clicked(bool)"), self.createButtonPressedSlot)
        
    def initializeLayout(self):
        
        self.setLayout(QGridLayout())
        self.layout().addWidget(QLabel("Name"), 0, 0)
        self.layout().addWidget(self._nameLineEdit, 0, 1, 1, 2)
        self.layout().addWidget(QLabel("Filename"), 1, 0)
        self.layout().addWidget(self._fileNameLineEdit, 1, 1)
        self.layout().addWidget(self._selectFileButton, 1, 2)
        
        self._propertyView = PropertyView(self._optionsGroupBox, "PropertyView")
        self.layout().addWidget(self._propertyView, 2, 0, 1, 3)
        self._propertyView.setShowAddDeleteButton(True)
        self._propertyView.setDataAccessor(CreateModuleDialogDataAccessor())
        self.clearButtonPressedSlot()
        
        buttonRowLayout = QHBoxLayout()
        buttonRowLayout.addWidget(self._cancelButton)
        buttonRowLayout.addWidget(self._clearButton)
        buttonRowLayout.addWidget(self._createModuleButton)
        self.layout().addLayout(buttonRowLayout, 3, 0, 1, 3)
        
    def selectFileSlot(self):
        ccFileFilter = "C++ file (*.cc)"
        defaultFile = self._fileNameLineEdit.text()
        fileName = guiFacade.showFileSaveDialog(defaultFile, "All files (*.*);;%s" % ccFileFilter, ccFileFilter)
        self._fileNameLineEdit.setText(fileName)
    
    def cancelButtonPressedSlot(self):
        self.hide()
        
    def clearButtonPressedSlot(self):
        self._propertyView.clear()
        self._properties = []
        self._propertyView.setDataObject(self._properties)
        self._propertyView.updateContent()
        
        self._fileNameLineEdit.setText("")
        self._nameLineEdit.setText("")
    
    def createButtonPressedSlot(self):
        pass
    
    
class CreateModuleDialogDataAccessor(BasicDataAccessor):
    
    def __init__(self):
        pass
    
    def properties(self, object):
        return object
    
    def addProperty(self, object, name, value, type):
        for propertyTuple in object:
            if propertyTuple[1] == name:
                return False
        object.append((type, name, value))
        return True
    
    def removeProperty(self, object, name):
        for propertyTuple in object:
            if propertyTuple[1] == name:
                object.remove(propertyTuple)
                return True
        return False
    
    def setProperty(self, object, name, value, categoryName):
        for propertyTuple in object:
            if propertyTuple[1] == name:
                propertyTuple[2] = value
                return value
        return "Cannot set property "+name+"."

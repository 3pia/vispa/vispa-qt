import os
import sys

from PyQt4.QtCore import *
from PyQt4.QtGui import *

import logging

from Vispa.Gui.PortConnection import PortConnection
from Vispa.Gui.VispaWidget import VispaWidget
from Vispa.Views.WidgetView import WidgetView
from Vispa.Share.UndoEvent import MultiUndoEvent
from Vispa.Plugins.AnalysisDesigner.UndoEvents import UndoMoveEvent, UndoConnectModulesEvent, UndoDropModuleEvent, UndoDeleteModuleEvent, UndoDisconnectModulesEvent
from Vispa.Plugins.AnalysisDesigner.ModuleWidget import ModuleWidget
#from Vispa.Plugins.AnalysisDesigner.PxlModuleDataAccessor import *

class AnalysisDesignerWorkspace(WidgetView):
    """ Central widget of AnalysisDesignerTab.
    
    Parent of ModuleWidgets and PortConnections.
    """
    FOCUSPOLICY = Qt.StrongFocus
    URI_MIME_TYPE = "text/uri-list"
    MODULE_MIME_TYPE = "text/x-module-name"
    GROUP_DRAG_STEP = 15

    COULD_NOT_DETERMINE_MODULE_TYPE_WARNING = """Could not determine module type.
Please validate the python code and check for correct user hook function names.

If problem remains, it is still possible to manually add the desired module and set the script file.

Check command line output or log file to get further hints for debugging.
    """

    GRID_SPACING = 30

    def __init__(self, parent=None):
        """ Constructor
        """
        self._selectionRect = None
        self._selectionRectStartPos = None
        self._tab = None
        self._groupDragInitWidget = None
        self._displayGridFlag = False

        WidgetView.__init__(self, parent)
        self.setFocusPolicy(self.FOCUSPOLICY)
        self.setAcceptDrops(True)
        self.setPalette(QPalette(Qt.black, Qt.white))
        self.enableMultiSelect()
        self._informationTagLabel = None

    def setTab(self, controller):
        """ Sets tab controller.
        """
        self._tab = controller
        self.setDisplayGridFlag(self._tab.plugin().displayGridFlag())

    def tab(self):
        """ Return tab controller.
        
        See setTab()
        """
        return self._tab

    def setDataAccessor(self, accessor):
        """ Print warning that this function is useless, as data accessor is retrieved through tab controller.
        """
        logging.warning("%s: setDataAccessor() - This function does not do anything, as dataAccessor() retrieves data accessor through tab controller." % self.__class__.__name__)

    def dataAccessor(self):
        if self.tab():
            return self.tab().dataAccessor()
        return None

    def setInformationTag(self, text):
        if not self._informationTagLabel:
            self._informationTagLabel = QLabel(self)
            font = self._informationTagLabel.font()
            font.setPointSize(10)
            self._informationTagLabel.setFont(font)
            self._informationTagLabel.show()
        self._informationTagLabel.setText(text)
        self._informationTagLabel.resize(self._informationTagLabel.sizeHint())

    def setDisplayGridFlag(self, checked):
        """
        """
        if checked != self._displayGridFlag:
            self.update()
        self._displayGridFlag = checked

    def dataObjectByName(self, name):
        """ Returns module with given name or None if there is no such one.
        
        Overwrites ConnectableWidgetOwner.getWidgetByName().
        """
        for widget in [child for child in self.children() if isinstance(child, ModuleWidget)]:
            if self.dataAccessor().name(widget.dataObject()) == name:
                return widget.dataObject()
        return None
    
    def moduleNames(self):
        """ Returns list of the names of all modules.
        """
        return [self.dataAccessor().name(widget.dataObject()) for widget in [child for child in self.children() if isinstance(child, ModuleWidget)]] 

    def mouseDoubleClickEvent(self, event):
        """ Add widget of type currently selected in tool view.
        """
        if self.tab().propertyView().valueChangedSuccessful():
            self.addModuleWidget(None, event.pos())

    def keyPressEvent(self, event):
        """ Calls
                    delete() method of selected child widgets if multi-select is activated.
                    copy() method if copy key or CTRL+V is pressed
        Overwrites VispaWidgetOwner keyPressEvent().
        """
        logging.debug("%s: keyPressEvent()" % (self.__class__.__name__))

        ctrlPressed = (event.modifiers() & Qt.ControlModifier) == Qt.ControlModifier
        # multi-select delete
        if self.multiSelectEnabled() and (event.key() == Qt.Key_Backspace or event.key() == Qt.Key_Delete):
            self.deleteSelectedModules()

        #elif event.key() == (Qt.Key_C or Qt.Key_Control):
        elif (ctrlPressed and event.key() == Qt.Key_C) or event.matches(QKeySequence.Copy): #actually it should be a keyReleaseEvent .. otherwise executed multiple times if keys are hold
            self.copySelectedModules()

        elif (ctrlPressed and event.key() == Qt.Key_X) or event.matches(QKeySequence.Cut): #actually it should be a keyReleaseEvent .. otherwise executed multiple times if keys are hold
            self.copySelectedModules()
            self.deleteSelectedModules()

        elif (ctrlPressed and event.key() == Qt.Key_V) or event.matches(QKeySequence.Paste): #actually it should be a keyReleaseEvent .. otherwise executed multiple times if keys are hold
            self.dropWidgetsFromClipboard()
        #zoom
        elif ctrlPressed and (event.key() == Qt.Key_Plus or event.key() == Qt.Key_Minus):
            if event.key() == Qt.Key_Plus:
                self.tab().scrollArea().incrementZoom()
            elif event.key() == Qt.Key_Minus:
                self.tab().scrollArea().decrementZoom()

        # navigation 
        elif event.key() == Qt.Key_Left or event.key() == Qt.Key_Right or event.key() == Qt.Key_Up or event.key() == Qt.Key_Down:
            if ctrlPressed:
                self.keyboardDragGroupEvent(event.key())
            else:
                self.keyboardNavigationEvent(event.key(), (event.modifiers() & Qt.ShiftModifier) == Qt.ShiftModifier)

    def copySelectedModules(self):
        logging.debug("%s: copySelectedModules()" % (self.__class__.__name__))
        selection = self.selectedWidgets()[:]

        widgetsToCopy = []
        for widget in selection:
            if isinstance(widget, ModuleWidget):
                widgetsToCopy.append(widget)

        firstItem = True
        for widget in widgetsToCopy:
            if firstItem:
                widget.copyToClipboard()
                firstItem = False
            else:
                widget.copyToClipboardAPPEND()

    def dropWidgetsFromClipboard(self):
        self.emit(SIGNAL("dropWidgetsFromClipboard"))

    def deleteSelectedModules(self):
        selection = self.selectedWidgets()[:]
        undoEvents = []
        for widget in selection:
            if isinstance(widget, PortConnection):
                if not widget.sinkPort().parent().isSelected() and not widget.sourcePort().isSelected():
                    # only remove connections if none of their parents is selected
                    # in which case connections are removed in an undoable way anyway)
                    undoEvents.append(UndoDisconnectModulesEvent(widget))
                    widget.delete()
        for widget in selection:
            if isinstance(widget, ModuleWidget):
                undoEvents.insert(0, UndoDeleteModuleEvent(widget))
                widget.delete()

        if len(undoEvents) > 0:
            self.tab().addUndoEvent(MultiUndoEvent(undoEvents, "Multiple delete"))
            self.emit(SIGNAL("modified"))

    def keyboardDragGroupEvent(self, key):
        if len(self.selectedWidgets()) == 0:
            self.deselectAllWidgets()
            return

        if key == Qt.Key_Left:
            offset = QPoint(-self.GROUP_DRAG_STEP, 0)
        elif key == Qt.Key_Right:
            offset = QPoint(self.GROUP_DRAG_STEP, 0)
        elif key == Qt.Key_Up:
            offset = QPoint(0, -self.GROUP_DRAG_STEP)
        elif key == Qt.Key_Down:
            offset = QPoint(0, self.GROUP_DRAG_STEP)
        else:
            self._groupDragInitWidget = None
            return

        if not self._groupDragInitWidget:
            #print "initialize griup drag", [w.title() for w in self.selectedWidgets()] 
            self.initWidgetMovement(self.selectedWidgets()[0])
            self._groupDragInitWidget = self.selectedWidgets()[0]
            self._groupDragInitWidget.resetMouseDragOffset()

        #print "offset = ", offset
        #print "group drag widget = ", self._groupDragInitWidget.title()

        self._groupDragInitWidget.dragWidget(self._groupDragInitWidget.pos() + offset)

    def keyboardNavigationEvent(self, key, shiftPressed=False):
        logging.debug("%s: keyboardNavigationEvent() - shiftPressed = %s" % (self.__class__.__name__, str(shiftPressed)))

        self._groupDragInitWidget = None
        selectedWidgets = self.selectedWidgets()
        #print "selectedWidgets", [w.title() for w in selectedWidgets]
        lenSelectedWidgets = len(selectedWidgets)
        if lenSelectedWidgets == 0:
            moduleWidgets = [child for child in self.children() if isinstance(child, ModuleWidget)]
            if len(moduleWidgets) == 0:
                return
            lastSelectedWidget = moduleWidgets[0]
        else:
            lastSelectedWidget = selectedWidgets[lenSelectedWidgets - 1]
        lastSelectedPos = lastSelectedWidget.pos()
        nextWidget = None

        for child in [c for c in self.children() if isinstance(c, ModuleWidget)]:
            if key == Qt.Key_Left and \
            (lastSelectedPos.x() > child.pos().x() and (not nextWidget or nextWidget.pos().x() < child.pos().x())):
                nextWidget = child
            elif key == Qt.Key_Right and \
            (lastSelectedPos.x() < child.pos().x() and (not nextWidget or nextWidget.pos().x() > child.pos().x())):
                nextWidget = child
            elif key == Qt.Key_Up and \
            (lastSelectedPos.y() > child.pos().y() and (not nextWidget or nextWidget.pos().y() < child.pos().y())):
                nextWidget = child
            elif key == Qt.Key_Down and \
            (lastSelectedPos.y() < child.pos().y() and (not nextWidget or nextWidget.pos().y() > child.pos().y())):
                nextWidget = child

        if not shiftPressed:
            self.deselectAllWidgets()

        if nextWidget:
            if not nextWidget in self.selectedWidgets():
                nextWidget.select(True, shiftPressed)
            else:
                lastSelectedWidget.select(False, shiftPressed)

        #print "selectedWidgets", [w.title() for w in self.selectedWidgets()]

    def deselectAllWidgets(self, exception=None):
        logging.debug("%s: deselectAllWidgets()" % (self.__class__.__name__))
        if not self.tab().propertyView().valueChangedSuccessful():
            # don't go into infinte loop
            return
        
        WidgetView.deselectAllWidgets(self, exception)
        self._groupDragInitWidget = None
        self.tab().plugin().showAnalysisInformation()

        atLeastOneSelected = False
        selection = self.selectedWidgets()[:]
        for widget in selection:
            if isinstance(widget, ModuleWidget):
                atLeastOneSelected = True
        if atLeastOneSelected:
            self.tab().enableMenu()
        else:
            self.tab().disableMenu()

    def dragEnterEvent(self, event):
        """ Accepts drag enter event if module is dragged.
        """
        #logging.debug(self.__class__.__name__ + ": dragEnterEvent()")
        if event.mimeData().hasFormat(self.URI_MIME_TYPE) or \
            event.mimeData().hasFormat(self.MODULE_MIME_TYPE):
                event.acceptProposedAction()

    def dropEvent(self, event):
        """ Handle drop of module.
        """
        #logging.debug(self.__class__.__name__ + ": dropEvent()")

        if event.mimeData().hasFormat(self.MODULE_MIME_TYPE):
            if self.addModuleWidget(str(event.mimeData().data(self.MODULE_MIME_TYPE)), event.pos()):
                event.acceptProposedAction()

        if event.mimeData().hasFormat(self.URI_MIME_TYPE):
            for url in event.mimeData().urls():
                url = str(url.toLocalFile())
                if sys.platform == "win32" and url[:1] == "/":
                    url = url[1:]
                if self.addModuleByFilename(url, event.pos()):
                    event.acceptProposedAction()

    def paintEvent(self, event):
        WidgetView.paintEvent(self, event)

        if self._displayGridFlag or self._selectionRect:
            painter = QPainter(self)

        if self._displayGridFlag:
            font = painter.font()
            font.setPointSize(10)
            painter.setPen(QColor(Qt.gray))
            painter.setFont(font)
            width = self.width()
            height = self.height()
            grid_spacing = self.GRID_SPACING * self.zoomFactor()

            x = -0.5 * grid_spacing
            while x < width:
                x += grid_spacing
                y = -0.3 * grid_spacing # leave enough space for information tag above grid
                while y < height:
                    y += grid_spacing
                    painter.drawText(x, y, "+")

        if self._selectionRect:
            painter.setPen(QPen(QColor(0, 0, 250), 3))  # QColor, width
            painter.drawRect(self._selectionRect)
            painter.fillRect(self._selectionRect, QColor(0, 0, 230, 100))

    def widgetSelected(self, widget, multiSelect=False):
        """ Tells tab controller to show properties of widget.
            and enables copy/cut/paste main menu
        """

        self.tab().enableMenu()
        WidgetView.widgetSelected(self, widget, multiSelect)
        if len(self.selectedWidgets()) == 1:
            self.tab().onSelected(widget.dataObject())
        else:
            self.tab().clearPropertyView()
            self.tab().plugin().showAnalysisInformation()

    def widgetDragged(self, widget):
        """ Set modification flag.
        """
        WidgetView.widgetDragged(self, widget)
        self.tab().addUndoEvent(UndoMoveEvent(self.lastMovedWidgets()))
        self.emit(SIGNAL("modified"))

    def addModuleWidget(self, moduleType=None, position=None, multisel=False):
        """ Adds widget of type moduleType at given QPosition.
        
        If moduleType is omitted the type will be determined from selection in available modules list of tool view.
        """
        if not moduleType:
            # get type from analysis designer tool view list
            moduleType = self.tab().selectedModuleName()
        dataObject = self.tab().createPxlModule(moduleType)
        if not dataObject:
            return False

        widget = self.tab().createModuleWidget(dataObject)

        if position:
            offset = QPoint(widget.width() / 2, widget.height() / 2)
            widget.move(position - offset)
            self._previusDragPosition = position - offset
            self.dataAccessor().updateModulePos(widget.dataObject(), widget.pos())

        self.moduleWidgetAdded(widget, multisel)
        self.tab().addUndoEvent(UndoDropModuleEvent(widget))
        return widget

    def moduleWidgetAdded(self, widget, multiselect=False):
        """ Does post-processing for addModuleWidget() such as setting modification flag.
        
        This function is separated in order to make it accessible from UndoEvents.
        """
        #self.tab().setModified()
        self.emit(SIGNAL("modified"))
        self.autosizeScrollArea()
        widget.select(True, multiselect)
        widget.setFocus()

    def addModuleByFilename(self, filename, position=None):
        """ Creates script filter modules and sets name and script to given filename.
        """
        base = os.path.basename(str(filename))
        ext = os.path.splitext(base)[1].lower().strip(".")

        moduleType = self.tab().plugin().moduleTypeByFile(filename)
        if not moduleType:
            if ext == "py":
                self.tab().showWarningMessage(self.COULD_NOT_DETERMINE_MODULE_TYPE_WARNING)
            else:
                self.tab().showWarningMessage("Unsupported file extension '" + str(ext) + "'.")
            return

        if moduleType == "File Input":
    	    options = [{"type": "usage placeholder", "name": "File names", "value": "['" + filename + "']"}]
    	else:
	        options = [{"type": "usage placeholder", "name": "filename", "value": filename}]

        #options += self.tab().plugin().scriptFileOptions(filename)
        widget = self.addModuleWidget(moduleType, position)
        widget.setOptions(options)
        widget.loadPxlPorts() # update ports
        widget.select() # update property view
        widget.width()  # calls rearrangeContent(), needed for correct resizing
        return widget

    def addPortConnection(self, port1, port2):
        """ Asks tab controller to creates connection between the two provided PortWidgets.
        """
        connection = self.tab().createPortConnection(port1, port2)
        if connection:
            self.tab().createPxlConnection(connection)
            self.tab().addUndoEvent(UndoConnectModulesEvent(connection))
            #self.tab().setModified()
            self.emit(SIGNAL("modified"))

    def clear(self):
        """ Deletes all objects in the Workspace
        """
        #logging.debug(__name__ + ": clear")
        if self._informationTagLabel:
            self._informationTagLabel.deleteLater()
            self._informationTagLabel = None
        self.widgetDict = {}
        for w in self.children():
            if isinstance(w, QWidget):
                w.setParent(None)
                w.deleteLater()

    def childWidgetsCount(self):
        return len([child for child in self.children() if isinstance(child, ModuleWidget)])

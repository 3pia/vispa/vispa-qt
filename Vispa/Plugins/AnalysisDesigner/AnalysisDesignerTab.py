import shutil
import os
import sys
import cPickle
import re

import logging

from PyQt4 import QtCore
from PyQt4 import QtGui
from PyQt4.QtCore import SIGNAL, QSize, qVersion, QStringList, Qt, QDir, QVariant, QPoint
from PyQt4.QtGui import QIcon, QPixmap, QColor, QPalette, QSplitter, QTextEdit, QHeaderView, QFrame, QVBoxLayout, QHBoxLayout, QProgressBar, QToolButton, QStandardItemModel, QSizePolicy, QGridLayout, QLabel, QGroupBox, QPushButton, QCheckBox, QTreeWidget, QAction, QCursor, QMenu, QMessageBox, QFileDialog, QDirModel, QFileSystemModel, QTreeView, QTreeWidgetItem


from Vispa.Main.Preferences import *
import Vispa.Main.Preferences as Preferences
import Vispa.Main.PreferencesEditor as PreferencesEditor
from Vispa.Main.SplitterTab import SplitterTab, SplitterToolBar
from Vispa.Gui.Header import FrameWithHeader
from Vispa.Gui.PortConnection import PortConnection
from Vispa.Gui.PortWidget import PortWidget, SourcePort, SinkPort
from Vispa.Gui.ZoomableScrollArea import ZoomableScrollArea
from Vispa.Gui.SimpleDraggableTreeWidget import SimpleDraggableTreeWidget
from Vispa.Gui.ToolBoxContainer import ToolBoxContainer
from Vispa.Views.PropertyView import PropertyView

from Vispa.Main.GuiFacade import guiFacade

from AnalysisDataAccessor import AnalysisDataAccessor
from LayerToolBox import LayerToolBox
from AnalysisDesignerWorkspace import AnalysisDesignerWorkspace
from ModuleWidget import ModuleWidget

from AnalysisExecutionThread import AnalysisExecutionThread
from Vispa.Plugins.AnalysisDesigner.UndoEvents import UndoChangePropertyViewValueEvent, UndoDisconnectModulesEvent, UndoDeleteModuleEvent

import pxl.core

class PXLLogLevelSelectorWidget(QtGui.QComboBox):
  def __init__(self, parent=None):
    QtGui.QComboBox.__init__(self, parent)
    L = ""
    i = 0
    self.addItem('Enviroment variable')
    while True:
      n = pxl.core.LogLevelToString(i)
      if n == L:
        break
      L = n
      i += 1
      self.addItem(n.rstrip())
  def getLogLevel(self):
    return self.currentIndex() - 1
  def setLogLevel(self, level):
    self.setCurrentIndex(level + 1)


class PXLLogSettingWidget(PreferencesEditor.SettingWidget):
  def __init__(self, setting, parent=None):
    PreferencesEditor.SettingWidget.__init__(self, setting, parent)
    self.__comboBox = PXLLogLevelSelectorWidget(self)
    self.__comboBox.setLogLevel(setting.getValue())
    layout = QtGui.QHBoxLayout()
    layout.addWidget(self.__comboBox)
    self.setLayout(layout)
    #self.__comboBox.setActive(setting.isEnabled())
  def apply(self):
    self._setting.setValue(self.__comboBox.currentIndex() - 1)
  def showDefault(self):
    self.__comboBox.setCurrentIndex(0)



s = Preferences.addSetting("Analysis Designer", "PXL default log-level", -1, "Default log level used for the analysis execution")
s.setEditWidget(PXLLogSettingWidget(s))




class AnalysisDesignerTab(SplitterTab):
    """ This is the main frame of the Analysis Designer Plugin.
    
    The tab is split in three horizontal parts, from left to right: Tool View, Workspace, Property View.
    """

    def __init__(self, plugin, parent=None):
        """ constructor """
        logging.debug(self.__class__.__name__ + ": __init__()")
        self._availableModuleList = None
        self._executionSplitter = None
        self._executionOutputTextEdit = None
        self._leftToolContainer = None      # available modules python scripts
        self._rightToolContainer = None     # propertyView, layers
        self._layerToolBoxFrame = None
        self._layerToolBox = None
        self._executionView = None
        self._runButton = None
        self._stopButton = None

        SplitterTab.__init__(self, plugin, parent)
        self.setDataAccessor(AnalysisDataAccessor(self))
        self._pxlAnalysis = self._accessor.createAnalysis()
        self.createToolBar()
        self._toolViewToolBarId = self.toolBar().addSection(SplitterToolBar.ALIGNMENT_RIGHT)
        self._centerViewToolBarId = self.toolBar().addSection(SplitterToolBar.ALIGNMENT_RIGHT)
        self._propertyViewToolBarId = self.toolBar().addSection(SplitterToolBar.ALIGNMENT_RIGHT)

        self.createToolView()
        self.createWorkspaceView()
        self.createPropertyAndLayerView()


        #self.setFindEnabled(True)
        self.setAllowSelectAll(True)
        self._widgetCounter = {}
        self._analysisPath = ""
        self._executionThread = None
        self._analysisRunningMessageId = None
        self._autorunExecutionFlag = True
        self._analysisAbortedFlag = False
        self._originalSizes = [100, 1, 100]
        self._rememberAutorunExecutionSettingFlag = False
        self._layerActions = {}

        if self.plugin().pxlErrorMessage():
            logging.error(self.__class__.__name__ + ": __init__() - " + self.plugin().pxlErrorMessage())
            self.showErrorMessage(self.plugin().pxlErrorMessage())
            self.plugin().resetPxlErrorMessage()

        self._loadIni()

        self.enableUndo()
        self.setCopyPasteEnabled(False)
        self.createLayerActions()
        self.showAnalysisInformation()

        #needed for MIME Data
        self._PXL_ANALYSIS_ELEMENT_MIME_TYPE = "text/x-pxl-analysis-element"

    def scrollArea(self):
        return self._scrollArea

    def fillAvailableModulesList(self):
        """ Sets controller for this tab and connects signals etc.
        """

        # fill list of available modules with icons
        items = []
        for moduleType in self.plugin().availablePxlModules():
            widget = ModuleWidget(self._availableModuleList)
            #widget.setModuleController(self.plugin().createModuleController(moduleType))
            widget.setTitle(moduleType)
            widget.setTab(self)
            #widget.loadModuleConfiguration()
            widget.rearangeContent()
            item = QTreeWidgetItem([moduleType])
            item.setIcon(0, QIcon(QPixmap.grabWidget(widget)))
            items.append(item)
            widget.hide()
            widget.deleteLater()
        self._availableModuleList.populate(items)
        if len(items) > 0:
            self._availableModuleList.setCurrentItem(items[0])

    def setMainWindow(self, main):
        """ Overwrites method of AbstractTab.
        
        Sets sizes for the three splitter parts based on main window's width.
        """
        SplitterTab.setMainWindow(self, main)
        #weights = [2*self.toolViewPreferredWidth(), main.width() - self.toolViewPreferredWidth() -self.propertyView().width(), 5*self.propertyView().width()]
        #weights = [30, 200, 70]
        weights = [30, 200, 70, 30]
        self.horizontalSplitter().setSizes(weights)
        self.updateToolBarSizes()

    def enableMenu(self):
        self.setCopyPasteEnabled(True)
        self.mainWindow().application().updateMenu()

    def disableMenu(self):
        self.setCopyPasteEnabled(False)
        self.mainWindow().application().updateMenu()

    def cut(self):
        self.workspace().copySelectedModules()
        self.workspace().deleteSelectedModules()

    def copy(self):
        self.workspace().copySelectedModules()

    def paste(self):
        self.dropWidgetsFromClipboard()

    def workspace(self):
        """ Returns workspace of this tab.
        """
        return self._analysisWorkspace

    def toolViewPreferredWidth(self):
        """ Returns desired width of tool view (left side of window containing available modules).
        """
        return self._availableModuleList.sizeHintForColumn(0)

    def workspaceRightClicked(self, point):
        popup = QMenu(self._availableModuleList)
        popup.addAction(guiFacade.createAction('Insert module from module store here', self.availableModuleDoubleClicked))
        popup.addAction(guiFacade.createAction('Paste module from clipboard', self.dropWidgetsFromClipboard))
        self._insertPoint = self.workspace().mapFromGlobal(point)
        popup.exec_(point)

    def availableModuleRightClicked(self, point):
        popup = QMenu(self._availableModuleList)
        popup.addAction(guiFacade.createAction('Insert module', self.availableModuleDoubleClicked))
        popup.exec_(point)

    def availableModuleDoubleClicked(self, item=None, column=None):
        moduleType = self.selectedModuleName()
        if hasattr(self, "_insertPoint"):
            point = self._insertPoint
            del self._insertPoint
        else:
            point = QPoint(55, self.workspace().childrenRect().bottom() + 100)
        self.workspace().addModuleWidget(moduleType, point)

    def scriptsDoubleClicked(self, url=None):
        if not url:
            for url in self._scriptFileSystemModel.mimeData(self._scriptTreeView.selectedIndexes()).urls():
                url = str(url.toLocalFile())
                if sys.platform == "win32" and url[:1] == "/":
                    url = url[1:]
        if hasattr(self, "_insertPoint"):
            point = self._insertPoint
            del self._insertPoint
        else:
            point = QPoint(55, self.workspace().childrenRect().bottom() + 100)
        ext = os.path.splitext(url)[1]
        print "scriptsDoubleClicked ", url, ext
        if os.path.isfile(url):
            if ext == ".py" or ext == ".pxlio":
                self.workspace().addModuleByFilename(url, point)
            elif ext == ".xml":
                self.mainWindow().application().doubleClickOnFile(url)

    def createWorkspaceView(self):
        self._centerFrameWithHeader = FrameWithHeader(self.horizontalSplitter())
        self._centerFrameWithHeader.header().setText("Analysis Designer")
        #self._treeViewMenuButton = self._centerFrameWithHeader.header().createMenuButton()
        self._scrollArea = ZoomableScrollArea(self._centerFrameWithHeader)
        self._scrollArea.setMargins(50, 50)
        self._centerFrameWithHeader.addWidget(self._scrollArea)
        self._analysisWorkspace = AnalysisDesignerWorkspace()
        self.scrollArea().setWidget(self._analysisWorkspace)
        self.connect(self._analysisWorkspace, SIGNAL('mouseRightPressed'), self.workspaceRightClicked)

        self._analysisWorkspace.setTab(self)
        self.connect(self._analysisWorkspace, SIGNAL('modified'), self.setModified)
        self.connect(self._analysisWorkspace, SIGNAL('selected'), self.onSelected)
        self.connect(self._analysisWorkspace, SIGNAL('dropWidgetsFromClipboard'), self.dropWidgetsFromClipboard)

    def createPropertyAndLayerView(self):
        """ Creates PropertyView object, adds it to this tab and makes it available via propertyView().
        """

        self._rightToolContainer = ToolBoxContainer(self.horizontalSplitter())

        self._propertyView = PropertyView(self._rightToolContainer, "PropertyView")
        self._propertyView.setShowAddDeleteButton(True)
        self._propertyView.setDataAccessor(self.dataAccessor())
        self._rightToolContainer.addWidget(self._propertyView)

        self.connect(self.propertyView(), SIGNAL('valueChanged'), self.propertyViewValueChanged)

        if self.dataAccessor().hasLayers():
            self._layerToolBox = LayerToolBox()

            self._layerToolBoxFrame = FrameWithHeader(self._rightToolContainer)
            self._layerToolBoxFrame.header().setText("Layer")
            self._layerToolBoxFrame.header().createMenuButton()
            self._layerToolBoxFrame.addWidget(self._layerToolBox)
            self._rightToolContainer.addWidget(self._layerToolBoxFrame)

            self.connect(self._layerToolBox, SIGNAL("itemSelectionChanged()"), self.layerSelectionChangedSlot)
            self.connect(self._layerToolBox, SIGNAL("deleteButtonPressed"), self.removeLayer)
            self.connect(self._layerToolBox, SIGNAL("rightMouseButtonClicked"), self.layerToolBoxFrameButtonClickedSlot)
            self.connect(self.layerToolBoxFrameMenuButton(), SIGNAL("clicked(bool)"), self.layerToolBoxFrameButtonClickedSlot)
            self.connect(self._layerToolBoxFrame.header(), SIGNAL("mouseRightPressed"), self.layerToolBoxFrameButtonClickedSlot)

            self._rightToolContainer.splitter().setSizes([700, 300])

        self.toolBar().takeToolBoxContainerButtons(self._rightToolContainer, self._propertyViewToolBarId)

    def layerToolBox(self):
        return self._layerToolBox

    def createToolView(self):
        """ Creates left part of window holding list of available pxl modules.
        """
        self._leftToolContainer = ToolBoxContainer(self.horizontalSplitter())

        self._availableModuleList = SimpleDraggableTreeWidget("Available Modules", True, "text/x-module-name")
        self._availableModuleList.setToolTip("drag & drop modules from here into the analysis")
        self._availableModuleList.setIconSize(QSize(25, 25))
        self._leftToolContainer.addWidget(self._availableModuleList)
        self.fillAvailableModulesList()
        self.connect(self._availableModuleList, SIGNAL("itemDoubleClicked(QTreeWidgetItem*,int)"), self.availableModuleDoubleClicked)
        self.connect(self._availableModuleList, SIGNAL('mouseRightPressed'), self.availableModuleRightClicked)

        [major, minor, revision] = str(qVersion()).split(".")
        if major > 4 or (major == 4 and minor >= 4):
            logging.debug(self.__class__.__name__ + ": createToolView() - Using ScriptFileSystemModel.")
            self._scriptFileSystemModel = ScriptFileSystemModel()
            self._scriptFileSystemModel.setRootPath(homeDirectory)
        else:
            logging.debug(self.__class__.__name__ + ": createToolView() - Using ScriptFilterDirModel.")
            self._scriptFileSystemModel = ScriptFilterDirModel()

        self._scriptTreeView = fileSystemTreeView()
        self._lightBlueBackgroundColor = QColor(Qt.blue).lighter(195)
        self._scriptTreeView.palette().setColor(QPalette.Base, self._lightBlueBackgroundColor)       # OS X
        self._scriptTreeView.palette().setColor(QPalette.Window, self._lightBlueBackgroundColor)
        self._scriptTreeView.setModel(self._scriptFileSystemModel)
        self._scriptTreeView.setRootIndex(self._scriptFileSystemModel.index(homeDirectory))
        self._scriptTreeView.setDragEnabled(True)
        self.connect(self._scriptFileSystemModel, SIGNAL("rowsInserted ( const QModelIndex &, int , int  )"), self.autosizeToolContainer)
        self.connect(self._scriptFileSystemModel, SIGNAL("rowsInserted ( const QModelIndex &, int , int  )"), self.fitScriptTreeViewToContent)
        self.connect(self._scriptTreeView, SIGNAL("collapsed(const QModelIndex &)"), self.fitScriptTreeViewToContent)
        self.connect(self._scriptTreeView, SIGNAL("expanded(const QModelIndex &)"), self.fitScriptTreeViewToContent)
        self.connect(self._scriptTreeView, SIGNAL("doubleClicked"), self.scriptsDoubleClicked)
        self.fitScriptTreeViewToContent()

        self._scriptTreeViewFrame = FrameWithHeader()
        self._scriptTreeViewFrame.header().setText("Python Scripts")
        self._scriptTreeViewFrame.header().setToolTip("click on '>' for options of this view")
        self._scriptTreeViewFrame.header().createMenuButton()
        self._scriptTreeViewFrame.setToolTip("drag & drop scripts from here into the analysis")
        self._scriptTreeViewFrame.addWidget(self._scriptTreeView)
        self._leftToolContainer.addWidget(self._scriptTreeViewFrame)

        #self._leftToolContainer.addWidget(self._scriptTreeView)

        self.toolBar().takeToolBoxContainerButtons(self._leftToolContainer, self._toolViewToolBarId)

        self.connect(self.scriptLocationMenuButton(), SIGNAL("clicked(bool)"), self.scriptViewMenuButtonClickedSlot)
        self.connect(self._scriptTreeViewFrame.header(), SIGNAL("mouseRightPressed"), self.scriptViewMenuButtonClickedSlot)
        self.updateScriptLocation()

    def updateScriptLocation(self):
        logging.debug("%s: updateScriptLocation()" % self.__class__.__name__)
        if hasattr(self._scriptFileSystemModel, "setRootpath"):
            self._scriptFileSystemModel.setRootPath(self.plugin().scriptLocation())
        self._scriptTreeView.setRootIndex(self._scriptFileSystemModel.index(self.plugin().scriptLocation()))
        filterList = QStringList()
        filterList.append("*.py")
        filterList.append("*.pxlio")
        filterList.append("*.xml")
        self._scriptFileSystemModel.setNameFilters(filterList)
        self.autosizeToolContainer()

    def scriptLocationMenuButton(self):
        return self._scriptTreeViewFrame.header().menuButton()

    def layerToolBoxFrameMenuButton(self):
        return self._layerToolBoxFrame.header().menuButton()

    def autosizeToolContainer(self):
        """ Sets sizes of tool container's splitter weighted by initially shown number of rows in available modules and script tree views.
        
        This function works as slot for the rowInserted() signal of self._scriptFileSystemModel.
        It automatically disconnects itself from the signal, so this function is only called once automatically.
        """
        #weights = [self._availableModuleList.model().rowCount()*200, self._scriptTreeView.model().rowCount(self._scriptFileSystemModel.index(self.plugin().scriptLocation()))*100]
        weights = [50, 50]
        self._leftToolContainer.splitter().setSizes(weights)
        self.disconnect(self._scriptFileSystemModel, SIGNAL("rowsInserted ( const QModelIndex &, int , int  )"), self.autosizeToolContainer)

    def fitScriptTreeViewToContent(self):
        """ Sets column width of self._scriptTreeView to fit content.
        
        This function works as slot for the rowInserted() signal of self._scriptFileSystemModel.
        """
        #logging.debug(self.__class__.__name__ + ": fitScriptTreeViewToContent()")
        self._scriptTreeView.resizeColumnToContents(0)

    def selectedModuleName(self):
        """ Returns currently selected string selected in available modules list.
        
        If nothing is selected None is returned.
        """
        item = self._availableModuleList.currentItem()
        if item:
            return str(item.text(0))
        return None

    def executionOutputTextEdit(self):
        return self._executionOutputTextEdit

    def createExecutionToolBar(self):
        self._executionProgressBar = QProgressBar(self.toolBar())
        self._executionProgressBar.setMinimum(0)
        self._executionProgressBar.setMaximum(100)
        self._executionProgressBar.setValue(0)
        self._executionProgressBar.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self._minimizeButton = QToolButton(self.toolBar())
        self._minimizeButton.setText("v")
        self._minimizeButton.setCheckable(True)
        self._originalButton = QToolButton(self.toolBar())
        self._originalButton.setText("-")
        self._originalButton.setCheckable(True)
        self._originalButton.setChecked(True)
        self._maximizeButton = QToolButton(self.toolBar())
        self._maximizeButton.setText("^")
        self._maximizeButton.setCheckable(True)
        self._closeButton = QToolButton(self.toolBar())
        self._closeButton.setText("x")

        self.toolBar().addWidgetToSection(self._executionProgressBar, self._centerViewToolBarId)
        self.toolBar().addWidgetToSection(self._minimizeButton, self._centerViewToolBarId)
        self.toolBar().addWidgetToSection(self._originalButton, self._centerViewToolBarId)
        self.toolBar().addWidgetToSection(self._maximizeButton, self._centerViewToolBarId)
        self.toolBar().addWidgetToSection(self._closeButton, self._centerViewToolBarId)

    def createExecutionView(self):
        self.createExecutionToolBar()
        self._executionSplitter = QSplitter(Qt.Horizontal, self)
        self._executionOutputTextEdit = QTextEdit(self._executionSplitter)
        self._executionOutputTextEdit.setAcceptRichText(True)
        self._executionOutputTextEdit.setFont(QtGui.QFont("mono"))
        self._executionOutputTextEdit.setLineWrapMode(QTextEdit.NoWrap);

        self._executionView = QFrame(self._executionSplitter)
        self._executionView.setFrameShadow(QFrame.Raised)
        self._executionView.setFrameStyle(QFrame.StyledPanel)
        self._executionView.setAutoFillBackground(True)
        self._executionView.setLayout(QVBoxLayout())

        self._executionView.layout().setContentsMargins(0, 0, 0, 1)

        headerLayout = QHBoxLayout()
        headerLayout.setSpacing(0)
        headerLayout.setContentsMargins(0, 0, 0, 0)

        model = QStandardItemModel(self._executionView)
        model.setHorizontalHeaderLabels(["Analysis Execution"])
        header = QHeaderView(Qt.Horizontal, self._executionView)
        header.setModel(model)
        header.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        header.setStretchLastSection(True)
        header.setFixedHeight(25)
        self._executionView.layout().addWidget(header)

        # statistics box
        statBoxLayout = QGridLayout()
        statBoxLayout.addWidget(QLabel("#Modules:"), 0, 0)
        self._numberOfWidgetsLabel = QLabel()
        statBoxLayout.addWidget(self._numberOfWidgetsLabel, 0, 1)
        statBoxLayout.addWidget(QLabel("#Input Files:"), 1, 0)
        self._numberOfInputFilesLabel = QLabel()
        statBoxLayout.addWidget(self._numberOfInputFilesLabel, 1, 1)
        statBoxLayout.addWidget(QLabel("#Events:"), 2, 0)
        self._numberOfEventsLabel = QLabel()
        # TODO: make tool tip more specific
        self._numberOfEventsLabel.setToolTip("Due to the file structure there might be more than the shown number of events.")
        statBoxLayout.addWidget(self._numberOfEventsLabel, 2, 1)

        statBoxRowLayout = QHBoxLayout()
        statBoxRowLayout.addStretch(1)
        statBox = QGroupBox("Statistics")
        statBox.setLayout(statBoxLayout)
        statBoxRowLayout.addWidget(statBox)
        statBoxRowLayout.addStretch(3)
        self._executionView.layout().addLayout(statBoxRowLayout)
        self._executionView.layout().addStretch(3)

        self.__pxlLogLevelSelector = PXLLogLevelSelectorWidget()
        self.__pxlLogLevelSelector.setLogLevel(Preferences.getSetting('Analysis Designer', 'PXL default log-level'))
        pxlLogLayout = QtGui.QHBoxLayout()
        pxlLogLayout.addStretch(1)
        pxlLogLayout.addWidget(QtGui.QLabel("PXL Log Level"))
        pxlLogLayout.addWidget(self.__pxlLogLevelSelector)
        pxlLogLayout.addStretch(1)
        self._executionView.layout().addLayout(pxlLogLayout)
        self._runButton = QPushButton("Run")
        self._runButton.setDefault(True)
        self._stopButton = QPushButton("Stop")
        runControlLayout = QHBoxLayout()
        runControlLayout.addStretch(1)
        runControlLayout.addWidget(self._stopButton)
        runControlLayout.addWidget(self._runButton)
        runControlLayout.addStretch(1)
        self._executionView.layout().addLayout(runControlLayout)
        # autorun check boxes
        autorunCheckBox = QCheckBox("Automatically start execution")
        autorunCheckBox.setToolTip("If checked the analysis will be executed automatically when user switches to execution mode.")
        self.connect(autorunCheckBox, SIGNAL("stateChanged(int)"), self.autorunCheckBoxValueChangedSlot)
        if self.autorunExecutionEnabled():
            autorunCheckBox.setCheckState(Qt.Checked)
        rememberAutorunSettingCheckBox = QCheckBox("Remember decision")
        self.connect(rememberAutorunSettingCheckBox, SIGNAL("stateChanged(int)"), self.rememberAutorunSettingCheckBoxValueChanedSlot)
        if self.rememberAutorunSettingEnabled():
            rememberAutorunSettingCheckBox.setCheckState(Qt.Checked)
        self._executionView.layout().addWidget(autorunCheckBox)
        self._executionView.layout().addWidget(rememberAutorunSettingCheckBox)

        self._executionView.layout().addStretch(2)

        self.connectExecutionSignals()

        self._executionSplitter.setSizes([750, 250])
        #self.updateToolBarSizes()

        self.connectExecutionSignals()


    def updateExecutionView(self):
        if not self._executionView:
            return
        if self.analysisIsRunning():
            self._executionProgressBar.setMaximum(0)
            self._executionProgressBar.setValue(0)
        elif self.analysisAborted():
            self._executionProgressBar.setMaximum(100)
            self._executionProgressBar.setValue(0)
        else:
            self._executionProgressBar.setMaximum(100)
            self._executionProgressBar.setValue(100)
        numberOfEvents = 0
        self._numberOfWidgetsLabel.setText(str(self.workspace().childWidgetsCount()))
        self._numberOfInputFilesLabel.setText("NA")
        self._numberOfEventsLabel.setText(">= %d" % numberOfEvents)
        # should be long-term goal

    def connectExecutionSignals(self):
        self.connect(self._runButton, SIGNAL('clicked(bool)'), self.startExecution)
        self.connect(self._stopButton, SIGNAL('clicked(bool)'), self.stopExecution)
        self.connect(self._minimizeButton, SIGNAL('clicked(bool)'), self.minimizeExectionView)
        self.connect(self._originalButton, SIGNAL('clicked(bool)'), self.originalExectionView)
        self.connect(self._maximizeButton, SIGNAL('clicked(bool)'), self.maximizeExectionView)
        self.connect(self._closeButton, SIGNAL('clicked(bool)'), self.switchToDesignMode)

    def executionSplitter(self):
        return self._executionSplitter

    def executionView(self):
        return self._executionView

    def horizontalSplitterMovedSlot(self, pos, index):
        self.updateToolBarSizes()

    def updateToolBarSizes(self):
        self.toolBar().setSectionSizes(self.horizontalSplitter().sizes())

    def minimizeButton(self):
        return self._minimizeButton

    def originalButton(self):
        return self._originalButton

    def maximizeButton(self):
        return self._maximizeButton


    #@staticmethod
    def staticSupportedFileTypes():
        """ Returns supported file type: xml.
        """
        return [('xml', 'Analysis Designer file')]
    staticSupportedFileTypes = staticmethod(staticSupportedFileTypes)

    def readFile(self, filename):
        """ Creates AnalysisXmlFile object and tells it to read xml file given in filename.
        """
        logging.debug("AnalysisDesignerTab: readFile()")
        status, message = self.dataAccessor().readFile(self._pxlAnalysis, filename)
        #if not status==True:
        #return False
        #else:
        if status:
            self._analysisPath = os.path.dirname(os.path.abspath(filename))
            self.propertyView().useRelativePaths(self._analysisPath)

            for pxlModule in self.dataAccessor().modules(self._pxlAnalysis):
                moduleName = pxlModule.getName()
                #try to find an index if it is there ...

                index = self.getIndexFromText(moduleName)
                if index == None:
                    index = 1
                else:
                    moduleName = self.getTextWithoutIndex(moduleName, str(index))

                if not (moduleName in self._widgetCounter):
                    self._widgetCounter[moduleName] = index
                else:
                    self._widgetCounter[moduleName] += 1

                self.createModuleWidget(pxlModule)
            for connection in self._pxlAnalysis.getConnections():
                sinkModuleWidget = None
                sourceModuleWidget = None
                sinkPort = None
                sourcePort = None
                sinkModuleWidget = self.workspace().getWidgetByName(connection.sinkModuleName)
                if sinkModuleWidget:
                    sinkPort = sinkModuleWidget.sinkPort(connection.sinkName)
                sourceModuleWidget = self.workspace().getWidgetByName(connection.sourceModuleName)
                if sourceModuleWidget:
                    sourcePort = sourceModuleWidget.sourcePort(connection.sourceName)
                self.createPortConnection(sourcePort, sinkPort)

            # set proper gray-scale mode for disabled modules
            for moduleWidget in self.moduleWidgets():
                if not self.dataAccessor().isEnabled(moduleWidget.dataObject()):
                    moduleWidget.enable(False)
                    self.enableModuleChain(moduleWidget, enable=False)

            self.scrollArea().autosizeScrollWidget()
            self.updateInformationTag()
            self.showAnalysisInformation()

        if isinstance(message, str):
            self.showErrorMessage(message)
        return status

    def getIndexFromText(self, text):
        index_str = re.search("(\d+)$", text)
        if not index_str == None:
            index_str = index_str.group(0)

        index = None

        if index_str == None:
            logging.debug("%s: getNameWithoutIndex() - Cannot resolve index from module named '%s'" % (self.__class__.__name__, text))
        else:
            try:
                index = int(index_str)
            except ValueError:
                index = None
        return index

    def getTextWithoutIndex(self, text, index_str):
        return text.rstrip(index_str)

    def moduleWidgets(self):
        return [child for child in self.workspace().children() if isinstance(child, ModuleWidget)]

    def writeFile(self, filename):
        """ Creates AnalysisXmlFile to write file to filename.
        """
        logging.debug('AnalysisDesignerTab: writeFile()')

        # write module coordinates into user record
        for moduleWidget in self.moduleWidgets():
            pos = QtCore.QPointF(moduleWidget.x() / self.workspace().zoomFactor(), moduleWidget.y() / self.workspace().zoomFactor())
            self.dataAccessor().updateModulePos(moduleWidget.dataObject(), pos)

        success = self.dataAccessor().writeFile(self._pxlAnalysis, filename)
        self.updateInformationTag()
        if success:
            self._analysisPath = os.path.dirname(filename)
            self.propertyView().useRelativePaths(self._analysisPath)
        return success

    def activated(self):
        """ Shows analysis designer's menus when user selects tab.
        """
        #logging.debug('AnalysisDesignerTab: activated()')
        guiFacade.showPluginMenu(self.plugin().analysisDesignerMenu())
        guiFacade.showPluginToolBar(self.plugin().analysisToolBar())
        guiFacade.showZoomToolBar()
        guiFacade.showPluginMenu(self.plugin().viewMenu())
        self.updateToolBarSizes()
        self.updateInformationTag()

        if not self.propertyView().valueChangedSuccessful():
            guiFacade.showPropertyValueChangedError()

    def setZoom(self, zoom):
        """  Sets zoom of tab's scroll area.
        
        Needed for zoom tool bar. See Tab setZoom().
        """
        self.scrollArea().setZoom(zoom)

    def zoom(self):
        """ Returns zoom of tab's scoll area.
        
        Needed for zoom tool bar. See Tab zoom().
        """
        return self.scrollArea().zoom()

    def setDataAccessor(self, accessor):
        """ Sets data accessor of tab's PropertyView.
        """
        self._accessor = accessor
        self.connect(accessor, QtCore.SIGNAL("moduleNameChanged"), self.moduleNameChangedSlot)
        self.connect(accessor, QtCore.SIGNAL("updatedWidgetPos"), self.loadSelectedModulePositionUserRecord)
        self.connect(accessor, QtCore.SIGNAL("moduleEnabled"), self.moduleEnabledSlot)
        self.connect(accessor, QtCore.SIGNAL("moduleBypassChanged"), self.moduleBypassChangedSlot)

    def dataAccessor(self):
        """ Returns data accessor. See setDataAccessor.
        """
        return self._accessor

    def moduleNameChangedSlot(self, newName, oldName):
        moduleWidget = self.workspace().getWidgetByName(oldName)
        if moduleWidget:
            moduleWidget.setTitle(newName)
        else:
            logging.warning("%s: moduleNameChangedSlot(): Could not find ModuleWidget with name '%s'. Aborting..." % (self.__class__.__name__, oldName))

    def moduleBypassChangedSlot(self, moduleName, bypassSource):
        moduleWidget = self.workspace().getWidgetByName(moduleName)
        if moduleWidget:
            self.enableModuleChain(moduleWidget, not moduleWidget.grayScaleModeEnabled())
        else:
            logging.warning("%s: moduleBypassChangedSlot(): Could not find ModuleWidget with name '%s'. Aborting..." % (self.__class__.__name__, oldName))

    def moduleEnabledSlot(self, moduleName, enabled):
        moduleWidget = self.workspace().getWidgetByName(moduleName)
        if moduleWidget:
            moduleWidget.enable(enabled)
            moduleWidget.update()
            self.enableModuleChain(moduleWidget, enabled)
        else:
            logging.warning("%s: moduleEnabledSlot(): Could not find ModuleWidget with name '%s'." % (self.__class__.__name__, moduleName))

    def enableModuleChain(self, widget, enable=True, firstWidgetFlag=True):
        if firstWidgetFlag:
            bypassSource = self.dataAccessor().bypassSource(widget.dataObject())

        for port in widget.sourcePorts():
            if firstWidgetFlag and port.name() == bypassSource:
                continue
            for connection in port.attachedConnections():
                connection.enableGrayScaleMode(not enable)
                connection.update()
                daughterWidget = connection.sinkPort().parent()

                # veto, if daughter widget has other incoming active connections
                veto = False
                for sinkPort in daughterWidget.sinkPorts():
                    for sinkConnection in sinkPort.attachedConnections():
                        if sinkConnection != connection and enable != connection.grayScaleModeEnabled():
                            veto = True
                            break
                    if veto:
                        break
                if veto:
                    break

                daughterWidget.enableGrayScaleMode(not enable)
                daughterWidget.update()
                self.enableModuleChain(daughterWidget, enable, False)

    def createPxlModule(self, moduleType):
        """ Creates a pxl module for the given type.
        
        If the module type is unknown None will be returned.
        """
        if not self._pxlAnalysis:
            logging.error("%s: createPxlModule(): Analysis is not set (see setDataAccessor(). Aborting..." % self.__class__.__name__)
            return

        logging.debug("%s: createPxlModule(): %s" % (self.__class__.__name__, moduleType))
        if self.plugin().pxlModuleAvailable(moduleType):
            if not moduleType in self._widgetCounter:
                self._widgetCounter[moduleType] = 0
            
            existingModuleNames = self.workspace().moduleNames()
            while True:
                # set default name of module
                self._widgetCounter[moduleType] += 1
                moduleName = moduleType + str(self._widgetCounter[moduleType])
                if not moduleName in existingModuleNames:
                    # this is necessary for the following case:
                    # An analysis contains exactly one module of type 'Module' and name 'Module2'.
                    # If analysis is reloaded and another module of the same type is added, the default name
                    # is 'Module2', since it is the second of that type. However, that name is already taken.
                    break

            pxlModule = self.dataAccessor().addModule(self._pxlAnalysis, moduleType, moduleName, 0)
            return pxlModule
        return None

    def removePxlModule(self, pxlModule):
        """ Removes given pxl module from this analysis.
        """
        self.dataAccessor().removeModule(self._pxlAnalysis, pxlModule)

    #copies e.g. a pxlModule to the qt clipboard
    def copyPxlObjectToClipboard(self, append, pxlObject):
        logging.debug(self.__class__.__name__ + ": copyPxlObjectToClipboard()")
        #logging.debug("AnalysisDesignerTab: copyModuleToClipboard(): added module of type '%s' to the clipboard", moduleType)

        serialized_data = self.dataAccessor().serialize(pxlObject)
        if not append:
            mime = self.serializeForClipboard([serialized_data])
            guiFacade.clipboard().setMimeData(mime)
        else:
            clipboard_data = self.deserializeFromClipboard()
            clipboard_data.append(serialized_data)
            guiFacade.clipboard().setMimeData(self.serializeForClipboard(clipboard_data))

    #returns QtCore.QMimeData()
    def serializeForClipboard(self, data):
        logging.debug(self.__class__.__name__ + ": serializeForClipboard()")

        mime = QtCore.QMimeData()
        encodedData = QtCore.QByteArray()
        encodedData.append(cPickle.dumps(data))
        mime.setData(self._PXL_ANALYSIS_ELEMENT_MIME_TYPE, encodedData)
        #object_to_copy.setMimeData(mime)
        return mime

    #returns list of elements in the clipboard
    def deserializeFromClipboard(self):
        logging.debug(self.__class__.__name__ + ": deserializeFromClipboard()")
        mimeData = guiFacade.clipboard().mimeData()
        data = []
        if mimeData.hasFormat(self._PXL_ANALYSIS_ELEMENT_MIME_TYPE):
            encodedData = mimeData.data(self._PXL_ANALYSIS_ELEMENT_MIME_TYPE)
            data = cPickle.loads(encodedData.data())
        return data

    def dropWidgetsFromClipboard(self):
        """ Creates new objects from clipboard
        """
        logging.debug(self.__class__.__name__ + ": dropWidgetsFromClipboard()")

        #pxlModuleDict = self.dataAccessor().deserialize(encodedData.data())
        pxlModuleDict_list = self.deserializeFromClipboard()
        firstModule = True
        for pxlModuleDict in pxlModuleDict_list:
            moduleType = None
            moduleOptions = None
            moduleProperties = None

            if "type" in pxlModuleDict:
                moduleType = pxlModuleDict["type"]

            if "options" in pxlModuleDict:
                moduleOptions = pxlModuleDict["options"]

            if "properties" in pxlModuleDict:
                moduleProperties = pxlModuleDict["properties"]

            if (moduleType):
                if moduleType == "PortConnection":
                    pass
                elif self.plugin().pxlModuleAvailable(moduleType):
                    userRecords = []
                    mainProperties = []

                    if moduleProperties:
                        userRecords = self.dataAccessor().findProperties(moduleProperties, "User Records")
                        mainProperties = self.dataAccessor().findProperties(moduleProperties, "Main")

                    xPos = 0
                    yPos = 0
                    for entry in userRecords:
                        if "name" in entry:
                            if entry["name"] == "xPos":
                                xPos = entry["value"]
                            elif entry["name"] == "yPos":
                                yPos = entry["value"]
                    pos = QPoint(xPos, yPos)
                    widget = self.workspace().addModuleWidget(moduleType, pos, not firstModule) #(moduleType, motherPos)     #pxlModule = widget.dataObject()

                    for entry in mainProperties:
                        if "name" in entry:
                            if entry["name"] == "Name":
                                newModuleName = entry["value"]

                                #try to find an index if it is there ...
                                index = self.getIndexFromText(newModuleName)
                                if index == None:
                                    index = 1
                                else:
                                    newModuleName = self.getTextWithoutIndex(newModuleName, str(index))

                                #rename module if necessary
                                if not (newModuleName == moduleType):
                                    if not (newModuleName in self._widgetCounter):
                                        self._widgetCounter[newModuleName] = index
                                    else:
                                        self._widgetCounter[newModuleName] += 1

                                    self.dataAccessor().setName(widget.dataObject(), newModuleName + str(self._widgetCounter[newModuleName]))

                    for entry in userRecords:
                        if "name" in entry:
                            #do not overwrite new coordinates
                            if entry["name"] == "xPos":
                                pass
                            elif entry["name"] == "yPos":
                                pass
                            else:
                                self.dataAccessor().setProperty(widget.dataObject(), entry["name"], entry["value"], "User Records")

                    widget.setOptions(moduleOptions)
                    widget.loadPxlPorts() # update ports
                    widget.select(True, True) # update property view
                    widget.width()  # calls rearrangeContent(), needed for correct resizing

                    firstModule = False

    def mimeData(self, items):
        """ Returns QMimeData for drag and drop or copy&paste.
        """
        logging.debug(self.__class__.__name__ + ": mimeData()")
        mime = QMimeData()
        encodedData = QByteArray()

        for item in items:
            encodedData.append(item.text(0))
        mime.setData(self.mimeType(), encodedData)
        return mime

    def createModuleWidget(self, pxlModule):
        """ Creates new an instance of ModuleWidget and sets its data object to pxlModule.
        """
        logging.debug("AnalysisDesignerTab: createModuleWidget()")

        xPos = self.dataAccessor().moduleXPos(pxlModule)
        yPos = self.dataAccessor().moduleYPos(pxlModule)

        moduleWidget = ModuleWidget(self.workspace())
        moduleWidget.setTab(self)
        moduleWidget.setDataObject(pxlModule)
        moduleWidget.setModuleController(self.plugin().createModuleController(moduleWidget))
        moduleWidget.setTitle(self.dataAccessor().name(pxlModule))
        moduleWidget.show()
        if xPos != None and yPos != None:
            moduleWidget.move(xPos, yPos)
        self.dataAccessor().updateModulePos(pxlModule, moduleWidget.pos())
        self.connect(moduleWidget, QtCore.SIGNAL("deleteButtonPressed"), self.moduleWidgetDeleteButtonPressedSlot)
        self.connect(moduleWidget, QtCore.SIGNAL("widgetDeleted"), self.moduleWidgetDeletedSlot)
        self.connect(moduleWidget, QtCore.SIGNAL("dragFinished"), self.moduleWidgetDragFinishedSlot)
        self.connect(moduleWidget, QtCore.SIGNAL("widgetCopyToClipboard"), self.moduleWidgetCopyToClipboardSlot)
        self.connect(moduleWidget, QtCore.SIGNAL("widgetCopyToClipboardAPPEND"), self.moduleWidgetCopyToClipboardAppendSlot)

        #self.setModified()    # not here, this function is also used in cases where this is unwanted
        return moduleWidget

    def moduleWidgetDeleteButtonPressedSlot(self):
        logging.debug("%s: moduleWidgetDeleteButtonPressedSlot()" % self.__class__.__name__)
        moduleWidget = self.sender()
        if isinstance(moduleWidget, ModuleWidget):
            self.clearPropertyView()
            self.showAnalysisInformation()
            self.addUndoEvent(UndoDeleteModuleEvent(moduleWidget))
            self.setModified()

    def moduleWidgetDeletedSlot(self):
        logging.debug("%s: moduleWidgetDeletedSlot()" % self.__class__.__name__)
        moduleWidget = self.sender()
        if isinstance(moduleWidget, ModuleWidget):
            self.removePxlModule(moduleWidget.dataObject())

    def moduleWidgetCopyToClipboardSlot(self):
        logging.debug("%s: moduleWidgetCopyToClipboardSlot()" % self.__class__.__name__)
        widget = self.sender()
        if isinstance(widget, ModuleWidget):
            self.copyPxlObjectToClipboard(False, widget.dataObject())

    def moduleWidgetCopyToClipboardAppendSlot(self):
        logging.debug("%s: moduleWidgetCopyToClipboardAppendSlot()" % self.__class__.__name__)
        moduleWidget = self.sender()
        if isinstance(moduleWidget, ModuleWidget):
            self.copyPxlObjectToClipboard(True, moduleWidget.dataObject())

    def createPxlConnection(self, connection):
        sourcePort = connection.sourcePort()
        sinkPort = connection.sinkPort()
        sourceModuleName = self.dataAccessor().name(sourcePort.parent().dataObject())
        sinkModuleName = self.dataAccessor().name(sinkPort.parent().dataObject())
        return self.dataAccessor().connectModules(self._pxlAnalysis, sourceModuleName, sourcePort.name(), sinkModuleName, sinkPort.name())

    def removePxlConnection(self, connection):
        """ Removes the pxl connection from pxl analysis that is associated with given PortConnection object.
        """
        sourcePort = connection.sourcePort()
        sinkPort = connection.sinkPort()
        sourceModuleName = self.dataAccessor().name(sourcePort.parent().dataObject())
        sinkModuleName = self.dataAccessor().name(sinkPort.parent().dataObject())
        return self.dataAccessor().disconnectModules(self._pxlAnalysis, sourceModuleName, sourcePort.name(), sinkModuleName, sinkPort.name())

    def createPortConnection(self, port1, port2):
        """ Creates connection between the two provided PortWidgets.
        """
        logging.debug(__name__ + ": createPortConnection()")
        if port1 == port2:
            return None

        if isinstance(port1, SourcePort) and isinstance(port2, SinkPort):
            sourcePort = port1
            sinkPort = port2
        elif isinstance(port1, SinkPort) and isinstance(port2, SourcePort):
            sourcePort = port2
            sinkPort = port1
        else:
            return None

        existingConnection = self.workspace().portConnection(sourcePort)
        if existingConnection:
            messageResult = self.showMessageBox("Do you want to replace the existing connection with the new one?",
                                                                       "Multiple connections to one source are not supported.",
                                                                       QtGui.QMessageBox.Yes | QtGui.QMessageBox.No,
                                                                       QtGui.QMessageBox.Yes)
            if messageResult != QtGui.QMessageBox.Yes:
                return None
            existingConnection.delete()

        connection = PortConnection(self.workspace(), sourcePort, sinkPort)
        connection.show()
        self.connect(connection, QtCore.SIGNAL("connectionDeleted"), self.connectionDeletedSlot)
        self.connect(connection, QtCore.SIGNAL("deleteButtonPressed"), self.connectionDeleteButtonPressedSlot)
        self.connect(connection, QtCore.SIGNAL("connectionCopyToClipboard"), self.connectionWidgetCopyToClipboardSlot)
        self.connect(connection, QtCore.SIGNAL("connectionCopyToClipboardAPPEND"), self.connectionWidgetCopyToClipboardAppendSlot)
        #self.setModified()    # not here, this function is also used in cases where this is unwanted
        return connection

    def connectionDeleteButtonPressedSlot(self):
        logging.debug("%s: connectionDeleteButtonPressedSlot()" % self.__class__.__name__)
        connection = self.sender()
        if isinstance(connection, PortConnection):
            self.addUndoEvent(UndoDisconnectModulesEvent(connection))
            self.setModified()

    def connectionDeletedSlot(self):
        logging.debug("%s: connectionDeletedSlot()" % self.__class__.__name__)
        connection = self.sender()
        if isinstance(connection, PortConnection):
            self.removePxlConnection(connection)

    def connectionWidgetCopyToClipboardSlot(self):
        logging.debug("%s: connectionWidgetCopyToClipboardSlot()" % self.__class__.__name__)
        widget = self.sender()
        if isinstance(widget, PortConnection):
            self.copyConnectionToClipboard(False, widget)

    def connectionWidgetCopyToClipboardAppendSlot(self):
        logging.debug("%s: connectionWidgetCopyToClipboardAppendSlot()" % self.__class__.__name__)
        widget = self.sender()
        if isinstance(widget, PortConnection):
            self.copyConnectionToClipboard(True, widget)

    #copies a connection between pxl modules to the qt clipboard 
    def copyConnectionToClipboard(self, append, connection):
        logging.debug(self.__class__.__name__ + ": copyConnectionToClipboard()")

        if isinstance(connection, PortConnection):
            serialized_data = connection.serialize()
            if not append:
                mime = self.serializeForClipboard([serialized_data])
                guiFacade.clipboard().setMimeData(mime)
            else:
                clipboard_data = self.deserializeFromClipboard()
                clipboard_data.append(serialized_data)
                guiFacade.clipboard().setMimeData(self.serializeForClipboard(clipboard_data))

    def onSelected(self, dataObject):
        """ Sets selected object of tab's property view to widget.
        """
        if not self.propertyView().valueChangedSuccessful():
            return
        
        #logging.debug(self.__class__.__name__ +": onSelected()")
        if self.propertyView().dataObject() != dataObject:
            statusMessage = self.statusBarStartMessage("Updating property view")
            self.propertyView().setDataObject(dataObject)
            self.propertyView().updateContent()
            self.statusBarStopMessage(statusMessage)
        else:
            self.propertyView().updateContent()

    def clearPropertyView(self):
        """ Clears tab's property view.
        
        Provided for convenience
        """
        self.propertyView().clear()

    def propertyViewValueChanged(self, propertyName, newValue, oldValue, categoryName):
        logging.debug(self.__class__.__name__ + ": propertyViewValueChanged()")

        dataObject = self.propertyView().dataObject()
        if dataObject == self._pxlAnalysis:
            self.updateInformationTag()

        self.setModified()

        self.addUndoEvent(UndoChangePropertyViewValueEvent(self.workspace(), self.dataAccessor().name(self.propertyView().dataObject()), propertyName, newValue, oldValue, categoryName))

    def updateInformationTag(self):
        """ The information tag is displayed in the top-left corner of the AnalysisWorkspace.
        """
        text = ""
        if self.plugin().showInformationTagFlag():
            author = self.dataAccessor().author(self._pxlAnalysis)
            modDate = self.dataAccessor().modificationDate(self._pxlAnalysis)
            modTime = self.dataAccessor().modificationTime(self._pxlAnalysis)

            text = ""
            if author:
                text += "Created by <i>%s</i>." % author
            if modDate or modDate:
                if author:
                    text += " "
                text += "Last modified: <i>"
                if modDate:
                    text += modDate
                if modTime:
                    text += " (%s)" % modTime
                text += "</i>."

        #logging.debug("%s: updateInformationTag(): %s" % ( self.__class__.__name__, text))    
        self.workspace().setInformationTag(text)

    def selectAll(self):
        """ Selects all modules in workspace.
        """
        self.workspace().setFocus(QtCore.Qt.OtherFocusReason)
        for child in self.workspace().children():
            if isinstance(child, ModuleWidget) or isinstance(child, PortConnection):
                child.select(True, True)       # select, mulitSelect

    def find(self):
        """ Test code executed on find event.
        """
        logging.debug('find')
        return

    def saveImage(self, filename=None):
        """ Save screenshot of the center view to file.
        """
        self.workspace().hideMenuWidgets()
        self.workspace().deselectAllWidgets()
        self.workspace().exportImage(filename)

    def reloadFile(self):
        self.clearPropertyView()
        self.workspace().clear()
        self._pxlAnalysis.shutdown()
        self._pxlAnalysis = None
        self._pxlAnalysis = self._accessor.createAnalysis()
        SplitterTab.reloadFile(self)

    def updateContent(self):
        """ Done by AnalysisXmlFile
        """
        pass

    def toggleDesignExecutionMode(self, showFlag=True):
        if not showFlag and self._executionThread and self._executionThread.isRunning():
            self.stopExecution()

        # create execution view
        startExecution = self._autorunExecutionFlag
        if showFlag and not self.executionView():
            self.createExecutionView()
            self.verticalSplitter().setSizes(self._originalSizes) # why?
            startExecution = True
        if not self.executionView():
            # nothing to do if execution view has not yet been created
            return

        if startExecution and showFlag:
            self.updateToolBarSizes()
            self.startExecution()

    def switchToDesignMode(self):
        self.toggleDesignExecutionMode(False)
        self.minimizeExectionView()

    def switchToExecutionMode(self):
        """ Shows execution controls and output instead of property view.
        
        If this is the first this is called for a certain tab, the execution will be started automatically.
        """
        self.toggleDesignExecutionMode(True)

    def setExecutionButtonsChecked(self, minimizeButtonChecked, orignalButtonChecked, maximizeButtonChecked, verticalSplitterSizes):
        if self.originalButton().isChecked():
            self._originalSizes = self.verticalSplitter().sizes()
        self.minimizeButton().setChecked(minimizeButtonChecked)
        self.originalButton().setChecked(orignalButtonChecked)
        self.maximizeButton().setChecked(maximizeButtonChecked)
        self.verticalSplitter().setSizes(verticalSplitterSizes)

    def minimizeExectionView(self):
        self.setExecutionButtonsChecked(True, False, False, [100, 1, 0])

    def originalExectionView(self):
        self.setExecutionButtonsChecked(False, True, False, self._originalSizes)

    def maximizeExectionView(self):
        self.setExecutionButtonsChecked(False, False, True, [0, 1, 100])

    def autorunCheckBoxValueChangedSlot(self, checkState):
        self._autorunExecutionFlag = (checkState == QtCore.Qt.Checked)
        self._saveIni()

    def autorunExecutionEnabled(self):
        return self._autorunExecutionFlag

    def rememberAutorunSettingCheckBoxValueChanedSlot(self, checkState):
        self._rememberAutorunExecutionSettingFlag = (checkState == QtCore.Qt.Checked)
        self._saveIni()

    def rememberAutorunSettingEnabled(self):
        return self._rememberAutorunExecutionSettingFlag

    def startExecution(self):
        logging.debug(__name__ + ": startExecution()")
        self._analysisAbortedFlag = False
        if not self._executionThread:
            if self.isEditable() and self.isModified():
                msgBox = QtGui.QMessageBox(self.mainWindow())
                msgBox.setParent(self.mainWindow(), QtCore.Qt.Sheet)     # QtCore.Qt.Sheet: Indicates that the widget is a Macintosh sheet.
                msgBox.setText("The analysis has been modified.")
                msgBox.setInformativeText("Do you want to save your changes before execution?")
                msgBox.setStandardButtons(QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)
                msgBox.setDefaultButton(QtGui.QMessageBox.Yes)
                ret = msgBox.exec_()
                if ret == QtGui.QMessageBox.Yes:
                    self.save()
            self.executionOutputTextEdit().clear()
            self._analysisRunningMessageId = self.statusBarStartMessage("Running analysis")
            self._executionThread = AnalysisExecutionThread("%s -p \"%s\"" % (self.plugin().pxlrunPath(), self.filename()),
                self.__pxlLogLevelSelector.getLogLevel(), self)
            self.connect(self._executionThread, QtCore.SIGNAL('newStdOut(PyQt_PyObject)'), self.addStandardOutput)
            self.connect(self._executionThread, QtCore.SIGNAL('newStdErr(PyQt_PyObject)'), self.addErrorOutput)
            self.connect(self._executionThread, QtCore.SIGNAL('finished()'), self.executionFinished)
            self._executionThread.start()
            self.updateExecutionView()
        else:
            logging.debug(__name__ + ": startExecution(): Already running...")

    def executionFinished(self):
        logging.debug(__name__ + ": executionFinished()")
        if self._executionThread:
            if self._executionThread.aborted():
                self.statusBarStopMessage(self._analysisRunningMessageId, "aborted")
            else:
                self.statusBarStopMessage(self._analysisRunningMessageId)
            self._executionThread = None
            self.updateExecutionView()

    def analysisIsRunning(self):
        return self._executionThread and self._executionThread.isRunning()

    def analysisAborted(self):
        return self._analysisAbortedFlag

    def addStandardOutput(self, line):
        #self.executionOutputTextEdit().setTextColor(QtCore.Qt.black)
        self.executionOutputTextEdit().append("<font></font>"+line.replace("\n","<br>"))

    def addErrorOutput(self, line):
        #self.executionOutputTextEdit().setTextColor(QtCore.Qt.red)
        self.executionOutputTextEdit().append(line)

    def stopExecution(self):
        logging.debug(__name__ + ": stopExecution()")
        self._analysisAbortedFlag = True
        if self._executionThread:
            self._executionThread.stop()

    def setModified(self, modified=True):
        """ Overwrites Tabs function and also updates execution view if it exitsts and is visible.
        """
        SplitterTab.setModified(self, modified)
        if modified:
            self.updateExecutionView()

    def _saveIni(self):
        """ write options to ini
        """
        if not self._rememberAutorunExecutionSettingFlag:
            return

        logging.debug(__name__ + ": _saveIni")
        if not self.plugin():
            logging.waring(self.__class__.__name__ + ": _saveIni() - No plugin set. Aborting...")
            return
        ini = getPreferences()
        if not ini.has_section(self.plugin().INI_SECTION_NAME):
            ini.add_section(self.plugin().INI_SECTION_NAME)
        ini.set(self.plugin().INI_SECTION_NAME, "autorunAnalysis", self._autorunExecutionFlag)
        writePreferences()

    def _loadIni(self):
        """ read options from ini
        """
        logging.debug(__name__ + ": _loadIni")
        ini = getPreferences()
        if ini.has_option(self.plugin().INI_SECTION_NAME, "autorunAnalysis"):
            self._rememberAutorunExecutionSettingFlag = True
            self._autorunExecutionFlag = ini.getboolean(self.plugin().INI_SECTION_NAME, "autorunAnalysis")

    def scriptViewMenuButtonClickedSlot(self, point=None):
        popup = QtGui.QMenu(self.scriptLocationMenuButton())
        popup.addAction(self.plugin().selectScriptLocationAction())
        if not isinstance(point, QtCore.QPoint):
            point = self.scriptLocationMenuButton().mapToGlobal(QtCore.QPoint(self.scriptLocationMenuButton().width(), 0))
        popup.exec_(point)

    def createLayerActions(self):
        self._layerActions["add"] = guiFacade.createAction("Add layer", self.addLayer)
        self._layerActions["remove"] = guiFacade.createAction("Delete layer", self.removeLayer)

    def layerToolBoxFrameButtonClickedSlot(self, point=None):
        #popup=QtGui.QMenu(self.layerToolBoxFrameMenuButton())
        popup = QtGui.QMenu(self._layerToolBox)
        popup.addAction(self._layerActions["add"])
        popup.addAction(self._layerActions["remove"])
        if not isinstance(point, QtCore.QPoint):
            point = self.layerToolBoxFrameMenuButton().mapToGlobal(QtCore.QPoint(self.layerToolBoxFrameMenuButton().width(), 0))
        popup.exec_(point)

    def addLayer(self):
        selectedLayerItems = self.layerToolBox().selectedItems()
        if len(selectedLayerItems) > 0:
            if selectedLayerItems[0].text(0) == self.layerToolBox().BASE_LAYER_NAME:
                name = "new layer"
            else:
                name = "new sub-layer"
            # TODO: check whether layer name already exists
            self._pxlAnalysis.addLayer(name)    # need to add layer to analysis class first, activate layer is triggerd by adding item to layerToolBox
            layerItem = QtGui.QTreeWidgetItem(selectedLayerItems[0], [name])
            #layerItem.setFlags(QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
            selectedLayerItems[0].setExpanded(True)     # expand parent layer to show new layer
            self.editLayerName(layerItem)
        else:
            logging.error("%s.addLayer() - Did not find base layer. Aborting...")
            #self.layerToolBox().insertTopLevelItem(0, QtGui.QTreeWidgetItem(["new layer"]))

    def removeLayer(self):
        for item in self.layerToolBox().selectedItems():
            parent = item.parent()
            if parent:
                parent.removeChild(item)
                self.dataAccessor().removeLayer(self._pxlAnalysis, str(item.text(0)))
                self.showStatusBarMessage("Removed layer %s." % item.text(0))
            else:
                self.showStatusBarMessage("Cannot remove base layer.")
                # do not remove base layer:
                #self.layerToolBox().removeItemWidget(item, 0)

    def activateLayer(self, layerName):
        print "activateLayer() ", layerName
        self.dataAccessor().activateLayer(self._pxlAnalysis, layerName)

    def layerSelectionChangedSlot(self):
        selectedItems = self.layerToolBox().selectedItems()
        if len(selectedItems) > 0:
            self.activateLayer(str(selectedItems[0].text(0)))

    def editLayerName(self, layerItem):
        # TODO: remember old name in order to forward it to pxl, add signal on name change
        qModelIndex = self.layerToolBox().indexFromItem(layerItem)
        self.layerToolBox().setCurrentIndex(qModelIndex)
        self.layerToolBox().edit(qModelIndex)

    def showAnalysisInformation(self):
        self.onSelected(self._pxlAnalysis)

    def analysis(self):
        return self._pxlAnalysis

    def analysisPath(self):
        return self._analysisPath

    def loadSelectedModulePositionUserRecord(self):
        selectedWidgets = self.workspace().selectedWidgets()
        if len(selectedWidgets) < 1:
            return False
        widget = selectedWidgets[0]
        xPos = self.dataAccessor().moduleXPos(widget.dataObject())
        yPos = self.dataAccessor().moduleYPos(widget.dataObject())
        widget.move(xPos, yPos)

    def moduleWidgetDragFinishedSlot(self):
        widget = self.sender()
        if widget and widget.isSelected():
            self.dataAccessor().updateModulePos(widget.dataObject(), widget.pos())
            self.onSelected(widget.dataObject())



class fileSystemTreeView(QTreeView):
  """ A Tree View also Providing copy, move and delete for files"""
  def __init__(self):
    QTreeView.__init__(self)

    self.insertAction = QAction('Insert into analysis', self)
    self.copyAction = QAction('Copy', self)
    self.moveAction = QAction('Move', self)
    self.deleteAction = QAction('Delete', self)
    self.connect(self.insertAction, SIGNAL("triggered()"), self.mouseDoubleClickEvent)
    self.connect(self.copyAction, SIGNAL("triggered()"), self._fileCopy)
    self.connect(self.deleteAction, SIGNAL("triggered()"), self._fileDelete)
    self.connect(self.moveAction, SIGNAL("triggered()"), self._fileMove)

    self.contextMenu = QMenu()
    self.contextMenu.addAction(self.insertAction)
    self.contextMenu.addAction(self.copyAction)
    self.contextMenu.addAction(self.deleteAction)
    self.contextMenu.addAction(self.moveAction)
    self.connect(self, SIGNAL("customContextMenuRequested(const QPoint &)"), self.showMenu)

    self.setContextMenuPolicy(Qt.CustomContextMenu)
    self.setHeaderHidden(True)

  def showMenu(self, qpoint):
    self.currentindex = qpoint
    self.contextMenu.exec_(QCursor.pos())

  #def setModel(self.model()):
  #  QTreeView.setModel(self.model())
  #  self._model = model

  def mouseDoubleClickEvent(self, event=None):
      self.emit(SIGNAL("doubleClicked"))

  def _fileDelete(self):
    file = self.model().fileName(self.indexAt(self.currentindex))
    msgBox = QMessageBox(self)
    msgBox.setText('Delete ' + file + ' ?')
    msgBox.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
    msgBox.setDefaultButton(QMessageBox.Ok)
    ret = msgBox.exec_()
    if ret == QMessageBox.Ok:
      self.model().remove(self.indexAt(self.currentindex))
      #os.unlink(file)

  def _fileCopy(self):
    sourcefile = self.model().filePath(self.indexAt(self.currentindex))
    dialog = QFileDialog(self)
    dialog.setFileMode(QFileDialog.AnyFile)
    dialog.setNameFilter('Python Scripts (*.py)')
    #dialog.setText('Please Enter Target File Name')
    #lineEdit = QlineEdit(dialog)
    if not dialog.exec_():
      # exit if cancel
      return

    fileNames = dialog.selectedFiles()
    targetfile = fileNames.takeFirst()
    #check if target exists
    if os.path.exists(targetfile):
      msgBox = QMessageBox(self)
      msgBox.setText('Overwrite ' + targetfile + ' ?')
      msgBox.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
      msgBox.setDefaultButton(QMessageBox.Ok)
      ret = msgBox.exec_()
      if ret == QMessageBox.Cancel:
        return
    shutil.copy2(sourcefile, targetfile)

  def _fileMove(self):
    sourcefile = self.model().filePath(self.indexAt(self.currentindex))
    dialog = QFileDialog(self)
    dialog.setFileMode(QFileDialog.AnyFile)
    dialog.setNameFilter('Python Scripts (*.py)')
    if not dialog.exec_():
      # exit if cancel
      return

    fileNames = dialog.selectedFiles()
    targetfile = fileNames.takeFirst()
    #check if target exists
    if os.path.exists(targetfile):
      msgBox = QMessageBox(self)
      msgBox.setText('Overwrite ' + targetfile + ' ?')
      msgBox.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
      msgBox.setDefaultButton(QMessageBox.Ok)
      ret = msgBox.exec_()
      if ret == QMessageBox.Cancel:
        return
    shutil.move(sourcefile, targetfile)


class ScriptFilterDirModel(QDirModel):
    """ Directory model with one column containing file name.
    
    Files are read in application's thread, so this class may block while reading directories containing many files.
    """
    def __init__(self):
        """ Constructor.
        """
        QDirModel.__init__(self)
        self.setFilter(QDir.AllDirs | QDir.NoDotAndDotDot | QDir.AllEntries)

    def columnCount(self, index):
        """ Returns 1
        
        Only show file name column.
        """
        return 1

    def headerData(self, section, orientation, role):
        """ Show name for first column.
        """
        if orientation == Qt.Horizontal and section == 0:
            if role == Qt.DisplayRole:
                return QVariant("Python Scripts")
            else:
                # prevent empty space where icon would usually be
                # --> same look as QTreeWidget
                return QVariant()
        return QFileSystemModel.headerData(self, section, orientation, role)


[major, minor, revision] = str(qVersion()).split(".")
if int(major) > 4 or (int(major) == 4 and int(minor) >= 4):

  class ScriptFileSystemModel(QFileSystemModel):
        """ File system model with one column containing file name.
    
        Files are read in separate thread.
        Depends on Qt >= 4.4
        """
        def __init__(self):
            """ Constructor.
            """
            QFileSystemModel.__init__(self)
            self.setReadOnly(False)
            self.setNameFilterDisables(False)    # dont show files not matching name filters
        def columnCount(self, index):
            """ Returns 1
        
            Only show file name column.
            """
            return 1

#        def headerData(self, section, orientation, role):
#            """ Show name for first column.
#            """
#            if orientation == Qt.Horizontal and section == 0:
#                if role == Qt.DisplayRole:
#                    return QVariant("Python Scripts")
#                else:
#                    # prevent empty space where icon would usually be
#                    # --> same look as QTreeWidget
#                    return QVariant()
#            return QFileSystemModel.headerData(self, section, orientation, role)
del major, minor, revision

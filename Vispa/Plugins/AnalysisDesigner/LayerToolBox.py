from PyQt4.QtCore import SIGNAL, Qt
from PyQt4.QtGui import QTreeWidget, QTreeWidgetItem

class LayerToolBox(QTreeWidget):
    
    BASE_LAYER_NAME = "__base_layer__"
    
    def __init__(self, parent=None):
        QTreeWidget.__init__(self, parent)
        self.setHeaderHidden(True)
        self.setColumnCount(1)
        baseLayerItem = QTreeWidgetItem([self.BASE_LAYER_NAME])
        self.insertTopLevelItem(0, baseLayerItem)
        baseLayerItem.setSelected(True)
        
    def mousePressEvent(self, event):
        QTreeWidget.mousePressEvent(self, event)
        if event.button() == Qt.RightButton:
            self.emit(SIGNAL("rightMouseButtonClicked"), self.mapToGlobal(event.pos()))
        #if not self.itemAt(event.pos()):    
        #    self.clearSelection()
        
    def keyPressEvent(self, event):
        """ Calls delete() method if backspace or delete key is pressed when widget has focus.
        """
        QTreeWidget.keyPressEvent(self, event)
        if event.key() == Qt.Key_Backspace or event.key() == Qt.Key_Delete:
            self.emit(SIGNAL("deleteButtonPressed"))
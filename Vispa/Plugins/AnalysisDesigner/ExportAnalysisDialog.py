import os.path
from xml.dom.minidom import *
import tarfile
import tempfile
import logging

from PyQt4 import QtGui 
from PyQt4 import QtCore 

import signal

class ExportAnalysisDialog(QtGui.QDialog):
  '''Create a tarball of the analysis, with only relative paths - the
  tarbhall will include inputfiles and python scripts.'''
  
  TYPE_ATTRIBUTE_NAME = 'type'
  NAME_ATTRIBUTE_NAME = 'name'
  MODULE_TAG_NAME = 'module'
  OPTION_TAG_NAME = 'option'
  PYMODULES = ['PyAnalyse', 'PyDecide', 'PyModule', 'PySwitch' ]
  

  def __init__(self,parent,infilename):

    logging.debug(__name__ +": Start export dialog")
    QtGui.QDialog.__init__(self, parent)
    self.FILES = {}
    self.setFocusPolicy(QtCore.Qt.StrongFocus)
    self.setWindowFlags(QtCore.Qt.Window)
    self.setWindowTitle("Export Analysis")
    
    self._infilename = os.path.abspath(infilename)

    # the _prefix will be the subfolder iun the archive
    self._prefix = '.'.join(os.path.basename(infilename).split('.')[:-1])
    
    infile = open(self._infilename,'r')
    self.doc = parse(infile)
    infile.close()
    
    self._layout = QtGui.QVBoxLayout(self)
    self._txt = QtGui.QLabel('These files will be included in the archive. Uncheck items will not be included in the tarball.')
    self._layout.addWidget(self._txt)
    self._fileList = QtGui.QListWidget()
    self._fileList.setSelectionMode(0)
    self._layout.addWidget(self._fileList)
    self._cwidget = QtGui.QWidget()
    self._saveAsButton =QtGui.QPushButton('Save As...')
    self._cancelButton =QtGui.QPushButton('Cancel')
    self._hl = QtGui.QHBoxLayout(self._cwidget)
    self._hl.addWidget(self._saveAsButton)
    self._hl.addWidget(self._cancelButton)
    self.connect(self._cancelButton, QtCore.SIGNAL("clicked()"),self.close)
    self.connect(self._saveAsButton, QtCore.SIGNAL("clicked()"),self._createTar)
    self._layout.addWidget(self._cwidget)
    
    # get modules to write to archive
    moduleElements = self.doc.getElementsByTagName(self.MODULE_TAG_NAME)
    for moduleElement in moduleElements:
      
      moduleType = moduleElement.getAttribute(self.TYPE_ATTRIBUTE_NAME)
      if moduleType in self.PYMODULES:
        self._modPath(moduleElement,'PyScripts')
      elif moduleType == "File Input":
        self._modPath(moduleElement,'InputData')
      elif moduleType == "File Output":
        self._modPath(moduleElement,'')

    #create list 
    for key in self.FILES.keys():
      item = QtGui.QListWidgetItem(key)
      if os.path.isfile(key):
        #item.setBackgroundColor(QtGui.QColor(20,200,10))
        item.setFlags(QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsEnabled)
        item.setCheckState(2)
        self._fileList.addItem(item)

      else:
        #item.setBackgroundColor(QtGui.QColor(200,20,10))
        #pop the files for tarfile!
        #self.FILES.pop(key)
        f = item.font()
        f.setStrikeOut(True)
        item.setFont(f)
        item.setCheckState(0)
        item.setFlags(QtCore.Qt.NoItemFlags)
      self._fileList.addItem(item)
    #self.connect(self._fileList, QtCore.SIGNAL("itemDoubleClicked(QListWidgetItem*)"),self.selectDeselectItem)

  #def selectDeselectItem(self,item):
  #  itemname = str(item.text())
  #  if itemname in self.FILES.keys():
  #    item.setBackgroundColor(QtGui.QColor(240,230,10))
  #    self.FILES.pop(itemname)

  #  elif os.path.isfile(itemname):
  #    item.setBackgroundColor(QtGui.QColor(20,200,10))
  #    self.FILES[itemname] = itemname


  def _createTar(self):
    #create tmpfile for xml file
    for i in range(self._fileList.count()):
      item = self._fileList.item(i)
      if item.checkState() == 0:
        itemname = str(item.text().toAscii())
        self.FILES.pop(itemname)

    logging.debug(__name__ +": Create tar file")
    ofilename = os.path.join(tempfile.gettempdir(),os.path.basename(self._infilename))
    ofile = open(ofilename,'w')
    ofile.write(self.doc.toxml())
    ofile.close()
    self.FILES[ofilename] = os.path.basename(ofilename)

    dialog = QtGui.QFileDialog(self)
    dialog.setDefaultSuffix("tgz")
    dialog.setFileMode(QtGui.QFileDialog.AnyFile)
    dialog.setAcceptMode(QtGui.QFileDialog.AcceptOpen | QtGui.QFileDialog.AcceptSave)
    dialog.setNameFilter('gzipped tar archive files (*.tgz)')
    
    if not dialog.exec_():
        # exit if cancel
        self.close()
        return

    fileNames = dialog.selectedFiles()
    targetfile = fileNames.takeFirst()
    
    #check if target exists
    # this is already done by QFileDialog (Mac OS X) -> True for all platforms?
#    if os.path.exists(targetfile):
#      msgBox= QtGui.QMessageBox(self)
#      msgBox.setText('Overwrite ' + targetfile + ' ?')
#      msgBox.setStandardButtons(QtGui.QMessageBox.Ok | QtGui.QMessageBox.Cancel)
#      msgBox.setDefaultButton(QtGui.QMessageBox.Ok)
#      ret = msgBox.exec_()
#      if ret == QtGui.QMessageBox.Cancel:
#        return

    t = tarfile.open(str(targetfile),'w:gz')
    for key in self.FILES:
      t.add(key,arcname=os.path.join(self._prefix,self.FILES[key]))
    t.close()

    #remove tmpfile
    os.unlink(ofilename)
    self.close()

  def _modPath(self,moduleElement,targetFolder):
    ''' Searches for optionelements named filename, add the files to
    self.FILES and change path in xml file 
    '''
    optionElements = moduleElement.getElementsByTagName(self.OPTION_TAG_NAME)
    for optionElement in optionElements:
      if optionElement.getAttribute('name') == 'filename':
        for node in optionElement.childNodes:
          if node.nodeType == Node.CDATA_SECTION_NODE:
            #make shure, i get always full path of files
            value = os.path.join(os.path.abspath(os.path.dirname(self._infilename)),node.nodeValue)
            
            self.FILES[value] = os.path.join(targetFolder,os.path.basename(value))
            #change data in xml file
            node.nodeValue = self.FILES[value] 
            

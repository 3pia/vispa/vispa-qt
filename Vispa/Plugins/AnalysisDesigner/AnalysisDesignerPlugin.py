import os.path
import imp
import ConfigParser
from StringIO import StringIO
import logging
import inspect
import subprocess

from PyQt4.QtGui import *
from PyQt4.QtCore import *

from Vispa.Main.Preferences import preferencesDirectory, homeDirectory
import Vispa.Main.Preferences as Preferences
from Vispa.Main.PluginManager import pluginmanager

from Vispa.Main.VispaPlugin import VispaPlugin
from Vispa.Main.Filetype import Filetype
from Vispa.Main.Exceptions import PluginNotLoadedException, exception_traceback, NoCurrentTabException
from Vispa.Main.GuiFacade import guiFacade
from Vispa.Plugins.AnalysisDesigner.AnalysisDesignerTab import AnalysisDesignerTab
from Vispa.Plugins.AnalysisDesigner.ExportAnalysisDialog import ExportAnalysisDialog
from Vispa.Plugins.AnalysisDesigner.AnalysisDataAccessor import AnalysisDataAccessor
from Vispa.Plugins.AnalysisDesigner.ModuleController import *
from Vispa.Plugins.AnalysisDesigner.CreateModuleDialog import CreateModuleDialog

from Vispa.Main.Preferences import find_in_path


try:
    import pxl.modules
    import pxl.core
except Exception:
    raise PluginNotLoadedException("cannot import pxl modules: " + exception_traceback())


DEFAULT_MODULE_CONTROLLER_CONFIGURATION = """
[File Input]
show_port_lines = False
show_port_names = False
icon_file = :/resources/module_in.svg

[File Output]
show_port_lines = False
show_port_names = False
icon_file = :/resources/module_out.svg

[PyAnalyse]
show_port_lines = True
show_port_names = True
icon_file = :/resources/module_analyse.svg

[PyDecide]
show_port_lines = True
show_port_names = True
icon_file = :/resources/module_if.svg

[PyGenerator]
show_port_lines = False
show_port_names = False
icon_file = :/resources/module_python.svg

[PyModule]
show_port_lines = True
show_port_names = True
icon_file = :/resources/module_python.svg

[PySwitch]
show_port_lines = True
show_port_names = True
icon_file = :/resources/module_switch.svg
"""



class AnalysisDesignerPlugin(VispaPlugin):
    """ MainWindow of AnalysisDesigner """

    INI_SECTION_NAME = "Analysis Designer"

    PXL_ERROR_MESSAGE = """Could not load pxl plugin system. You won't be able to edit pxl specific options.\n
Although you can open analysis files and you have limited edit functionality it is not recommend to save changes.\n
For details see error output or log file."""


    def __init__(self, name=None, fl=0):
        """ constructor """
        logging.debug('AnalysisDesignerPlugin: __init__')

        VispaPlugin.__init__(self)

        self._pxlErrorMsg = None
        self._pxlModuleFactory = None
        self._availablePxlModules = []
        self._moduleControllers = {}
        self._moduleControllerConfigFileName = os.path.abspath(os.path.join(preferencesDirectory, "ad_module-controllers.ini"))
        self._moduleControllerConfigParser = None
        self._showInformationTagFlag = True
        self._displayGridFlag = False

        self.registerFiletypesFromTab(AnalysisDesignerTab)
        self.addNewFileAction("&New physics analysis", self.newAnalysisDesignerTab)
        guiFacade.createStartupScreenEntry(prototypingActions=self._createNewFileActions, executionFiletypes=self.filetypes())
        self._startUp = True
        self._createModuleDialog = None
        self._settingsDialog = None
        self._exportAnalysisDialog = None
        self._selectScriptLocationAction = guiFacade.createAction("Select script location...", self.showSelectScriptLocationDialog)

        Preferences.addSetting(self.INI_SECTION_NAME, "Script-location", homeDirectory, "Default location for python scripts")
        Preferences.addSetting(self.INI_SECTION_NAME, "Show information-tag", True, "Show information about the analysis")
        pxlrun_path = ""
        pxlrun_paths = find_in_path("pxlrun")
        if len(pxlrun_paths) > 0:
            pxlrun_path = pxlrun_paths[0]
        Preferences.addSetting(self.INI_SECTION_NAME, "Pxlrun-path", pxlrun_path, "Path to pxlrun to use for analysis execution", self.__updatePxlrunPath)

        self._scriptLocation = Preferences.getSetting(self.INI_SECTION_NAME, "Script-location")

        self._showInformationTagFlag = Preferences.getSetting(self.INI_SECTION_NAME, "Show information-tag")

        self.setPxlrunPath(Preferences.getSetting(self.INI_SECTION_NAME,
          "Pxlrun-path"))


    def startUp(self):
        self._startUp = False
        self._viewMenu = guiFacade.createPluginMenu('&View')
        self._analysisDesignerMenu = guiFacade.createPluginMenu('&Analysis Designer')
        self._analysisToolBar = guiFacade.createPluginToolBar('Analysis ToolBar')
        self._fillMenu()
        self._initializePxlModuleFactory()
        self._loadModuleControllers()
        self._loadModuleControllerConfiguration()

    def pxlErrorMessage(self):
        """ Returns pxl error message.
        
        The message initially is None but is set to if loading pxl modules did not work.
        """
        return self._pxlErrorMsg

    def resetPxlErrorMessage(self):
        """ Resets pxl error message to None.
        
        The error message is only shown once and afterwards reset.
        """
        self._pxlErrorMsg = None

    def analysisDesignerMenu(self):
        """ Returns analysis designer menu.
        """
        return self._analysisDesignerMenu

    def analysisToolBar(self):
        """ Returns analysis toolbar.
        """
        return self._analysisToolBar

    def viewMenu(self):
        """ Returns view menu.
        """
        return self._viewMenu

    def _loadModuleControllers(self):
        controllers = __import__("ModuleController", globals(), locals())
        for c in dir(controllers):
            if c.endswith("ModuleController"):
                if globals()[c].MODULE_NAME != "":
                    name = globals()[c].MODULE_NAME
                else:
                    name = c
                self._moduleControllers[name] = globals()[c]

    def createModuleController(self, moduleWidget):
        """ Creates and returns a module controller for the given moduleType.
        
        If there no specific module controller for this type a PxlModuleController is created.
        """
        moduleType = moduleWidget.type()
        if moduleType in self._moduleControllers.keys():
            controllerName = moduleType
        else:
            controllerName = "PxlModuleController"

        controller = self._moduleControllers[controllerName](moduleWidget, self)
        return controller

    def _loadModuleControllerConfiguration(self):
        self._moduleControllerConfigParser = ConfigParser.ConfigParser()
        self._moduleControllerConfigParser.readfp(StringIO(DEFAULT_MODULE_CONTROLLER_CONFIGURATION))
        self._moduleControllerConfigParser.read(self._moduleControllerConfigFileName)

        configFileModules = self._moduleControllerConfigParser.sections()
        for moduleName in configFileModules:
            if self._moduleControllerConfigParser.has_option(moduleName, "controller"):
                desiredControllerName = self._moduleControllerConfigParser.get(moduleName, "controller")
                for name, controller in self._moduleControllers.iteritems():
                    if desiredControllerName == controller.__name__:
                        self._moduleControllers[moduleName] = controller
                        break

    def moduleControllerConfigParser(self):
        return self._moduleControllerConfigParser

    def scriptFileOptions(self, filename):
        """ Returns options set in a python script given as filename.
        
        If there is no such controller None is returned.
        """
        logging.debug(self.__class__.__name__ + ": scriptFileOptions()")
        pythonModule = os.path.splitext(os.path.basename(filename))[0]
        try:
            script = imp.load_source(pythonModule, filename)
        except ImportError:
            logging.warning(__name__ + ": scriptFileOptions(): Could not analyse python script " + str(pythonModule) + ":" + exception_traceback())
            return []
        options = []
        for variable in dir(script):
            value = getattr(script, variable)
            if not variable.startswith("_") and\
               not inspect.isfunction(value) and\
               not inspect.ismodule(value) and\
               not inspect.isclass(value) and\
               not inspect.isbuiltin(value) and\
               not inspect.ismethod(value):
                options += [(variable, value)]
        return options

    def moduleTypeByFile(self, filename):
        """ Returns name of module controller which can deal with given filename.
        
        If there is no such controller None is returned.
        """
        logging.debug(self.__class__.__name__ + ": moduleTypeByFile()")
        base = os.path.basename(str(filename))
        pythonModule, rawExt = os.path.splitext(base)
        ext = rawExt.lower().strip(".")

        if ext == "py":
            script = None
            try:
                script = imp.load_source(pythonModule, filename)
            except ImportError:
                logging.warning(__name__ + ": moduleTypeByFile(): Could not import python script %s: %s" % (str(pythonModule), exception_traceback()))
            except SyntaxError:
                logging.warning(__name__ + ": moduleTypeByFile(): Could not analyse python script %s: %s" % (str(pythonModule), exception_traceback()))

            if script:
                for name, controller in self._moduleControllers.iteritems():
                    if controller.supportsScript(script):
                        return name
        else:
            for name, controller in self._moduleControllers.iteritems():
                if controller.supportsFile(filename):
                    return name
        return None

    def pxlrunPath(self):
        return self._pxlrunPath

    def setPxlrunPath(self, path):
        self._pxlrunPath = path
        p = subprocess.Popen([self._pxlrunPath, '--getDataPath' ], stdout=subprocess.PIPE)
        p.wait()
        if p.returncode == 0:
          self.__pxlDataPath = p.stdout.readlines()[-1].replace("\n", "")

    def getPxlDataPath(self):
      return self.__pxlDataPath

    def __updatePxlrunPath(self):
        newPath = Preferences.getSetting(self.INI_SECTION_NAME, "Pxlrun-path")
        logging.debug("%s : __updatePxlrunPath() to '%s'" % (self.__class__.__name__, newPath))
        self.setPxlrunPath(newPath)

    def _fillMenu(self):
        # view menu
        displayInformationTagAction = QtGui.QAction("Display information tag", self.viewMenu())
        displayInformationTagAction.setToolTip("Display an information string in top-left corner of Analysis Designer window, including information of author.")
        displayInformationTagAction.setCheckable(True)
        displayInformationTagAction.setChecked(self._showInformationTagFlag)
        self.connect(displayInformationTagAction, QtCore.SIGNAL("toggled(bool)"), self.setShowInformationTagFlag)
        self.viewMenu().addAction(displayInformationTagAction)

        displayGridAction = QtGui.QAction("Display orientation grid", self.viewMenu())
        displayGridAction.setCheckable(True)
        displayGridAction.setChecked(self._displayGridFlag)
        self.connect(displayGridAction, QtCore.SIGNAL("toggled(bool)"), self.setDisplayGridFlag)
        self.viewMenu().addAction(displayGridAction)

        saveImageAction = guiFacade.createAction('&Save image...', self.saveImage, "Ctrl+I")
        self.viewMenu().addAction(saveImageAction)
        zoomAction = guiFacade.createAction('&Zoom...', self.zoomDialog, "Ctrl+Shift+Z")
        self.viewMenu().addAction(zoomAction)

        # analysis menu
        showAnalysisInformationAction = guiFacade.createAction('Show analysis information', self.showAnalysisInformation, image="information")
        self._analysisDesignerMenu.addAction(showAnalysisInformationAction)
        self._analysisToolBar.addAction(showAnalysisInformationAction)

        exportAction = guiFacade.createAction('Export analysis...', self.callExportDialog)
        self._analysisDesignerMenu.addAction(exportAction)

        # feature not yet implemented
        #createModuleDialogAction = guiFacade.createAction("Create module...", self.showCreateModuleDialog)
        #self._analysisDesignerMenu.addAction(createModuleDialogAction)

        runAction = guiFacade.createAction('Run analysis...', self.switchToExecutionMode, "F2", image='runanalysis')
        self._analysisDesignerMenu.addAction(runAction)
        self._analysisToolBar.addAction(runAction)

        # Check for batch System
        pluginmanager.initializePlugin("BatchSystemPlugin")
        batchSystemPlugin = pluginmanager.plugin("BatchSystemPlugin")
        if batchSystemPlugin != None:
            createBatchJobFromAnalysisAction = guiFacade.createAction('Create Batch Job From Analysis',
                self.__createBatchJobFromAnalysis, image='stack-new')
            self._analysisToolBar.addAction(createBatchJobFromAnalysisAction)
            self._analysisDesignerMenu.addAction(createBatchJobFromAnalysisAction)



    def __createBatchJobFromAnalysis(self):
        analysisTab = guiFacade.currentTab()
        allowCreation = True
        if analysisTab.isModified():
          allowCreation = self.__modifiedSaveConfirmation(analysisTab)

        if allowCreation:
          analysisDataAccessor = analysisTab.dataAccessor()
          designerTab = pluginmanager.plugin("BatchSystemPlugin").newBatchJobDesignerTab()
          #find pxlrun in path
          cl = Preferences.getSetting(self.INI_SECTION_NAME, "Pxlrun-path")

          designerTab.setCommand(cl)
          for module in analysisDataAccessor.modules(analysisTab.analysis()):
              for option in analysisDataAccessor.optionDescriptions(module):
                  value = analysisDataAccessor.option(module, option)
                  designerTab.addOption(module.getName().replace(" ", "\ "), module.getName().replace(" ", "\ ") + "." + option.name.replace(" ", "\ "),
                          '--set=' + module.getName().replace(" ", "\ ") + "." + option.name.replace(" ", "\ ") + '=',
                      str(value))
          designerTab.addOption("pxlrun", "XML File", "", analysisTab.filename())

    def _initializePxlModuleFactory(self):
        try:
            self._pxlModuleFactory = pxl.modules.ModuleFactory.instance()    # from python pxlplugin module
            pxl.core.PluginManager.instance().loadPlugins()
            self._availablePxlModules = self._pxlModuleFactory.getAvailableModules()
            builtin_classes = FileModuleController.__subclasses__() + PythonModuleController.__subclasses__()
            builtin_names = [cls.MODULE_NAME for cls in builtin_classes]
            external_names = [name for name in self._availablePxlModules if name not in builtin_names]
            external_names = sorted(external_names)
            self._availablePxlModules = tuple(builtin_names + external_names)
        except Exception:
            logging.error(__name__ + ": _initializePxlPluginManager(): " + exception_traceback())
            self._pxlErrorMsg = self.PXL_ERROR_MESSAGE

#    def pxlModuleFactory(self):
#        """ Returns pxl plugin manager.
#        """
#        return self._pxlModuleFactory

    def availablePxlModules(self):
        """ Returns list with names of available pxl modules.
        """
        return self._availablePxlModules

    def pxlModuleAvailable(self, moduleName):
        """ Checks if pxl module with name moduleName available.
        """
        return moduleName in self._availablePxlModules

    def setShowInformationTagFlag(self, checked):
        """
        """
        self._showInformationTagFlag = checked
        #self._saveIni()
        try:
            guiFacade.currentTab().updateInformationTag()
        except NoCurrentTabException:
            logging.warning(self.__class__.__name__ + ": setShowInformationTagFlag() - No tab controller found.")

    def showInformationTagFlag(self):
        return self._showInformationTagFlag

    def setDisplayGridFlag(self, checked):
        """
        """
        self._displayGridFlag = checked
        #self._saveIni()
        try:
            guiFacade.currentTab().workspace().setDisplayGridFlag(checked)
        except NoCurrentTabException:
            logging.warning(self.__class__.__name__ + ": setShowInformationTagFlag() - No tab controller found.")

    def displayGridFlag(self):
        return self._displayGridFlag

    def saveImage(self):
        """ Calls saveImage() function of current tab controller.
        """
        try:
            guiFacade.currentTab().saveImage()
        except NoCurrentTabException:
            logging.warning(self.__class__.__name__ + ": saveImage() - No tab controller found.")

    def zoomDialog(self):
        """ Calls zoomDialog() function of current tab controller.
        """
        try:
            guiFacade.currentTab().zoomDialog()
        except NoCurrentTabException:
            logging.warning(self.__class__.__name__ + ": zoomDialog() - No tab controller found.")

    def callExportDialog(self):
        tc = guiFacade.currentTab()

        allowExport = True
        if tc.isModified():
            msgBox = QMessageBox(tc.tab().mainWindow())
            msgBox.setParent(tc.tab().mainWindow(), Qt.Sheet)     # Qt.Sheet: Indicates that the widget is a Macintosh sheet.
            msgBox.setText("The document has been modified.")
            msgBox.setInformativeText("Do you want to save your changes?")
            msgBox.setStandardButtons(QMessageBox.Save | QMessageBox.Discard | QMessageBox.Cancel)
            msgBox.setDefaultButton(QMessageBox.Save)
            ret = msgBox.exec_()
            if ret == QMessageBox.Save:
                allowExport = True
                tc.save()
            elif ret == QMessageBox.Cancel:
                allowExport = False

        if not tc.filename():
          allowExport = tc.save()

        if allowExport:
          if not self._exportAnalysisDialog:
            self._exportAnalysisDialog = guiFacade.showDialog(ExportAnalysisDialog, tc.filename())
          else:
            guiFacade.showDialog(self._exportAnalysisDialog)

    def showAnalysisInformation(self):
        """ Shows analysis meta information in property view.
        """
        tc = guiFacade.currentTab()
        if not isinstance(tc, AnalysisDesignerTab):
            logging.waring(self.__class__.__name__ + ": showAnalysisInformation() - Can only show analysis information if analysis designer tab is activated. Aborting...")
            return
        tc.showAnalysisInformation()

    def switchToExecutionMode(self):
        """ Shows execution controls and output instead of property view by calling tab controller's switchToExecutionMode() function.
        """
        tc = guiFacade.currentTab()
        if not isinstance(tc, AnalysisDesignerTab):
            logging.waring(self.__class__.__name__ + ": switchToExecutionMode() - Can only switch to execution mode if analysis designer tab is activated. Aborting...")
            return
        tc.switchToExecutionMode()

    def __modifiedSaveConfirmation(self, tab):
        msgBox = QMessageBox(tab.mainWindow())
        msgBox.setParent(tab.mainWindow(), Qt.Sheet)     # Qt.Sheet: Indicates that the widget is a Macintosh sheet.
        msgBox.setText("The document has been modified.")
        msgBox.setInformativeText("Do you want to save your changes?")
        msgBox.setStandardButtons(QMessageBox.Save | QMessageBox.Discard | QMessageBox.Cancel)
        msgBox.setDefaultButton(QMessageBox.Save)
        ret = msgBox.exec_()
        if ret == QMessageBox.Save:
            tab.save()
            return True
        elif ret == QMessageBox.Cancel:
            return False
        elif ret == QMessageBox.Discard:
          return True

    def openFile(self, filename=""):
        """  Call newAnalysisDesignerTab() with filename as argument.
        
        Also makes sure file extension is 'xml'.
        """
        logging.debug("AnalysisDesignerPlugin: openFile()")
        if self._startUp:
            self.startUp()
        base = os.path.basename(str(filename))
        ext = os.path.splitext(base)[1].lower().strip(".")
        if ext == "xml":
            return self.newAnalysisDesignerTab(filename)
        return False

    def newAnalysisDesignerTab(self, filename=None):
        """ Creates new analysis designer tab and adds it to tabWidget of main window.
        """
        logging.debug(self.__class__.__name__ + ": newAnalysisDesignerTab()")
        if self._startUp:
            self.startUp()
        if not filename:
            statusMessage = guiFacade.statusBarStartMessage("Creating new analysis")
        tab = AnalysisDesignerTab(self)
        tab.setDataAccessor(AnalysisDataAccessor(self))
        guiFacade.addTab(tab)

        if filename:
            return tab.open(filename)
        else:
            guiFacade.statusBarStopMessage(statusMessage)
            return True

    def showCreateModuleDialog(self):
        if not self._createModuleDialog:
            self._createModuleDialog = guiFacade.showDialog(CreateModuleDialog)
        else:
            guiFacade.showDialog(self._createModuleDialog)

    def showSettingsDialog(self):
        if not self._settingsDialog:
            self._settingsDialog = guiFacade.showDialog(SettingsDialog)
            self.connect(self._settingsDialog, SIGNAL("pxlrunpath"), self._pxlrunpathSlot)
            self._settingsDialog.setPxlrunPath(self._pxlrunPath)
        else:
            guiFacade.showDialog(self._settingsDialog)

    def _pxlrunpathSlot(self, path):
        self._pxlrunPath = path
        #self._saveIni()

    def selectScriptLocationAction(self):
        return self._selectScriptLocationAction

    def showSelectScriptLocationDialog(self):
        directory = guiFacade.showDirectoryOpenDialog(self._scriptLocation)
        if os.path.isdir(directory):
            self._scriptLocation = directory
            #self._saveIni()
            controller = guiFacade.currentTab()
            if isinstance(controller, AnalysisDesignerTab):
                controller.updateScriptLocation()

    def scriptLocation(self):
        return self._scriptLocation

import logging

from PyQt4.QtCore import SIGNAL,Qt, QPoint
from PyQt4.QtGui import QFrame,QHeaderView,QToolButton,QStandardItemModel,QVBoxLayout,QSizePolicy,QWidget,QMenu

from Vispa.Main.SplitterTab import SplitterTab
from Vispa.Main import Preferences 
from Vispa.Gui.Header import FrameWithHeader
from Vispa.Gui.ZoomableScrollArea import ZoomableScrollArea
from Vispa.Gui.ZoomableScrollableWidgetOwner import ZoomableScrollableWidgetOwner
from Vispa.Gui.Zoomable import Zoomable
from Vispa.Gui.FindDialog import FindDialog
from Vispa.Gui.BoxContentDialog import BoxContentDialog
from Vispa.Gui.Zoomable import Zoomable
from Vispa.Share.FindAlgorithm import FindAlgorithm
from Vispa.Views.AbstractView import NoneView
from Vispa.Views.TreeView import TreeView
from Vispa.Views.AbstractView import AbstractView

from Vispa.Main.GuiFacade import guiFacade

class BrowserTab(SplitterTab):
    """ The BrowserTab has three views and is controlled by the BrowserControllerTab.
    """
    def __init__(self, plugin, parent=None, topLevelPropertyView=False):
        logging.debug(__name__ + ": __init__")
        SplitterTab.__init__(self, plugin, parent, topLevelPropertyView)

        self._treeView = None
        self._centerView = None

        self.createTreeView()
        self.createCenterView()
        self.createPropertyView()
        if topLevelPropertyView:
            self.horizontalSplitter().setSizes([300, 400])
            self.setSizes([700, 300])
        else:
            self.horizontalSplitter().setSizes([300, 400, 300])

        self._dataAccessor = None

        self.setFindEnabled()
        self._findAlgorithm = None
        self._findDialog = None
        self._filterAlgoritm = None
        self._filterDialog = None
        self._filterObjects = None
        self._boxContentDialog = BoxContentDialog(self)
        self.connect(self._boxContentDialog, SIGNAL("scriptChanged"), self.scriptChanged)

        if self._scrollArea:
            self.connect(self._scrollArea, SIGNAL('wheelZoom()'), self.resetZoomButtonPressedBefore)
            self.connect(self._scrollArea, SIGNAL("zoomChanged(float)"), self.zoomChanged)
            self.connect(self.centerViewMenuButton(), SIGNAL("clicked(bool)"), self.centerViewMenuButtonClicked)
            self.connect(self.treeViewMenuButton(), SIGNAL("clicked(bool)"), self.treeViewMenuButtonClicked)

        if not (isinstance(self.treeView(), AbstractView) and isinstance(self.treeView(), QWidget)):
            raise TypeError(__name__ + " requires a center view of type AbstractView and QWidget.")
        if not (isinstance(self.centerView(), AbstractView) and isinstance(self.centerView(), QWidget)):
            raise TypeError(__name__ + " requires a center view of type AbstractView and QWidget.")
        if not isinstance(self, BrowserTab):
            raise TypeError(__name__ + " requires a tab of type BrowserTab.")
        self.connect(self.treeView(), SIGNAL("selected"), self.onTreeViewSelected)
        self.connect(self.treeView(), SIGNAL("mouseRightPressed"), self.treeViewMenuButtonClicked)
        self.connect(self.centerView(), SIGNAL("selected"), self.onSelected)
        self.connect(self.centerView(), SIGNAL("mouseRightPressed"), self.centerViewMenuButtonClicked)
        self.connect(self.treeViewHeader(), SIGNAL("mouseRightPressed"), self.treeViewMenuButtonClicked)
        self.connect(self.centerViewHeader(), SIGNAL("mouseRightPressed"), self.centerViewMenuButtonClicked)
         
        self.loadIni()

    def createTreeView(self,parent=None):
        """ Create the tree view.
        """
        if not parent:
            parent=self.horizontalSplitter()
        
        self._treeviewArea = FrameWithHeader(parent)
        self._treeviewArea.header().setText("Tree View")
        self._treeviewArea.header().setToolTip("click on '>' for options of this view")
        self._treeViewMenuButton = self._treeviewArea.header().createMenuButton()
        self._treeView = TreeView(self._treeviewArea)
        self._treeviewArea.addWidget(self._treeView)
        
    def createCenterView(self,parent=None):
        """ Create the center view.
        """
        if not parent:
            parent=self.horizontalSplitter()
            
        self._centerArea = FrameWithHeader(parent)
        self._centerArea.header().setText("Center View")
        self._centerArea.header().setToolTip("click on '>' for options of this view")
        self._centerArea.header().createMenuButton()
        
        self._scrollArea=ZoomableScrollArea(self._centerArea)
        self._centerArea.addWidget(self._scrollArea)
        self.setCenterView(NoneView())
            
    def setCenterView(self,view):
        """ Set the center view.
        """
        logging.debug(self.__class__.__name__ +": setCenterView()")
        if self.centerView():
            self.centerView().close()
        self._scrollArea.takeWidget()
        self._centerView = view
        if isinstance(self.centerView(), ZoomableScrollableWidgetOwner):
            if isinstance(self.centerView(), Zoomable):
                self.centerView().setZoom(self._scrollArea.zoom())
            self._scrollArea.setWidget(self.centerView())
            self._scrollArea.show()
        else:
            self.centerView().resize(self._scrollArea.size())
            self._scrollArea.hide()
            self._centerArea.layout().addWidget(self.centerView())
            self.centerView().show()

    def treeView(self):
        return self._treeView
    
    def centerView(self):
        return self._centerView
    
    def scrollArea(self):
        return self._scrollArea
    
    def treeViewMenuButton(self):
        return self._treeviewArea.header().menuButton()
    
    def centerViewMenuButton(self):
        return self._centerArea.header().menuButton()
    
    def treeViewHeader(self):
        return self._treeviewArea.header()
    
    def centerViewHeader(self):
        return self._centerArea.header()
    
    def setCenterViewHeader(self,text):
        self._centerArea.header().setText(text)

    def setTreeViewHeader(self,text):
        self._treeviewArea.header().setText(text)

    def currentCenterViewClassId(self):
        if self.centerView():
            return self.plugin().viewClassId(self.centerView().__class__)
        return None

    def enableCenterViewSelectionMenu(self, enable=True, exceptionViewClassId=None):
        disabledCenterViewIds = []
        for viewClass in self.plugin().availableCenterViews():
            viewClassId=self.plugin().viewClassId(viewClass)
            if enable==False and viewClassId!=exceptionViewClassId:
                disabledCenterViewIds+=[viewClassId]
            if enable==True and viewClassId==exceptionViewClassId:
                disabledCenterViewIds+=[viewClassId]
        self.plugin().setDisabledCenterViewIds(disabledCenterViewIds)

    def activated(self):
        """ Shows view menu when user selects tab.
        """
        self.updateViewMenu()
        guiFacade.showPluginMenu(self.plugin().viewMenu())

    def switchCenterView(self, requestedViewClassId):
        if self.currentCenterViewClassId() == requestedViewClassId:
            self.updateViewMenu()
            return True
        requestedViewClass = None
        for viewClass in self.plugin().availableCenterViews():
            if requestedViewClassId == self.plugin().viewClassId(viewClass):
                requestedViewClass = viewClass
        if not requestedViewClass and len(self.plugin().availableCenterViews())>0:
            logging.warning(self.__class__.__name__ +": switchCenterView() - Unknown view class id "+ requestedViewClassId +".")
            requestedViewClass = self.plugin().availableCenterViews()[0]
        elif not requestedViewClass:
            logging.error(self.__class__.__name__ +": switchCenterView() - Unknown view class id "+ requestedViewClassId +". Aborting...")
            return False
        self.setCenterView(requestedViewClass())
        self.updateViewMenu()

        #reconnect data accessors and stuff
        if hasattr(self.centerView(), "setEditable"):
            self.centerView().setEditable(self.isEditable())
        if self._dataAccessor!=None:
            self.centerView().setDataAccessor(self._dataAccessor)
        self.centerView().setFilter(self.filter)
        selection = self.treeView().selection()
        if selection:
            self.centerView().setDataObjects([selection])
        self.updateCenterView()
        self.connect(self.centerView(), SIGNAL("selected"), self.onSelected)
        self.connect(self.centerView(), SIGNAL("modified"), self.setModified)
        self.connect(self.centerView(), SIGNAL("mouseRightPressed"), self.centerViewMenuButtonClicked)
        self.saveIni()
        return True
                        
    def showBoxContentDialog(self):
        guiFacade.showDialog(self._boxContentDialog)
        
    def setEditable(self, edit):
        """ Makes sure an existing property view's read-only mode is set accordingly.
        """
        SplitterTab.setEditable(self, edit)
        if self.propertyView():
            self.propertyView().setReadOnly(not edit)
            
    def setDataAccessor(self, accessor):
        """ Set the DataAccessor and show data in the TreeView.
        """
        logging.debug(__name__ + ": setDataAccessor")
        self._dataAccessor = accessor
        self.treeView().setDataAccessor(self._dataAccessor)
        self.centerView().setDataAccessor(self._dataAccessor)
        self.propertyView().setDataAccessor(self._dataAccessor)
        self.treeView().setFilter(self.filter)
        self.centerView().setFilter(self.filter)

    def dataAccessor(self):
        return self._dataAccessor

    def setZoom(self, zoom):
        """  Sets zoom of tab's scroll area.
        
        Needed for zoom tool bar. See Tab setZoom().
        """
        if hasattr(self,"scrollArea"):
            self.scrollArea().setZoom(zoom)
    
    def zoom(self):
        """ Returns zoom of tab's scoll area.
        
        Needed for zoom tool bar. See Tab zoom().
        """
        if hasattr(self,"scrollArea"):
            return self.scrollArea().zoom()
        else:
            return 100.0

    def updateCenterView(self,propertyView=True):
        """ Fill the center view from an item in the TreeView and update it """
        logging.debug(__name__ + ": updateCenterView")
        statusMessage = self.statusBarStartMessage("Updating center view")
        if self.centerView().updateContent():
            self.centerView().restoreSelection()
            select = self.centerView().selection()
            if select != None:
                if self.propertyView().dataObject() != select and propertyView:
                    self.propertyView().setDataObject(select)
                    self.propertyView().updateContent()
        self.statusBarStopMessage(statusMessage)

    def onTreeViewSelected(self, select):
        """ When object is selected in the TreeView update center view and PropertyView.
        """
        logging.debug(__name__ + ": onTreeViewSelected")
        self.onSelected(select)
        self.centerView().setDataObjects([self.treeView().selection()])
        self.updateCenterView()

    def onSelected(self, select):
        """ When object is selected in the center view update PropertyView.
        """
        logging.debug(__name__ + ": onSelected")
        if self.propertyView().dataObject() != select:
            statusMessage = self.statusBarStartMessage("Updating property view")
            self.propertyView().setDataObject(select)
            self.propertyView().updateContent()
            self.statusBarStopMessage(statusMessage)

    def updateContent(self, filtered=False, propertyView=True):
        """ Updates all three views and restores the selection, e.g. after moving to next event.
        """
        logging.debug(__name__ + ": updateContent")
        # run filter if used
        if self._filterDialog and not filtered:
            self._filterAlgoritm.setDataObjects(self._dataAccessor.topLevelObjects())
            self._filterDialog.filter()
            return
        statusMessage = self.statusBarStartMessage("Updating all views")
        if self._findAlgorithm:
            self._findAlgorithm.setDataObjects(self._dataAccessor.topLevelObjects())
            self._findDialog.edited()
        self.treeView().setDataObjects(self._dataAccessor.topLevelObjects())
        if self.updateTreeView():
            if propertyView:
                self.propertyView().setDataObject(self.treeView().selection())
            if not propertyView or self.propertyView().updateContent():
                selection = self.treeView().selection()
                if selection:
                    self.centerView().setDataObjects([selection])
                self.updateCenterView(propertyView)
        self.statusBarStopMessage(statusMessage)

    def updateTreeView(self):
        if self.treeView().updateContent():
            self.treeView().restoreSelection()
            return True
        else:
            return False
    
    def find(self):
        """ Open find dialog and find items.
        """
        logging.debug(__name__ + ": find")
        if not self._findAlgorithm:
            self._findAlgorithm = FindAlgorithm()
            self._findAlgorithm.setDataAccessor(self._dataAccessor)
            self._findAlgorithm.setFilter(self.filter)
        self._findAlgorithm.setDataObjects(self._dataAccessor.topLevelObjects())
        if not self._findDialog:
            self._findDialog = guiFacade.showDialog(FindDialog)
            self._findDialog.setFindAlgorithm(self._findAlgorithm)
            self.connect(self._findDialog, SIGNAL("found"), self.select)
        else:
            guiFacade.showDialog(self._findDialog)
        
    def filterDialog(self):
        """ Open filter dialog and filter items.
        """
        logging.debug(__name__ + ": filterDialog")
        if not self._filterAlgoritm:
            self._filterAlgoritm = FindAlgorithm()
            self._filterAlgoritm.setDataAccessor(self._dataAccessor)
        self._filterAlgoritm.setDataObjects(self._dataAccessor.topLevelObjects())
        if not self._filterDialog:
            self._filterDialog = guiFacade.showDialog(FindDialog)
            self._filterDialog.setFindAlgorithm(self._filterAlgoritm)
            self.connect(self._filterDialog, SIGNAL("filtered"), self.filtered)
        else:
            guiFacade.showDialog(self._filterDialog)
    
    def filtered(self, filterObjects):
        self._filterObjects = filterObjects
        self.updateContent(True)

    def select(self, object):
        """ Select an object in all views.
        """
        logging.debug(__name__ + ": select : " + str(object))
        self.treeView().select(object)
        self.centerView().select(object)
        if self.propertyView().dataObject() != object:
            statusMessage = self.statusBarStartMessage("Updating property view")
            self.propertyView().setDataObject(object)
            self.propertyView().updateContent()
            self.statusBarStopMessage(statusMessage)

    def scriptChanged(self, script):
        """ Update box content of center view when script is changed.
        """
        if hasattr(self.centerView(), "setBoxContentScript"):
            self.centerView().setBoxContentScript(script)
            self.updateCenterView()

    def close(self):
        self.cancel()
        return SplitterTab.close(self)
    
    def boxContentDialog(self):
        return self._boxContentDialog

    def saveImage(self, filename=None):
        """ Save screenshot of the center view to file.
        """
        self.centerView().exportImage(filename)

    def filter(self, objects):
        """ Filter all final state objects using the output of the filterDialog.
        """
        #logging.debug(__name__ + ": filter")
        if self._filterObjects != None:
            return [o for o in objects if o in self._filterObjects or len(self.filter(self._dataAccessor.children(o)))>0]
        else:
            return objects

    def cancel(self):
        """ Cancel all operations in tab.
        """
        logging.debug(__name__ + ": cancel")
        self.treeView().cancel()
        self.centerView().cancel()
        self.propertyView().cancel()

    def isBusy(self):
        return self.treeView().isBusy() or\
               self.centerView().isBusy() or\
               self.propertyView().isBusy()

    def saveIni(self):
        """ write options to ini """
        logging.debug(__name__ + ": saveIni")
        if not self.plugin():
            logging.waring(self.__class__.__name__ +": saveIni() - No plugin set. Aborting...")
            return
        ini = Preferences.getPreferences()
        if not ini.has_section("view"):
            ini.add_section("view")
        if self.currentCenterViewClassId():
            ini.set("view", "CurrentView", self.currentCenterViewClassId())
        if hasattr(self.centerView(), "boxContentScript"):
            ini.set("view", "box content script", self.centerView().boxContentScript())
        Preferences.writePreferences()

    def loadIni(self):
        """ read options from ini """
        logging.debug(__name__ + ": loadIni")
        ini = Preferences.getPreferences()
        if ini.has_option("view", "CurrentView"):
            proposed_view = ini.get("view", "CurrentView")
            self.switchCenterView(proposed_view)
        elif self.plugin().defaultCenterViewId():
            self.switchCenterView(self.plugin().defaultCenterViewId())
        elif len(self.plugin().availableCenterViews()) > 0:
            self.switchCenterView(self.plugin().viewClassId(self.plugin().availableCenterViews()[0]))
        if ini.has_option("view", "box content script"):
            self._boxContentDialog.setScript(str(ini.get("view", "box content script")))
            if hasattr(self.centerView(), "setBoxContentScript"):
                self.centerView().setBoxContentScript(str(ini.get("view", "box content script")))

    def updateViewMenu(self):
        """ Enable/disable menu entries, when center view changes.
        """
        self.plugin().boxContentAction().setVisible(hasattr(self.centerView(),"setBoxContentScript"))
        self.plugin().saveImageAction().setVisible(hasattr(self.centerView(),"exportImage"))
        self.plugin().zoomAction().setVisible(hasattr(self.centerView(),"setZoom"))
        self.plugin().expandAllAction().setVisible(hasattr(self.treeView(),"expandAll"))
        self.plugin().expandToDepthAction().setVisible(hasattr(self.treeView(),"expandToDepth"))
        self.plugin().collapseAllAction().setVisible(hasattr(self.treeView(),"collapseAll"))
        for action in self.plugin().viewMenu().actions():
            if action.data().toString()!="":
                action.setEnabled(not action.data().toString() in self.plugin().disabledCenterViewIds())
                currentAction=action.data().toString()==self.currentCenterViewClassId()
                action.setChecked(currentAction)
                if currentAction:
                    self.setCenterViewHeader(action.text().replace("&",""))
        if self.mainWindow():
            if isinstance(self.centerView(), Zoomable):
                guiFacade.showZoomToolBar()
            else:
                guiFacade.hideZoomToolBar()

    def centerViewMenuButtonClicked(self, point=None):
        popup=QMenu(self.centerViewMenuButton())
        popup.addAction(self.plugin()._boxContentAction)
        popup.addAction(self.plugin()._saveImageAction)
        popup.addAction(self.plugin()._zoomAction)
        popup.addSeparator()
        for action in self.plugin().viewMenu().actions():
            if action.data().toString()!="":
                popup.addAction(action)
        if not isinstance(point,QPoint):
            point=self.centerViewMenuButton().mapToGlobal(QPoint(self.centerViewMenuButton().width(),0))
        popup.exec_(point)

    def treeViewMenuButtonClicked(self, point=None):
        popup=QMenu(self.treeViewMenuButton())
        popup.addAction(self.plugin()._expandAllAction)
        popup.addAction(self.plugin()._expandToDepthAction)
        popup.addAction(self.plugin()._collapseAllAction)
        popup.addAction(self.plugin()._filterAction)
        popup.addSeparator()
        if not isinstance(point,QPoint):
            point=self.treeViewMenuButton().mapToGlobal(QPoint(self.treeViewMenuButton().width(),0))
        popup.exec_(point)

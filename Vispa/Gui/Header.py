from PyQt4.QtCore import Qt,SIGNAL
from PyQt4.QtGui import QWidget, QLabel, QToolButton, QFrame, QVBoxLayout, QHBoxLayout, QSizePolicy


class FrameWithHeader(QFrame):

    def __init__(self, parent=None):
        QFrame.__init__(self, parent)
        self.setFrameShadow(QFrame.Raised)
        self.setFrameStyle(QFrame.StyledPanel)
        self.setLayout(QVBoxLayout())
        self.layout().setContentsMargins(0, 0, 0, 0)
        self.layout().setSpacing(0)

        self._header = Header(Qt.Horizontal, self)
        self.layout().addWidget(self._header)

    def addWidget(self, widget):
        if isinstance(widget, QFrame):
            widget.setFrameStyle(QFrame.NoFrame)
        self.layout().addWidget(widget)

    def insertWidget(self, position, widget):
        """ Inserts widget at position
        """
        if isinstance(widget, QFrame):
            widget.setFrameStyle(QFrame.NoFrame)
        self.layout().insertWidget(position, widget)

    def header(self):
        return self._header

class Header(QWidget):

    def __init__(self, orientation, parent=None):
        QWidget.__init__(self,parent)
        self.setLayout(QHBoxLayout())
        self.__orientation = orientation
        #ToDo: Refactor Menu Buttons
        self._menuButton = None
        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        self.setFixedHeight(26)
        self.__label = QLabel(self)
        self.__label.setAlignment(Qt.AlignHCenter)
        self.layout().addWidget(self.__label)
        self.layout().setContentsMargins(1, 1, 1, 1)

    def createMenuButton(self, label=">"):
        #self.setFixedHeight(26) # make sure label is high enough for the button, not needed without border
        self._menuButton = QToolButton(self)
        self.layout().insertWidget(0, self._menuButton)
        self._menuButton.setStyleSheet("QToolButton { border: 0px };")
        self._menuButton.setAutoRaise(True)
        if label == ">":
            self._menuButton.setArrowType(Qt.RightArrow)
        else:
            self._menuButton.setText(label)
        return self._menuButton

    def menuButton(self):
        return self._menuButton

    def setText(self, text):
        self.__label.setText(text)
        #if self.orientation() == Qt.Horizontal:
        #    self.model().setHorizontalHeaderLabels([text])
        #elif self.orientation() == Qt.Vertical:
        #    self.model().setVerticalHeaderLabels([text])

    def mousePressEvent(self,event):
        #QHeaderView.mousePressEvent(self,event)
        if event.button()==Qt.RightButton:
            self.emit(SIGNAL("mouseRightPressed"), event.globalPos())

from PyQt4.QtCore import SIGNAL
from PyQt4.QtGui import QTextEdit,QPushButton,QGridLayout,QTextCursor,QDialog,QMessageBox

class TextDialog(QDialog):
    """ QDialog object to edit text by using an editor window.
    """
    def __init__(self, parent=None, title="Edit text...", text="", readonly=False, help=None):
        super(TextDialog,self).__init__(parent)
        self.setWindowTitle(title)
        self.resize(600,500)
        self._text=text
        self._help=help
        self._ok = QPushButton('&Ok', self)
        self.connect(self._ok, SIGNAL('clicked()'), self.accept)
        if not readonly:
            self._cancel = QPushButton('&Cancel', self)
            self.connect(self._cancel, SIGNAL('clicked()'), self.reject)
        if help:
            self._helpButton = QPushButton('&Help', self)
            self.connect(self._helpButton, SIGNAL('clicked()'), self.showHelp)
        self._edit=QTextEdit()
        self._edit.setPlainText(self._text)
        layout=QGridLayout()
        layout.addWidget(self._edit,0,0,1,4)
        layout.addWidget(self._ok,1,3)
        if not readonly:
            layout.addWidget(self._cancel,1,0)
        if help:
            layout.addWidget(self._helpButton,1,1)
        self.setLayout(layout)
        self._edit.setReadOnly(readonly)
        self._edit.setFocus()
        self._edit.moveCursor(QTextCursor.End)

    def getText(self):
        return self._edit.toPlainText().toAscii()

    def showHelp(self):
        QMessageBox.about(self, 'Info', self._help)

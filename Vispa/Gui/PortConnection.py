import logging

from PyQt4.QtCore import Qt, QPoint, QPointF, QRectF, QSizeF, SIGNAL
from PyQt4.QtGui import QColor, QPainter, QPen, QLinearGradient, QRadialGradient, QPainterPath, qGray, QKeySequence

from Vispa.Gui.ZoomableWidget import ZoomableWidget
from Vispa.Gui.VispaWidgetOwner import VispaWidgetOwner

class PointToPointConnection(ZoomableWidget):
    """ Visualizes a connection between two points.
    
    There are several types of connections available (see setType()):
       ORTHOGONAL    - Start and end points are connected by a set of horizontal and vertical lines (default).
       STRAIGHT      - Start and end points are connected by a straight line. 
       DIAGONAL      - Start and end points are connected by a set of three diagonal lines with different angles.
    """
    CONNECTOR_LENGTH = 20   # Length in pixels of the start/end part of the connection in direction of the startOrientation.
    CONNECTION_THICKNESS = 5
    FILL_COLOR1 = QColor(155, 0, 0)   # border
    FILL_COLOR2 = QColor(255, 0, 0)   # center

    SELECT_COLOR = QColor('darkblue')
    SELECTED_FRAME_WIDTH = 4            # Width in pixels of colored (SELECT_CORLOR) frame, when selected

    FOCUSPOLICY = Qt.ClickFocus

    CONNECTION_TYPE = "ORTHOGONAL"

    class ConnectionDirection:
        UNDEFINED = 0
        UP = 1
        LEFT = 2
        DOWN = 3
        RIGHT = 4

    class DrawOrientation:
        HORIZONTAL = 0
        VERTICAL = 1

    class CornerType:
        UNDEFINED = 0
        TOP_RIGHT = 1
        TOP_LEFT = 2
        BOTTOM_LEFT = 3
        BOTTOM_RIGHT = 4
        STRAIGHT = 5

    def __init__(self, workspace, sourcePoint, targetPoint):
        #logging.debug(__name__ +": __init__()")
        self._recalculateRouteFlag = True
        self._sourceDirection = PointToPointConnection.ConnectionDirection.RIGHT
        self._targetDirection = PointToPointConnection.ConnectionDirection.LEFT
        self._route = None
        self._selectableFlag = True
        self._selectedFlag = False
        self._deletableFlag = True
        self._deletedFlag = False
        self._sourcePoint = sourcePoint
        self._targetPoint = targetPoint
        self._totalPainterPath = None
        self._dragReferencePoint = QPoint()
        self._fillColor1 = None
        self._fillColor2 = None
        self._grayScaleModeFlag = False

        ZoomableWidget.__init__(self, workspace)
        self.setFocusPolicy(self.FOCUSPOLICY)
        self.setType(self.CONNECTION_TYPE)
        self.setFillColors(self.FILL_COLOR1, self.FILL_COLOR2)

        self.updateConnection()

        self.setMouseTracking(True)

        # debug
        #self.setAutoFillBackground(True)
        #self.setPalette(QPalette(Qt.green))

    def setFillColors(self, fillColor1, fillColor2):
        self._fillColor1 = fillColor1
        self._fillColor2 = fillColor2

    def setType(self, type):
        """ Sets type of connection.
        
        The type argument is a string of capitalized letters and should be one of the available types described in the class documentation.
        """
        self._type = type

    def routeChanged(self):
        self._recalculateRouteFlag = True

    def setZoom(self, zoom):
        ZoomableWidget.setZoom(self, zoom)
        self.forceRouteRecalculation()

    def sourcePoint(self):
        return self._sourcePoint

    def targetPoint(self):
        return self._targetPoint

    def updateTargetPoint(self, point):
        self._targetPoint = point
        self.updateConnection()

    def setSourceDirection(self, dir):
        self._sourceDirection = dir

    def sourceDirection(self):
        return self._sourceDirection

    def setTargetDirection(self, dir):
        self._targetDirection = dir

    def targetDirection(self):
        return self._targetDirection

    def setSelectable(self, sel):
        self._selectableFlag = sel

    def isSelectable(self):
        return bool(self._selectableFlag)

    def setDeletable(self, sel):
        self._deletableFlag = sel

    def isDeletable(self):
        return bool(self._deletableFlag)

    def isSelected(self):
        return self._selectedFlag

    def select(self, sel=True, multiSelect=False):
        if not self.isSelectable():
            return
        changed = False
        if self._selectedFlag != sel:
            changed = True

        self._selectedFlag = sel

        if self._selectedFlag:
            self.raise_()

        if changed:
            self.update()

        if not multiSelect and self.isSelected() and isinstance(self.parent(), VispaWidgetOwner):
            self.parent().deselectAllWidgets(self)

    def setDragReferencePoint(self, pos):
        self._dragReferencePoint = pos

    def dragReferencePoint(self):
        return self._dragReferencePoint

    def cornerTypeString(self, type):
        if type == self.CornerType.TOP_RIGHT:
            return "TOP_RIGHT"
        elif type == self.CornerType.TOP_LEFT:
            return "TOP_LEFT"
        elif type == self.CornerType.BOTTOM_LEFT:
            return "BOTTOM_LEFT"
        elif type == self.CornerType.BOTTOM_RIGHT:
            return "BOTTOM_RIGHT"
        elif type == self.CornerType.STRAIGHT:
            return "STRAIGHT"
        return "UDEFINED_CORNER"

    def connectionDirectionString(self, dir):
        if dir == self.ConnectionDirection.DOWN:
            return "DOWN"
        elif dir == self.ConnectionDirection.LEFT:
            return "LEFT"
        elif dir == self.ConnectionDirection.RIGHT:
            return "RIGHT"
        elif dir == self.ConnectionDirection.UP:
            return "UP"
        return "UNDEFINED_DIRECTION"

    def forceRouteRecalculation(self):
        self._recalculateRouteFlag = True

    def routeIsValid(self):
        try:
            if type(self._route).__name__ != 'list':
                logging.error("PointToPointConnection.routeIsValid() - 'route' does not contain a list an thus is not a valid route.")
                return False
        except:
            logging.error("PointToPointConnection.routeIsValid() - 'route' is not valid.")
            return False
        return True

    def betweenTwoPoints(self, point, first, second):
        """ Checks whether 'point' lies between 'first' and 'second'.
        
        This function can currently (08-11-15) only deal with horizontal and vertical distances.
        """

        halfthick = 0.5 * self.CONNECTION_THICKNESS * self.zoomFactor()
        direction = self.getPointToPointDirection(first, second)

        topLeft = QPointF()
        bottomRight = QPointF()
        if direction == self.ConnectionDirection.LEFT:
            # horizontal, negative
            topLeft.setX(second.x())
            topLeft.setY(second.y() - halfthick)
            bottomRight.setX(first.x())
            bottomRight.setY(first.y() + halfthick)
        elif direction == self.ConnectionDirection.RIGHT:
            # horizontal, positive
            topLeft.setX(first.x())
            topLeft.setY(first.y() - halfthick)
            bottomRight.setX(second.x())
            bottomRight.setY(second.y() + halfthick)
        elif direction == self.ConnectionDirection.UP:
            # vertical, negative
            topLeft.setX(second.x() - halfthick)
            topLeft.setY(second.y())
            bottomRight.setX(first.x() + halfthick)
            bottomRight.setY(first.y())
        elif direction == self.ConnectionDirection.UP:
            # vertical, positive
            topLeft.setX(first.x() - halfthick)
            topLeft.setY(first.y())
            bottomRight.setX(second.x() + halfthick)
            bottomRight.setY(second.y())
        else:
            return False

        rect = QRectF(topLeft, bottomRight)
        return rect.contains(QPointF(point))

    def belongsToRoute(self, point):
        """ Checks whether 'point' is part of the connection.
        
        'point' has to be in coordinates of self.drawPanel.
        """

        if not self.routeIsValid() or not self._totalPainterPath:
            return False

        return self._totalPainterPath.contains(QPointF(point))

        lastP = None
        for thisP in self._route:
            if lastP != None:
                if self.getRectBetweenTwoPoints(lastP, thisP).contains(QPointF(point)):
                    return True
            lastP = thisP

        return False

    def getPointByDistance(self, start, distance, direction):
        """ Returns a point which is about 'distance' pixels remotely from 'start' in direction 'direction'.
        """

        if direction == self.ConnectionDirection.DOWN:
            return QPoint(start.x(), start.y() + distance)
        elif direction == self.ConnectionDirection.LEFT:
            return QPoint(start.x() - distance, start.y())
        elif direction == self.ConnectionDirection.RIGHT:
            return QPoint(start.x() + distance, start.y())
        elif direction == self.ConnectionDirection.UP:
            return QPoint(start.x(), start.y() - distance)
        else:
            logging.error("PointToPointConnection.getPointByDistance() - Unknown ConnectionDirection.")

    def nextPointByDistance(self, route, distance, direction):
        """ Directly adds getPointByDistance() to 'route'.
        """

        if len(route) < 1:
            logging.error("PointToPointConnection.nextPointByDistance() - Can not calculate next point for empty route.")
            return

        start = route[len(route) - 1]
        return self.getPointByDistance(start, distance, direction)

    def nextPointByTarget(self, route, target, orientation):
        """ Adds a point to 'route', so the route approaches to the 'target' point in a specific 'orientation'.
        
        This means that the added point and 'target' have one coordinate in common.
        If 'orientation' points in horizontal direction (left or right) the x components will be equal.
        If 'orientation' points in vertical direction (up or down) the y components will be equal.
        """
        if len(route) < 1:
            logging.error("PointToPointConnection.nextPointByTarget() - Can not calculate next point for empty route.")
            return

        start = route[len(route) - 1]

        if orientation == self.DrawOrientation.HORIZONTAL:
            return QPoint(target.x(), start.y())
        elif orientation == self.DrawOrientation.VERTICAL:
            return QPoint(start.x(), target.y())
        else:
            logging.error("PointToPointConnection.nextPointByTarget() - Unknown DrwaOrientation.")

    def calculateRoute(self):
        """ Calculates the route and stores all route points in internal list.
        
        If start and end points have not changed since last last calculation, the route wont be recalculated unless forceRouteRecalculation() was called before.
        If route was recalculated function returns True otherwise False.
        """
        if not self._recalculateRouteFlag and self._route != None and len(self._route) > 1:
            if self._route[0] == self.sourcePoint() and self._route[len(self._route) - 1] == self.targetPoint():
                # Nothing has changed, so route not recalculated
                return False

        # Recaclucating route

        # Whenever the start directions point at each other, the connection has to go through the center of the points
        throughCenter = False
        rowKind = False
        columnKind = False

        sourceDirection = self.sourceDirection()
        targetDirection = self.targetDirection()

        if self._type == "ORTHOGONAL":
            if sourceDirection == self.ConnectionDirection.RIGHT and targetDirection == self.ConnectionDirection.LEFT:
                throughCenter = True
                rowKind = True
            elif sourceDirection == self.ConnectionDirection.LEFT and targetDirection == self.ConnectionDirection.RIGHT:
                throughCenter = True
                rowKind = True
            elif sourceDirection == self.ConnectionDirection.DOWN and targetDirection == self.ConnectionDirection.UP:
                throughCenter = True
                columnKind = True
            elif sourceDirection == self.ConnectionDirection.UP and targetDirection == self.ConnectionDirection.DOWN:
                throughCenter = True
                columnKind = True

        self._route = []

        sP = QPoint(self.sourcePoint())        # start
        eP = QPoint(self.targetPoint())          # end
        self._route.append(sP)


        if throughCenter:
            # ORTHOGONAL
            centerP = (sP + eP) * 0.5
            firstP = self.nextPointByDistance(self._route, self.CONNECTOR_LENGTH * self.zoomFactor(), sourceDirection)
            lastP = self.getPointByDistance(eP, self.CONNECTOR_LENGTH * self.zoomFactor() , targetDirection)
            self._route.append(firstP)
            if rowKind:
                cornerSquareSideLength = self.cornerSquareSideLength()
                #if lastP.x() - firstP.x() > self.CONNECTOR_LENGTH * self.zoomFactor() * 0.5:
                #if (eP.x() - sP.x()) > ((self.CONNECTOR_LENGTH) * 2* self.zoomFactor()):
                #if (lastP.x() - firstP.x()) > cornerSquareSideLength:
                #if (centerP.x() - firstP.x()) > 0:
                if firstP.x() < (lastP.x() - cornerSquareSideLength):
                    self._route.append(self.nextPointByTarget(self._route, centerP, self.DrawOrientation.HORIZONTAL))
                    self._route.append(centerP)
                    self._route.append(self.nextPointByTarget(self._route, lastP, self.DrawOrientation.VERTICAL))
                elif abs(sP.y() - eP.y()) > (cornerSquareSideLength * 2):
                    self._route.append(firstP)
                    self._route.append(self.nextPointByTarget(self._route, centerP, self.DrawOrientation.VERTICAL))
                    #self._route.append(centerP)
                    #intermediateLength =  self.CONNECTOR_LENGTH * self.zoomFactor()
                    #intermediateLength = cornerSquareSideLength
                    #intermediateLength =  min(self.CONNECTOR_LENGTH * self.zoomFactor(), max(0, lastP.x() - firstP.x()))
                    intermediateLength = min(cornerSquareSideLength, abs(lastP.x() - firstP.x()))
                    self._route.append(self.nextPointByTarget(self._route, lastP + QPoint(-intermediateLength, 0), self.DrawOrientation.HORIZONTAL))
                    #self._route.append(self.nextPointByDistance(self._route, self.CONNECTOR_LENGTH * self.zoomFactor() , self.targetDirection()))
                    self._route.append(self.nextPointByTarget(self._route, lastP, self.DrawOrientation.VERTICAL))

            elif columnKind:
                #print "    columnKind"
                route.append(self.nextPointByTarget(self._route, centerP, self.DrawOrientation.VERTICAL))
                route.append(centerP)
                route.append(self.nextPointByTarget(self._route, lastP, self.DrawOrientation.HORIZONTAL))
            else:
                logging.error("PointToPointConnection.calculateRoute() - Sorry connections going through the center have to be either rowKind or columKind.")

            self._route.append(lastP)
            self._route.append(eP)

            # clean route: remove points that do not really contribute
            #print "clean route - before", self._route
            routeTemp = self._route[:]
            previousPoint = None
            currentPoint = None
            nextPoint = None
            for point in routeTemp:
                previousPoint = currentPoint
                currentPoint = nextPoint
                nextPoint = point
                if not previousPoint or not currentPoint:
                    continue

                currentX = currentPoint.x()
                currentY = currentPoint.y()
                if (previousPoint.x() == currentX and nextPoint.x() == currentX) or (previousPoint.y() == currentY and nextPoint.y() == currentY):
                    self._route.remove(currentPoint)
                    currentPoint = previousPoint
            #print "clean route - after", self._route

        else:
            #print "calculateRoute() straight"
            # STRAIGHT or DIAGONAL
            if self._type == "DIAGONAL":
                width = abs(sP.x() - eP.x())
                height = abs(sP.y() - eP.y())
                if width > 0:
                    directionX = (sP.x() - eP.x()) / width
                else:
                    directionX = 0
                if height > 0:
                    directionY = (sP.y() - eP.y()) / height
                else:
                    directionY = 0
                if width > height:
                    diagonal = height / 2
                else:
                    diagonal = width / 2
                self._route.append(QPoint(sP.x() - directionX * diagonal, sP.y() - directionY * diagonal))
                self._route.append(QPoint(sP.x() - directionX * (width - diagonal), sP.y() - directionY * (height - diagonal)))
            self._route.append(eP)

        self._recalculateRouteFlag = False
        return True

    def getPointToPointDirection(self, firstPoint, secondPoint):

        if not firstPoint or not secondPoint:
            return self.ConnectionDirection.UNDEFINED

        if firstPoint.y() == secondPoint.y():    # horizontal
            if firstPoint.x() < secondPoint.x():
                return self.ConnectionDirection.RIGHT
            else:
                return self.ConnectionDirection.LEFT
        elif firstPoint.x() == secondPoint.x():    # vertical
            if firstPoint.y() < secondPoint.y():
                return self.ConnectionDirection.DOWN
            else:
                return self.ConnectionDirection.UP

        return self.ConnectionDirection.UNDEFINED

    def cornerIsDefined(self, type):
        if type == self.CornerType.TOP_RIGHT or type == self.CornerType.TOP_LEFT or type == self.CornerType.BOTTOM_LEFT or type == self.CornerType.BOTTOM_RIGHT:
            return True
        return False

    def getCornerType(self, firstDirection, secondDirection):
        if (firstDirection == self.ConnectionDirection.UP and secondDirection == self.ConnectionDirection.RIGHT) or (firstDirection == self.ConnectionDirection.LEFT and secondDirection == self.ConnectionDirection.DOWN):
            return self.CornerType.TOP_LEFT
        elif (firstDirection == self.ConnectionDirection.RIGHT and secondDirection == self.ConnectionDirection.DOWN) or (firstDirection == self.ConnectionDirection.UP and secondDirection == self.ConnectionDirection.LEFT):
            return self.CornerType.TOP_RIGHT
        elif (firstDirection == self.ConnectionDirection.DOWN and secondDirection == self.ConnectionDirection.LEFT) or (firstDirection == self.ConnectionDirection.RIGHT and secondDirection == self.ConnectionDirection.UP):
            return self.CornerType.BOTTOM_RIGHT
        elif (firstDirection == self.ConnectionDirection.LEFT and secondDirection == self.ConnectionDirection.UP) or (firstDirection == self.ConnectionDirection.DOWN and secondDirection == self.ConnectionDirection.RIGHT):
            return self.CornerType.BOTTOM_LEFT

        return self.CornerType.UNDEFINED

    def cornerSquareSideLength(self):
        cornerRoundness = 3
        return self.CONNECTION_THICKNESS * cornerRoundness * self.zoomFactor()

    def drawCorner(self, painter, position, cornerType, maxRadius=None):
        #logging.debug(self.__class__.__name__ +": drawCorner() "+ self.cornerTypeString(cornerType))
        thickness = self.CONNECTION_THICKNESS * self.zoomFactor()
        halfthick = thickness / 2
        radius = 0.5 * self.cornerSquareSideLength()
        if maxRadius:
            maxRadius = max(maxRadius, thickness)
            radius = min(radius, maxRadius)

        if cornerType == self.CornerType.TOP_RIGHT:
            startAngle = 0

            outerCorner = QPointF(position.x() + halfthick - 2 * radius, position.y() - halfthick)
            innerCorner = QPointF(outerCorner.x(), outerCorner.y() + (thickness))
            center = QPointF(outerCorner.x() + radius, outerCorner.y() + radius)

            outerRect = QRectF(outerCorner, QSizeF(2 * radius, 2 * radius))
            innerRect = QRectF(innerCorner, QSizeF((2 * radius - thickness), (2 * radius - thickness)))

            outerStart = QPointF(outerCorner.x() + 2 * radius, outerCorner.y() + (radius + halfthick))
            innerStart = QPointF(outerCorner.x() + (radius - halfthick), outerCorner.y())

        elif cornerType == self.CornerType.TOP_LEFT:
            startAngle = 90

            outerCorner = QPointF(position.x() - halfthick, position.y() - halfthick)
            innerCorner = QPointF(outerCorner.x() + (thickness), outerCorner.y() + (thickness))
            center = QPointF(outerCorner.x() + radius, outerCorner.y() + radius)

            outerRect = QRectF(outerCorner, QSizeF(2 * radius, 2 * radius))
            innerRect = QRectF(innerCorner, QSizeF((2 * radius - thickness), (2 * radius - thickness)))

            outerStart = QPointF(outerCorner.x() + (radius + halfthick), outerCorner.y())
            innerStart = QPointF(outerCorner.x(), outerCorner.y() + (radius + halfthick))

        elif cornerType == self.CornerType.BOTTOM_LEFT:
            startAngle = 180

            outerCorner = QPointF(position.x() - halfthick, position.y() + halfthick - 2 * radius)
            innerCorner = QPointF(outerCorner.x() + (thickness), outerCorner.y())
            center = QPointF(outerCorner.x() + radius, outerCorner.y() + radius)

            outerRect = QRectF(outerCorner, QSizeF(2 * radius, 2 * radius))
            innerRect = QRectF(innerCorner, QSizeF((2 * radius - thickness), (2 * radius - thickness)))

            outerStart = QPointF(outerCorner.x(), outerCorner.y() + (radius - halfthick))
            innerStart = QPointF(outerCorner.x() + (radius + halfthick), outerCorner.y() + (2 * radius))

        elif cornerType == self.CornerType.BOTTOM_RIGHT:
            startAngle = 270

            outerCorner = QPointF(position.x() + halfthick - 2 * radius, position.y() + halfthick - 2 * radius)
            innerCorner = QPointF(outerCorner.x(), outerCorner.y())
            center = QPointF(outerCorner.x() + radius, outerCorner.y() + radius)

            outerRect = QRectF(outerCorner, QSizeF(2 * radius, 2 * radius))
            innerRect = QRectF(innerCorner, QSizeF((2 * radius - thickness), (2 * radius - thickness)))

            outerStart = QPointF(outerCorner.x() + (radius - halfthick), outerCorner.y() + 2 * radius)
            innerStart = QPointF(outerCorner.x() + 2 * radius, outerCorner.y() + (radius - halfthick))

        else:
            # No defined corner, so nothing to draw.
            #print "PointToPointConnection.drawCorner() - No valid corner, aborting..."
            return None

        # workaround does not seem to be necessary any more (2011-04-30)
#        if painter.redirected(painter.device()):
#            # e.q. QPixmap.grabWidget()
#            painter.setBrush(self._fillColor1)
#        else:
        brush = QRadialGradient(center, radius)
        if radius >= thickness:
            brush.setColorAt((radius - thickness) / radius, self._fillColor1)   # inner border 
            brush.setColorAt((radius - halfthick + 1) / radius, self._fillColor2)   # center of line
        else:
            # If zoom is too small use single color
            brush.setColorAt(0, self._fillColor1)
        brush.setColorAt(1, self._fillColor1)                                   # outer border
        painter.setBrush(brush)

        path = QPainterPath()
        path.moveTo(outerStart)
        path.arcTo(outerRect, startAngle, 90)
        path.lineTo(innerStart)
        path.arcTo(innerRect, startAngle + 90, -90)
        path.closeSubpath()

        #painter.setPen(Qt.NoPen)

        #if self._selectedFlag:
        self._totalPainterPath += path
        painter.fillPath(path, brush)


#        # Helper lines
#            
#        painter.setBrush(Qt.NoBrush)
#        painter.setPen(QPen(QColor(0,255,255)))
#        painter.drawPath(path)
#        painter.setPen(QPen(QColor(0,255,0)))
#        painter.drawRect(innerRect)
#        painter.setPen(QPen(QColor(0,0, 255)))
#        painter.drawRect(outerRect)
#            
#         Mark important points
#        painter.setPen(QPen(QColor(0,0, 0)))
#        painter.drawEllipse(outerCorner, 2, 2)
#        painter.drawEllipse(innerCorner, 2, 2)
#        painter.drawEllipse(center, 2, 2)
#        painter.drawEllipse(outerStart, 2, 2)
#        painter.drawEllipse(innerStart, 2, 2)

    def getRectBetweenTwoPoints(self, firstP, secondP, firstCorner=CornerType.UNDEFINED, secondCorner=CornerType.UNDEFINED):
        #print "getRectBetweenTwoPoints()"
        halfthick = 0.5 * self.CONNECTION_THICKNESS * self.zoomFactor()
        cornerRoundness = halfthick ** 0.5
        offset = 2 * halfthick  #* (cornerRoundness + 1) - 1       # -1 prevents one pixel gaps which sometimes appear at corners.

        direction = self.getPointToPointDirection(firstP, secondP)

        firstOffset = 0
        if self.cornerIsDefined(firstCorner):
            firstOffset = offset

        secondOffset = 0
        if self.cornerIsDefined(secondCorner):
            secondOffset = offset

        topLeft = QPointF()
        bottomRight = QPointF()
        if direction == self.ConnectionDirection.LEFT:
            # horizontal, negative
            topLeft.setX(secondP.x() + secondOffset)
            topLeft.setY(secondP.y() - halfthick)
            bottomRight.setX(firstP.x() - firstOffset + 1)
            bottomRight.setY(firstP.y() + halfthick)
        elif direction == self.ConnectionDirection.RIGHT:
            # horizontal, positive
            topLeft.setX(firstP.x() + firstOffset)
            topLeft.setY(firstP.y() - halfthick)
            bottomRight.setX(secondP.x() - secondOffset + 1)
            bottomRight.setY(secondP.y() + halfthick)
        elif direction == self.ConnectionDirection.UP:
            # vrtical, negative
            topLeft.setX(secondP.x() - halfthick)
            topLeft.setY(secondP.y() + secondOffset)
            bottomRight.setX(firstP.x() + halfthick)
            bottomRight.setY(firstP.y() - firstOffset + 1)
        elif direction == self.ConnectionDirection.DOWN:
            # vertical, positive
            topLeft.setX(firstP.x() - halfthick)
            topLeft.setY(firstP.y() + firstOffset)
            bottomRight.setX(secondP.x() + halfthick)
            bottomRight.setY(secondP.y() - secondOffset + 1)
        else:
            return QRectF(topLeft, bottomRight)

        return QRectF(topLeft, bottomRight)

#    def drawLineSection(self, painter, firstP, secondP, firstCorner, secondCorner):
#        direction = self.getPointToPointDirection(firstP, secondP)
#        
#        thickness = self.CONNECTION_THICKNESS * self.zoomFactor()
#        halfthick = thickness / 2
#        cornerRoundness = halfthick ** 0.5
#        cornerOffset = halfthick * cornerRoundness * (4 * self.zoomFactor()**2)
#        innerCorner = halfthick * (cornerRoundness + 1)
#        outerCorner = halfthick * (cornerRoundness - 1)
#        
#        rect = self.getRectBetweenTwoPoints(firstP, secondP, firstCorner, secondCorner)
#            # Paint witch color gradient (PyQt4)
#        if direction == self.ConnectionDirection.LEFT:     # horizontal, negative
#            brush = QLinearGradient(rect.x(), rect.y(), rect.x(), rect.y() + halfthick)
#        elif direction == self.ConnectionDirection.RIGHT:  # horizontal, positive
#            brush = QLinearGradient(rect.x(), rect.y(), rect.x(), rect.y() + halfthick)
#        elif direction == self.ConnectionDirection.UP:     # vertical, negative
#            brush = QLinearGradient(rect.x(), rect.y(), rect.x() + halfthick, rect.y())
#        elif direction == self.ConnectionDirection.DOWN:   # vertical, positive
#            brush = QLinearGradient(rect.x(), rect.y(), rect.x() + halfthick, rect.y())
#        
#        brush.setSpread(QLinearGradient.ReflectSpread)
#        brush.setColorAt(0, self._fillColor1)
#        brush.setColorAt(1, self._fillColor2)
#        painter.setBrush(brush)
#        
#        painter.drawRect(rect)

    def getGapFillerPath(self, previousRect, previousDirection, nextRect, nextDirection):
        """ This function returns a QPainterPath that fills a gap between to line sections.
        A gap is typically a space which is to litte to draw a proper corner, thus this gap filler
        path can serve a minimalistic replacement.
        """

        topLeft = None
        bottomLeft = None
        topRight = None
        bottomRight = None

        # direction is the direction of the missing (not to be drawn) part
        if previousDirection == self.ConnectionDirection.DOWN and nextDirection == self.ConnectionDirection.RIGHT:     # vertical, negative+
            #print "previousDirection == RIGHT, nextDirection == RIGHT"
            topLeft = previousRect.bottomRight()
            bottomLeft = previousRect.bottomLeft()
            topRight = nextRect.topLeft()
            bottomRight = nextRect.bottomLeft()
        elif previousDirection == self.ConnectionDirection.RIGHT and nextDirection == self.ConnectionDirection.DOWN:     # vertical, negative+
            #print "previousDirection == RIGHT, nextDirection == RIGHT"
            topLeft = previousRect.topRight()
            bottomLeft = previousRect.bottomRight()
            topRight = nextRect.topRight()
            bottomRight = nextRect.topLeft()
        elif previousDirection == self.ConnectionDirection.UP and nextDirection == self.ConnectionDirection.RIGHT:
            #print "previousDirection == RIGHT, nextDirection == RIGHT"
            topLeft = previousRect.topLeft()
            bottomLeft = previousRect.topRight()
            topRight = nextRect.topLeft()
            bottomRight = nextRect.bottomLeft()
        elif previousDirection == self.ConnectionDirection.RIGHT and nextDirection == self.ConnectionDirection.UP:
            #print "previousDirection == RIGHT, nextDirection == RIGHT"
            topLeft = previousRect.topRight()
            bottomLeft = previousRect.bottomRight()
            topRight = nextRect.bottomLeft()
            bottomRight = nextRect.bottomRight()
        elif previousDirection == self.ConnectionDirection.RIGHT and nextDirection == self.ConnectionDirection.RIGHT:     # vertical, negative+
            #print "previousDirection == RIGHT, nextDirection == RIGHT"
            topLeft = previousRect.topRight()
            bottomLeft = previousRect.bottomRight()
            topRight = nextRect.topLeft()
            bottomRight = nextRect.bottomLeft()
        elif previousDirection == self.ConnectionDirection.DOWN and nextDirection == self.ConnectionDirection.DOWN:     # vertical, negative+
            #print "previousDirection == RIGHT, nextDirection == RIGHT"
            topLeft = previousRect.bottomLeft()
            bottomLeft = nextRect.topLeft()
            topRight = previousRect.bottomRight()
            bottomRight = nextRect.topRight()
        elif previousDirection == self.ConnectionDirection.UP and nextDirection == self.ConnectionDirection.UP:     # vertical, negative+
            #print "previousDirection == RIGHT, nextDirection == RIGHT"
            topLeft = previousRect.topLeft()
            bottomLeft = nextRect.bottomLeft()
            topRight = previousRect.topRight()
            bottomRight = nextRect.bottomRight()
        #else:
        #    print "previousDirection == %s, nextDirection == %s --> unhandled" % (self.connectionDirectionString(previousDirection), self.connectionDirectionString(nextDirection))


        path = QPainterPath()
        if bottomLeft and bottomRight and topLeft and topRight:
            path.moveTo(bottomLeft)
            path.lineTo(bottomRight)
            #path.quadTo(topRight, bottomRight)
            path.lineTo(topRight)
            path.lineTo(topLeft)
            #path.quadTo(bottomLeft, topLeft)
            path.closeSubpath()
        return path

    def drawSection(self, painter, sectionIndex, drawCorner):
        """ This is going to replace drawLineSection
        """
        firstP = self.mapFromParent(self._route[sectionIndex])
        secondP = self.mapFromParent(self._route[sectionIndex + 1])
        direction = self.getPointToPointDirection(firstP, secondP)


        previousP = None
        nextP = None
        if sectionIndex == 0:
            previousDirection = self.sourceDirection()
        else:
            previousP = self.mapFromParent(self._route[sectionIndex - 1])
            previousDirection = self.getPointToPointDirection(previousP, firstP)
        if sectionIndex > len(self._route) - 3:
            nextDirection = self.targetDirection()
        else:
            nextP = self.mapFromParent(self._route[sectionIndex + 2])
            nextDirection = self.getPointToPointDirection(secondP, nextP)

        firstCorner = self.getCornerType(previousDirection, direction)
        secondCorner = self.getCornerType(direction, nextDirection)


        if self.CONNECTION_TYPE != "ORTHOGONAL" or direction == self.ConnectionDirection.UNDEFINED:
            self.drawStraightLine(painter, firstP, secondP)

            previousRect = self.getRectBetweenTwoPoints(previousP, firstP, None, firstCorner)
            nextRect = self.getRectBetweenTwoPoints(secondP, nextP, secondCorner, None)

            path = self.getGapFillerPath(previousRect, previousDirection, nextRect, nextDirection)

            #if self._selectedFlag:
            self._totalPainterPath += path
            painter.fillPath(path, painter.brush())
            return


        # check whether the distance is too tight for a normal line section and corners
        minDist = self.CONNECTION_THICKNESS * self.zoomFactor() * 2.5
        if previousP and drawCorner:
            # also take corner into account, if previous line section was too tight
            xDist = abs(firstP.x() - previousP.x())
            yDist = abs(firstP.y() - previousP.y())
            if (xDist > 0 and xDist < minDist) or (yDist > 0 and yDist < minDist):
                return

        xDist = abs(firstP.x() - secondP.x())
        yDist = abs(firstP.y() - secondP.y())
        if (xDist > 0 and xDist < minDist) or (yDist > 0 and yDist < minDist):
            # TODO: make this work in any case, extract this into separate function 
            previousRect = self.getRectBetweenTwoPoints(previousP, firstP, None, firstCorner)
            nextRect = self.getRectBetweenTwoPoints(secondP, nextP, secondCorner, None)

            path = self.getGapFillerPath(previousRect, previousDirection, nextRect, nextDirection)

            #if self._selectedFlag:
            self._totalPainterPath += path
            painter.fillPath(path, painter.brush())
#            
#            painter.setPen(QPen(QColor(0,0, 0)))
#            painter.drawEllipse(previousRect.topRight(), 2, 2)
#            painter.drawEllipse(previousRect.bottomRight(), 2, 2)
#            painter.drawEllipse(nextRect.topLeft(), 2, 2)
#            painter.drawEllipse(nextRect.bottomLeft(), 2, 2)
            return

        if drawCorner:
            self.drawCorner(painter, firstP, firstCorner)
            return

#        print "_____________________ darawSection _______________________"
#        print "firstP", firstP
#        print "secondP", secondP
#        print "previousDirection", self.connectionDirectionString(previousDirection)
#        print "  firstCorner", self.cornerTypeString(firstCorner)
#        print "direction", self.connectionDirectionString(direction)
#        print "  secondCorner", self.cornerTypeString(secondCorner)
#        print "nextDirection", self.connectionDirectionString(nextDirection)
#        print "\n\n"

        thickness = self.CONNECTION_THICKNESS * self.zoomFactor()
        halfthick = thickness / 2

        rect = self.getRectBetweenTwoPoints(firstP, secondP, firstCorner, secondCorner)
            # Paint witch color gradient (PyQt4)
        if direction == self.ConnectionDirection.LEFT:     # horizontal, negative
            brush = QLinearGradient(rect.x(), rect.y(), rect.x(), rect.y() + halfthick)
        elif direction == self.ConnectionDirection.RIGHT:  # horizontal, positive
            brush = QLinearGradient(rect.x(), rect.y(), rect.x(), rect.y() + halfthick)
        elif direction == self.ConnectionDirection.UP:     # vertical, negative
            brush = QLinearGradient(rect.x(), rect.y(), rect.x() + halfthick, rect.y())
        elif direction == self.ConnectionDirection.DOWN:   # vertical, positive
            brush = QLinearGradient(rect.x(), rect.y(), rect.x() + halfthick, rect.y())
        else:
            # Should already be drawn above --> direction == self.ConnectionDirection.UNDEFINED
            return

        # workaround does not seem to be necessary any more (2011-04-30)
#        if painter.redirected(painter.device()):
#            # e.q. QPixmap.grabWidget()
#            painter.setBrush(self._fillColor1)
#        else:
        brush.setSpread(QLinearGradient.ReflectSpread)
        brush.setColorAt(0, self._fillColor1)
        brush.setColorAt(1, self._fillColor2)
        painter.setBrush(brush)
        path = QPainterPath()
        path.addRect(rect)

        #if self._selectedFlag:
        self._totalPainterPath += path
        painter.fillPath(path, brush)
        #painter.fillRect(rect, brush)

    def drawStraightLine(self, painter, firstP, secondP):
        """ Draw a straight line between two points.
        """
        thickness = self.CONNECTION_THICKNESS * self.zoomFactor()
        halfthick = max(0.5, thickness / 2)
        pen = QPen(self._fillColor2, 2 * halfthick, Qt.SolidLine, Qt.RoundCap)
        painter.setPen(pen)

#        path = QPainterPath()
#        path.moveTo(QPointF(firstP))
#        path.lineTo(QPointF(secondP))
#        path.closeSubpath()
#        self._totalPainterPath += path

        painter.drawLine(firstP, secondP)

    def updateConnection(self):
        """ Recalculates route and then positions and sizes the widget accordingly.
        """
        #logging.debug(self.__class__.__name__ +": updateConnection()")
        #print "          sourcePoint, targetPoint", self.sourcePoint(), self.targetPoint()
        if self.calculateRoute():
            tl = self.topLeft()
            br = self.bottomRight()
            self.move(tl)
            self.resize(br.x() - tl.x(), br.y() - tl.y())
            self.update()
            return True
        return False

    def paintEvent(self, event):
        """ Handles paint events.
        """
        if self.updateConnection():
            event.ignore()
        else:
            #print "paintEvent() accept"
            self.draw()

    def draw(self):
        """ Draws connection.
        """
        self.calculateRoute()
        if not self.routeIsValid():
            return
        painter = QPainter(self)

        if self.zoom() > 30:
            painter.setRenderHint(QPainter.Antialiasing)

        #logging.debug(self.__class__.__name__ +": draw()")
        self._totalPainterPath = QPainterPath()
        self._totalPainterPath.setFillRule(Qt.WindingFill)
        #self._selectedFlag = True

        # TODO: remove this, if new selection mechanism works
#        if True or self._selectedFlag:
#            # Selected
#            framePen = QPen(self.SELECT_COLOR)
#            framePen.setWidth(self.SELECTED_FRAME_WIDTH)
#        else:
#            #self.select(False)
#            framePen = QPen(Qt.NoPen)
#            
#        if self.zoom() > 30:
#            painter.setRenderHint(QPainter.Antialiasing)            

#        painter.setPen(Qt.black)
#        for thisP in self._route:
#            painter.drawEllipse(self.mapFromParent(thisP), self.CONNECTION_THICKNESS * self.zoomFactor(), self.CONNECTION_THICKNESS* self.zoomFactor())

        painter.setPen(Qt.NoPen)
        for i in range(0, len(self._route) - 1):
            self.drawSection(painter, i, True)

        # avoids incontinuous intersections, if straight lines are drawn on top of both corners (looks better)
        for i in range(0, len(self._route) - 1):
            self.drawSection(painter, i, False)

        if self._selectedFlag:
            # Selected
            framePen = QPen(self.SELECT_COLOR)
            framePen.setWidth(self.SELECTED_FRAME_WIDTH)
            painter.setPen(framePen)
            painter.setBrush(Qt.NoBrush)
            painter.drawPath(self._totalPainterPath)

    def topLeft(self):
        """ Places a rect around the route and returns the top-left point in parent's coordinates.
        """
        if not self.routeIsValid():
            return None
        thickness = self.CONNECTION_THICKNESS * self.zoomFactor()
        xMin = self._route[0].x()
        yMin = self._route[0].y()
        for point in self._route:
            if point.x() < xMin:
                xMin = point.x()
            if point.y() < yMin:
                yMin = point.y()

        return QPoint(xMin - thickness, yMin - thickness)

    def bottomRight(self):
        """ Places a rectangle around the route and returns the bottom-right point in parent's coordinates.
        """
        if not self.routeIsValid():
            return None
        thickness = self.CONNECTION_THICKNESS * self.zoomFactor()
        xMax = self._route[0].x()
        yMax = self._route[0].y()
        for point in self._route:
            if point.x() > xMax:
                xMax = point.x()
            if point.y() > yMax:
                yMax = point.y()

        return QPoint(xMax + thickness, yMax + thickness)

    def mousePressEvent(self, event):
        """ Selects connection if event.pos() lies within the connection's borders.
        
        Otherwise the event is propagated to underlying widget via AnalysisDesignerWorkspace.propagateEventUnderConnectionWidget().
        """
        logging.debug(__name__ + ": mousePressEvent")
        #if self.belongsToRoute(self.mapToParent(event.pos())):    # old belongsToRoute() method
        if self.belongsToRoute(event.pos()):
            self.select()
        else:
            if not hasattr(self.parent(), "propagateEventUnderConnectionWidget") or not self.parent().propagateEventUnderConnectionWidget(self, event):
                event.ignore()

    def mouseReleaseEvent(self, event):
        """ Calls realeseMouse() to make sure the widget does not grab the mouse.
        
        Necessary because ConnectableWidgetOwner.propagateEventUnderConnectionWidget() may call grabMouse() on this widget.
        """
        #logging.debug(self.__class__.__name__ +": mouseReleaseEvent()")
        self.releaseMouse()
        ZoomableWidget.mouseReleaseEvent(self, event)

    def keyPressEvent(self, event):
        """ Handle delete and backspace keys to delete connections.
             Handle copy() method if copy key or CTRL+V is pressed
        """
        logging.debug("%s: keyPressEvent()" % (self.__class__.__name__))

        ctrlPressed = (event.modifiers() & Qt.ControlModifier) == Qt.ControlModifier

        if self._selectedFlag and (event.key() == Qt.Key_Backspace or event.key() == Qt.Key_Delete):
            #print __name__ +'.keyPressEvent', event.key()
            self.delete()
            self.emit(SIGNAL("deleteButtonPressed"))
        #elif event.key() == (Qt.Key_C or Qt.Key_Control):
        elif (ctrlPressed and event.key() == Qt.Key_C) or event.matches(QKeySequence.Copy): #actually it should be a keyReleaseEvent .. otherwise executed multiple times if keys are hold
            self.emit(SIGNAL("connectionCopyToClipboard"))
            #self.copySelectedModules()
        elif (ctrlPressed and event.key() == Qt.Key_V) or event.matches(QKeySequence.Paste): #actually it should be a keyReleaseEvent .. otherwise executed multiple times if keys are hold
            self.emit(SIGNAL("dropWidgetsFromClipboard"))
            #self.dropModulesFromClipboard()

    def delete(self):
        """ Deletes this connection.
        """
        if self._deletedFlag:
            return False
        if not self.isDeletable():
            return False
        self.deleteLater()
        self.emit(SIGNAL("connectionDeleted"))
        return True

    def mouseMoveEvent(self, event):
        #if self.belongsToRoute(self.mapToParent(event.pos())):
        if self.belongsToRoute(event.pos()):
            self.setCursor(Qt.OpenHandCursor)
        else:
            self.unsetCursor()
        ZoomableWidget.mouseMoveEvent(self, event)

    def grayScaleModeEnabled(self):
        return self._grayScaleModeFlag

    def enableGrayScaleMode(self, enable):
        self._grayScaleModeFlag = enable
        if enable:
            self.setFillColors(self.grayScaleColor(self.FILL_COLOR1), self.grayScaleColor(self.FILL_COLOR2))
        else:
            self.setFillColors(self.FILL_COLOR1, self.FILL_COLOR2)

    def grayScaleColor(self, qcolor):
        grayValue = qGray(qcolor.red(), qcolor.green(), qcolor.blue())
        return QColor(grayValue, grayValue, grayValue)


class PortConnection(PointToPointConnection):
    """ Connection line between to PortWidgets.
    """
    def __init__(self, workspace, sourcePort, sinkPort):
        """ Constructor.
        
        Creates connection from source port widget to sink port widget.
        """
        self._sourcePort = sourcePort
        self._sinkPort = sinkPort
        PointToPointConnection.__init__(self, workspace, None, None)
        self._sourcePort.attachConnection(self)
        self._sinkPort.attachConnection(self)

    def sourcePort(self):
        """ Returns attached source port.
        """
        return self._sourcePort

    def sinkPort(self):
        """ Returns attached sink port.
        """
        return self._sinkPort

    def sourcePoint(self):
        """ Returns connection point of attached source port.
        """
        return self._sourcePort.connectionPoint()

    def targetPoint(self):
        """ Returns connection point of attached sink port.
        """
        return self._sinkPort.connectionPoint()

    def sourceDirection(self):
        """ Returns initial direction of source port.
        """
        return self._sourcePort.connectionDirection()

    def targetDirection(self):
        """ Returns initial direction of sink port.
        """
        return self._sinkPort.connectionDirection()

    def attachedToPort(self, port):
        """ Returns True if port is either source or sink port attached to this connection.
        """
        if port == self._sourcePort:
            return True
        if port == self._sinkPort:
            return True
        return False

    #serializes PortConnections (that are only available in VISPA and are not pxl objects)
    def serialize(self):
        logging.debug(self.__class__.__name__ + ": serialize()")
        connectionDict = {}

        moduleType = "PortConnection"
        moduleOptions = {}
        moduleProperties = {}

        connectionDict = {
                          "type": moduleType,
                          "options": moduleOptions,
                          "properties": moduleProperties
         }

        return connectionDict

    def delete(self):
        if PointToPointConnection.delete(self):
            self._sinkPort.detachConnection(self)
            self._sourcePort.detachConnection(self)

class LinearPortConnection(PortConnection):
    """ PortConnection with linear connection
    """
    CONNECTION_TYPE = "STRAIGHT"

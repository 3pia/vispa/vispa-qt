#!/usr/bin/env python
from distutils.core import setup

import sys
import os
import PyQt4.QtCore

import Vispa
from Vispa.Main import Preferences


#
# Version Checks
#
def checkVersion(version,require):
    '''Create standarized output for version checks'''
    if version<require:
        print "  Error: Version >=",require,"required, but ",version,"found."
        sys.exit(2)
    else:
        print "  Version",version,"found."

print "Checking dependencies:"
print "- Python:"
checkVersion(".".join([str(i) for i in sys.version_info[:3]]),"2.4.0")
print "- Qt4:"
checkVersion(PyQt4.QtCore.qVersion(),"4.4.3")


#
# check for old vispa installation and eventually warn
#
print "Checking for old Vispa installation ..."
currentDirectory = os.path.abspath(os.path.curdir)
f = sys.path
dircontent = []
for p in f:
  try:
    dircontent = os.listdir( p )
  except:
    #print "WARNING! Folder", p, "from sys.path doesn't exist!"
    continue 
  if ("Vispa" in dircontent) and p!=currentDirectory:
    fp = os.path.join(p, "Vispa")
    fpi = os.path.join(fp, '__init__.py')
    if os.path.isdir(p) and os.path.isfile(fpi):
      m = "\nWARNING! Vispa module found on the system in %s.\n This could be a left over from a previous installation. Not removing this old installation might cause problems.\n" % (p)
      print m


#
# Write version number for Mac OS X application bundle
#
if os.path.isdir(os.path.abspath("xcode")):
    file = open(os.path.join("xcode", "vispa_version.xcconfig"), "w")
    file.write("// WARNING: This file is auto-generated. Do not modify it.\n")
    file.write("CURRENT_PROJECT_VERSION = %s" % Vispa.__version__)
    file.close()


#
# python packages (folder with __init__.py) in the pytohn root package
#

# core packages
packages =['Vispa','Vispa.Main','Vispa.Plugins','Vispa.Gui','Vispa.Share','Vispa.Views']

# read plugin packages to include
pluginDirectory =  Preferences.pluginDirectory
plugindirs = [f for f in os.listdir(pluginDirectory) if os.path.isdir(os.path.join(pluginDirectory, f)) and not f.startswith(".")]
pluginpackages = ['Vispa.Plugins.'+f for f in plugindirs]
packages.extend(pluginpackages)
print 'Installing packages:\n-', "\n- ".join(packages)

#
# Additional files
#

def recursive_glob(rootdir='.', suffix=''):
  """
  Found on http://stackoverflow.com/questions/2186525/use-a-glob-to-find-files-recursively-in-python
  """
  return [os.path.join(rootdir, filename)
    for rootdir, dirnames, filenames in os.walk(rootdir)
    for filename in filenames if filename.endswith(suffix)]



data_files = [
  ('share/Vispa/examples/',recursive_glob('examples')),
  ('share/Vispa/doc',recursive_glob('doc')),
  ('share/applications',['vispa.desktop']),
  ('share/icons',['VispaButton.svg'])
  ]

print 'Additional Files will be installed:'
for a in data_files:
  for f in a[1]:
    print '  ',a[0] + '/' + f.split('/')[-1]
print

#
# The distutils setup script
#

setup(name='vispa',
    version=Vispa.__version__,
    description='Visual Physics Analysis (VISPA), an integrated development environment for physics analysis.',
    author = 'Martin Erdmann',
    author_email = 'vispa@lists.rwth-aachen.de',
    url='//http://vispa.physik.rwth-aachen.de/',
    license='GPL',
    scripts =['bin/vispa'],
    packages = packages,
    #package_data = {'Vispa' : ['examples']},
    # This can be improved using pkg_resources?
    data_files=data_files,
)

